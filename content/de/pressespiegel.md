---
layout: about
title: Pressespiegel - Commown
description: Sie sprechen über Commown, die Kooperative für nachhaltige Elektronik
date: 2021-05-25T10:26:51.419Z
translationKey: Pressespiegel
menu:
  main:
    name: Pressespiegel
header:
  title: Pressespiegel
sections:
  - id: Pressespiegel
    color: text-bleu-fonce-100
    bg_color: bg-bleu-clair-75
    containers:
      - title: Sie sprechen über uns
        ctas: []
        blocks:
          - title: Utopia
            text: "Die Flut an Elektroschrott wächst unaufhaltsam – auf Kosten unserer
              Umwelt und unserer Gesundheit. Eine Lösung dieses Problems heißt:
              Kreislaufwirtschaft. Erfahre hier, wie innovative Ansätze und
              nachhaltige Geschäftsmodelle Elektroschrott reduzieren und wie du
              dem Drang widerstehst, neue Technik kaufen zu müssen."
            ctas:
              - outline: false
                text: Zum Artikel
                href: https://utopia.de/sponsored-content/wiederverwerten-statt-wegwerfen-elektroschrott-reduzieren-durch-kreislaufwirtschaft/
          - title: Solderpunks Podcast
            text: "Commown vermietet zB Handys. Je länger ein Handy gemietet wird, desto
              günstiger wird der Mietpreis. Zusammen mit einer langen Garantie
              von Ersatzteilen und Serviceleistungen wird so eine lange Nutzung
              unterstützt. Aber es scheint noch mehr dahinter zu stecken: Als
              gemeinnützige Genossenschaft lobbyiert Commown für
              zukunftsfähigere Elektronik. Wir haben nachgefragt."
            ctas:
              - outline: false
                text: Zum YouTube-Video
                href: https://www.youtube.com/watch?v=eBttzqWBZgg
      - blocks:
          - title: ZDFheute Good News
            text: Das Geschäftsmodell der Kooperative Commown klingt ganz simpel, hat aber
              großes Potenzial. Geräte wie Handys oder Laptops werden nicht
              gekauft, sondern bei der Genossenschaft gemietet. Geht ein Gerät
              kaputt, wird es repariert. Dauert die Reparatur länger, stellt
              Commown sofort ein Ersatzgerät zur Verfügung.
            ctas:
              - outline: false
                text: Zum Artikel
                href: https://www.zdf.de/nachrichten/briefing/good-news-blackfriday-kauf-nix-tag-zdfheute-update-100.html
          - title: Circular City Guide Berlin
            text: Im Kapitel "Elektronik im Kreislauf" (S. 71) können Sie lesen, wie die
              Nutzung und das Teilen nachhaltiger Elektronik dazu beitragen
              kann, den ökologischen Fußabdruck zu verringern.
            ctas:
              - outline: false
                text: Zum Guide
                href: https://www.circularcityguide.circular.berlin/wp-content/uploads/2023/12/Circular-City-Guide_231214.pdf
      - blocks:
          - title: Circularity.fm Podcast
            text: What is the best legal entity to start a sustainable business? The way an
              organization is set up defines its modus operandi. Whether money
              is a means to an end or an end in itself is decided at that early
              moment. In this episode, Élie, co-founder of Commown, shares the
              rationale and details of running an organization as a cooperative
              in collective interest.
            ctas:
              - outline: false
                text: Zur Podcastfolge
                href: https://circularity.fm/ep-10-commown-chosing-a-legal-entity-that-puts-the-planet-first/
          - title: Utopia
            text: Mit Commown kannst Du einen Computer oder ein Smartphone mieten. Was das
              Startup ist und was sie genau machen, kannst Du hier nachlesen.
            ctas:
              - outline: false
                text: Zum Artikel
                href: https://utopia.de/ratgeber/commown-die-kooperative-fuer-nachhaltige-elektronik/
      - blocks:
          - title: Süddeutsche Zeitung
            text: Die Genossenschaft vermietet Smartphones, Laptops und Kopfhörer - und
              setzt ausschließlich auf gut reparierbare Geräte. Das klingt
              umweltfreundlich, aber kann man damit auch Geld verdienen?
            ctas:
              - outline: false
                href: https://www.sueddeutsche.de/wirtschaft/commown-fairphone-modular-reparierbarkeit-1.5436335
                text: Zum Artikel
          - title: Kostbar
            text: "Die Genossenschaft Commown vermietet ausschließlich reparierbare Geräte
              an Mitglieder: das Fairphone, Kopfhörer von Gerrard Street,
              Laptops von why! Die Mietpreise verstehen sich inklusive Wartung
              und eventuell anfallender Reparaturen."
            ctas:
              - outline: false
                text: Zum Artikel
                href: https://kostbar-oldenburg.de/2021/10/28/blog/die-module-spielen-verrueckt/
      - blocks:
          - text: "Faire Elektronikgeräte mieten statt kaufen – bei Commown: »Es liegt
              demnach im Interesse aller Verbraucher*innen, aus einem so
              schädlichen System auszubrechen.«"
            title: Nachhaltige Jobs
            ctas:
              - outline: false
                text: Zum Artikel
                href: https://www.nachhaltigejobs.de/commown/m?utm_content=buffera0a99&utm_medium=social&utm_source=facebook.com&utm_campaign=buffer&fbclid=IwAR1vkXOAjtl03tXsKRqs2_lMp-etRz8pF9oCgabdB4mwDie-NKbhcH9FSOs
  - id: Pressemitteilung
    containers:
      - title: Pressemitteilung
        blocks:
          - title: ""
            text: "[Hier](https://nuage.commown.coop/s/G8foPbpm57ypZgX) finden Sie die
              aktuelle Pressemitteilung der Kooperative Commown (Juli 2024)."
---
