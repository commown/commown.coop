---
title: Datenschutzerklärung
description: null
date: 2021-05-25T10:26:51.419Z
translationKey: datenschutzerklarung
menu:
  footer:
    name: Datenschutzerklärung
    weight: 12
header:
  title: Datenschutzerklärung
---
Wir freuen uns über Ihren Besuch auf unserer Webseite. Nachfolgend informieren wir Sie über den Umgang mit Ihren personenbezogenen Daten gemäß Artikel 13 Datenschutzgrundverordnung (DSGVO).

## 1. Wer ist für die Datenverarbeitung verantwortlich und an wen kann ich mich wenden?

Commown SCIC betreibt die Webseite https://commown.coop/de/ („Webseite“). Wenn Sie über die Webseite Verträge abschließen, ist Ihre Vertragspartnerin – abhängig von der gemieteten Ware – die: Commown SCIC. Commown SCIC ist eine französische Gesellschaft in der französischen Gesellschaftsform der Sociète Cooperative par Actions Simplifiés. Einzelheiten zur Gesellschaftsform können wir Ihnen auf Nachfrage gerne erläuternd zur Verfügung stellen.
Daraus ergeben sich folgende Verantwortlichkeiten:

Verantwortlich für den Betrieb der Webseite ist:
Commown SCIC
8 av. Dante
Pépinière de Hautepierre
STRAßBURG 67200
Frankreich
vertreten durch die Geschäftsführer Elie Assémat, Adrien Montagut, Florent Cayré, Frédéric Wagner
E-Mail: support /a/ commown.coop

Verantwortlich beim Abschluss von Verträgen über die Webseite und bei der Durchführung dieser Verträge ist:
Commown SCIC
8 av. Dante
Pépinière de Hautepierre
STRAßBURG 67200
Frankreich
vertreten durch die Geschäftsführer Elie Assémat, Adrien Montagut, Florent Cayré, Frédéric Wagner
E-Mail: support /a/ commown.coop
Tel: +33 980735662 
Nachfolgend wird die Gesellschaft als „Commown“ oder “wir” bezeichnet.

## 2. Wofür verarbeiten wir Ihre Daten und auf welcher Rechtsgrundlage?

2.1 Datenverarbeitung bei der Nutzung der Commown Services
Wir verarbeiten personenbezogene Daten im Einklang mit den Bestimmungen der Europäischen Datenschutz-Grundverordnung (DSGVO) und dem Bundesdatenschutzgesetz (BDSG) zu folgenden Zwecken:

a) Zur Erfüllung vertraglicher und vorvertraglicher Pﬂichten (Art. 6 Abs. 1 S. 1 lit. b DSGVO)
Die Verarbeitung personenbezogener Daten (Artikel 4 Nr. 2 DSGVO) erfolgt zur Bereitstellung dieser Webseite und zur Vermarktung der Produkte, insbesondere zum Abschluss und zur Abwicklung von Verträgen, zur Abrechnung, zur Durchführung vorvertraglicher Maßnahmen, zur Beantwortung von Anfragen im Zusammenhang mit unserer Geschäftsbeziehung sowie für alle mit dem Betrieb und der Verwaltung des Unternehmens erforderlichen Tätigkeiten.
Die Zwecke der Datenverarbeitung richten sich in erster Linie nach dem konkreten Produkt. Die weiteren Einzelheiten zum Zweck der Datenverarbeitung im Rahmen von Verträgen können Sie den jeweiligen Vertragsunterlagen und Geschäftsbedingungen entnehmen.
Insbesondere verarbeitet Commown die personenbezogenen Informationen, die Sie als Nutzer bei der Registrierung, für vertragliche Zwecke oder im Rahmen einer Anfrage zur Verfügung stellen. Es handelt sich insbesondere um folgende Daten: Name, Geburtsdatum, E-Mail- Adresse, Anschrift (Rechnungs- und ggf. abweichende Versandadresse), Bestellinformationen, optional Telefonnummer und Bankverbindung. Zusätzlich speichert Commown das Passwort, das der Nutzer frei wählen kann. Das Passwort wird nicht im Klartext gespeichert, sondern nur ein sogenannter Hash-Wert. Ein Wohnsitznachweis wird von Commown nur verlangt, um Betrugsversuche zu vermeiden. Darin enthaltene sensible Informationen können von Kunden geschwärzt werden. Diese werden auf keinen Fall von Commown weiterverarbeitet.

b) Im Rahmen der Interessenabwägung (Art. 6 Abs. 1 S. 1 lit. f DSGVO)
Darüber hinaus verarbeiten wir Ihre Daten über die Bereitstellung der Webseite und die eigentliche Erfüllung des Vertrages hinaus zur Wahrung berechtigter Interessen von uns oder Dritten, wie insbesondere in den folgenden Fällen:
• Beantwortung Ihrer Anfragen außerhalb eines Vertrages oder vorvertraglicher Maßnahmen;
• Werbung oder Markt- und Meinungsforschung, soweit Sie der Nutzung Ihrer Daten nicht widersprochen haben, dazu zählt die Bestandskundenwerbung;
• Auswertung unserer Werbemaßnahmen, z. B. Tracking des Klick- und Öffnungsverhaltens bei E-Mail-Kampagnen
• Geltendmachung rechtlicher Ansprüche und Verteidigung bei rechtlichen Streitigkeiten;
• Gewährleistung der IT-Sicherheit und des IT-Betriebs;
• Prüfung der Bonität;
• Verhinderung und Aufklärung von Straftaten;
• Maßnahmen zur Geschäftssteuerung und Weiterentwicklung von Produkten.
Unser berechtigtes Interesse besteht darin, unsere Produkte optimal zu vermarkten und diese und unser Unternehmen weiter zu entwickeln bzw. unser Unternehmen gegen Beeinträchtigungen und Gefahren zu schützen und seine Ansprüche durchzusetzen.

c) Aufgrund Ihrer Einwilligung (Art. 6 Abs. 1 S. 1 lit. a DSGVO)
Soweit Sie uns eine Einwilligung zur Verarbeitung von personenbezogenen Daten für bestimmte Zwecke (z. B. Auswertung oder Nutzung von Daten für Marketingzwecke, Erhalt von Werbung per E-Mail) erteilt haben, ist die Rechtmäßigkeit dieser Verarbeitung auf Basis Ihrer Einwilligung gegeben. Eine erteilte Einwilligung kann jederzeit widerrufen werden. Bitte beachten Sie, dass der Widerruf erst für die Zukunft wirkt.

d) Aufgrund gesetzlicher Vorgaben (Art. 6 Abs. 1 S. 1 lit. c DSGVO)
Zudem unterliegen wir diversen rechtlichen Verpﬂichtungen, das heißt gesetzlichen Anforderungen (z. B. Geldwäschegesetz, Steuergesetze), die die Verarbeitung von Daten erfordern.

2.2 Bonitätsprüfung, Risikoanalyse und Betrugsprävention
Für die Entscheidung über die Begründung eines Vertragsverhältnisses führen wir eigene Analysen zur Missbrauchs- und Betrugserkennung durch. Dabei verwenden wir insbesondere die folgenden Datenkategorien:
• Kundenmerkmale (z. B. Daten aus Bonitätsauskünften, Alter, Mobilfunkanbieter, E-Mail-Anbieter).
• Warenkorbdaten wie Gerätekategorien
• Verhaltensdaten (z. B. Anzahl der Bestellungen und deren Status, Verhalten auf der Website)
• Zahlungsdaten wie Zahlungsmethoden
• Abgleich von Kontodaten mit anderen Nutzerkonten in Bezug auf übereinstimmende Daten

Die eigenen Analysen zur Missbrauchs- und Betrugserkennung können Wahrscheinlichkeitswerte (Score-Werte) beinhalten, die auf Basis wissenschaftlich anerkannter mathematisch-statistischer Verfahren berechnet werden und in deren Berechnung unter anderem (aber nicht ausschließlich) Anschriftendaten einﬂießen. Rechtsgrundlage hierfür ist Artikel 6 Absatz 1 Buchstabe f) DSGVO. Das berechtigte Interesse ergibt sich aus unserem Interesse an einer Reduzierung des Vertragsrisikos, an einem Schutz vor Forderungsausfällen und vor Gefahren der missbräuchlichen Inanspruchnahme unserer Leistungen durch Dritte. Ihre schutzwürdigen Belange werden gemäß den gesetzlichen Bestimmungen berücksichtigt.
In Einzelfällen überprüfen wir die Berechnung bzw. das Berechnungsergebnis manuell.
Zur Missbrauchs- und Betrugsprävention und zur Vermeidung von Forderungsausfällen bei laufenden und künftigen Mietverträgen des Kunden ist die längerfristige Speicherung (hierzu unter 3.) der Bonitätsdaten und der Daten aus unseren eigenen Analysen zur Missbrauchs- und Betrugserkennung erforderlich. Rechtsgrundlage ist Art. 6 Absatz 1 Buchstabe f) DSGVO. Unser berechtigtes Interesse ergibt sich aus unserem Interesse daran, betrügerische Verhaltensweisen- bzw. -muster zu erkennen, Entwicklungen in der Bonität unserer Kunden zu erkennen und zu berücksichtigen, die Mietverträge zu bewerten (das Risikoportfolio und die Ausfallwahrscheinlichkeit sind unter anderem für Investoren relevant) und unser Risikomanagement zu überprüfen und zu verbessern (durch Analysen der Datensätze – nur in anonymisierter Form).

## 3. Welche Daten verarbeiten wir beim Besuch unserer Webseite?

3.1 Nutzungsdaten
Wenn Sie unsere Webseiten besuchen, werden auf unserem Webserver temporär sogenannte Nutzungsdaten zu statistischen Zwecken als Protokoll ausgewertet, um die Qualität unserer Webseiten zu verbessern. Dieser Datensatz besteht aus
Die genannten Protokolldaten werden nur anonymisiert ausgewertet.

3.2. Cookies und andere Technologien

3.2.1 Erforderliche Cookies
Auf unseren Webseiten setzen wir Cookies ein, die zur Nutzung unseren Webseiten erforderlich sind.
Cookies sind kleine Textdateien, die auf Ihrem Endgerät gespeichert und ausgelesen werden können. Man unterscheidet zwischen Session-Cookies, die wieder gelöscht werden, sobald Sie ihren Browser schließen und permanenten Cookies, die über die einzelne Sitzung hinaus gespeichert werden.
Wir nutzen diese erforderlichen Cookies nicht für Reichweitenanalyse-, Tracking- oder Werbezwecke. Wir nutzen sie zur Darstellung unserer Webseiten, zur Erbringung unserer Dienstleistung und für die unten genannten technischen Funktionen und Inhalte von Drittanbietern.
Ein Aufruf unserer Seiten führt dazu, dass Inhalte der Drittanbieter nachgeladen werden, der diese Funktionen und Inhalte bereitstellt. Hierdurch erhält der Drittanbieter die Information, dass Sie unsere Seite aufgerufen haben sowie die in diesem Rahmen technisch erforderlichen Nutzungsdaten.
Teilweise enthalten diese Cookies lediglich Informationen zu bestimmten Einstellungen und sind nicht personenbeziehbar. Sie können auch notwendig sein, um die Benutzerführung, Sicherheit und Umsetzung der Seite zu ermöglichen.
Wir nutzen diese Cookies auf Grundlage von Art. 6 Abs. 1 S. 1 lit. f DSGVO in dem Interesse, unsere Seite möglichst ansprechend und informativ zu gestalten und unsere Dienstleistung ohne Einschränkung erbringen zu können.
Sie können Ihren Browser so einstellen, dass er Sie über die Platzierung von Cookies informiert. So wird der Gebrauch von Cookies für Sie transparent. Sie können Cookies zudem jederzeit über die entsprechende Browsereinstellung löschen und das Setzen neuer Cookies verhindern. Bitte beachten Sie, dass unsere Webseiten dann ggf. nicht angezeigt werden können und einige Funktionen technisch nicht mehr zur Verfügung stehen.

i) cookie “website_lang”
Anbieter: Commown
Zweck: enthält die Benutzersprache für den Online-Shop
Speicherdauer: nur für die Dauer der Benutzersitzung

ii) cookie “session_id”
Anbieter: Commown
Zweck: enthält die Benutzersprache für den Online-Shop
Speicherdauer: nur für die Dauer der Benutzersitzung

iii) cookie pll_lang
Anbieter: Commown
Zweck: enthält die Sprache des Benutzers für die Unternehmenswebseite
Speicherdauer: 1 Jahr\
\
iv) hCaptcha\
\
Art und Umfang der Verarbeitung\
Wir haben auf unseren Webseiten Komponenten von hCaptcha integriert. hCaptcha ist ein Dienst der Intuition Machines, Inc. (IMI) mit Sitz in den USA und ermöglicht es uns zu unterscheiden, ob eine Kontaktanfrage von einer natürlichen Person stammt oder automatisiert mittels eines Programmes geschieht. Durch dieses Tool werden Captchas einblendet – kleine Aufgaben, die für Menschen einfach zu lösen, für Maschinen aber schwer zu bewältigen sind. Diese Captchas helfen uns, Spams zu identifizieren.\
\
Das Toole hCaptcha verarbeitet grundsätzlich technische Informationen über Ihre Nutzung unserer Webseite, wie Mausbewegungen, Scrollpositionen, gedrückte Tasten, Touch Events auf Touch-Displays und ggf. Bewegungen Ihres Gerätes (z.B., wenn Sie unsere Webseite mit einem Smartphone benutzen). Außerdem erhebt hCaptcha die Antworten auf die kleinen Aufgaben, die Ihnen gestellt werden. Diese Daten werden von hCaptcha ausschließlich in unserem Auftrag verarbeitet, um unsere Webseite vor Bots und Spam zu schützen. Ihre (nicht personenbezogenen) Antworten auf die kleinen Aufgaben, die ihnen gestellt werden (z.B. das Erkennen bestimmter Gegenstände auf Bildern) wird zudem von hCaptcha verwendet, um damit Algorithmen zu trainieren.\
\
Die hCaptcha-Analyse im "unsichtbaren Modus" kann vollständig im Hintergrund stattfinden. Website- oder App-Besucher werden nicht darauf hingewiesen, dass eine solche Analyse stattfindet, wenn dem Nutzer keine Challenge angezeigt wird. Die Verarbeitung der Daten der betroffenen Person in der EU erfolgt auf der rechtlichen Grundlage der Standardvertragsklauseln (SCC).\
\
Zweck und Rechtsgrundlage\
Die Nutzung des Dienstes erfolgt vor dem Hintergrund, die Verfügbarkeit der Webseite sicherzustellen, vor Bots zu schützen und auf Grundlage unserer berechtigten Interessen gemäß Art. 6 (1) f DS-GVO.

Speicherdauer\
Weitere Informationen zu hCaptcha und den Datenschutzrichtlinien und Nutzungsbedingungen des IMI finden Sie unter den folgenden Links: https://hcaptcha.com/privacy/ und https://hcaptcha.com/terms.

3.3 Zugangsgeschützter Bereich

Sofern Sie unseren zugangsgeschützten Bereich nutzen möchten, ist eine vorherige Registrierung notwendig.
Wir erheben hierbei nur die Daten, die zur Registrierung erforderlich sind. Die Verarbeitung erfolgt auf Grundlage von Art. 6 Abs. 1 S. 1 lit. b DSGVO bzw. auf Grundlage von Art. 6 Abs. 1 lit. f DSGVO in dem Interesse, Ihnen die Dienste und Informationen des zugangsgeschützten Bereichs zur Verfügung zu stellen.
Sofern wir darüber hinaus weitere Daten erheben, sind diese als freiwillig gekennzeichnet und basieren auf Ihrer Einwilligung gemäß Art. 6 Abs. 1 S. 1 lit. a DSGVO.

## 4. Wer ist Empfänger meiner Daten?

Innerhalb des jeweils verantwortlichen Unternehmens erhalten diejenigen Stellen Zugriff auf Ihre Daten, die diese zur Erfüllung unserer vertraglichen und gesetzlichen Pflichten benötigen.
Wir geben Ihre Daten an die in dieser Datenschutzerklärung nachfolgend genannten Empfänger weiter. Ferner geben wir sie weiter an folgende Kategorien von Empfängern, wenn dies zur Erfüllung eines mit Ihnen bestehenden Vertragsverhältnisses bzw. zur Durchführung vorvertraglicher Maßnahmen (Art. 6 Abs. 1 S. 1 lit. b DSGVO) oder zur Wahrung berechtigter Interessen (Art. 6 Abs. 1 S. 1 lit. f DSGVO) erforderlich ist.
Soweit die Verarbeitung zur Wahrung berechtigter Interessen erforderlich ist, etwa bei der Nutzung von IT-Dienstleistungen, besteht unser berechtigtes Interesse darin, Funktionen auszulagern.
Darüber hinaus erfolgt eine Weitergabe oder Übermittlung Ihrer personenbezogenen Daten, wenn gesetzliche Bestimmungen dies gebieten (Art. 6 Abs. 1 S. 1 lit. c DSGVO) oder Sie eingewilligt haben (Art. 6 Abs. 1 S. 1 lit. a DSGVO).

## 5. Werden meine personenbezogenen Daten außerhalb der Europäischen Union verarbeitet?

Für die Verarbeitung Ihrer Daten setzen wir auch Dienstleister ein, die sich in Drittländern außerhalb der Europäischen Union befinden. In diesen Ländern herrscht möglicherweise ein anderes Datenschutzniveau als innerhalb der Europäischen Union. Sofern es keinen Beschluss der EU-Kommission gibt, dass diese Drittländer allgemein ein angemessenes Schutzniveau bieten, haben wir besondere Maßnahmen getroffen, um sicherzustellen, dass Ihre Daten in den Drittländern ebenso sicher wie innerhalb der Europäischen Union verarbeitet werden. Mit Dienstleistern in Drittländern schließen wir die Standard-Datenschutzklauseln der Europäischen Kommission ab. Diese sehen geeignete Garantien für den Schutz Ihrer Daten bei Dienstleistern im Drittland vor. Eine Kopie dieser Datenschutzklauseln können Sie unter den oben angegebenen Kontaktdaten anfordern. Zudem verschlüsseln oder pseudonymisieren wir personenbezogene Daten vor der Übermittlung an einen Dienstleister in einem Drittland, sofern dies technisch möglich und angemessen ist.

## 7. Wie lange werden meine Daten gespeichert?

Ihre Daten werden gemäß den gesetzlichen Regelungen verarbeitet und gemäß den vorgesehenen Löschfristen gelöscht.
Soweit erforderlich, verarbeiten und speichern wir Ihre personenbezogenen Daten jedenfalls für die Dauer unserer Vertragsbeziehung, was beispielsweise auch die Anbahnung und die Abwicklung eines Vertrages umfasst. Dabei ist zu beachten, dass unsere Vertragsbeziehung meist ein Dauerschuldverhältnis ist.
Bei Vertragsbeziehungen, aber auch bei sonstigen zivilrechtlichen Ansprüchen, richtet sich die Speicherdauer darüber hinaus auch nach den gesetzlichen Verjährungsfristen, die zum Beispiel nach den §§ 195 ﬀ. des Bürgerlichen Gesetzbuches (BGB) in der Regel drei Jahre, in gewissen Fällen aber auch bis zu dreißig Jahre betragen können.
Außerdem unterliegen wir verschiedenen Aufbewahrungs- und Dokumentationspﬂichten, die sich unter anderem aus dem Handelsgesetzbuch (HGB) und der Abgabenordnung (AO) ergeben. Die dort vorgegebenen Fristen zur Aufbewahrung bzw. Dokumentation betragen sechs Jahre für Korrespondenz im Zusammenhang mit einem Vertragsschluss und 10 Jahre für Buchungsbelege und Geschäftsbriefe (§§ 238, 257 Abs. 1 und 4 HGB, § 147 Abs. 1 und 3 AO).
Logﬁles werden grundsätzlich nach dem Ende der jeweiligen Browser-Sitzung gelöscht, spätestens nach sieben Tagen, es sei denn, ihre weitere Speicherung ist ausnahmsweise erforderlich und rechtmäßig. Die Speicherdauer von Cookies hängt vom Einzelfall ab und beträgt in der Regel zwischen zwölf und 24 Monaten.
Kundendaten und Ihr Kundenkonto löschen wir grundsätzlich fünf Jahre nach dem Ende Ihres letzten Mietvertrages oder nach Ihrem letzten Login, je nachdem, was später eintritt .

## 8. Welche Datenschutzrechte habe ich?

Sie haben uns gegenüber das Recht auf Auskunft (Art. 15 DSGVO), das Recht auf Berichtigung (Art. 16 DSGVO), das Recht auf Löschung (Art. 17 DSGVO), das Recht auf Einschränkung der Verarbeitung (Art. 18 DSGVO) sowie das Recht auf Datenübertragbarkeit (Art. 20 DSGVO). Beim Auskunftsrecht und beim Löschungsrecht gelten die Einschränkungen nach §§ 29 Abs. 1 Satz 2, 34 und 35 BDSG.
Sie haben zudem das Recht, der Datenverarbeitung durch uns zu widersprechen (Art. 21 DSGVO). Soweit unsere Verarbeitung Ihrer personenbezogenen Daten auf einer Einwilligung beruht (Art. 6 Abs. 1 S. 1 lit. a DSG-VO), können Sie diese jederzeit widerrufen; die Rechtmäßigkeit der aufgrund der Einwilligung bis zum Widerruf erfolgten Datenverarbeitung bleibt hiervon unberührt.
Zur Geltendmachung all dieser Rechte sowie zu weiteren Fragen zum Thema personenbezogene Daten können Sie sich jederzeit an unseren Datenschutzbeauftragten oder an unsere oben genannten Kontaktdaten wenden.
Unabhängig hiervon haben Sie das Recht, bei einer Aufsichtsbehörde – insbesondere in dem EU-Mitgliedstaat Ihres Aufenthaltsorts, Ihres Arbeitsplatzes oder des Orts des mutmaßlichen Verstoßes – eine Beschwerde einzulegen, wenn Sie der Ansicht sind, dass die Verarbeitung der Sie betreﬀenden personenbezogenen Daten gegen die DSGVO oder andere geltende Datenschutzgesetze verstößt (Art. 77 DSGVO, § 19 BDSG).