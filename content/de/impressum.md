---
title: Impressum
description: null
menu:
  footer:
    name: Impressum
    weight: 10
header:
  title: Impressum
date: 2021-05-25T10:26:51.419Z
translationKey: impressum
---
Diensteanbieter gemäß § 5 TMG\
Commown SCIC\
8 A rue Schertz
Parc Phoenix - Bât. B2
Strasbourg 67100 
Frankreich

vertreten durch die Geschäftsführer Elie Assémat, Adrien Montagut, Florent Cayré\
E-Mail: support/a/ commown.coop\
Tel: +33 980735662 \
Registergericht: Amtsgericht STRAßBURG\
Handelsregisternummer: Siret 828 811 489 00024\
Umsatzsteuer-Identifikationsnummer gemäß §27 a Umsatzsteuergesetz:\
UST-ID Nr. FR14828811489

Vertragspartner beim Abschluss von Verträgen über die Webseite https://commown.coop/de

Commown SCIC\
8 A rue Schertz
Parc Phoenix - Bât. B2
Strasbourg 67100 
Frankreich\
support /a/ commown.coop\
Tel: +33 980735662 

vertreten durch die Geschäftsführer Elie Assémat, Adrien Montagut, Florent Cayré, Frédéric Wagner

Unternehmensgegenstand: Vermietung elektronischer Geräte, insbesondere Smartphones, Computer, Laptops und Headsets

Registergericht: Amtsgericht Straßburg (Tribunal d’Instance)\
Handelsregisternummer: Siret 828 811 489 00032\
Umsatzsteuer-Identifikationsnummer gemäß §27 a Umsatzsteuergesetz:\
UST-ID Nr. FR14828811489

Hinweis für Besucher aus Österreich und Schweiz: Alle hier gegebenen Informationen zu Allgemeinen Geschäftsbedingungen beziehen sich auf die Gesetzgebung in Deutschland

Verantwortlicher im Sinne des § 18 MStV\
Elie Assémat, Adrien Montagut, Florent Cayré, Frédéric Wagner\
Geschäftsführer von Commown SCIC, 8 A rue Schertz, Parc Phoenix - Bât. B2, Strasbourg 67100, Frankreich

## I. Haftungsausschluss

Haftung für Inhalte\
Die Inhalte dieser Webseite wurden mit großer Sorgfalt und nach bestem Wissen erstellt. Für ihre Aktualität, Richtigkeit, Vollständigkeit oder Qualität kann Commown jedoch keine Gewähr übernehmen.

Haftung für Links\
Unser Angebot enthält Links zu externen Webseiten Dritter, auf deren Inhalte wir keinen Einfluss haben. Deshalb können wir für diese fremden Inhalte auch keine Gewähr übernehmen. Für die Inhalte der verlinkten Seiten ist stets der jeweilige Anbieter oder Betreiber der Seiten verantwortlich.

## II. Urheberrecht

Die auf dieser Webseite enthaltenen Inhalte unterliegen dem Urheberrecht und anderen Gesetzen zum Schutz geistigen Eigentums. Die Vervielfältigung, Bearbeitung, Verbreitung und jede Art der Verwertung außerhalb der Grenzen des Urheberrechtes bedürfen der schriftlichen Zustimmung von Commown. Downloads und Kopien dieser Seite sind nur für den privaten, nicht kommerziellen Gebrauch gestattet. Soweit die Inhalte auf dieser Seite nicht vom Betreiber erstellt wurden, werden die Urheberrechte Dritter beachtet. Insbesondere werden Inhalte Dritter als solche gekennzeichnet.

## III. Datenschutz

Bitte beachten Sie dazu unsere Datenschutzerklärung.

Die Europäische Kommission bietet eine Plattform zur Online-Streitbeilegung an, die unter http://ec.europa.eu/consumers/odr/ zu finden ist. Commown ist weder bereit noch verpflichtet, an Streitbeilegungsverfahren vor Verbraucherschlichtungsstellen teilzunehmen.