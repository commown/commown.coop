---
title: Allgemeine Geschäftsbedingungen und besonderen Servicebedingungen
description: "Allgemeine Geschäftsbedingungen - Commown: die Kooperative für
  nachhaltige Elektronik"
date: 2021-05-25T10:26:51.419Z
translationKey: legal
menu:
  footer:
    name: AGB
    weight: 14
header:
  title: Allgemeine Geschäftsbedingungen
---
*Die geltenden AGB und BB sind die **in der Sprache**, in der das Abonnement abgeschlossen wurde (und nicht die des Landes Ihrer Rechnungsadresse*).

## 1. Angebote für Berufstätige

Allgemeine Geschäftsbedingungen (“AGB”) für Dienstleistungen für gewerbsmäßige Kunden : [AGB_allgemein_B2B_COMMOWN_deutsch_20230617.pdf](https://shop.commown.coop/web/content/187256?download=1).

Besondere Bedingungen ("BB") - Anmietung mit Serviceleistungen für Gewerbetreibende (B2B) von SMARTPHONES der Marken FAIRPHONE, SHIFT, CROSSCALL : [BS_SMARTPHONES_B2B_COMMOWN_deutsch_20221128.pdf](https://shop.commown.coop/web/content/146859?download=1).\
\
Besondere Geschäftsbedingungen für Dienstleistungen ("BB") - Vermietung mit Dienstleistungen für gewerbsmäßige Kunden für AUDIOGERAT : [BB_AUDIOGERAT_B2B_COMMOWN_DEUTSCH](https://shop.commown.coop/web/content/260746?download=true)

Besondere Geschäftsbedingungen für Diesntleistungen ("BB") - Vermietung mit Dienstleistungen für gewerbsmäßige Kunden für TRAGBARE/STATIONÄRE PC : [BB_COMPUTER_B2B_COMMOWN_DEUTSCH](https://shop.commown.coop/web/content/161149?download=true)

## 2. Angebote für Privatpersonen

Das Angebot Smartphone Fairphone/Shiftphone mit Serviceleistungen (Essenziell) unterliegt ebenfalls den Besonderen Servicebedingungen, die hier einsehbar sind: [BB Smartphone mit Serviceleistungen - Essenziell von COMMOWN](https://shop.commown.coop/web/content/196345?download=1).

Das Angebot Audiogerät Gerrard Street/ShiftSound unterliegt ebenfalls den Besonderen Servicebedingungen, die hier einsehbar sind: [BB Gerrard Street von COMMOWN](https://shop.commown.coop/web/content/146867?download=1).

Das Angebot Computer unterliegt ebenfalls den Besonderen Servicebedingungen, die hier einsehbar sind: [BB Computer von COMMOWN](https://shop.commown.coop/web/content/258940?download=1).

Das Serviceangebot in Verbindung mit der Nutzung der verantwortungsvollen elektronischen Geräte von COMMOWN unterliegt den Allgemeinen Geschäftsbedingungen, die nachstehend einsehbar sind.

Allgemeine Geschäftsbedingungen über von COMMOWN erbrachte Dienstleistungen für Verbraucher und nicht gewerbliche Kunden. (Die Verwendung der männlichen Form erfolgt rein aus Gründen der Lesbarkeit des Dokuments. Es sind trotz der Verwendung der männlichen Form immer gleichermaßen alle Geschlechter gemeint.)

Stand : 17.06.2023 (deutsche Version)

## ARTIKEL 1 – GELTUNGSBEREICH DER ALLGEMEINEN GESCHÄFTSBEDINGUNGEN

1.1. ALLGEMEINER ANWENDUNGSBEREICH

Diese Allgemeinen Geschäftsbedingungen über Dienstleistungen (nachfolgend “AGB” genannt) gelten ohne Einschränkung oder Vorbehalt für jeden Abschluss über Mietdienstleistungen für eine feste Laufzeit (und deren folgender periodischen Verlängerung). Die Option, Produkte aus den Bereichen Computer, Telefonie und Ton (nachfolgend “die Dienstleistungen” genannt), die COMMOWN auf den unter den Adressen der COMMOWN zugänglichen Websiten für Verbraucher und nicht-gewerbliche Kunden (nachfolgend „die Kunden“ genannt) anbietet, käuflich zu erwerben ist ausgeschlossen. Unter diesen Websiten sind insbesondere die Websites auf den Namen COMMOWN unter den Domains (Punkt)de, (Punkt)fr,(Punkt)com und (Punkt)coop gemeint, aber auch andere zugehörige Domain-Name, (im Folgenden “die COMMOWN-Website”).

Diese AGB gelten unter Ausschluss anderer, zwischen dem Kunden und COMMOWN (nachfolgend “die Parteien”) vereinbarter Besonderer Geschäftsbedingungen (nachfolgend “BB”). Der Kunde wird über diese Bedingungen informiert, bevor eine Verpflichtung eingegangen wird und sofern BG im Einzelfall anwendbar sind.

Die wesentlichen Merkmale der Produkte und Dienstleistungen (Preise, Bindungsfristen usw.) werden auf der COMMOWN-Website dargestellt. Der Kunde ist verpflichtet, diese zu lesen und zur Kenntnis zu nehmen, bevor er die Dienste abonniert. Der Kunde ist allein für die Auswahl und das Abonnement der Dienste verantwortlich.

Der Kunde wird darauf hingewiesen, dass die im Rahmen der Dienstleistungen vermieteten Produkte jederzeit im Eigentum von COMMOWN bleiben (Eigentumsvorbehalt). Der Verkauf von Produkten oder Zubehör ist nicht Gegenstand dieser AGB und impliziert – sofern zulässig – die Annahme bestimmter Verkaufsbedingungen, die von COMMOWN im gegebenen Falle zur Verfügung gestellt werden können.

Der Kunde bestätigt, dass er geschäftsfähig ist, um die auf der COMMOWN-Website angebotenen Dienstleistungen abzuschließen und zu erwerben, bzw. dass er die Erlaubnis seines Vormunds oder Betreuers hat, wenn er unzurechnungsfähig ist, oder die seines gesetzlichen Vertreters, wenn er minderjährig ist. \[COMMOWN kann in den beiden letzgenannten Fälle jederzeit Nachweise über die vorliegende Erlaubnis verlangen.]

COMMOWN verpflichtet sich, dem Kunden diese AGB und gegebenenfalls BB vor der Bestellung in seinem persönlichen Online-Bereich auf der COMMOWN-Website zur Verfügung zu stellen (und darüber hinaus jederzeit mit einem Klick von allen Fußzeilen der Website aus zugänglich zu machen), sowie sie als Anhang der E-Mail, die den Erhalt des Abonnementantrags bestätigt, beizufügen. Dem Kunden wird empfohlen, die AGB und BB sicher und dauerhaft zu speichern, damit er sie bei Bedarf jederzeit während der Laufzeit des Vertrages aufrufen kann.

Die AGB und BB im öffentlichen Teil der COMMOWN-Website können jederzeit geändert werden. Etwaige Änderungen sind nicht rückwirkend anwendbar: die AGB und BB, die zum Zeitpunkt des Abonnements der Dienste in Kraft sind und zu diesem Zeitpunkt auf der COMMOWN-Website vor der sogenannten Validierungsphase zugänglich sind, haben Vorrang vor jeder anderen Version oder jedem anderen ihnen widersprechenden Dokument.
Dennoch können die für den Kunden geltenden AGB und BB jederzeit von COMMOWN geändert werden. Ib diesem Fall werden Kunden, die von nachträglichen Änderungen ihres Abonnements betroffen sind, mindestens einen Monat vor dem Datum des Inkrafttretens per Post oder E-Mail informiert und es wird ihnen ein außerordentliches Kündigungsrecht eingeräumt, sofern sie mit dem Inkrafttreten der geänderten AGB und BB nicht einverstanden sind und diesen widersprechen (siehe unten).

Insbesondere wird bei Änderung der AGB und BB angeben:

Das Datum des Inkrafttretens der geänderten AGB und BB, sowie
die Möglichkeit für den Kunden, nur im Falle von Änderungen, die nicht durch die Umsetzung von neuen gesetzlichen Bestimmungen veranlasst sind, seinen Vertrag ohne Vertragsstrafe und ohne Recht auf Entschädigung innerhalb von vier (4) Monaten nach dem Inkrafttreten der Änderung zu kündigen.

Nach Ablauf von vier (4) Monaten ab Inkrafttreten der Änderungen und bei Ausbleiben eines Kündigungswunsches des Kunden (sofern die Änderung nicht durch die Umsetzung von neuen gesetzlichen Anforderungen veranlasst sind), gelten die neuen AGB oder BB als vom Kunden akzeptiert.

Die Marke “COMMOWN” bzw. die Website COMMOWN verweist auf folgende Firma:

Die Marke “COMMOWN” oder die Website wird genutzt bzw. betrieben von der gleichnamigen Société Coopérative d’Intérêt Collectif par Actions Simplifiée à Capital Variable, eingetragen im Registre du Commerce et des Sociétés du Tribunal d’Instance de Straßburg (Handelsregister des zuständigen Gerichts in Straßburg), Frankreich, unter der Nummer SIREN 828 811 489 und der Umsatzsteuernummer FR14828811489.

Die Kontaktinformationen von COMMOWN lauten wie folgt:

8 A rue Schertz
Parc Phoenix - Bât. B2
F-67100 Straßburg
Frankreich

Änderungen der vorstehenden Angaben zu “COMMOWN” nach dem Datum dieser AGB gelten ab dem Datum ihrer rechtsgültigen Veröffentlichung im entsprechenden Handelsregister, ohne dass es einer Änderung dieser AGB bedarf. Über Änderungen der Geschäftsanschrift soll der Kunde in geeigneter Weise informiert werden.

Der Begriff “Produkt” bezieht sich auf jedes einzelne gemietete elektronische Gerät, einschließlich aller Zubehörteile, Ersatzteile und Originalverpackungen, die mit dem gemieteten Produkt verbunden sind und dem Kunden überlassen wurden.

Sämtliche erforderlichen Informationen sind in deutscher Sprache verfasst, und COMMOWN kann in deutscher, französischer und englischer Sprache kontaktiert werden. Für den Mietvertrag, die AGB die BB ist einzig die deutschsprachige Fassung verbindlich.

1.2. RÄUMLICHER ANWENDUNGSBEREICH

Auf Basis der deutschen AGB und BB werden die Dienstleistungen der COMMOWN in Deutschland und Österreich angeboten. Hierfür ist die COMMOWN.de Website ausschlaggebend.

COMMOWN behält sich das Recht vor, jedem Kunden, der außerhalb Deutschlands oder Österreichs ansässig ist, die Dienstleistungen (Vermietung und/oder damit zusammenhängende Dienstleistungen) zu verweigern oder diese zu beenden; dies gilt auch für jeden Kunden, der innerhalb Deutschlands oder Österreichs ansässig ist, das Produkt jedoch \[ausschließlich] außerhalb dieses Gebiets nutzt, sowie für jeden Kunden, der innerhalb Deutschlands oder Österreich ansässig ist und dessen steuerlicher Wohnsitz sich während der Vertragslaufzeit ins Ausland verlagert.

Es liegt in der Verantwortung des Kunden, COMMOWN unverzüglich über jede Veränderung des steuerlichen Wohnsitzes oder jede andere Abweichung von der bevorstehenden Klausel zu informieren. Die Information soll schriftlich erfolgen.

COMMOWN hat das Recht, in jedem Einzelfall die Auswirkungen der Abweichung zu überprüfen und die Abweichung abweichend von der obenstehenden Klausel zu akzeptieren. Geschieht dies, so gilt:
COMMOWN behält sich Insbesondere, einen angemessenen Versandkostenaufschlag zu berechnen.
Für den Fall, dass Zoll- oder Importabgaben oder andere lokale oder staatliche Steuern fällig werden, werden diese dem Kunden in Rechnung gestellt.
COMMOWN kann nicht für Lieferprobleme aufgrund höherer Gewalt verantwortlich gemacht werden und die Haftung ist in jedem Fall auf die Transportkosten beschränkt. COMMOWN behält sich das Recht vor, dem Kunden jegliche Beschädigung oder den Verlust des Produkts in Rechnung zu stellen.

## ARTIKEL 2 – ABONNEMENT, VALIDIERUNG UND VERTRAGSABSCHLUSS

2.1. ABONNIEREN

Elektronische Abonnements des COMMOWN-Mietvertrags werden auf der COMMOWN-Website vorgenommen. Für Deutschland und Österreich ist hierbei die deutschsprachige Website COMMOWN.de alleine ausschlaggebend.

Bei Beantragung eines Abonnements verpflichtet sich der Kunde, sicherzustellen, dass das gemietete Produkt ausschließlich für den rechtmäßigen Gebrauch bestimmt ist und nicht in Verbindung mit illegalen Aktivitäten steht.

Der Kunde wählt auf der Website die Dienste, die er abonnieren möchte, nach den folgenden Verfahren und vor der Annahme aus (Doppelklick-Regel):

AUSWAHL: Identifizierung, Wahl und Auswahl des Produkts, Kenntnisnahme der Preisbedingungen, der Zahlungsbedingungen und -fristen, der technischen Merkmale und der zugehörigen Dienstleistungen sowie gegebenenfalls der Dauer der mit der Anmietung des Produkts verbundenen Verpflichtung; Auswahl der verschiedenen Optionen in Bezug auf das Produkt, sofern verfügbar (z. B. für die Anmietung eines Smartphones die Farbe des Gehäuses, für die Anmietung eines PCs die verschiedenen technischen Merkmale oder das Betriebssystem, für Smartphones und PCs das Betriebssystem usw.) und Kenntnisnahme aller Klauseln der AGB und BB, die mit dem gemieteten Produkt verbunden sind;
WARENKORB (1. Klick): Validierung des Warenkorbs nach Überprüfung des Inhalts, des Einzel- und Gesamtpreises inkl. MwSt. der Produkte, der Dienstleistungen, der gewählten Optionen und ggf. der Bedingungen für eine Sicherheitsleistung, die auf den Artikelseiten des Webshops beschrieben sind;
IDENTIFIZIERUNG: Aufnahme der Daten des Kunden (einschließlich Rechnungs- und Lieferadresse);
ANGABEN ZUR LIEFERUNG: Überprüfung der Lieferbedingungen, Tarife und Konditionen (und gegebenenfalls Wahl der Lieferoption);
AKZEPTANZ: Validierung aller Klauseln der AGB und der BB, die mit dem gemieteten Produkt verbunden sind, und deren vorbehaltlose Annahme, Validierung des Mietantrags mit Dienstleistungen durch den Kunden, sowie Kenntnisnahme der und Einverständnis mit den Datenschutzbestimmungen der COMMOWN (2. Klick).
ZAHLUNG: Erfassung der Bankdaten, die für die Einrichtung des SEPA-Lastschriftmandats erforderlich sind; Zahlung einer ersten Zahlung (ggf. Kaution und/oder 1. Rate).

2.2. VALIDIERUNG DES ANTRAGS AUF DEN MIETVERTRAG

2.2.1 DURCH DEN KUNDEN:

Der Kunde wird insbesondere auf die Art und Weise der Annahme des elektronischen Mietvertragsabschlusses über die COMMOWN-Website aufmerksam gemacht.

Vor Abschluss des Online-Abonnements erklärt der Kunde, dass er die vorliegenden AGB, die den zum Zeitpunkt des Abonnements geltenden Bedingungen entsprechen, sowie die BB des abonnierten Angebots und die im Impressum der COMMOWN-Website aufgeführten allgemeinen Nutzungsbedingungen sowie die Datenschutzbedingungen gelesen hat. Mit dem Abschluss des Validierungs- und Zahlungsvorgangs, der sich auf diese Dokumente bezieht und ihre Annahme erfordert, erklärt der Kunde, dass er sie ohne Einschränkung oder Vorbehalt akzeptiert.

Die vom Kunden bei der elektronischen Anmeldung des Mietvertrages gemachten Angaben (insbesondere Name und Anschrift für Rechnung und Lieferung sowie die Bankverbindung) sind verbindlich. COMMOWN haftet nicht für Fehler im elektronischen Anmeldeprozess, die die Ausführung der Dienstleistung, einschließlich der Lieferung des Produkts oder der Supportleistungen während des Vertrags, verhindern oder verzögern können.

Die Zahlung des Abonnements des Mietvertrags erfolgt in zwei Stufen:

Ab der Validierung des Abonnementantrags durch den Kunden kann die Zahlung einer Anzahlung (wie auf der Produktseite angegeben) oder der ersten Rate vom Kunden über die sichere Schnittstelle des Zahlungsanbieters von COMMOWN verlangt werden.
Die Miete ist ab Lieferung des Produkts monatlich nachträglich ausschließlich per SEPA-Lastschrift ohne vorherige Ankündigung auf das Bankkonto des Kunden zu zahlen. Zu diesem Zweck muss der Kunde zum Zeitpunkt der Validierung der Anfrage zuvor seine Bankverbindung (IBAN) auf der sicheren Schnittstelle des Zahlungsanbieters von COMMOWN eingegeben haben. Der Kunde gibt außerdem eine Handynummer ein, auf die er einen vertraulichen Code erhält. Die Eingabe dieses PIN-Codes auf der Bestätigungsseite ist gleichbedeutend mit der elektronischen Unterschrift auf dem SEPA-Lastschriftmandat und hat in diesem Zusammenhang die gleiche rechtliche Bedeutung wie eine handschriftliche Unterschrift. Der Kunde wird hierüber durch die Zusendung einer E-Mail des zugelassenen Zahlungsdienstleisters informiert.
Der Kunde ist allein für die Validierung des SEPA-Mandats durch seine Bank verantwortlich und verpflichtet sich, eventuell auftretende technische oder administrative Probleme zu melden, damit gegebenenfalls eine alternative Lösung mit COMMOWN umgesetzt werden kann. COMMOWN behält sich das Recht vor, die Bestellung zu stornieren, wenn das SEPA-Mandat nicht eingerichtet werden kann oder die erste Zahlung gleich aus welchem Grund abgelehnt wird.

2.2.2 DURCH COMMOWN :

Das Abonnement des Mietvertrags gilt erst ab der Annahme des Abonnements durch COMMOWN als gültig und endgültig zwischen den Parteien abgeschlossen (Vertragsabschluss durch Angebot des Kunden und Annahme des Angebotes durch COMMOWN). Voraussetzungen hierfür sind insbesondere:

Zustimmung durch die entsprechenden Banken oder Zahlungsdienstleister und Verbuchung der ersten Zahlung (Anzahlung und/oder erste Rate) durch COMMOWN verbucht wurde;
Analyse der Kundeninformationen sowie Erhalt der gegebenenfalls erforderlichen Nachweisdokumente sowie Prüfung derselben durch COMMOWN
Bestätigung durch COMMOWN, dass das vom Kunden zum Zeitpunkt des Abonnements gewählte Produkt auf Lager ist.

HINWEIS: COMMOWN behält sich insbesondere das Recht vor, den Antrag eines Kunden abzulehnen, mit dem ein Streit über die Erfüllung eines früheren Vertrages mit COMMOWN oder einem der Geschäftspartner von COMMOWN besteht oder bestanden hat.

COMMOWN behält sich außerdem das Recht vor, zusätzliche Unterlagen vom Kunden anzufordern, um das Abonnement sowie die vom Kunden gemachten Angaben zu validieren.

Nimmt COMMOWN den Antrag des Kunden an (Validierung der drei oben genannten Bedingungen), wird dem Kunden eine E-Mail zur Bestätigung des Abonnements zugestellt, die als Bestellung und Vertragsannahme gilt, einschließlich der notwendigen vertraglichen Informationen; die Zustellung der E-Mail erfolgt spätestens zum Zeitpunkt des Erhalts des Produkts.

Im Falle einer Ablehnung des Antrags des Kunden trotz Verbuchung der ersten Zahlung wird das Abonnement automatisch storniert und der Kunde wird per E-Mail darüber informiert. Falls das Abonnement storniert wird (und das Produkt nicht versandt wird), wird der Betrag der ersten Zahlung innerhalb von fünfzehn (15) Tagen nach der Benachrichtigung über die Ablehnung an den Kunden zurückerstattet.

COMMOWN behält sich das Recht vor, dem Kunden eine neue Dienstleistung mit einem ähnlichen Produkt anzubieten, wenn das Produkt zum Zeitpunkt der Beauftragung des Abonnements nicht mehr auf Lager ist. In diesem Fall gilt das neue Angebot des Kunden als Vertragsannahme durch COMMOWN. Sofern das neue Angebot für den Kunden nicht geeignet ist, so hat er der COMMOWN dies unverzüglich mitzuteilen. In diesem Fall wird das laufende Abonnement gekündigt.

COMMOWN empfiehlt dem Kunden dringend, die Abonnementbestätigung auf einem zuverlässigen und dauerhaften Medium als Nachweis zu archivieren.

2.2.3 ANGABEN ZUR AUFBEWAHRUNG DER VERTRAGSUNTERLAGEN UND ZUR ARCHIVIERUNG:

Die in den computergestützten Registern, die in den Computersystemen von COMMOWN und seinen Partnern unter angemessenen Sicherheitsbedingungen gespeichert werden aufgenommenen Daten gelten als Beweis für die Kommunikation, die Mietvertragsabonnements und die zwischen den Parteien getätigten Zahlungen.

Die Archivierung von Vertragsunterlagen und Rechnungen erfolgt auf einem zuverlässigen und dauerhaften Medium, so dass sie der Aufbewahrung auf einem dauerhaften Datenträger gemäß den Bestimmungen des Bürgerlichen Gesetzbuches (BGB) und des Einführungsgesetzes zum BGB (EGBGB) entspricht.
Die in den genannten Aufzeichnungen und Dokumenten enthaltenen personenbezogenen Daten entsprechen der Verordnung (EU) 2016/679 des Europäischen Parlaments und des Rates vom 27. April 2016 zum Schutz natürlicher Personen bei der Verarbeitung personenbezogener Daten und zum freien Datenverkehr (DSGVO) sowie den Anforderungen des Bundesdatenschutzgesetzes (BDSG). Die wesentlichen Bedingungen in Hinsicht auf den Datenschutz bei COMMOWN sowie die Rechte des Kunden sind in den Datenschutzbedingungen der COMMOWN niedergelegt, die Teil des Vertragsabschlusses werden (<https://commown.coop/de/datenschutzerklärung/>).

2.3. VERTRAGSBEGINN UND DAUER DER VERPFLICHTUNG

Der Mietvertrag tritt mit der Annahme des Angebots des Kunden durch COMMOWN in Kraft (siehe 2.2.2). Unabhängig davon beginnt die Mietlaufzeit erst am Tag der Übergabe des Produkts an den Kunden Die Übergabe des Kunden wird durch den Lieferanten oder Spediteur durch einen geeigneten Übergabe- oder Hinterlegungsnachweis erbracht. Dieser Nachweis Abliefernachweis des Spediteurs dient als Nachweis für den Mietbeginns und damit den Beginn der Mietvertragslaufzeit.

Die tatsächliche Lieferung der Produkte ist – sofern zutreffend – auch der Ausgangspunkt für die Rechnungsstellung und die Dauer der Verpflichtung des Kunden.

Die rechtliche und vertragliche Grundlage besteht dann insgesamt aus diesen ABG, der mit dem gemieteten Produkt verbundenen BB, den Datenschutzbedingungen der COMMOWN, der E-Mail mit der Bestätigung des Empfangs des Abonnementantrags durch den Kunden, der E-Mail zur Bestätigung des Abonnements durch COMMOWN und dem Nachweis des Empfangs des Produkts.

Die Mietverträge mit Laufzeitbindung werden zunächst für eine Bindungsdauer abgeschlossen, die entsprechend dem gewählten Angebot festgelegt und in den Abonnement-E-Mails festgehalten wird. Am Ende der Laufzeit verlängert sich der Vertrag stillschweigend zu den gleichen Bedingungen und auf unbestimmte Zeit, sofern nicht eine der Parteien den Vertrag gemäß Artikel 13 kündigt.

Mietverträge ohne Laufzeitbindung werden zunächst für einen festen Zeitraum von einem Monat abgeschlossen. Danach wird der Vertrag durch stillschweigende monatliche Verlängerung auf unbestimmte Zeit fortgesetzt, es sei denn, eine der Vertragsparteien kündigt den Vertrag gemäß Artikel 13.

## ARTIKEL 3 – MIETPREIS – ZAHLUNG DER MONATLICHEN RATEN

3.1. SICHERHEITSLEISTUNG

Falls zutreffend, wird dem Kunden zum Zeitpunkt des Abonnements eine Kaution in Höhe eines auf der Produktseite angegebenen Betrags berechnet.

Diese Kaution wird nicht in Rechnung gestellt und wird von COMMOWN für die Dauer des Mietzeitraums einbehalten. COMMOWN stellt Ihnen eine Quittung in Ihrem persönlichen Bereich der Website aus.

Diese Kaution wird bei Beendigung des Vertrages gemäß den in Artikel 15 festgelegten Bedingungen zurückerstattet.

3.2. BEDINGUNGEN, HÖHE UND ZAHLUNG DER MIETE

Die Miete wird monatlich in Rechnung gestellt. Die erste Rechnung wird am Tag des Erhalts des Produktes ausgestellt und sodann jeden Monat erneuert.

Die Höhe der monatlichen Rechnung berücksichtigt:
die zum Zeitpunkt der Bestellung geltende und vom Kunden akzeptierte monatliche Rate, die im persönlichen Online-Bereich des Kunden sowie in der E-Mail zur Bestätigung des Abonnements ausgewiesen wird;
etwaige von COMMOWN gewährte Rabatte oder abweichende Bedingungen, die auf der COMMOWN-Website oder in anderen offiziellen, gültigen Medien von COMMOWN oder ihren Partnern veröffentlicht werden;
Gebühren für einmalige Dienste oder zusätzliche monatliche Optionen, die vom Kunden nach dem Abonnementdatum angefordert werden;
Vertragsstrafen oder Gebühren, sofern in diesen AGB oder in den mit dem gemieteten Produkt verbundenen BB vorgesehen.

Die Rechnungen sind monatlich nachträglich zahlbar, es sei denn, der Kunde vereinbart eine abweichende Zahlung der monatlichen Raten im Voraus.

Die Zahlung per Lastschrift wird daher über nach dem vorgenannten Zeitraum abgebucht und mit der monatlichen Rechnungsstellung ausgelöst, ohne dass eine vorherige Ankündigung erforderlich ist.

In gleicher Weise werden alle anderen fälligen Beträge für die Abrechnung von Gebühren, Produktersatz oder Vertragsstrafen, die in diesen AGB oder in den mit dem gemieteten Produkt verbundenen BB vorgesehen sind, unabhängig von ihrer Höhe im Verhältnis zu den monatlichen Raten, per Lastschriftverfahren beglichen, ausgelöst durch die Ausstellung der entsprechenden Rechnung, die dem Kunden in seinem persönlichen Online-Kundenbereich zur Verfügung gestellt wird, ohne dass es einer vorherigen Ankündigung bedarf.

Rechnungen werden dem Kunden in seinem persönlichen Online-Bereich innerhalb von maximal fünfzehn (15) Tagen ab Ausstellungsdatum zur Verfügung gestellt.
Im Falle einer Kündigung wird dem Kunden die Restrechnung, falls vorhanden, innerhalb von fünfzehn (15) Tagen nach Rückgabe des Produkts an COMMOWN zur Verfügung gestellt.

Auch hier rät COMMOWN dem Kunden dringend, alle Rechnungen auf einem zuverlässigen und dauerhaften Medium als Nachweis zu archivieren.

Im Falle eines Kontowechsels, eines Lastschrifteinzugs oder sonstiger bankbedingter Einschränkungen oder Vorkommnisse wird der Kunde gebeten, COMMOWN so schnell wie möglich, in jedem Fall aber mindestens zehn (10) Tage vor dem nächsten Fälligkeitstermin des Lastschrifteinzugs zu kontaktieren, um die erforderlichen Maßnahmen zu treffen oder ggf. die Unterzeichnung eines neuen Lastschriftmandats zu ermöglichen.

Führt eine verspätete Mitteilung zu einer Ablehnung der Zahlung, behält sich COMMOWN das Recht vor, dem Kunden die entstandenen Kosten in Rechnung zu stellen.

3.3. ZAHLUNGSSÄUMNIS

Für jeden zum vertraglichen Fälligkeitsdatum nicht gezahlten Betrag sind Verzugszinsen zu entrichten, die auf der Grundlage der monatlichen Raten mit einem Jahreszinssatz von fünfzehn Prozent (15 %) berechnet werden.

Bei Zahlungsverzug können dem Kunden gegebenenfalls anfallende Inkassokosten in voller Höhe in Rechnung gestellt werden.

Verzugszinsen und Inkassokosten fallen automatisch an und können nach der ersten Mahnung von COMMOWN fällig werden. Im Falle der Ablehnung einer Lastschrift wird dem Kunden für die Bearbeitung des Vorfalls eine Gebühr von fünf (5) Euro inkl. MwSt. pro Ablehnung in Rechnung gestellt, ohne dass es einer Begründung oder vorherigen Information durch COMMOWN bedarf.

Darüber hinaus behält sich COMMOWN das Recht vor, bei Nichteinhaltung der oben genannten Zahlungsbedingungen die Erbringung der mit dem vom Kunden abonnierten Angebot verbundenen Dienstleistungen auszusetzen oder zu stornieren und/oder die Erfüllung seiner Verpflichtungen auszusetzen.

COMMOWN behält sich das in § 543 Abs.1 BGB normierte Recht vor, den Vertrag nach Nichtzahlung von zwei (2) monatlichen aufeinanderfolgenden Raten zu kündigen. Ebenfalls behält sich COMMOWN das Recht vor, den Vertrag zu kündigen, wenn der Kunde in einem Zeitraum, der sich über mehr als zwei Monate erstreckt, mit der Entrichtung der Miete in der Höhe rückständig ist, die insgesamt die Miete für zwei Monate erreicht.
Diese Kündigung hat die sofortige Rückgabe der Produkte gemäß Artikel 15 zur Folge.
In diesem Fall werden dem Kunden die für die Restlaufzeit des Vertrages fälligen monatlichen Raten in Rechnung gestellt und belastet.

Die Verweigerung der Rückgabe des Produkts oder die Nichtbezahlung der fälligen Beträge hat zur Folge:
die Nichtrückerstattung der Kaution (falls vorhanden) durch COMMOWN, ohne dass COMMOWN dem Kunden eine besondere Mitteilung machen muss;
im Falle der Nichtrückgabe des Produkts die Berechnung des Produkts auf der Grundlage einer in den mit dem Angebot verbundenen BB vorgesehenen Vertragsstrafe oder andernfalls des Kaufpreises des Produkts einschließlich Mehrwertsteuer abzüglich einer Wertminderung von 25 % pro vollem Jahr der Vermietung;
im Falle der Nichtrückgabe des Produkts die Anwendung einer weiteren Vertragsstrafe von fünf (5) Euro (einschließlich Steuern) pro Kalendertag bis zur tatsächlichen Rückgabe des Produkts;
die Einleitung eines gerichtlichen Verfahrens zur Beilegung der Streitigkeit, dessen Kosten je nach Ausgang des Verfahrens gegebenenfalls ausschließlich vom Kunden getragen zu tragen sind.

## ARTIKEL 4 – SICHERE ZAHLUNG

Die Eingabe der Bankverbindung und die Unterzeichnung eines SEPA-Lastschriftmandats ist über das Zahlungssystem des von COMMOWN beauftragten Dienstleisters gesichert. Der Kunde erklärt sich mit der Verwendung seiner personenbezogenen Daten für die Unterzeichnung und Speicherung eines Lastschriftmandats einverstanden.
Der Kunde erkennt an, dass elektronische Medien zumindest den Anfang eines schriftlichen Beweises darstellen und dass im Streitfall die von COMMOWN erstellten elektronischen Dokumente Vorrang vor den vom Kunden erstellten Dokumenten haben, es sei denn, der Kunde weist die mangelnde Zuverlässigkeit oder Authentizität der von COMMOWN erstellten Dokumente nach.

## ARTIKEL 5 – BETRUGSKONTROLLE

COMMOWN prüft alle Verträge, die auf seiner Website unterzeichnet wurden. Diese Kontrollen dienen dem Schutz von COMMOWN vor betrügerischen Praktiken.

COMMOWN ist alleiniger Empfänger der Daten zum Mietvertrag. Wenn die Daten für das Abonnement nicht übertragen werden, kann die Transaktion nicht abgeschlossen und validiert werden.

Im Falle eines Abonnements mit einer von der Rechnungsadresse abweichenden Lieferadresse behält sich COMMOWN das Recht vor, beide genannten Personen (die Person an der Rechnungsadresse und die Person an der Lieferadresse) zu kontaktieren. Dazu erklärt sich der Kunde bereit, die Kontaktinformationen einer Person an der Lieferadresse anzugeben.

Im Rahmen dieser Verfahren behält sich COMMOWN oder eine von ihr beauftragte Person das Recht vor, vom Kunden zusätzliche für die Validierung des Mietvertrags erforderlichen Dokumente anzufordern, insbesondere: Nachweis des Wohnsitzes und der Identität des Kunden und der für die Übergabe angegebenen Person; Nachweis des Einkommens oder der Bankverbindung (IBAN). Diese Anfragen erfolgen per E-Mail und/oder per Brief mit Empfangsbestätigung und setzen den Abschluss des Mietvertrags aus (siehe Artikel 2.2). Der Mietvertrag kann erst abgeschlossen werden, wenn alle angeforderten Nachweise durch den Kunden geliefert und von COMMOWN oder einer von ihr beauftragten Person positiv geprüft wurden.

Ein Abonnement gilt erst dann als endgültig angenommen, wenn es von den Diensten von COMMOWN geprüft und validiert wurde. Das/die Produkt(e) wird/werden erst nach dieser Validierung versandt, was bedeutet, dass die Lieferfrist erst nach der Validierung zu laufen beginnt.

## ARTIKEL 6 – VERFÜGBARKEIT

Angebote von Dienstleistungen einschließlich Produkte, Mietzeiten und Preise sind gültig, solange sie auf der Website sichtbar sind, unter Vorbehalt der Verfügbarkeit im Lager.

Die Verfügbarkeit der Produkte hängt von den Herstellern und von den Finanzierungsquellen von COMMOWN ab. Dem Kunden sind die Unwägbarkeiten der verantwortungsbewussten Elektronik bekannt und er ist darüber informiert, dass er aufgrund äußerer Umstände gegebenenfalls auf die Lieferung warten muss. Der Kunde wird über die voraussichtliche Zeit bis zur Verfügbarkeit des Produktes informiert.

Ist der Kunde mit der angegebenen Lieferzeit nicht einverstanden, kann er seine Mietanfrage während der angegebenen Lieferzeit jederzeit per E-Mail oder Post stornieren. Der Kunde erhält dann den Betrag der ersten Zahlung (Anzahlung und/oder erste monatliche Rate) innerhalb von fünfzehn (15) Tagen nach Erhalt der Kündigung zurück (siehe Artikel 3.1).

## ARTIKEL 7 – LIEFERUNG / AUSLIEFERUNG

Im Falle eines Abonnements von Mietverträgen, die mehrere Produkte kombinieren, können die Produkte in mehreren Paketen und zu verschiedenen Terminen geliefert werden.

7.1. Art der Lieferung

Die Lieferung des Produkts erfolgt an die bei der Bestellung angegebene Lieferadresse, je nach Produkttyp durch einen vom Hersteller auf Wunsch von COMMOWN beauftragten oder direkt von COMMOWN beauftragten Drittanbieter.

Wenn die Lieferadresse von der Rechnungsadresse abweicht oder wenn sich die Adresse zwischen dem Abschluss des Abonnements und der tatsächlichen Lieferung des Produkts ändert, muss der Kunde COMMOWN über diese Änderung informieren. Diese Änderung setzt die Validierung des Abonnements durch COMMOWN aus. COMMOWN kann dann gegebenenfalls neue Belege verlangen und/oder das Abonnement ablehnen.

Bitte beachten Sie, dass die Telefonnummer oder E-Mail-Adresse als Teil der Rechnungs-/Lieferadresse gilt, da sie zur Kontaktaufnahme mit dem Kunden verwendet wird, um die Lieferung sicherzustellen.

7.2. Liefertermin

Der Kunde wird von COMMOWN vor der Auslieferung des Produkts kontaktiert, um die Bedürfnisse des Kunden und verschiedene Punkte im Zusammenhang mit der Inbetriebnahme des Geräts zu überprüfen. COMMOWN kann nicht für Lieferverzögerungen verantwortlich gemacht werden, die durch den Spediteur oder den Hersteller zu verantworten sind. COMMOWN wird dem Kunden jedoch auf Anfrage Informationen über die Lieferung des Mietprodukts zur Verfügung stellen.

7.3. Abholung und Nachverfolgung

Das Produkt wird mit einer Quittung geliefert. Mit der Annahme der Lieferung des Mietprodukts überträgt sich das rechtliche Gewahrsam auf den Kunden (Besitz).

Der Kunde wird darauf hingewiesen, dass es in seiner Verantwortung liegt, die Durchführbarkeit der Anlieferung des Mietproduktes zu prüfen, d.h. der Anlieferungsort muss leicht zugänglich und mit der Aufnahme des Produktes vereinbar sein.
Es liegt auch in der Verantwortung des Kunden, bei der Vertragsannahme die Besonderheiten des Zugangs (z.B. Aufzug, Concierge, Etage) anzugeben. Der Kunde trägt die alleinige Verantwortung für Lieferausfälle, die auf fehlende Informationen zum Zeitpunkt der Bestellung zurückzuführen sind.
Im Falle der Nichtlieferung behält sich COMMOWN das Recht vor, den Mietvertrag zu kündigen oder dem Kunden die entstandenen Mehrkosten in Rechnung zu stellen.

7.4. Überprüfen des Zustands des Produkts bei Lieferung

Es liegt in der Verantwortung des Kunden, vor der Annahme des Pakets den Zustand des gemieteten Produkts zu überprüfen. Dabei ist mindestens erforderlich:

die schnelle Prüfung des Zustands des gelieferten Produkts;
Übereinstimmung des Ablieferungsnachweises des Spediteurs mit den Angaben des Kunden;
Übereinstimmung des gelieferten Produktes mit dem abgeschlossenen Abonnement (Art des Produkts, Menge, etc.).

Im Falle der Lieferung eines beschädigten oder unvollständigen Produkts oder eines Produkts, dessen Verpackung beschädigt, offen oder aufgerissen ist oder dessen Kleber gerissen ist, gilt:

Der Kunde ist verpflichtet, die Lieferung abzulehnen und COMMOWN die Gründe für seine Ablehnung mitzuteilen;
Der Kunde ist verpflichtet, diese Informationen innerhalb von achtundvierzig (48) Stunden nach der verweigerten Lieferung per E-Mail oder Post an COMMOWN zu senden.

Nimmt der Kunde ein Paket an, wird davon ausgegangen, dass es wie bestellt, vollständig und in gutem Zustand geliefert wurde.

7.5. Lieferung und Versand während der Vertragslaufzeit

Während der Laufzeit des Vertrages können die Geräte zu Servicezwecken durch den Kunden oder durch Commown versendet werden.

In jedem Fall ist der Kunde für die Nachverfolgung von Sendungen des Kunden verantwortlich. Es liegt in der Verantwortung des Kunden, dafür zu sorgen, dass alle Pakete mit einer Tracking-Nummer versehen sind, und im Falle eines Zwischenfalls bei der Zustellung alle notwendigen Schritte mit dem Spediteur zu unternehmen. Sofern der Verlust oder die Verspätung des Pakets durch den Kunden verschuldet wurde, behält sich COMMOWN das Recht vor, dem Kunden die entstandenen Kosten in Rechnung zu stellen.

Im Falle einer Änderung der Kontaktdaten, der E-Mail, der Telefonnummer oder sonstiger Kontaktinformationen oder einer abweichenden Lieferadresse wird der Kunde gebeten, COMMOWN so schnell wie möglich zu informieren, damit die notwendigen Aktualisierungen vorgenommen werden können, um Probleme bei der Lieferung zu vermeiden. Wenn die verspätete Benachrichtigung zu Problemen bei der Lieferung (Mehrkosten, Verlust, Verzögerung usw.) führt, behält sich COMMOWN das Recht vor, dem Kunden die entstandenen Kosten in Rechnung zu stellen.

7.6. Umzug des Kunden

Sofern der Kunde umzieht, muss er COMMOWN so schnell wie möglich informieren und die neue Adresse per Post, E-Mail oder über das COMMOWN-Kontaktformular mitteilen sowie das mit dem Mietvertrag verknüpfte Kundenkonto aktualisieren (Adresse und aktualisierter Adressnachweis).

Wenn das Fehlen oder die Verspätung von Informationen im Zusammenhang mit einem Umzug zu Problemen bei einer Lieferung (Mehrkosten, Verlust, Verzögerung usw.) führt, behält sich COMMOWN das Recht vor, dem Kunden die entstandenen Kosten in Rechnung zu stellen.

Im Falle eines Umzugs außerhalb des von COMMOWN abgedeckten lokalen Gebiets (siehe 1.2.) behält sich COMMOWN das Recht vor, den Vertrag zu kündigen. Der Transport des Produkts im Rahmen eines Umzugs ist nicht Bestandteil der vertraglichen Leistungen. Sie erfolgt daher auf alleinige Kosten des Kunden, und der Kunde ist allein verantwortlich für eventuelle Schäden am Produkt, das Eigentum von COMMOWN ist.

## ARTIKEL 8 – WIDERRUFSRECHTE

8.1. GRUNDLAGEN

Während COMMOWN bis zur Lieferung des Produkts ein Widerrufsrecht hat, hat der Kunde über eine Frist von vierzehn (14) Tagen ab der Lieferung des Mietprodukts, um sein Widerrufsrecht gemäß den Regeln des Verbraucherschutzes auszuüben. Wenn die vierzehntägige Frist an einem Samstag, Sonntag oder Feiertag abläuft, verlängert sie sich bis zum nächsten Werktag.

Die Kosten und Verfahren für die Rücksendung des Produkts im Falle eines Widerrufes gehen zu Lasten des Kunden.

8.2. AUSÜBUNG DES WIDERRUFSRECHTES

Der Kunde, der von seinem Widerrufsrecht Gebrauch machen möchte, wird gebeten, das im Anhang zu diesen AGB erhältliche Rücktrittsformular zu verwenden. Es ist auf jeden Fall erforderlich, den Rücktritt mittels einer schriftlichen Erklärung auf auf Papier oder per E-Mail unmissverständlich mitzuteilen.

Der Kunde wird darüber informiert, dass COMMOWN ab dem Zeitpunkt, an dem er seinen Widerrufswunsch mitteilt, alle Anstrengungen unternehmen wird, das Produkt zurückzunehmen oder zurücknehmen zu lassen.

Aufgrund des Wertes und der leichten Zerbrechlichkeit der Produkte kann der Kunde den Transportdienstleister nicht selbst wählen, sondern muss den von COMMOWN autorisierten Transportdienstleister nutzen. Der Kunde trägt die Kosten für die Rücksendung. \[Beschränkungen hier ausgelassen, da nach deutschem Recht nicht erforderlich]

Der Kunde hat eine Frist von vierzehn (14) Tagen ab dem Datum der Mitteilung seines Widerrufs, um das Produkt an COMMOWN unter eigener Kostentragung zurückzusenden. Wenn der Kunde die Rücksendekosten nicht bezahlt und das Produkt nicht innerhalb dieser Frist an COMMOWN zurücksendet, gilt der Widerruf als nichtig und kann nicht wirksam werden. Der Kunde ist daher in diesem Fall trotz des Widerrufs zur Zahlung der monatlichen Mietraten verpflichtet.

Macht der Kunde von seinem Rücktrittsrecht Gebrauch, wird der Betrag einer gegebenenfalls gezahlten Kaution innerhalb von maximal dreißig (30) Tagen nach Ausübung des Rücktrittsrechts auf seinem Bankkonto gutgeschrieben. COMMOWN kann die Rückerstattung aufschieben, bis das Produkt zurückerlangt worden ist.

Das Rücktrittsrecht wird ohne Vertragsstrafe eingeräumt, mit Ausnahme der Rücksendekosten, die in der Verantwortung des Kunden bleiben.

Der Kunde kann jedoch für eine Wertminderung des Produkts haftbar gemacht werden, die auf eine andere als die zur Feststellung der Art, der Eigenschaften und der ordnungsgemäßen Funktion des Produkts erforderliche Handhabung zurückzuführen ist. Darüber hinaus hat der Kunde mit Erhalt des Produkts den rechtlichen Besitz an dem gemieteten Produkt. Er übernimmt daher alle direkten oder indirekten Folgen eines Ereignisses, das seine Verantwortung gegenüber Dritten während dieses Zeitraums betrifft.

## ARTIKEL 9 – VERTRAGLICHE NUTZUNGSBEDINGUNGEN

9.1. Untervermietung und Verkauf

Der Kunde darf das Mietprodukt nicht untervermieten oder in irgendeiner anderen Weise darüber verfügen.

Die Untervermietung des Mietprodukts, die unentgeltliche oder entgeltliche Überlassung an einen Dritten oder der Austausch gegen ein anderes Produkt verstößt gegen die Bestimmungen und den Geist des Vertrags und kann zu einem Anspruch auf Ersatz des COMMOWN entstandenen Schadens führen.

9.2. Verwendung, Wartung

Der Kunde muss das Produkt für die Dauer des Mietzeitraums unter normalen Bedingungen nutzen und es durch die Verwendung der vom Hersteller empfohlenen Wartungsprodukte in gutem Betriebszustand halten.

Der Kunde ist sich bewusst, dass eines der Hauptziele von COMMOWN darin besteht, die Veralterung des Produkts zu bekämpfen, indem seine Lebensdauer und damit seine Wiederverwendung durch die Mitglieder der Gemeinschaft maximiert wird. Der Kunde verpflichtet sich daher, dieses gemeinsame Ziel zu verfolgen, das Produkt durch besondere Sorgfalt bei der Verwendung in gutem Zustand zu halten.

Der Kunde darf das Produkt in keiner Weise modifizieren (oder verändern).
Der Kunde darf ohne Zustimmung von COMMOWN keine Änderungen (Hinzufügen, Entfernen oder Ersetzen von Teilen) oder Reparaturen an den Geräten vornehmen. Die Nichteinhaltung dieses Verbots stellt eine Vertragsverletzung durch den Kunden dar, die auf Initiative von COMMOWN ohne Entschädigung des Kunden zur Auflösung des Vertrags führen kann. COMMOWN behält sich das Recht vor, dem Kunden die Kosten für die Wiederherstellung des ursprünglichen Zustands des Produkts in Rechnung zu stellen, falls dies erforderlich ist.
I
m Falle eines Wechsels des Betriebssystems ohne Zustimmung von COMMOWN ist COMMOWN nicht mehr zur Erbringung von Supportleistungen verpflichtet, ohne dass dies eine Verletzung der Pflichten von COMMOWN darstellt.

9.3. Daten

Bei der Vermietung von elektronischen Geräten, die Daten speichern, liegt es in der Verantwortung des Kunden, seine Daten zu sichern. COMMOWN kann nicht für den Verlust von Daten durch den Benutzer verantwortlich gemacht werden.

COMMOWN verpflichtet sich, alle Daten des Kunden bei Rückgabe des Produkts durch den Kunden dauerhaft zu vernichten.

COMMOWN stellt dem Kunden lizenzierte Software zur Verfügung und kann nicht für die Verwendung von nicht lizenzierten oder urheberrechtlich geschützten Produkten durch den Kunden haftbar gemacht werden.

## ARTIKEL 10 – GEISTIGES EIGENTUM

Alle Dokumente, Produkte, Texte, Kommentare, Informationen, Logos, Markenzeichen, Illustrationen und Bilder, die auf der COMMOWN-Website und auf allen verbreiteten digitalen oder physischen Datenträgern wiedergegeben oder zugänglich gemacht werden, sind von ihren Eigentümern sowohl in wirtschaftlicher als auch in moralischer Hinsicht urheberrechtlich geschützt.

Jede Darstellung, Verbreitung oder öffentliche Kommunikation, ob kommerziell oder nicht kommerziell, dieser Dokumente, Produkte, Texte, Kommentare, Informationen, Logos, Marken, Illustrationen und Bilder, jede vollständige oder teilweise Reproduktion für andere Zwecke und ganz allgemein jede Weitergabe an Dritte, auf welche Weise auch immer, einschließlich Verkauf, Vermietung, Tausch, Verleih, ist streng verboten, es sei denn, es liegt eine ausdrückliche vorherige Zustimmung des Eigentümers, also der COMMOWN vor.

Der Kunde verpflichtet sich, nicht zu versuchen, die für die Nutzung des Dienstes erforderliche Software zu umgehen oder zu verändern. Im Übrigen hat der Kunde nach Treu und Glauben zu leisten.

Auf die von Gesetz her bestimmten Bestrafungen bei Verstößen gegen die vorstehenden Bestimmungen wird ausdrücklich hingewiesen.

## ARTIKEL 11 – HAFTUNG UND GARANTIEN

11.1. Voraussetzung

Im Allgemeinen sind die Fotos und Abbildungen, die die Produkte ganz oder teilweise auf der COMMOWN-Website und auf allen verteilten digitalen oder physischen Medien wiedergeben, nicht Gegenstand des Vertrages. Der Kunde wird gebeten, die genauen technischen Eigenschaften und das Aussehen der Produkte zur Kenntnis zu nehmen, bevor er mit der Zeichnung des Mietvertrags fortfährt, da nur diese Eigenschaften wesentlich und entscheidend für die Annahme des Zeichnungsangebots durch den Kunden sind.

COMMOWN haftet nicht für offensichtliche Fehler, die der Kunde bei der Auswahl der Eigenschaften und Einsatzbedingungen des Produkts macht.

COMMOWN haftet nicht für Hypertext-Links, die gegen gesetzliche oder behördliche Bestimmungen verstoßen.

Der Kunde erkennt an, dass er die Informationen über die Interoperabilität der Produkte auf der COMMOWN-Website gelesen hat und dass die vollständige oder teilweise Unmöglichkeit der Nutzung der Produkte aufgrund von Inkompatibilität von Diensten oder Hardware keine Entschädigung oder Rückerstattung oder irgendeine Haftung von COMMOWN zur Folge hat. 

Es gelten die gesetzlichen Gewährleistungsbestimmungen. Diese werden durch eine allfällige Garantie nicht berührt.

11.2 Rechtliche Verwahrung, Eigentum

Mit dem Erhalt des Produkts geht die Gefahr für das Produkt auf den Kunden über. Der Kunde erhält Besitz an dem Produkt und hat das rechtliche Gewahrsam und die Verantwortung für das Produkt, bis das Produkt von COMMOWN zurückerhalten wird. Der Kunde übernimmt alle direkten oder indirekten Folgen von Ereignissen, die während der Mietzeit eintreten, unabhängig davon, ob sie durch eine Versicherung gedeckt sind oder nicht, mit voller Haftung gegenüber Dritten.

COMMOWN fordert daher den Kunden auf, eine Versicherung abzuschließen, die seine Interessen und die Integrität des Produkts schützt.
Der Kunde verpflichtet sich, das Produkt jederzeit zur Verfügung zu stellen, wenn COMMOWN ihn dazu auffordert, um den tatsächlichen Zustand zu beurteilen (in diesem Fall werden die Versandkosten von COMMOWN getragen). In diesem Fall erstellt COMMOWN eine Gutschrift entsprechend der Zeit, in der das Gerät nicht benutzt wurde.

COMMOWN haftet nicht für die Folgen von Ereignissen, die während der Mietzeit eintreten, deren Schaden aber erst nach Rückgabe des Produkts an COMMOWN.

Der Kunde erwirbt keine Eigentumsrechte an dem Produkt, noch an seinen Teilen, einschließlich Zubehör. Diese bleiben Eigentum von COMMOWN, unabhängig davon, ob sie dem Kunden in Rechnung gestellt wurden oder nicht. Das gleiche gilt für ausgetauschte Teile und Baugruppen. Der Kunde verpflichtet sich, die ausschließlichen Eigentumsrechte von COMMOWN jederzeit und auf eigene Kosten durchzusetzen.

Im Falle einer Pfändung ist der Kunde verpflichtet, COMMOWN unverzüglich zu benachrichtigen, alle Erklärungen abzugeben und alle Maßnahmen zu treffen, um die Eigentumsrechte von COMMOWN zu wahren.

Wenn die Pfändung nicht innerhalb von sieben (7) Tagen nach der Pfändung aufgehoben wird, wird der Vertrag gekündigt und der Kunde ist verpflichtet, COMMOWN die bis zum Ende des Mietvertrags fälligen Beträge zu zahlen. Der Kunde wird COMMOWN weiterhin alle Kosten und Auslagen, die durch die Pfändung entstehen, gegen Vorlage von Belegen erstatten.

## ARTIKEL 12 – IM MIETVERTRAG ENTHALTENE LEISTUNGEN

12.1. DEFINITIONEN

Panne:
Jede Störung, die das ordnungsgemäße Funktionieren des Geräts beeinträchtigt und auf ein internes Phänomen des gemieteten Produkts zurückzuführen ist und dessen Nutzung verhindert.

Bruch:
Jegliche vollständige oder teilweise Zerstörung des gemieteten Geräts, die dessen ordnungsgemäße Funktion beeinträchtigt und auf einen Unfall, Ungeschicklichkeit oder Fahrlässigkeit zurückzuführen ist. Jeder Glas- oder Fensterbruch wird als Bruch angesehen. Bruch schließt unbeabsichtigte Oxidation ein.

Unbeabsichtigte Oxidation:
Jegliche Korrosion durch chemische Einwirkung auf die Komponenten des Mietprodukts, die dessen ordnungsgemäße Funktion beeinträchtigt und auf einen Unfall, Ungeschicklichkeit oder Fahrlässigkeit zurückzuführen ist. Die Oxidation der Komponenten kann mehrere Ursachen haben, wie z. B. ein Sturz des Geräts ins Wasser oder seine Einwirkung in einer zu feuchten Umgebung.

Zubehör und Anschlüsse :
Für die Zwecke der Garantie gelten als Zubehör und Anschlüsse des Geräts die in der Originalverpackung des Herstellers und die von COMMOWN während der Mietzeit gelieferten Teile.

12.2. ALLGEMEINE BEDINGUNGEN

Neben der Vermietung des Produkts zur normalen persönlichen Nutzung durch den Kunden umfasst das Mietangebot eine Reihe von zugehörigen Dienstleistungen, deren Art und Umfang in den entsprechenden BB des abonnierten Angebots beschrieben sind.

Wenn nicht anders angegeben, gelten die zugehörigen Leistungen für die gesamte Mietdauer.

Im Rahmen der normalen Vertragsabwicklung innerhalb der in Artikel 1 genannten räumlichen Grenzen verpflichtet sich COMMOWN, die Versandkosten für die Rücksendung des Produkts im Zusammenhang mit den mit dem Mietangebot verbundenen Dienstleistungen zu übernehmen (außer in Sonderfällen, die in den AGB oder BB des Angebots geregelt sind).

Generell verpflichtet sich der Kunde, alle Unterlagen zur Verfügung zu stellen, die COMMOWN zur Beurteilung der Gültigkeit der Anfrage für die damit verbundenen Dienstleistungen für notwendig erachtet.

Im Falle einer Panne, eines Bruchs oder eines Kontakts mit Wasser verpflichtet sich der Kunde, keine Arbeiten an dem Produkt vorzunehmen (weder selbst noch durch Dritte), sondern die von COMMOWN gegebenen Anweisungen zu befolgen. Kommt der Kunde dem nicht nach, behält sich COMMOWN das Recht vor, den Mietvertrag entschädigungslos zu kündigen, und es wird eine Gebühr für die Reparatur oder den Ersatz des Produkts erhoben.

Wenn an einem Produkt, das personenbezogene Daten enthält, Arbeiten durchgeführt werden sollen, sei es durch den Kunden oder durch COMMOWN, verpflichtet sich der Kunde, alle seine Daten vorher zu sichern. Ebenso verpflichtet sich der Kunde, wenn er ein solches Produkt an COMMOWN sendet, es zunächst von einem persönlichen Konto (Cloud oder anderweitig) zu trennen, alle ortsbezogenen Anwendungen zu deaktivieren, die seine Steuerung ermöglichen, und es auf den Werkszustand zurückzusetzen.
Andernfalls kann COMMOWN nicht für den Verlust von Daten verantwortlich gemacht werden und behält sich außerdem das Recht vor, alle mit dem Mietangebot verbundenen Leistungen zu verweigern.

Generell kann COMMOWN nicht für materielle oder immaterielle Schäden verantwortlich gemacht werden, die durch Ausfall, Bruch, Verlust oder Diebstahl der gemieteten Geräte entstehen (z.B. Verlust von Computerdaten, etc.).

12.3. AUSSCHLÜSSE

Die mit dem abonnierten Angebot verbundenen Leistungen gelten nicht in den folgenden Fällen:

Schäden werden vom Kunden am Mietprodukt vorsätzlich verursacht oder provoziert, auch als Folge eines Ereignisses höherer Gewalt;
Schäden, die durch atomare Spaltung, die Einwirkung von elektrischem Strom oder in Verbindung mit der Einwirkung einer magnetischen Quelle entstehen, oder die mit Trockenheit, Verschmutzung oder übermäßiger Temperatur zusammenhängen, sowie Kratzer, Absplitterungen und Schrammen, die die Funktion des Produkts nicht beeinträchtigen;
Schäden, die durch einen Krieg, ein kriegsähnliches Ereignis oder einen massiven Computervirus verursacht werden;
Ausfall des gemieteten Produkts verursacht durch ein Verbrauchsmaterial oder ein Zubehörteil des Produkts.

Schließlich wird daran erinnert, dass jeder Betrug oder jede vorsätzliche Falschdarstellung durch den Kunden zum Verlust aller Rechte an den mit dem Serviceangebot verbundenen Dienstleistungen und zur Kündigung des Mietvertrags ohne Entschädigung des Kunden führt, dem alle COMMOWN entstandenen Kosten sowie die in Artikel 13.3 vorgesehene Kündigungsgebühr in Rechnung gestellt und berechnet werden.

## ARTIKEL 13 – BEENDIGUNG DES MIETVERTRAGS

13.1 Beendigung auf Wunsch des Kunden vor Ablauf der Vertragslaufzeit

Möchte der Kunde den Vertrag vor Ende der Vertragslaufzeit kündigen, schuldet er COMMOWN weiterhin alle bis zum Ende der Vertragslaufzeit fälligen monatlichen Zahlungen. Der Restbetrag der Raten wird nach Eingang der Stornierungsanfrage per Lastschrift eingezogen. Das Produkt muss vor Ablauf der Vertragslaufzeit gemäß den in Artikel 15 beschriebenen Bedingungen zurückgegeben werden. Wenn das Produkt nicht vor Ende der Vertragslaufzeit zurückgegeben wird, wird die Abrechnung auf monatlicher Basis fortgesetzt, bis das Produkt tatsächlich zurückgegeben wird.

13.2 Beendigung außerhalb eines Verpflichtungszeitraums

Die Beendigung des Mietverhältnisses kann vom Kunden jederzeit ohne Begründung per E-Mail oder einfacher Post an die COMMOWN-Zentrale mit einer Kündigungsfrist von fünfzehn (15) Kalendertagen verlangt werden.

Die Beendigung des Mietverhältnisses kann von COMMOWN jederzeit ohne Begründung durch Einschreiben an den Kunden mit einer Kündigungsfrist von dreißig (30) Kalendertagen verlangt werden.

Der Kunde verpflichtet sich dann, das Produkt innerhalb von sieben (7) Kalendertagen ab dem Datum der Kündigung zurückzugeben. Zu diesem Zweck muss sich der Kunde mit COMMOWN in Verbindung setzen, um die Bedingungen für die Rückgabe des Produkts zu vereinbaren und die Abläufe zu erleichtern. Für die Einhaltung der Fristen bleibt der Kunde allein verantwortlich.

Der Kunde bleibt in jedem Fall für die fälligen monatlichen Raten haftbar, einschließlich der letzten vor dem Versanddatum des zurückgegebenen Produkts in Rechnung gestellten monatlichen Rate (d.h. ein begonnener monatlicher Abrechnungszyklus bleibt fällig).

13.3 . Kündigung wegen Nichteinhaltung vertraglicher Verpflichtungen durch den Kunden

Wenn der Kunde einer seiner Verpflichtungen aus dem Vertrag nicht nachkommt, insbesondere wenn er die monatlichen Raten nicht bezahlt, wird der Mietvertrag von Rechts wegen und ohne jede Formalität aufgelöst, wenn COMMOWN dies für erforderlich hält, und zwar fünfzehn (15) Tage nach der Zustellung einer ganz oder teilweise wirkungslos gebliebenen Mahnung. In diesem Fall behält sich COMMOWN das Recht vor, die gemieteten Produkte im Voraus zurückzuholen, wobei die Kosten dafür vom Kunden als Entschädigung getragen werden. Wir erinnern Sie daran, dass der Kunde verpflichtet ist, seine Post- und Telefondaten zu aktualisieren. COMMOWN behält sich das Recht vor, rechtliche Schritte einzuleiten, falls die Rücknahme der gemieteten Geräte nicht möglich ist.

Darüber hinaus haftet der Kunde für die ausstehenden monatlichen Zahlungen und für alle ausstehenden monatlichen Zahlungen bis zum Ende der Laufzeit des Mietvertrages. Der Restbetrag der monatlichen Raten wird dann auf einmal per Lastschrift nach Mitteilung des Widerrufs gezahlt, unabhängig vom Zeitpunkt der Rückgabe des Produkts, die unter den in Artikel 15 beschriebenen Bedingungen erfolgt.

13.4. Ausschlussklausel für die Einhaltung von Vorschriften

Für den Fall, dass das Produkt neuen gesetzlichen Vorschriften entsprechen muss, behält sich COMMOWN das Recht vor, den Mietvertrag mit sofortiger Wirkung zu kündigen, ohne dass der Kunde eine finanzielle Entschädigung erhält, wobei die Kosten für die Rücksendung zu Lasten von COMMOWN gehen. Der Kunde ist dann nicht mehr an die Laufzeit des Vertrages gebunden, schuldet aber weiterhin die ausstehenden monatlichen Zahlungen.

3.5. Tod

Im Falle des Todes des Kunden endet der Mietvertrag automatisch, vorbehaltlich des Nachweises des Todes. In diesem Fall haben die Anspruchsberechtigten eine Frist von zwei Monaten, um das Produkt unter den in Artikel 15 beschriebenen Bedingungen zurückzugeben.

13.6. Ausschlussklausel für den Fall von Lagermangel

Die Verfügbarkeit von Produkten und Ersatzteilen hängt von den Lieferanten entlang der gesamten Wertschöpfungskette ab (Hersteller, Verbauer, Produzenten). Der Kunde ist sich der Besonderheiten der Geräte und der Gefahren der verantwortlichen Elektronik bewusst und weiß genau, dass kein alternatives Gerät (oder Ersatzteil) einer anderen Marke oder eines anderen Modells als das gemietete von COMMOWN als Ersatz oder Reparatur geliefert werden kann.

Für den Fall, dass der offizielle Hersteller (Marke des Geräts) und Lieferant von COMMOWN ein Gerätemodell oder ein Ersatzteil nicht mehr vorrätig hat, behält sich COMMOWN das Recht vor, den Mietvertrag mit sofortiger Wirkung zu kündigen, ohne dass dem Kunden eine finanzielle Entschädigung zusteht, wobei die Kosten für die Rücksendung des Geräts zu Lasten von COMMOWN gehen.

Der Kunde ist nicht mehr an die Laufzeit des Vertrages gebunden, schuldet aber weiterhin die ausstehenden monatlichen Zahlungen.

Dem Kunden wird ein Angebot für ein neues Modell des jeweiligen Geräts unterbreitet und er kann sich für die Übernahme eines Vertrags mit diesem neuen Gerät entscheiden.

## ARTIKEL 14 – ÜBERTRAGUNG DES VERTRAGS AUF EINEN ANDEREN KUNDEN VOR ENDE DER VERTRAGSLAUFZEIT

Definitionen für diesen Absatz:

Kunde A: Kunde mit einer aktuellen Miete mit Verpflichtung und dem zugehörigen Produkt in gutem Zustand.

Kunde B: eine auf der COMMOWN-Website registrierte Person, die ihre Identität bestätigt und ein SEPA-Lastschriftmandat unterzeichnet hat.

Möchte Kunde A seinen Mietvertrag vor Ablauf der Bindungsfrist je nach abonniertem Angebot beenden, kann er die Übergabe an einen Kunden B, beginnend ab dem vierten Mietmonat in seinem Namen, anbieten.

Kunde B erhält die gleichen Vertragsbedingungen wie Kunde A, die Restlaufzeit von Kunde A und das überholte Produkt, ohne Möglichkeit des Rückgriffs.

Wenn Kunde B die Annahme des Übertrages schriftlich (per Brief oder E-Mail) sowie durch Unterzeichnung eines elektronischen Lastschriftmandats und Zahlung der Kaution auf der COMMOWN-Website bestätigt und wenn COMMOWN mit der Analyse der von Kunde B übermittelten Elemente einverstanden ist, dann bestätigt COMMOWN dem Kunden A die Beendigung des Vertrags. Kunde A bleibt für die Rückgabe des Produkts an COMMOWN unter den in Artikel 15 genannten Bedingungen verantwortlich.

COMMOWN wird das Produkt dann spätestens innerhalb von fünfzehn (15) Kalendertagen nach Erhalt und Bestätigung des Zustands und der Funktionalität des Produkts durch Kunde A an Kunde B versenden.

COMMOWN trägt alle Versandkosten im Zusammenhang mit der Rücksendung durch Kunde A und dem Versand des Produkts an Kunde B.

Kunde A haftet für alle fälligen monatlichen Raten, die laufende Monatsrate zum Zeitpunkt des tatsächlichen Empfangs des Produkts durch COMMOWN sowie eine Gebühr zwischen 45 (fünfundvierzig) und 70 (siebzig) Euro inklusive Mehrwertsteuer je nach Gewicht des Produkts entsprechend der nachstehenden Übersicht:

bis 0,99 kg Gewicht: 30 EUR inkl. MwSt.
zwischen 1,0 bis 1,99 kg Gewicht: 45 EUR inkl. MwSt.
zwischen 2,0 und 3,99 kg Gewicht: 60 EUR inkl. MwSt.
über 4,0 kg Gewicht: 75 EUR inkl. MwSt.

Die genaue Höhe der Gebühr wird dem Kunden mitgeteilt, bevor COMMOWN mit dem Verfahren beginnt. Diese zusätzlichen Gebühren dienen der Deckung der den Kosten für die Rückgabe des Produkts, seine Wiederaufbereitung und die Verwaltungsgebühren für die Übertragung des Vertrags.

Kunde B haftet für die monatlichen Zahlungen ab dem Datum des Erhalts des Produkts.

## ARTIKEL 15 – RÜCKGABE DES PRODUKTS

15.1 Bedingungen

Unabhängig vom Grund für die Beendigung des Mietverhältnisses und mit Ausnahme der in Artikel 13.5 genannten Sonderbedingungen verpflichtet sich der Kunde, das Produkt innerhalb von sieben (7) Kalendertagen ab dem Datum der Beendigung an COMMOWN zurückzugeben.

Aufgrund des Wertes und der Zerbrechlichkeit der gelieferten Geräte kann der Kunde den Transportdienstleister nicht wählen und muss den von COMMOWN autorisierten Dienstleisternutzen. In den besonderen Fällen, in denen COMMOWN sich bereit erklärt, den Versand durch den Kunden abwickeln zu lassen, erfolgt dies gemäß den von COMMOWN vorgegebenen Transportbedingungen (d.h. mindestens per verfolgtem Paketversand mit Rückschein und Versicherungsmöglichkeit bis zum Wiederbeschaffungswert des Produkts).

Unabhängig vom Grund für die Beendigung der Vermietung des Produkts muss der Kunde seine monatlichen Zahlungen weiterhin erfüllen. Der Kunde bleibt auch für den vollen Betrag der letzten Monatsrate, die vor dem Versanddatum des zurückgegebenen Produkts in Rechnung gestellt wurde, haftbar (d.h. ein eventuell begonnener monatlicher Abrechnungszyklus bleibt fällig).

15.2 Zustand des Produkts

Das Produkt muss in gutem Zustand sein, und die Abnutzung muss auf den normalen Gebrauch zurückzuführen sein. Das Produkt muss in einem optimalen Sauberkeitszustand zurückgegeben werden.

Das Produkt muss in seiner ursprünglichen Hardwarekonfiguration zurückgegeben werden, andernfalls werden dem Kunden die Kosten für die Wiederherstellung des ursprünglichen Zustands in Rechnung gestellt.

Das Produkt muss vollständig sein und alle Zubehörteile enthalten, unabhängig davon, ob sie dem Kunden in Rechnung gestellt wurden oder nicht, ob sie bei der Erstlieferung des Produkts oder während der Mietzeit geliefert wurden. Das gleiche gilt für Teile und Baugruppen, die während der Mietzeit ausgetauscht werden.
Das Produkt muss in seiner Originalverpackung, vollständig und in gutem Zustand zurückgegeben werden.

Anormale Beschädigungen, Pannen, Brüche oder von COMMOWN beim Empfang festgestellte Schäden oder fehlende Bauteile oder Teile werden dem Kunden in Rechnung gestellt, und zwar in Höhe der Kosten, die COMMOWN für die Reparatur, die Reinigung, den Ersatz oder das Recycling des Produkts entstanden sind.

Wenn das Produkt beschädigt ist und nicht repariert werden kann, haftet der Kunde für den wirtschaftlichen Wert des Produkts.

Bei Rückgabe eines Produkts, das personenbezogene Daten enthält, muss der Kunde, sofern es sich noch in einem funktionsfähigen Zustand befindet, alle auf dem Gerät gespeicherten personenbezogenen Daten löschen, das Gerät von allen persönlichen Konten (Cloud oder anderweitig) trennen, alle standortbasierten Anwendungen deaktivieren, die eine Steuerung des Geräts ermöglichen, und es auf den Werkszustand zurücksetzen.

Es liegt in der Verantwortung des Kunden, seine Daten zu sichern. COMMOWN ist weder verpflichtet, Daten im Auftrag des Kunden wiederherzustellen, noch haftet COMMOWN für den Verlust von Kundendaten.

15.3. Gebühren und Kaution

Im normalen Verlauf des Vertrages und mit Ausnahme von Sonderfällen, die in den vorliegenden AGB oder in den mit dem gemieteten Produkt verbundenen BB angegeben sind, verpflichtet sich COMMOWN, die Versandkosten für die Rücksendung des Produkts zu übernehmen.

Werden die Versandkosten vom Kunden vorgestreckt (vorbehaltlich der vorherigen Zustimmung von COMMOWN) und von COMMOWN vertraglich übernommen, werden sie innerhalb von fünfzehn (15) Tagen nach Erhalt der entsprechenden Belege von COMMOWN erstattet.

Wenn das Produkt in dem in Artikel 15.2 geforderten Zustand zurückgegeben wird, wird die Kaution – sofern bezahlt – innerhalb von maximal dreißig (30) Tagen nach Erhalt des zurückgegebenen Produkts an den Kunden zurückerstattet.

Im Falle einer Nichtkonformitätsprüfung, die weitere Fachkenntnisse erfordert, oder aufgrund anderer äußerer und unvorhergesehener Zwänge, behält sich COMMOWN das Recht vor, die Rückerstattungsfrist auf maximal drei (3) Monate ab dem Datum des Eingangs des zurückgegebenen Produkts zu verschieben.

Im Falle einer Nichtübereinstimmung, die eine Gebühr gemäß Artikel 15.2 zur Folge hat, behält sich COMMOWN das Recht vor, die Rückerstattung der Kaution bis zur Zahlung der Gebühr aufzuschieben.

Nichtrückgabe des Produktes oder Nichtzahlung fälliger Beträge führen zu:
Nichtrückerstattung der Kaution (falls vorhanden) durch COMMOWN, ohne dass COMMOWN besondere Schritte gegenüber dem Kunden unternehmen muss;
im Falle der Nichtrückgabe des Produkts Berechnung des Produkts auf der Grundlage einer in den mit dem Angebot verbundenen CPs vorgesehenen Geldstrafe oder andernfalls des Kaufpreises des Produkts einschließlich Mehrwertsteuer abzüglich einer Wertminderung von 25 % pro vollem Jahr der Vermietung;
im Falle der Nichtrückgabe des Produkts die Anwendung einer Strafe von fünf (5) Euro (einschließlich Steuern) pro Kalendertag bis zur tatsächlichen Rückgabe des Produkts;
Einleitung eines gerichtlichen Verfahrens zur Beilegung der Streitigkeit, dessen Kosten ausschließlich vom Kunden getragen werden können.

## ARTIKEL 16 – NICHT-VERZICHT AUF RECHTE

Die Tatsache, dass eine der Parteien zu einem bestimmten Zeitpunkt von einer der Klauseln dieses Vertrages keinen Gebrauch macht, kann von der anderen Partei nicht als Verzicht auf ihre Rechte angesehen werden.

## ARTIKEL 17- ÜBERTRAGUNG VON RECHTEN

Alle Abonnements werden in Anbetracht der Umstände und der wirtschaftlichen Situation des Kunden angenommen und können vom Kunden ohne die ausdrückliche schriftliche Zustimmung von COMMOWN nicht abgetreten oder übertragen werden, mit Ausnahme des in Artikel 14 beschriebenen Verfahrens.

## ARTIKEL 18 – WAHL DES WOHNSITZES

Für die Anwendung dieser AGB und der mit dem Mietprodukt verbundenen BB wählen die Parteien den Hauptort ihres jeweiligen Wohnsitzes bzw. Sitzes. In jedem Fall ist deutsches Recht anwendbar, im Falle dass der Gerichtsstand sich nach dem Sitz vom COMMOWN richtet wird Berlin als Gerichtsstand bestimmt.

Jede Änderung des Wohnsitzes des Kunden muss COMMOWN innerhalb von sieben (7) Tagen nach dieser Änderung mitgeteilt werden, andernfalls behält sich COMMOWN das Recht vor, den Vertrag zu kündigen.

## ARTIKEL 19 – RECHTSREGIME UND STREITSCHLICHTUNG

Auf Verträge, die der Kunde mit COMMOWN unter Anwendung dieser AGB auf der COMMOWN-Website abschließt, ist deutsches Recht anwendbar.

Im Falle einer Streitigkeit bemühen sich die Parteien nach besten Kräften um eine gütliche Beilegung ihrer Streitigkeit, einschließlich der Mediation gemäß Artikel 22. Im Falle des Scheiterns ist das Gericht am Wohnsitz des Beklagten zuständig, sofern das Gesetz nichts anderes vorsieht.

## ARTIKEL 20 – KUNDENDIENST

Bei Problemen im Zusammenhang mit dem Produkt wird der Kunde gebeten, sich an den Kundendienst von COMMOWN zu wenden, und zwar:

über das Kontaktformular auf der COMMOWN-Website;
über die Community-Support-Tools auf der COMMOWN-Website;
über ein anderes von COMMOWN eingerichtetes Werkzeug.

## ARTIKEL 21 – DATENSCHUTZ

Es wird insoweit auf die Datenschutzerklärung bzw. die Datenschutzbedingungen der COMMOWN verwiesen.

## ARTIKEL 22 – AUSSERGERICHTLICHE STREITBEILEGUNG

Wenn der Kunde ein Verbraucher ist, gilt:

Die Europäische Kommission bietet eine Online-Streitbeilegungs-Plattform an, die Sie unter http://ec.europa.eu/consumers/odr/ finden. COMMOWN ist bereit, an Streitbeilegungsverfahren vor einer Verbraucherschlichtungsstelle teilzunehmen.

## ARTIKEL 23 – Vorvertragliche Informationen – Annahme durch den Kunden

Der Kunde nimmt zur Kenntnis, dass ihm vor seiner Bestellung und dem Vertragsabschluss die vorliegenden AGB, die zum Mietprodukt gehörenden BB und alle im Rahmen der Verbraucherschutzgesetzgebung aufgeführten Informationen in lesbarer und verständlicher Form zur Verfügung gestellt wurden, insbesondere die folgenden Informationen:

die wesentlichen Merkmale der Dienste und der Produkte;
den Preis der Produkte und die damit verbundenen Kosten (z. B.: Lieferkosten, Vertragsstrafen, Ersatzkosten usw.);
die Dauer der Verpflichtung, falls vorhanden;
falls der Vertrag nicht sofort erfüllt wird, das Datum oder die voraussichtliche Uhrzeit, bis zu der COMMOWN sich verpflichtet, die bestellten Leistungen zu erbringen;
Informationen über die Identität von COMMOWN, ihre postalischen und elektronischen Kontaktdaten und ihre geschäftlichen Aktivitäten, falls nicht aus dem Zusammenhang ersichtlich;
Informationen zu gesetzlichen und vertraglichen Garantien und deren Umsetzungsmodalitäten;
die Funktionalitäten der digitalen Inhalte und ggf. deren Interoperabilität;
die Möglichkeit, im Streitfall eine konventionelle Mediation in Anspruch zu nehmen, sofern gegeben;
Informationen über das Widerrufsrecht (Existenz, Bedingungen, Frist, Verfahren zur Ausübung dieses Rechts und Standard-Widerrufsformular), Kündigungsverfahren und andere wichtige Vertragsbedingungen;
die akzeptierten Zahlungsmittel.

WIDERRUFSFORMULAR

Für Rückgabe und Widerruf können Sie unser Widerrufsformular nutzen.
An:
COMMOWN
8 A rue Schertz
Parc Phoenix - Bât. B2
F-67100 Straßburg
Frankreich
Ich widerrufe hiermit den von mir abgeschlossenen Vertrag über die Miete der folgenden Produkte:
Bestellnummer:
Bezeichnung:
Bestellt am/erhalten am:

Adresse:

E-Mail-Adresse:

Datum:
Unterschrift: