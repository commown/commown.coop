---
title: Shiftphone
description: Das ökologische und modulare Smartphone aus Deutschland
date: 2022-02-08T14:32:56.147Z
translationKey: shiftphone
type: product
menu:
  main:
    parent: Unsere Angebote
    name: Shiftphone
    weight: 2
  footer:
    name: Shiftphone
    weight: 3
header:
  title: Shiftphone
  subtitle: Einen Schritt näher zu einem verantwortungsvollen Smartphone
  ctas:
    - outline: false
      text: Ein SHIFT6mq bestellen
      href: https://shop.commown.coop/de_DE/shop/product/shift-6mq-mit-serviceleistungen-essenziell-mit-zweifach-schutz-schutzhulle-und-displayschutz-397
    - outline: false
      text: mein Smartphone funktioniert noch
      href: https://shop.commown.coop/de_DE/shop/product/aktionsgutschein-186
    - outline: false
      text: Kontaktieren Sie uns
      href: "#contact"
  text: Ab 28,80 € / Monat inkl. MwSt. im ersten Jahr (anschließend degressiv
    gestaffelter Preis, der sich jährlich verringert)
  figure:
    image: /media/shop_shift6mq_2.jpg
    alt: Fairphone 3
sections:
  - color: text-bleu-fonce-100
    containers:
      - blocks:
          - title: Ein Purpose Unternehmen mit eigener Fertigung in China
            text: >-
              SHIFT fertigt seit 2018 in der SHIFT-Manufaktur in Hangzhou
              (China). Die Manufaktur wurde mit der Hilfe der NGO „TAOS‟ nach
              ökologischen und sozialen Kriterien konzipiert. So kann garantiert
              werden, dass für Mitarbeiter vor Ort dieselben Arbeitsbedingungen
              gelten wie für die Mitarbeiter im SHIFT-Stammsitz in Falkenberg:
              eine flexible 40-Stunden-Woche und eine faire Entlohnung.


              Denn Arbeitszeit ist Lebenszeit und was uns Menschen ausmacht, sind Beziehungen und Kommunikation.
          - title: Positiver Impakt auf die Umwelt
            text: >-
              SHFT ist seit April 2018 Mitglied der Gemeinwohlökonomie
              (GWÖ).  Die GWÖ definiert die Erhöhung des Gemeinwohls als
              oberstes Unternehmensziel, noch vor finanziellen Gewinnen. Die
              verwendeten Materialien werden soweit möglich aus
              verantwortungsvollen Quellen bezogen


              Für die Herstellung der Hauptplatine verwendet SHIFT  z.B. die umweltfreundliche und bleifreie  „ECO Solder Paste“ des japanischen Herstellers Senju Metal.  SHIFT hat zudem seine gesamte Lieferkette bis Tier 3 in seinem [Wirkungsbericht ](https://www.shiftphones.com/downloads/SHIFT-wirkungsbericht-2019-05-10.pdf)veröffentlicht.
          - title: Fortgeschrittene Modularität
            text: Die hohe Modularität macht das SHIFTPHONE einzigartig. Mit seinen
              verschiedenen, leicht auseinandernehmbaren Modulen ist das
              SHIFT6mq ein perfektes Beispiel für Ökodesign. Unser Support
              schickt Ihnen bei Bedarf ein Modul zu, das Sie selbst auswechseln
              können. Dank dieser Modularität kann die Lebensdauer der Geräte
              unserer Kooperative erheblich verlängert werden und trägt so auch
              zur Reduzierung unseres ökologischen Fußabdrucks bei.
      - ctas:
          - outline: false
            text: Technische Daten Shiftphone 6mq
            href: https://shop.commown.coop/de_DE/shop/product/shift-6mq-mit-serviceleistungen-essenziell-mit-zweifach-schutz-schutzhulle-und-displayschutz-397
          - outline: true
            text: mein Smartphone funktioniert noch
            href: https://shop.commown.coop/de_DE/shop/product/aktionsgutschein-186
      - figure:
          image: /media/2021_03_commown_vignet_youtube.jpg
          alt: Die Kooperative Commown in diesem Video besser kennen
          iframe:
            code: <iframe
              src="https://www.youtube.com/embed/QRi4srOjtSM?autoplay=1&color=white"
              title="YouTube video player" frameborder="0" allow="accelerometer;
              autoplay; clipboard-write; encrypted-media; gyroscope;
              picture-in-picture" allowfullscreen></iframe>
            lang: html
          href: https://www.youtube.com/watch?v=S0fbZerTUjY
        blocks: []
    id: features
  - id: service
    bg_color: bg-bleu-clair-75
    color: text-bleu-fonce-100
    containers:
      - title: Der Service von Commown ist…
        title_max_w: 20ch
        icon: mobile
        blocks:
          - title: Ein umweltfreundliches All-Inclusive-Angebot
            text: >-
              Im Angebot ist inbegriffen:


              * Übernahme sämtlicher Reparaturen 

              * Akkuwechsel bei Bedarf

              * Unbegrenzter technischer Support

              * Diebstahl- und Bruchversicherung 

              * Keine versteckten Kosten, und ein degressiver Tarif über 5 Jahre, der langfristig unschlagbar ist

              * Langfristig haltbar und viel nachhaltiger, als ein neues Smartphone zu kaufen


              Wenn Sie das Shiftphone 6mq nach dem Verpflichtungszeitraum (am Ende des ersten Jahres) behalten, werden Ihre monatlichen Zahlungen wie hier aufgelistet reduziert, während die Serviceleistungen bestehen bleiben!
            icon: ""
          - figure:
              image: /media/shiftphone-graphique-monatlicher-tarif-modified-louis.png
              alt: ""
      - blocks:
          - title: Schneller Ersatz
            title_max_w: 10ch
            icon: refresh
            text: >-
              <!--StartFragment-->


              Bleiben Sie nicht wochenlang ohne Smartphone. Bei einem Ausfall schickt Ihnen Commown innerhalb von 48 Stunden ein neues Gerät.


              <!--EndFragment-->
          - title: "Reparaturen Inklusive      "
            title_max_w: 10ch
            icon: support
            text: >-
              <!--StartFragment-->


              Ihr Smartphone ist während der gesamten Vertragslaufzeit abgesichert, auch nach Ablauf der Herstellergarantie.


              <!--EndFragment-->
          - title: "Schutz gegen Schäden      "
            title_max_w: 14ch
            icon: shield-check
            text: >-
              <!--StartFragment-->


              Ihr Smartphone ist kaputt gegangen? Sie sind abgesichert! Ihr Smartphone wird repariert oder ersetzt.


              <!--EndFragment-->
          - title: " Hardware-Updates     "
            title_max_w: 16ch
            icon: puzzle
            text: >-
              <!--StartFragment-->


              Ist gerade ein neues Modul auf dem Markt erschienen?  Auf Wunsch erhalten Sie dieses Hardware-Update kostenlos.


              <!--EndFragment-->
          - title: "Akkuwechsel     "
            title_max_w: 10ch
            text: >-
              <!--StartFragment-->


              Wir sind uns bewusst, dass der Akku eine der Schwachstellen von Smartphones im Allgemeinen ist. 


              Einmal im Jahr und bei Bedarf können Sie einen neuen Akku erhalten.


              <!--EndFragment-->
            icon: battery-low
          - title: "Diebstahlschutz     "
            title_max_w: 12ch
            text: >-
              <!--StartFragment-->


              Sie sind im Falle eines Diebstahls Ihres Smartphones  abgesichert! Es wird ersetzt.




              <!--EndFragment-->
            icon: shield-exclamation
          - title: "Faire Preise    "
            title_max_w: 10ch
            icon: scale
            text: >-
              <!--StartFragment-->


              Commown ist eine kollektive Interessengemeinschaft! Wir sind die Einzigen, die die Preise für diese Art von Service im Laufe der Zeit deutlich senken.


              <!--EndFragment-->
          - title: "Umfassende Prüfung     "
            title_max_w: 10ch
            icon: search
            text: >-
              <!--StartFragment-->


              Das Commown-Team prüft jedes Smartphone gründlich, bevor es verschickt wird.




              <!--EndFragment-->
          - title: "Solider Service     "
            title_max_w: 10ch
            icon: thumb-up
            text: >-
              <!--StartFragment-->


              Unser Kundenservice ergänzt den Service von Shiftphone, was uns erlaubt, noch besser für Sie da zu sein. (Selbstverständlich steht Ihnen unser deutschsprachiges Team mit Rat und Tat zur Seite.)


              <!--EndFragment-->
        ctas:
          - outline: false
            text: mehr erfahren
            href: https://commown.coop/de/wer-sind-wir/
  - id: faq
    containers:
      - title: Häufige Fragen
        title_max_w: 14ch
        icon: chat-alt
        blocks:
          - title: Wenn ich jetzt ein Shiftphone 6mq vorbestelle, wann bekomme ich es
              geliefert?
            details: true
            text: >
              Wenn Sie heute vorbestellen, sollten wir in der Lage sein, Ihnen
              das Shiftphone 6mq innerhalb von 5 Werktagen zu liefern, sofern
              alle Formalitäten erledigt sind.
          - title: Was passiert nach der Vertragslaufzeit?
            details: true
            text: >-
              <!--StartFragment-->


              Der Vertrag läuft nach der Bindungsfrist mit einem Preisnachlass weiter und Sie können ihn jederzeit kündigen. Dieser Tarifvorteil für treue Kunden ist auf dem Produktblatt des Angebots beschrieben.


              <!--EndFragment-->
          - title: Bieten sie alternative Betriebssysteme an?
            details: true
            text: >-
              Ja, denn wir sind von der Bedeutung freier Software im Kampf gegen
              geplante Obsoleszenz überzeugt, aber noch nicht für das SHIFT6mq.\

              \

              Für das Fairphone 3, 3+ und 4 bieten wir als alternatives Betriebssystem /e/OS an.
            ctas:
              - outline: true
                text: "Mehr erfahren "
                href: https://wiki.commown.coop/Syst%C3%A8me-d%27exploitation-Android-ou-libre
        ctas:
          - outline: false
            text: Unsere andere Produkte sehen
            href: https://shop.commown.coop/de_DE/shop/category/alle-produkte-23
    color: text-bleu-fonce-100
  - id: entreprise-collectivite-association
    bg_color: bg-bleu-clair-75
    color: text-bleu-fonce-100
    containers:
      - title: "Unternehmen, Behörde, Verein? "
        blocks:
          - title: Mietlösung mit Serviceleistungen
            text: >-
              Commown bietet für Unternehmen Mietlösungen mit Serviceleistungen
              für Smartphones (Fairphone, Shiftphone), PCs und Laptops, Tablets
              und Headsets für die Arbeit im Homeoffice an.


              Mit dem Angebot von Commown für Unternehmen stellen Sie gesellschaftliche Verantwortung und Umweltbewusstsein wirksam unter Beweis. Unterstrichen wird dies durch Marken wie Fairphone (B-Corp zertifiziert) und Shiftphone (Mitglied der Gemeinwohlökonomie (GWÖ)). \

              \

              Commown ist zudem eine gemeinnützige Genossenschaft, die mit dem Nachhaltigkeitslabel Solar Impulse Efficient Solution ausgezeichnet und von Experten der Kreislaufwirtschaft wie Cradle to Cradle NGO, Zero Waste e.V., Circular Berlin, etc. empfohlen wurde.
          - figure:
              image: /media/sif_label_logo_institutional_2020_rvb.png
              alt: AFNOR Certification AFNOR e-Engagé RSE
              blend_mode: multiply
              grayscale: true
              initial: true
        ctas:
          - outline: false
            text: Unsere Seite auf Solar Impulse
            href: https://solarimpulse.com/efficient-solutions/commown
          - outline: true
            text: Kontaktieren Sie uns
            href: "#contact"
  - id: consomaction
    color: text-bleu-fonce-100
    containers:
      - title: Kein Bedarf an Elektronikgeräten?
        blocks:
          - text: >-
              **Unterstützen Sie unsere Genossenschaft** noch heute durch den
              Kauf eines Aktionsgutscheins!


              **Sie können diesen Gutschein auch verschenken**, um Ihre Werte mit anderen zu teilen. 
        ctas:
          - outline: false
            text: Mehr erfahren
            href: https://shop.commown.coop/de_DE/shop/product/aktionsgutschein-186
  - id: wetell
    color: text-bleu-fonce-100
    bg_color: bg-bleu-clair-75
    containers:
      - blocks:
          - text: >-
              Ergänzen Sie Ihr Commown-Abonnement durch einen Mobilfunkvertrag,
              der Ihren Überzeugungen entspricht.
              [WEtell](https://www.wetell.de/commown/?pk_cpn=ex-uvp-commown&wt_custom=cmyyyy)
              ist ein 100% klimaneutraler Mobilfunkanbieter, für den
              Klimaschutz, Datenschutz, Fairness und Transparenz an erster
              Stelle steht. Über den Link gelangen Sie zu einem exklusiven
              Angebot für Commown- und WETell-Kunden.


              Commown und WEtell sind Teil von FairTEC, einer Gruppe von Akteuren, die sich für Kreislaufwirtschaft, Nachhaltigkeit, und ethisch vertretbare Standards in der Elektronik- und Mobilfunkbranche einsetzen. Durch den Abschluss von Verträgen bei Commown und WEtell tragen Sie zur nachhaltigen Gestaltung sämtlicher Bereiche der Mobiltelefonie bei.
          - figure:
              image: /media/screenshot-2022-04-25-at-10.16.56.png
              alt: Video WEtell
              iframe:
                code: <iframe
                  src="https://www.youtube.com/embed/WO9zs7wwaSc?autoplay=1&color=white"
                  title="YouTube video player" frameborder="0"
                  allow="accelerometer; autoplay; clipboard-write;
                  encrypted-media; gyroscope; picture-in-picture"
                  allowfullscreen></iframe>
        title: WEtell x Commown
        color: text-bleu-fonce-100
---
