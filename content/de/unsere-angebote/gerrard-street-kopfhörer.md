---
slug: gerrard-street-kopfhörer
title: Gerrard Street Headset
description: Ein verantwortungsvollerer Klang ist möglich!
date: 2021-03-19T12:25:36+01:00
translationKey: audio
type: product
menu:
  main:
    parent: Unsere Angebote
    name: Gerrard Street Headset
    weight: 3
  footer:
    name: Gerrard Street Headset
    weight: 5
header:
  title: Reparierbares Headset
  subtitle: Ein verantwortungsvollerer Klang ist möglich!
  ctas:
    - outline: false
      text: Ein Gerrard Street Headset bestellen
      href: https://shop.commown.coop/de_DE/shop/category/audio-12
    - outline: false
      text: Ihr aktuelles Gerät funktioniert noch?
      href: https://commown.coop/de/unsere-angebote/aktionsgutschein/
    - outline: false
      text: Kontaktieren Sie uns
      href: "#contact"
  text: Ab 7,50€ pro Monat
  figure:
    image: /media/gerrard-street-boss-top.jpeg
    alt: Gerrard Street Headset
sections:
  - color: text-bleu-fonce-100
    containers:
      - blocks:
          - title: Reparierbar
            text: >-
              Gerrard Street hat einen vollständig modularen Kopfhörer
              entworfen, bei dem jedes Teil ohne Werkzeug von Hand abmontiert
              werden kann! Das macht die Reparatur ganz einfach. Werfen Sie
              Ihren Kopfhörer nicht weg, wenn er kaputt geht: Ersetzen Sie das
              defekte Modul einfach durch ein neues. 


              Wir nehmen das alte Gerät zurück und reparieren es, damit es weiterverwendet werden kann oder sorgen dafür, dass es ordnungsgemäß recycelt wird.
          - title: Hohe Klangqualität
            text: >-
              Genießen Sie als Sound- und Musikliebhaber einen kristallklaren
              Klang und einen kräftigen Bass. Exquisites Sounddesign, perfekt
              für akustische und elektronische Musik. Der erste modulare
              Kopfhörer auf dem Markt - mit Bluetooth oder kabelgebunden.


              In einem unabhängigen Test des französischen IT-Magazins “01net” wurde er mit der Note 8/10 bewertet (Nr. 933, S.61)
          - title: Ideal für die Arbeit im Homeoffice
            text: Das von Commown ausgesuchte Mikrofon BoomPro von V-Moda erzeugt dank der
              Reduzierung von Hintergrundgeräuschen  einen kristallklaren Klang
              - egal, wo Sie sich befinden. Seine Qualität hat es sogar zu einem
              Gaming-Accessoire gemacht! Sein flexibles Mikrofon und sein
              Controller mit Mute-Taste und Lautstärkeregler machen es zum
              idealen Begleiter bei der Arbeit im Homeoffice.
      - ctas:
          - outline: false
            text: Ein Headset BOSS bestellen
            href: https://shop.commown.coop/de_DE/shop/category/alle-produkte-23
          - outline: false
            text: Ein Headset DAY bestellen
            href: https://shop.commown.coop/de_DE/shop/category/alle-produkte-23
          - outline: true
            text: Mein aktuelles Headset funktioniert noch
            href: "#Aktionsgutschein"
    id: features
  - id: service
    bg_color: bg-bleu-clair-75
    color: text-bleu-fonce-100
    containers:
      - title: Der Service von Commown ist…
        title_max_w: 20ch
        icon: audio
        blocks:
          - title: Das Angebot von Commown
            text: >-
              **Ab 7,50 € pro Monat** profitieren Sie von der Nutzung eines
              Gerrard Street Kopfhörers mit Schutz gegen etwaige Vorfälle **wie
              Störungen oder Schäden. Das Ersatzteil kommt schnell und ohne
              Mehrkosten!** Sie können sich darauf verlassen, dass Commown für
              Sie da ist! \

              \

              Sie zahlen nur für die Nutzung. Sie brauchen den Kopfhörer nicht mehr? Kein Problem, Sie können Ihr Abonnement jederzeit einfach und flexibel kündigen. **Commown garantiert die Wiederaufbereitung und Reaktivierung des Kopfhörers, um Elektronikschrott zu vermeiden.**
          - figure:
              image: /media/illu_gerrard_street_gs_parts.jpeg
              alt: ""
      - blocks:
          - title: Schneller Austausch
            title_max_w: 10ch
            icon: refresh
            text: Verzichten Sie nicht tagelang auf Ihre Musik. Bei einem Ausfall sendet
              Ihnen Commown innerhalb von 48 Stunden das Ersatzteil oder einen
              anderen Kopfhörer zu.
          - title: Reparaturen Inklusive
            title_max_w: 10ch
            icon: support
            text: Ihr Kopfhörer ist während der gesamten Vertragslaufzeit abgesichert, auch
              nach Ablauf der Herstellergarantie.
          - title: Schutz gegen Schäden
            title_max_w: 14ch
            icon: shield-check
            text: Ihr Kopfhörer ist Ihnen kaputt gegangen? Sie sind abgesichert! Er wird
              repariert oder ersetzt.
          - title: Eine langfristige Vision
            title_max_w: 14ch
            text: Zur Unterstützung der nachhaltigen Vision des Herstellers und seiner
              Bemühungen, die Lebensdauer der Kopfhörer weiter zu verlängern,
              führt Commown einen Teil seiner Mieteinnahmen an Gerrard Street
              ab.
            icon: chart-pie
          - title: Freiheit
            title_max_w: 14ch
            icon: lock-open
            text: Sie sind ungebunden. Sie können jederzeit auf ein höherwertiges Modell
              umsteigen oder Ihr Abonnement kündigen. Das geht schnell und
              einfach über Ihren Kundenbereich.
          - title: Treue lohnt sich
            title_max_w: 10ch
            icon: thumb-up
            text: Wenn Sie nach Ihrer Anmeldung bei uns ein Jahresabonnement abschließen und
              im Voraus bezahlen, erhalten Sie 2 Monate gratis!
        ctas:
          - outline: false
            text: Ein Gerrard Street Headset bestellen
            href: https://shop.commown.coop/de_DE/shop/category/alle-produkte-23
  - id: faq
    containers:
      - title: Häufige Fragen
        title_max_w: 14ch
        icon: chat-alt
        blocks:
          - title: Ist mein Abonnement flexibel?
            details: true
            text: Das Angebot Gerrard Street von Commown  ist "flexibel". Ein
              Monatsabonnement kann monatlich, ein Jahresabonnement jährlich
              gekündigt werden. So einfach ist das!
          - title: Wird das Headset jemals mir gehören?
            details: true
            text: Bei Commown zahlen Sie nur für die Nutzung, der Kauf des Kopfhörers ist
              nicht möglich. Wenn das der Fall wäre, wären die Abonnementkosten
              viel höher. Das ist das Schöne an unserem Konzept! Sie können es
              mit Spotify vergleichen, das Ihnen erlaubt, zu hören, was Sie
              wollen, ohne die Musik zu besitzen. Wir tun dies, damit wir das
              Eigentum an unseren Produkten behalten und somit für deren
              Wiederverwendung und Recycling verantwortlich bleiben.
          - title: Wann bekomme ich mein Headset?
            details: true
            text: Die durchschnittliche Lieferzeit beträgt 3-4 Werktage, nachdem Sie Ihre
              Identität online bestätigt haben.
            ctas: []
          - title: Ist es möglich, das Modell zu wechseln?
            details: true
            text: Sie können Ihr Abonnement über Ihren Kundenbereich ändern. Nach erfolgtem
              Wechsel senden wir Ihnen das neue Modell sowie ein
              Rücksendeformular für Ihren alten Kopfhörer zu. Ein etwaiges
              Guthaben aus Ihrem alten Vertrag wird natürlich übertragen.
        ctas:
          - outline: false
            text: Ein Gerrad Street Headset bestellen
            href: https://shop.commown.coop/de_DE/shop/category/alle-produkte-23
    color: text-bleu-fonce-100
  - id: entreprise-collectivite-association
    bg_color: bg-bleu-clair-75
    color: text-bleu-fonce-100
    containers:
      - title: Unternehmen, Behörde, Verband?
        blocks:
          - title: Mietlösung mit Service
            text: >-
              Commown bietet für Unternehmen Mietlösungen mit Serviceleistungen
              für Smartphones (Fairphone, Shiftphone), PCs und Laptops, Tablets
              und Headsets für die Arbeit im Homeoffice an.


              Mit dem Angebot von Commown für Unternehmen stellen Sie gesellschaftliche Verantwortung und Umweltbewusstsein wirksam unter Beweis. Unterstrichen wird dies durch Marken wie Fairphone (B-Corp zertifiziert) und Shiftphone (Mitglied der Gemeinwohlökonomie (GWÖ)). \

              \

              Commown ist zudem eine gemeinnützige Genossenschaft, die mit dem Nachhaltigkeitslabel Solar Impulse Efficient Solution ausgezeichnet und von Experten der Kreislaufwirtschaft wie Cradle to Cradle NGO, Zero Waste e.V., Circular Berlin, etc. empfohlen wurde.
          - figure:
              image: /media/sif_label_logo_institutional_2020_rvb.png
              alt: Commown wurde mit dem Nachhaltigkeitslabel Solar Impulse Efficient Solution
                ausgezeichnet
              blend_mode: multiply
              grayscale: true
              initial: true
        ctas:
          - outline: false
            text: Ein Gerrard Street Headset bestellen
            href: https://shop.commown.coop/de_DE/shop/category/alle-produkte-23
          - outline: true
            text: Kontaktieren Sie uns
            href: "#contact"
  - id: consomaction
    color: text-bleu-fonce-100
    containers:
      - title: Kein Bedarf an Elektronikgeräten?
        blocks:
          - text: >-
              Unterstützen Sie unsere Genossenschaft noch heute durch den Kauf
              eines Aktionsgutscheins.


              Sie können diesen Gutschein auch verschenken, um Ihre Werte mit anderen zu teilen.
          - figure:
              image: ""
              alt: ""
              blend_mode: multiply
              initial: true
        ctas:
          - outline: false
            text: Mehr erfahren
            href: https://shop.commown.coop/de_DE/shop/product/aktionsgutschein-186
---
