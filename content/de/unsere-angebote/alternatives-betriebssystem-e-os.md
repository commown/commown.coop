---
title: Alternatives Betriebssystem /e/ OS
description: Kooperative Commown - Alternatives Betriebssystem /e/ OS
date: 2022-03-21T12:19:03.949Z
translationKey: e-OS
type: product
menu:
  main:
    parent: Unsere Angebote
    name: Alternatives Betriebssystem /e/ OS
    weight: 5
  footer:
    name: Alternatives Betriebssystem /e/ OS
    weight: 5
header:
  title: Alternatives Betriebssystem /e/ OS
  subtitle: Datenschutz & Kampf gegen veraltete Software
  ctas:
    - outline: false
      text: /e/OS auf dem Fairphone 3, 3+ oder 4 installieren
      href: https://shop.commown.coop/de_DE/shop/product/e-os-auf-dem-fairphone-3-3-oder-4-installieren-239
  text: Ab 25 €
  figure:
    image: /media/e-privacy-focused-google-free-android-operating-system.jpg
    alt: /e/OS
sections:
  - color: text-bleu-fonce-100
    containers:
      - blocks:
          - title: Privacy by Design
            text: Wir verwenden Ihren Standort nur, wenn Sie dies wünschen. Wir scannen Ihre
              Dokumente oder Bilder nicht, um mehr über Sie zu erfahren. Ihre
              Daten sind IHRE Daten!
          - title: Angetrieben von Open Source
            text: >-
              Wir glauben, dass jeder dazu beitragen kann, eine bessere
              Technologie zu entwickeln, die die persönlichen Daten des
              Benutzers respektiert.\

              Wir halten unseren Kodex öffentlich.
          - title: Transparent teilen
            text: Wir glauben, dass Datenschutz ein Recht ist. Jeder sollte über bewährte
              Verfahren für den Umgang mit personenbezogenen Daten informiert
              und über neue Bedrohungen auf dem Laufenden gehalten werden.
      - title: Warum /e/OS?
        blocks:
          - text: >-
              In den letzten zehn Jahren hat die Entwicklung der Mobiltelefone
              sie zu unseren täglichen Begleitern gemacht, die jederzeit in
              Reichweite sind. Aber sie haben sich auch still und leise in
              Überwachungsgeräte verwandelt, indem sie unsere persönlichen Daten
              mit dem Meistbietenden oder mit Regierungsbehörden teilen, um
              unser Handeln vorherzusagen und zu beeinflussen.


              [Eine Forschung](https://digitalcontentnext.org/blog/2018/08/21/google-data-collection-research/) von Digital Content Next im August 2018 veröffentlichte Studie enthüllte erschütternde Fakten über die passive Datenerhebung...


              Unsere Mission: Technologie, die die Privatsphäre der Nutzer respektiert, für jedermann zugänglich zu machen.


              Es ist an der Zeit, die Kontrolle über unsere persönlichen Daten auf unseren Telefonen wiederzuerlangen!


              **Wir entwickeln mobile Betriebssysteme, Apps und Code, die die Privatsphäre der Nutzer vor dem Gewinnen setzen.**


              Jeder verdient es, seine Privatsphäre intakt zu halten, vor allem auf seinem Smartphone! /e/ ist ein non-profit-Projekt, das von einem internationalen Kernteam aus erfahrenen Unternehmern, Entwicklern und Designern und einer lebendigen wachsenden Community von Mitwirkenden unterstützt wird.
    id: features
    bg_color: bg-bleu-clair-75
  - id: service
    bg_color: bg-white
    color: text-bleu-fonce-100
    containers:
      - title: Der Service von Commown ist…
        title_max_w: 20ch
        icon: mobile
        blocks:
          - title: Das Angebot von Commown
            text: >-
              Für die Installation von /e/OS haben Sie folgende Optionen :


              * ***Commown installiert /e/ auf meine aktuelle Bestellung vor***: Sie erhalten Ihr Fairphone mit /e/ als vorinstalliertem Betribsystem.

              * ***Ich miete bereits ein FP und möchte, dass Commown /e/ installiert***: In diesem Fall schicken wir Ihnen ein Fairphone 3 mit vorinstalliertem /e/. Sie müssen nur einen Datentransfer zwischen den beiden Fairphones vornehmen.
          - figure:
              image: /media/e_foundation_logo.png
              alt: ""
        color: null
      - blocks: []
        ctas:
          - outline: false
            text: mehr erfahren
            href: https://commown.coop/de/wer-sind-wir/
  - id: entreprise-collectivite-association
    bg_color: bg-bleu-clair-75
    color: text-bleu-fonce-100
    containers:
      - title: "Unternehmen, Behörde, Verein? "
        blocks:
          - title: Mietlösung mit Serviceleistungen
            text: >-
              Commown bietet für Unternehmen Mietlösungen mit Serviceleistungen
              für Smartphones (Fairphone, Shiftphone), PCs und Laptops, Tablets
              und Headsets für die Arbeit im Homeoffice an.


              Mit dem Angebot von Commown für Unternehmen stellen Sie gesellschaftliche Verantwortung und Umweltbewusstsein wirksam unter Beweis. Unterstrichen wird dies durch Marken wie Fairphone (B-Corp zertifiziert) und Shiftphone (Mitglied der Gemeinwohlökonomie (GWÖ)). \

              \

              Commown ist zudem eine gemeinnützige Genossenschaft, die mit dem Nachhaltigkeitslabel Solar Impulse Efficient Solution ausgezeichnet und von Experten der Kreislaufwirtschaft wie Cradle to Cradle NGO, Zero Waste e.V., Circular Berlin, etc. empfohlen wurde.
          - figure:
              image: /media/sif_label_logo_institutional_2020_rvb.png
              alt: AFNOR Certification AFNOR e-Engagé RSE
              blend_mode: multiply
              grayscale: true
              initial: true
        ctas:
          - outline: false
            text: Unsere Seite auf Solar Impulse
            href: https://solarimpulse.com/efficient-solutions/commown
          - outline: true
            text: Kontaktieren Sie uns
            href: "#contact"
  - id: consomaction
    color: text-bleu-fonce-100
    containers:
      - title: Kein Bedarf an Elektronikgeräten?
        blocks:
          - text: >-
              **Unterstützen Sie unsere Genossenschaft** noch heute durch den
              Kauf eines Aktionsgutscheins!


              **Sie können diesen Gutschein auch verschenken**, um Ihre Werte mit anderen zu teilen. 
        ctas:
          - outline: false
            text: Mehr erfahren
            href: https://shop.commown.coop/de_DE/shop/product/aktionsgutschein-186
---
