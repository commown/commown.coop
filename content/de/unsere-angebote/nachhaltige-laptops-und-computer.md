---
slug: nachhaltige-laptops-und-computer
title: Auf dem Weg zu nachhaltigen, umweltfreundlichen und reparierbaren PCs · Commown
description: Wir sollten anders konsumieren, indem wir uns für einen
  nachhaltigen und umweltfreundlichen PC (Computer) entscheiden. Entdecken Sie
  die von Commown angebotene Auswahl an Computern
date: 2021-03-19T12:25:36+01:00
translationKey: computers
type: product
menu:
  main:
    parent: Unsere Angebote
    name: Nachhaltige Laptops und PCs
    weight: 3
  footer:
    name: Nachhaltige Laptops & PCs
    weight: 6
header:
  title: Nachhaltige Laptops und Computer
  subtitle: UNTERSTÜTZEN UND FÖRDERN SIE VERANTWORTUNGSVOLLE HERSTELLER
  ctas:
    - outline: false
      text: In kürze verfügbar
      href: ""
  text: Ab 25,30 € / Monat
  figure:
    image: /media/why.png
    alt: PC durables
sections:
  - color: text-bleu-fonce-100
    containers:
      - blocks:
          - title: Niedriger Energieverbrauch
            text: Die PCs von PCVert werden in Toulouse zusammengestellt und sind so
              konzipiert und optimiert, dass sie einen sehr geringen
              Stromverbrauch haben. Die Laptops befinden sich, wann immer
              möglich, im Energiesparmodus.
          - title: Konzipiert für eine lange Lebensdauer
            text: Diese PCs und Laptops wurden mit dem Ziel konzipiert, so lange wie möglich
              zu halten, indem die Qualität der Teile verbessert und eine
              einfache Reparatur ermöglicht wurde. Durch die gemeinsame Nutzung
              der Geräte besteht außerdem ein Vorrat an Einzelteilen, der den
              auf dem Markt verfügbaren Teilen mengenmäßig um einiges voraus
              ist.
          - title: Fair
            text: Indem Sie Ihre Computer über die Kooperative Commown kaufen oder mieten,
              unterstützen Sie direkt den Wandel zu einer verantwortungsvolleren
              Elektronik. Dank unserer Einkaufsvolumen können wir mit den
              Herstellern verhandeln, um die Praktiken auf der gesamten
              Wertschöpfungskette weiter zu verbessern.
      - ctas:
          - outline: false
            text: Einen Laptop mieten
            href: ""
          - outline: false
            text: " Einen PC mieten"
            href: ""
          - outline: true
            text: Mein Computer funktioniert noch!
            href: "#consomaction"
    id: features
  - id: service
    bg_color: bg-bleu-clair-75
    color: text-bleu-fonce-100
    containers:
      - title: Der Service von Commown umfasst…
        title_max_w: 20ch
        icon: desktop
        blocks:
          - title: Das Angebot von Commown
            text: "**Ab 25,30 € / Monat nutzen Sie einen leistungsstarken Computer mit
              vielfältigen Serviceleistungen, die für eine lange Lebensdauer des
              Gerätes sorgen** (Kostenübernahme bei Schäden und Ausfällen,
              Nutzersupport…)."
          - figure:
              image: /media/dither_it_dsc01544.png
              alt: ""
      - blocks:
          - title: Verlängerte Hardwaregarantie
            title_max_w: 18ch
            icon: refresh
            text: Commown bietet eine Garantie auf die Teile Ihres Computers, die über die
              Herstellergarantie hinausgeht.
          - title: Datenschutz
            title_max_w: 14ch
            icon: lock-closed
            text: Mit Linux als unser Standard-Betriebssystem und einer personalisierten
              Begleitung schützen wir Ihre Daten.
          - title: Maßgeschneiderte PCs oder Laptops
            title_max_w: 14ch
            icon: desktop-computer
            text: Sie können Ihren PC oder Laptop je nach Bedarf konfigurieren.
          - title: Online-Support
            title_max_w: 10ch
            text: Sie haben eine Frage zu Ihrem PC oder Laptop oder zu Linux? Kontaktieren
              Sie uns per E-Mail oder über den Chat in Ihrem Kundenbereich. Die
              Antwort erhalten Sie innerhalb weniger Werktage.
            icon: chat-alt-2
          - title: Personalisierte Begleitung
            title_max_w: 12ch
            icon: terminal
            text: Commown führt Sie mit einer personalisierten Schulung in die Nutzung von
              Linux ein.
          - title: Lokale Arbeitsplätze
            title_max_w: 12ch
            icon: location-marker
            text: Sie unterstützen auch die Schaffung lokaler Arbeitsplätze für die
              Zusammenstellung und Wartung der PCs.
        ctas:
          - outline: false
            text: Einen Computer mieten
            href: ""
  - id: faq
    containers:
      - title: Häufige Fragen
        title_max_w: 14ch
        icon: chat-alt
        blocks:
          - title: Wann erhalte ich das Produkt?
            details: true
            text: Wenn Sie heute eine Vorbestellung aufgeben, sollte der Hersteller den
              Computer innerhalb von 5 Werktagen versenden können,
              vorausgesetzt, dass alle Formalitäten abgeschlossen sind.
          - title: Wie geht es nach dem Verpflichtungszeitraum weiter?
            details: true
            text: Nach Ablauf Ihrer Vertragslaufzeit läuft der Vertrag mit einem reduzierten
              Preis weiter. Sie können diesen jedoch jederzeit beenden. Dieser
              Preisvorteil für treue Kunden wird in der Produktbeschreibung des
              jeweiligen Angebotes beschrieben.
            icon: ""
          - title: Bieten Sie alternative Betriebssysteme an?
            details: true
            text: Ja, denn unserer Meinung nach spielen freie Softwares eine große Rolle im
              Vorgehen gegen die geplante Obsoleszenz. Für Computer bieten wir
              seit der Gründung der Genossenschaft an, Linux (Ubuntu LTS) zu
              installieren und unterstützen Sie gerne bei der Inbetriebnahme.
              Sie können aber auch Windows verwenden, sollte Linux für Sie nicht
              in Frage kommen.
            ctas: []
        ctas:
          - outline: false
            text: Lesen Sie unsere FAQs
            href: https://commown.coop/de/faq/
    color: text-bleu-fonce-100
  - id: entreprise-collectivite-association
    bg_color: bg-bleu-clair-75
    color: text-bleu-fonce-100
    containers:
      - title: Sind Sie ein Unternehmen, eine Einrichtung oder ein Verein?
        blocks:
          - title: Mieten mit Serviceleistungen
            text: >-
              Commown bietet für Unternehmen Mietlösungen mit Serviceleistungen
              für Smartphones (Fairphone, Shiftphone), PCs und Laptops, Tablets
              und Headsets für die Arbeit im Homeoffice an.


              Mit dem Angebot von Commown für Unternehmen stellen Sie gesellschaftliche Verantwortung und Umweltbewusstsein wirksam unter Beweis. Unterstrichen wird dies durch Marken wie Fairphone (B-Corp zertifiziert) und Shiftphone (Mitglied der Gemeinwohlökonomie (GWÖ)). \

              \

              Commown ist zudem eine gemeinnützige Genossenschaft, die mit dem Nachhaltigkeitslabel Solar Impulse Efficient Solution ausgezeichnet und von Experten der Kreislaufwirtschaft wie Cradle to Cradle NGO, Zero Waste e.V., Circular Berlin, etc. empfohlen wurde.
          - figure:
              image: /media/sif_label_logo_institutional_2020_rvb.png
              alt: AFNOR Certification AFNOR e-Engagé RSE
              blend_mode: multiply
              grayscale: true
              initial: true
        ctas:
          - outline: false
            text: Angebote für die berufliche Nutzung
            href: https://commown.coop/de/f%C3%BCr-unternehmen/
          - outline: true
            text: Kontaktieren Sie uns
            href: "#contact"
  - id: consomaction
    color: text-bleu-fonce-100
    containers:
      - title: Sie benötigen derzeit keine Elektronikgeräte? Unterstützen Sie unsere
          Kooperative!
        blocks:
          - text: >-
              Kaufen Sie einen Aktionsgutschein und u**nterstützen Sie schon
              heute unsere Kooperative.** Sie sparen bis zu 40 € bei Ihren
              monatlichen Zahlungen, wenn Sie schließlich doch ein
              Elektronikgerät benötigen.


              **Sie können diesen Gutschein auch verschenken,** um andere an Ihren Werten teilhaben zu lassen.
          - figure:
              image: /media/commown.png
              alt: Entdecken Sie unseren “zeitversetzten” Aktionsgutschein!
              blend_mode: multiply
              initial: true
        ctas:
          - outline: false
            text: Mehr erfahren
            href: https://commown.coop/de/unsere-angebote/aktionsgutschein/
---
