---
title: Fairphone
description: Einen Schritt näher zu einem verantwortungsvollen Smartphone
date: 2021-03-19T12:25:36+01:00
translationKey: fairphone-3
type: product
menu:
  main:
    parent: Unsere Angebote
    name: Fairphone
    weight: 1
  footer:
    name: Fairphone
    weight: 4
header:
  title: Fairphone
  subtitle: Einen Schritt näher zu einem verantwortungsvollen Smartphone
  ctas:
    - outline: false
      text: Ein Fairphone 4 bestellen
      href: https://shop.commown.coop/de_DE/shop/product/fairphone-4-256-8gb-mit-serviceleistungen-essenziell-mit-zweifach-schutz-schutzhulle-und-displayschutz-357
    - outline: false
      text: Ein Fairphone 5 bestellen
      href: https://shop.commown.coop/de_DE/shop/product/fairphone-5-mit-serviceleistungen-essenziell-mit-zweifach-schutz-schutzhulle-und-displayschutz-693
    - outline: false
      text: mein Smartphone funktioniert noch
      href: https://shop.commown.coop/de_DE/shop/product/aktionsgutschein-186
    - outline: false
      text: Kontaktieren Sie uns
      href: "#contact"
  text: Ab 17,80 € / Monat inkl. MwSt. im ersten Jahr (anschließend degressiv
    gestaffelter Preis, der sich jährlich verringert)
  figure:
    image: /media/fp3-fp4.jpg
    alt: Fairphone 3
sections:
  - id: service
    bg_color: bg-bleu-clair-75
    color: text-bleu-fonce-100
    containers:
      - title: Commown mit diesem Video besser kennenlernen
        figure:
          iframe:
            code: <iframe
              src="https://www.youtube.com/embed/QRi4srOjtSM?autoplay=1&color=white"
              title="YouTube video player" frameborder="0" allow="accelerometer;
              autoplay; clipboard-write; encrypted-media; gyroscope;
              picture-in-picture" allowfullscreen></iframe>
          alt: Die Kooperative Commown in diesem Video besser kennen
          href: https://www.youtube.com/watch?v=S0fbZerTUjY
          image: /media/2021_03_commown_vignet_youtube.jpg
        ctas:
          - outline: false
            text: Mehr über unsere Kooperative
            href: /de/wer-sind-wir/
      - title: Der Service von Commown ist…
        title_max_w: 20ch
        icon: mobile
        blocks:
          - title: Ein umweltfreundliches All-Inclusive-Angebot
            text: >-
              Im Angebot inbegriffen:


              * Übernahme sämtlicher Reparaturen 

              * Akkuwechsel bei Bedarf

              * Unbegrenzter technischer Support

              * Diebstahl- und Bruchversicherung 

              * Keine versteckten Kosten sowie ein degressiver Tarif über 5 Jahre, der langfristig unschlagbar ist

              * Langfristig haltbar und nachhaltiger, als ein neues Smartphone zu kaufen


              Wenn Sie das Fairphone nach dem Verpflichtungszeitraum (mit Ablauf des ersten Jahres) behalten, werden Ihre monatlichen Zahlungen wie hier aufgelistet reduziert, während die Serviceleistungen bestehen bleiben!
          - figure:
              image: /media/fairphone-et-shiftphone-graphique-monatlichter-tarif.png
              alt: ""
      - blocks:
          - title: Schneller Ersatz
            title_max_w: 10ch
            icon: refresh
            text: >-
              <!--StartFragment-->


              Verzichten Sie nicht wochenlang auf Ihr Smartphone. Bei einem Ausfall schickt Ihnen Commown innerhalb von 48 Stunden ein neues Gerät.


              <!--EndFragment-->
          - title: "Reparaturen Inklusive      "
            title_max_w: 10ch
            icon: support
            text: >-
              <!--StartFragment-->


              Ihr Smartphone ist während der gesamten Vertragslaufzeit abgesichert, auch nach Ablauf der regulären Herstellergarantie.


              <!--EndFragment-->
          - title: "Schutz gegen Schäden      "
            title_max_w: 14ch
            icon: shield-check
            text: >-
              <!--StartFragment-->


              Ihr Fairphone ist kaputt gegangen? Sie sind abgesichert! Ihr Smartphone wird repariert oder ersetzt.


              <!--EndFragment-->
          - title: " Hardware-Updates     "
            title_max_w: 16ch
            icon: puzzle
            text: >-
              <!--StartFragment-->


              Ist gerade ein neues Modul auf dem Markt erschienen? Auf Wunsch erhalten Sie dieses Hardware-Update kostenlos.


              <!--EndFragment-->
          - title: "Akkuwechsel     "
            title_max_w: 10ch
            text: >-
              <!--StartFragment-->


              Wir sind uns bewusst, dass der Akku eine der größten Schwachstellen von Smartphones im Allgemeinen ist. 


              Einmal im Jahr und bei Bedarf können Sie einen neuen Akku erhalten.


              <!--EndFragment-->
            icon: battery-low
          - title: "Diebstahlschutz     "
            title_max_w: 12ch
            text: >-
              <!--StartFragment-->


              Sie sind im Falle eines Diebstahls Ihres Fairphone abgesichert! Es wird ersetzt.


              <!--EndFragment-->
            icon: shield-exclamation
          - title: "Faire Preise    "
            title_max_w: 10ch
            icon: scale
            text: >-
              <!--StartFragment-->


              Commown ist eine kollektive Interessengemeinschaft! Wir sind die Einzigen, die die Preise für diese Art von Service im Laufe der Zeit deutlich senken.


              <!--EndFragment-->
          - title: "Umfassende Prüfung     "
            title_max_w: 10ch
            icon: search
            text: >-
              <!--StartFragment-->


              Das Commown-Team prüft jedes Fairphone gründlich, bevor es verschickt wird.




              <!--EndFragment-->
          - title: "Solider Service     "
            title_max_w: 10ch
            icon: thumb-up
            text: >-
              <!--StartFragment-->


              Unser Kundenservice ergänzt den Service von Fairphone, was uns erlaubt, noch besser für Sie da zu sein. (Selbstverständlich steht Ihnen unser deutschsprachiges Team mit Rat und Tat zur Seite.)


              <!--EndFragment-->
        ctas:
          - outline: false
            text: mehr erfahren
            href: https://commown.coop/de/wer-sind-wir/
  - color: text-bleu-fonce-100
    containers:
      - blocks:
          - title: Recycelte Materialien
            text: "Fairphone erhöht kontinuierlich den Recyclinganteil seiner Geräte:
              Recyceltes Kupfer und recycelter Kunststoff im Fairphone 3;
              Recycelter Kunststoff und recyceltes Indium, Kupfer und Aluminium
              im Fairphone 4. Das Fairphone 5 enthält bereits über 8 recycelte
              Materialien und besticht mit einem Reparierindex von 9,3/10! Die
              Modularität des Smartphones verbessert auch seine
              Wiederverwertbarkeit am Ende seines Lebens, da einige Module
              hauptsächlich Metalle, wie z.B. Gold enthalten."
          - title: Ethik
            text: Fairphone ist ein soziales Unternehmen, bei dem der Mensch im Mittelpunkt
              steht. Die Mitarbeiter in den Fabriken haben das Recht auf sichere
              Arbeitsbedingungen und faire Löhne und Fairphone überprüft dort
              regelmäßig die Einhaltung seiner Standards. Zudem leistet
              Fairphone vorbildliche Arbeit im Hinblick auf die
              Rückverfolgbarkeit seiner Materialien. Nach dem verarbeiteten Gold
              trägt nun auch das im Fairphone 4 verarbeitete Silber das
              Fairtrade-Siegel.
          - title: Fortgeschrittene Modularität
            text: Seine extreme Modularität macht das Fairphone einzigartig. Mit ihren
              verschiedenen, leicht auseinandernehmbaren Modulen sind die
              Fairphones perfekte Beispiele für Ökodesign. Das Fairphone 3+
              enthält beispielsweise 7 austauschbare Module, das neue Fairphone
              5 trägt sogar 11 austauschbare Module in sich! Unser Support
              schickt Ihnen bei Bedarf ein Modul zu, das Sie selbst auswechseln
              können. Dank dieser Modularität kann die Lebensdauer der Geräte
              unserer Kooperative erheblich verlängert werden und trägt so auch
              zur Reduzierung unseres CO2-Fußabdrucks bei.
      - ctas:
          - outline: false
            text: Technische Daten Fairphone 4
            href: https://shop.commown.coop/de_DE/shop/product/fairphone-4-mit-serviceleistungen-essenziell-mit-zweifach-schutz-schutzhulle-und-displayschutz-357#carac-techniques
          - outline: false
            text: Technische Daten Fairphone 5
            href: https://shop.commown.coop/de_DE/shop/product/fairphone-5-mit-serviceleistungen-essenziell-mit-zweifach-schutz-schutzhulle-und-displayschutz-693#carac-techniques
          - outline: true
            text: Mein Smartphone funktioniert noch
            href: https://shop.commown.coop/de_DE/shop/product/aktionsgutschein-186
    id: features
  - id: wetell
    color: text-bleu-fonce-100
    bg_color: bg-bleu-clair-75
    containers:
      - title: WEtell x Commown
        color: text-bleu-fonce-100
        blocks:
          - text: >-
              Ergänzen Sie Ihr Commown-Abonnement durch einen Mobilfunkvertrag,
              der Ihren Überzeugungen entspricht.
              [WEtell](https://www.wetell.de/commown/?pk_cpn=ex-uvp-commown&wt_custom=cmyyyy)
              ist ein 100% klimaneutraler Mobilfunkanbieter, für den
              Klimaschutz, Datenschutz, Fairness und Transparenz an erster
              Stelle steht. Über den Link gelangen Sie zu einem exklusiven
              Angebot für Commown- und WETell-Kunden.


              Commown und WEtell sind Teil von [FairTEC](https://fairtec.io/de/), einer Gruppe von Akteuren, die sich für Kreislaufwirtschaft, Nachhaltigkeit, und ethisch vertretbare Standards in der Elektronik- und Mobilfunkbranche einsetzen. Durch den Abschluss von Verträgen bei Commown und WEtell tragen Sie zur nachhaltigen Gestaltung sämtlicher Bereiche der Mobiltelefonie bei.
          - figure:
              image: /media/screenshot-wetell.jpg
              iframe:
                code: <iframe
                  src="https://www.youtube.com/embed/WO9zs7wwaSc?autoplay=1&color=white"
                  title="YouTube video player" frameborder="0"
                  allow="accelerometer; autoplay; clipboard-write;
                  encrypted-media; gyroscope; picture-in-picture"
                  allowfullscreen></iframe>
              alt: Video WeTell
  - id: faq
    containers:
      - title: Häufige Fragen
        title_max_w: 14ch
        icon: chat-alt
        blocks:
          - title: Wenn ich jetzt ein Fairphone vorbestelle, wann bekomme ich es geliefert?
            details: true
            text: >-
              Fairphone 3: Das Fairphone 3 ist aktuell nicht mehr vorrätig,
              weshalb die Lieferfristen mehrere Wochen betragen können.\

              \

              Fairphone 3+: Wenn Sie heute vorbestellen, sollten wir in der Lage sein, Ihnen das Fairphone 3+ innerhalb von 5 Werktagen zu liefern, sofern alle Formalitäten erledigt sind.\

              \

              Fairphone 4: Wenn Sie heute vorbestellen, sollten wir in der Lage sein, Ihnen das Fairphone 4 innerhalb von 5 Werktagen zu liefern, sofern alle Formalitäten erledigt sind.
          - title: Was passiert nach der Vertragslaufzeit?
            details: true
            text: >-
              <!--StartFragment-->


              Der Vertrag läuft nach der Bindungsfrist mit einem Preisnachlass weiter und Sie können ihn jederzeit kündigen. Dieser Tarifvorteil für treue Kunden ist auf dem Produktblatt des Angebots beschrieben.


              <!--EndFragment-->
          - title: Bieten sie alternative Betriebssysteme an?
            details: true
            text: >-
              Ja, denn wir sind von der Bedeutung freier Software im Kampf gegen
              geplante Obsoleszenz überzeugt. \

              \

              Für die  Fairphone 3, 3+ und 4 bieten wir als alternatives Betriebssystem [/e/OS](https://shop.commown.coop/de_DE/shop/product/e-os-auf-dem-fairphone-3-3-oder-4-installieren-239) an.
            ctas:
              - outline: true
                text: "Mehr erfahren "
                href: https://wiki.commown.coop/Syst%C3%A8me-d%27exploitation-Android-ou-libre
          - title: Handeln Sie nicht gegen Ihre Werte, wenn Sie ein 5G-Smartphone in Umlauf
              bringen?
            details: true
            text: >-
              Das ist tatsächlich ein Widerspruch. Einerseits halten wir 5G für
              eine vollkommen überflüssige Technologie, die nur dazu dient, das
              "wirtschaftliche Wachstum der Branche" weiter voranzutreiben. Sie
              wird gemäß der Erklärung des Hohen Klimarates früher oder später
              (zusammen mit dem vorzeitigen Neukauf von Smartphones) zu einem
              übermäßigen Ressourcenverbrauch und zur Vergrößerung unseres
              CO2-Abdrucks führen. Auch wenn der Kampf um die Frequenzbänder
              3,4‑3,8 GHz verloren ist, soll 5G nach seinen Befürwortern erst ab
              dem 26 GHz-Bereich "rentabel" sein… Wir werden uns also weiter
              dagegen aussprechen und hoffen, diese irrsinnige Flucht nach vorn
              stoppen zu können (man redet schon von 6G...). Fairphone konnte
              diese Technologie jedoch leider nicht übergehen, da es mit den
              anderen Smartphones auf dem Markt wettbewerbsfähig bleiben wollte.
              Daher bieten wir dieses Gerät, das nach ethischen und ökologischen
              Kriterien große Verbesserungen aufweist, an. \

              \

              Aber: \

              1 - Wenn Ihr aktuelles Smartphone noch funktioniert, benutzen Sie es so lange wie möglich weiter, bevor Sie sich ein neues besorgen. Sie können auch jetzt schon für ein neues Gerät vorsorgen und gleichzeitig sparen und unsere Kooperative unterstützen, indem Sie einen Aktionsgutschein über unsere Website kaufen. \

              \

              2 - Wenn Sie 5G sehr stört, entscheiden Sie sich für das Fairphone 3 oder das Fairphone 3+, die weiterhin voll einsatzfähig sind.
        ctas:
          - outline: false
            text: Unseren Shop besuchen
            href: https://shop.commown.coop/de_DE/shop/category/alle-produkte-23
    color: text-bleu-fonce-100
  - id: entreprise-collectivite-association
    bg_color: bg-bleu-clair-75
    color: text-bleu-fonce-100
    containers:
      - title: "Unternehmen, Behörde, Verein? "
        blocks:
          - title: Mietlösung mit Serviceleistungen
            text: >-
              Commown bietet für Unternehmen Mietlösungen mit Serviceleistungen
              für Smartphones (Fairphone, Shiftphone), PCs und Laptops, Tablets
              und Headsets für die Arbeit im Homeoffice an.


              Mit dem Angebot von Commown für Unternehmen stellen Sie gesellschaftliche Verantwortung und Umweltbewusstsein wirksam unter Beweis. Unterstrichen wird dies durch Marken wie Fairphone (B-Corp zertifiziert) und Shiftphone (Mitglied der Gemeinwohlökonomie (GWÖ)). \

              \

              Commown ist zudem eine gemeinnützige Genossenschaft, die mit dem Nachhaltigkeitslabel Solar Impulse Efficient Solution ausgezeichnet und von Experten der Kreislaufwirtschaft wie Cradle to Cradle NGO, Zero Waste e.V., Circular Berlin, etc. empfohlen wurde.
          - figure:
              image: /media/sif_label_logo_institutional_2020_rvb.png
              alt: AFNOR Certification AFNOR e-Engagé RSE
              blend_mode: multiply
              grayscale: true
              initial: true
        ctas:
          - outline: false
            text: Unsere Seite auf Solar Impulse
            href: https://solarimpulse.com/efficient-solutions/commown
          - outline: true
            text: Kontaktieren Sie uns
            href: "#contact"
  - id: consomaction
    color: text-bleu-fonce-100
    containers:
      - title: Kein Bedarf an Elektronikgeräten?
        blocks:
          - text: >-
              **Unterstützen Sie unsere Genossenschaft** noch heute durch den
              Kauf eines Aktionsgutscheins!


              **Sie können diesen Gutschein auch verschenken**, um Ihre Werte mit anderen zu teilen. 
        ctas:
          - outline: false
            text: Mehr erfahren
            href: https://shop.commown.coop/de_DE/shop/product/aktionsgutschein-186
  - {}
---
