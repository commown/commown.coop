---
slug: aktionsgutschein
title: Aktionsgutschein
description: Das nachhaltigste Gerät ist das, das Sie bereits haben!
date: 2021-03-19T12:25:36+01:00
translationKey: consomaction
type: product
menu:
  main:
    parent: Unsere Angebote
    name: Aktionsgutschein
    weight: 100
  footer:
    name: ""
    weight: 2.2
header:
  title: Ihr aktuelles Gerät funktioniert noch?
  subtitle: Das nachhaltigste Gerät ist das, das Sie bereits haben!
  ctas:
    - outline: false
      text: Aktionsgutschein
      href: https://shop.commown.coop/de_DE/shop/product/aktionsgutschein-186?category=8
  text: ""
  figure:
    image: /media/commown.png
    alt: Entdecken Sie unseren “zeitversetzten” Aktionsgutschein!
sections:
  - color: text-bleu-fonce-100
    containers:
      - blocks:
          - title: Finanzieller Vorteil / Geschenkidee
            text: Wenn Sie schon heute sicher sind, dass Sie ein Abo bei uns abschließen
              möchten, sobald ihr jetziges Gerät definitiv nicht mehr
              funktioniert, sparen Sie mit dem Kauf eines Gutscheins bei einer
              zukünftigen Bestellung bis zu 40 €. Hinzu kommt, dass dieser
              Gutschein kein Ablaufdatum hat und übertragbar ist, weshalb Sie
              ihn auch verschenken können.
          - title: Kampf gegen Marketing-Obsoleszenz
            text: "Geplante Obsoleszenz hat viele Gesichter. Oftmals versteckt sie sich
              hinter Marketing-Kampagnen, die zum Kauf neuer Geräte animieren.
              Nicht mit Commown: Wir ermuntern Sie dazu, Ihr derzeitiges Gerät
              so lange wie möglich zu nutzen und bieten Ihnen aus diesem Grund
              diesen zeitversetzt gültigen Gutschein an, den Sie frühestens nach
              Ablauf von zwei Monaten einlösen können!"
          - title: Unterstützen Sie die Kooperative
            text: Diese Interessensbekundung ist für unsere Kooperative eine wichtige
              Unterstützung. Sie verleiht uns Finanzinstitutionen gegenüber
              Gewicht und erlaubt uns, die Nachfrage besser zu antizipieren.
              Schon ab 10 € zählt Ihre Unterstützung!
      - ctas:
          - outline: false
            text: Weitere Informationen zu unserem Gutschein
            href: https://shop.commown.coop/de_DE/shop/product/aktionsgutschein-186?category=8
    id: features
---
