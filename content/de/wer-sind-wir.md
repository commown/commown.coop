---
layout: about
title: Commown · Wer sind wir?
description: "Commown ist eine gemeinnützige Genossenschaft, die eine Vermietung
  umweltfreundlicherer und ethischerer Geräte (#Fairphone 3 und 4, #modulare
  Kopfhörer, #Laptops) samt Serviceleistungen anbietet. Somit versucht Commown,
  die Lebensdauer seiner Geräte so weit wie möglich zu verlängern: was nicht
  mehr reparierbar ist, dient als Ersatzteillager für andere Geräte."
date: 2021-03-19T12:25:36+01:00
translationKey: about
menu:
  main:
    name: Wer sind wir
    weight: 10
  footer:
    name: Wer sind wir
    weight: 2
header:
  title: Die einzige Genossenschaft für nachhaltige Elektronik
  subtitle: Für mehr Fairness und Nachhaltigkeit im Elektronikbereich
introduction:
  title: "Die Vision von Commown "
  bg_color: bg-bleu-clair-75
  color: text-bleu-fonce-100
  text_left: Commown ist geprägt von einer globalen und kollektiven Vision des
    Planeten Erde. Ein Team begeisterter Menschen hat es sich zum Ziel gemacht,
    den Elektroniksektor zu einem nachhaltigen und verantwortungsbewussten
    Sektor zu transformieren.
  text_right: Die untypischen Karrierewege der Teammitglieder begannen alle mit
    ersten Erfahrungen, bei denen “der Sinn” nicht unbedingt vorhanden war.
    Entwicklung neuer Pestizide (Adrien), Forschung an Quantencomputern für
    US-amerikanische Geheimdienste (Elie), Versicherungsprüfungen für Total
    (Fred) und IT-Entwicklung für exotische Finanzprodukte (Florent)… Vor diesem
    Hintergrund gab es enormen Spielraum für Verbesserungen!
roadmap:
  title: Roadmap
  icon: roadmap
  events:
    - year: "01.2018"
      title: Gründung der Genossenschaft
      text: Beginn des Abenteuers Commown!
      icon: flag
    - year: "05.2020"
      title: Die ersten 1000 Geräte sind vermietet
      text: Die meisten davon sind Fairphones.
      icon: user-group
    - year: "03.2021"
      title: Insgesamt 1 000 000 € gesammelte Geldmittel
      text: Die Mittel stammen aus Fundraising und Bankkrediten.
      icon: speakerphone
    - year: "06.2021"
      text: Das Angebot wird auch auf Deutsch zugänglich.
      title: "Commown eröffnet eine Niederlassung in Berlin! "
    - year: Aktuell
      title: "Forschung und Entwicklung "
      text: Verbesserung der Dienstleistung für eine noch längere Lebensdauer der
        Geräte!
sections:
  - id: recompenses
    color: text-bleu-fonce-100
    bg_color: bg-bleu-clair-75
    containers:
      - title: Lernen Sie Commown in diesem Video besser kennen
      - figure:
          href: https://www.youtube.com/watch?v=LLE-W3qBfbs
          image: /media/2021_03_commown_vignet_youtube.jpg
          alt: "Vorstellung von Commown: Die Kooperative für nachhaltige Elektronik"
          iframe:
            lang: html
            code: <iframe
              src="https://www.youtube.com/embed/LLE-W3qBfbs?autoplay=1&color=white"
              title="YouTube video player" frameborder="0" allow="accelerometer;
              autoplay; clipboard-write; encrypted-media; gyroscope;
              picture-in-picture" allowfullscreen></iframe>
      - title: ""
        blocks:
          - title: Gewinner des europäischen C-Voucher-Programms
          - title: Mitglied der European Circular Economy Stakeholder Platform
          - title: Ausgewählt als eine der 1000 “clean and profitable solutions” von der
              Solar Impulse Foundation
            text: ""
          - title: Mitglied in den Vereinen ZeroWaste e.V., SEND e.V. und Circular Berlin
              e.V
        ctas:
          - outline: false
            text: Unsere Angebote
            href: /de/
  - id: nos-valeurs
    color: text-bleu-fonce-100
    bg_color: bg-white
    containers:
      - title: Unsere Werte
        color: text-bleu-fonce-100
        blocks:
          - title: " Umweltschutz"
            text: >-
              <!--StartFragment-->


              Die ökologischen Herausforderungen, mit denen sich unsere Gesellschaft konfrontiert sieht, sind alarmierend: Erschöpfung nicht erneuerbarer Ressourcen, Zusammenbruch der Artenvielfalt, Klimawandel. Die Überproduktion an elektronischen Geräten spielt bei diesen Entwicklungen eine entscheidende Rolle. Wir müssen alles dafür tun, um diese negativen Auswirkungen soweit es geht zu vermindern.


              <!--EndFragment-->
          - title: Schlichtheit
            text: >-
              <!--StartFragment-->


              Hier sind sich die Wissenschaftler einig: um die Grenzen unseres Planeten zu respektieren, müssen wir **auf jeden Fall umdenken und unsere Welt einfacher gestalten.** Wenn Sie kein Smartphone haben und nicht das Bedürfnis verspüren, eines zu besitzen, verzichten Sie darauf&#8239;! Wenn Ihr Gerät noch funktioniert, sollten Sie es so lange wie möglich nutzen, bevor Sie sich dann irgendwann für eines unserer Abos entscheiden.


              <!--EndFragment-->
        ctas: []
      - blocks:
          - title: Ethik
            text: >-
              <!--StartFragment-->


              Wir sind mit einer Explosion sozialer Ungerechtigkeiten konfrontiert. Die Arbeitsbedingungen in der Elektronikindustrie sind miserabel. Elektronische Geräte sind zu Instrumenten der Kontrolle und Vermarktung personenbezogener Daten geworden. **Freien Software-Programmen** kommt in diesem Kampf eine bedeutende Rolle zu, zum einen **für den Schutz der Privatsphäre**, zum anderen **für eine längere Lebensdauer der Geräte.**


              <!--EndFragment-->
          - title: Kooperation
            text: >-
              <!--StartFragment-->


              Gegenseitige Hilfe und Kooperation sind für uns die einzige Möglichkeit, zuversichtlich in die Zukunft zu blicken. Deshalb haben wir uns für die Unternehmensform einer Société Coopérative d’Intérêt Collectif (SCIC), einer gemeinnützigen Genossenschaft entschieden. Diese Unternehmensform erlaubt es verschiedenen Akteuren (Kunden, Herstellern, Angestellten), Genossenschafter zu werden. Hier gilt 1 Person = 1 Stimme, die Macht ist nicht an das Kapital gebunden und die Wertschätzung desselben gering. Der kooperative Geist fördert außerdem den Kontakt zu anderen gemeinnützigen Genossenschaften.


              <!--EndFragment-->
          - text: >-
              <!--StartFragment-->


              Mit der Entscheidung zur Gründung einer gemeinnützigen Genossenschaft fördert Commown bei seiner Unternehmensführung **Transparenz** und **Demokratie.** Commown stellt seinen Genossenschaftern jedes Jahr einen detaillierten Geschäftsbericht zur Verfügung.


              <!--EndFragment-->
            title: Transparenz
        ctas:
          - outline: true
            text: Unsere Satzung
            href: /media/satzung-genossenschaft-commown.pdf
        color: text-bleu-fonce-100
  - id: coherences-incoherences
    bg_color: bg-beige-25
    cols: true
    containers:
      - blocks:
          - title: Greenwashing ist heute gang und gäbe
            text: >-
              **Wie erkennt man den Unterschied? Indem wir auf die Details
              achten und mehr und mehr Transparenz fordern.**


              **Das ist Commown heute:**


              * Versand mit 100%iger CO2-Kompensation.

              * Fokus auf freie Softwares für Smartphones und PCs.

              * Lobbyarbeit an Institutionen zur Förderung der Nutzungsökonomie und einer verantwortungsvolleren Elektronik.

              * Sensibilisierung der Öffentlichkeit, um diese Themen zu demokratisieren.

              * Ein Firmensitz, der mit Ökostrom versorgt wird.

              * Server bei iKoula, einem nachhaltigen Cloud-Hosting-Anbieter.

              * Ein Bankkonto bei einer Genossenschaftsbank.

              * Ein Abo des Carsharing-Anbieters CITIZ für die wenigen Fahrten mit dem Auto.

              * Wir haben unser Twitter-Konto gelöscht und sind nun auf Mastodon aktiv.

              * Wir versuchen, für Reisen eher die Bahn als das Flugzeug zu nutzen.

              * Unser Vorstand ist mit 7 Männern und 7 Frauen nun paritätisch aufgestellt.

              * Wir haben von der Google-Suite auf Nextcloud und OnlyOffice umgestellt.

              * Und natürlich die Unternehmensform einer Genossenschaft!

                **… Vertrauen!**
        color: text-bleu-fonce-100
        title: Kohärenz
      - title: Inkohärenz
        blocks:
          - title: Nachfolgend finden Sie unsere aktuellen Ungereimtheiten
            text: >-
              **Indem wir sie schwarz auf weiß auflisten, verpflichten wir uns,
              sie so bald wie möglich zu beheben.**


              **Heutige Inkohärenzen von Commown:**


              * Wir haben ein Amazon-Konto (nur zweimal in zwei Jahren genutzt)

              * Wir haben ein Facebook-Konto (und nutzen unser Communecter-Konto noch nicht genug).

              * Wir nutzen nach wie vor die Werbetools von Facebook und Google.

              * Wir haben in 2 Jahren 16 Kontinentalflüge gemacht, das sind 16 Flüge zu viel.

              * Wir nutzen immer noch KI-Übersetzungstools wie DeepL für Übersetzungen zwischen Deutsch und Französisch.

                **… Transparenz!**
  - id: equipe
    color: text-bleu-fonce-100
    bg_color: bg-bleu-clair-75
    containers:
      - title: Das Team
        font_size: smaller
        blocks:
          - title: Adrien
            text: Früher Chemiker, heute olivenölsüchtiger Umweltaktivist.
          - title: Céline
            text: Junge Ingenieurin auf der Suche nach Sinn, Philanthropin im Herzen und
              Utopistin in ihrer Freizeit. Vor allem will sie sich für den
              Umweltschutz und das Gemeinwohl einsetzen.
          - title: Élie
            text: Ökologie in den Wurzeln und theoretische Physik im Kopf, für
              unerschütterlichen Zen-Optimismus.
          - title: Florent
            text: Langjähriger Praktiker der freien Software, versteht Arbeit nur als
              Mittel, um kooperativ Gemeingüter zu schaffen. Konvertierte zum
              Umweltschutz, als er verstand, dass der Planet das ultimative
              Gemeingut ist 🙂
      - font_size: smaller
        blocks:
          - title: Fred
            text: Vor 15 Jahren in den unternehmerischen Kessel gefallen, Elsässer ohne
              Grenzen, per Definition Rigo-Extremist, der sich aber in der
              Öko-Gruppentherapie behandeln lässt
          - title: Hassiba
            text: Ehemalige Sozialarbeiterin mit kaufmännischer Umschulung im Bereich Ethik
              und Umweltschutz!
          - title: Lucas
            text: Junger Techniker mit einer Leidenschaft für alles Technische, der schon
              heute digital und mit einer verantwortungsvolleren, ethischeren
              und ökologischeren Technik zum Gemeinwohl beiträgt!
          - title: Manon
            text: Öko-Spross, neu auf dem Gebiet der Kreislaufwirtschaft und Anhängerin des
              Plusquamperfekts. Ich kümmere mich gerne wohlwollend um andere.
              Wenn ich mich nicht um die Commowner kümmere, genieße ich es, auf
              einem Liegestuhl in der Sonne dem Gesang der Vögel zu lauschen.
  - id: merci
    color: text-bleu-clair-25
    bg_color: bg-bleu-fonce-100
    containers:
      - title: ""
        font_size: smaller
        blocks:
          - text: |-
              **Ein großes Dankeschön \
              an alle Freiwilligen und Selbstständigen,  \
              die Commown möglich gemacht haben!**
          - text: >-
              Chloé, Ariane, Alexandre, Fabien, Sébastien, Faustine, Mickael,
              Caroline, Mathieu, Sylvain, Guillaume, Iris, Yohan, Clément,
              Jessica, Bernat, Tony, Louise, Jean Philippe, Gauthier, Timothée,
              Derek… \

              und eines Tages vielleicht Du? &#128521;
  - id: ecoconception
    color: text-bleu-fonce-100
    bg_color: bg-white
    containers:
      - title: Die umweltfreundliche Gestaltung der Website
        title_max_w: 22ch
        icon: mobile
        blocks:
          - title: Langlebigkeit
            text: >-
              <!--StartFragment-->


              **Die Herstellung der Geräte spielt bei der umweltfreundlichen Gestaltung einer digitalen Dienstleistung eine entscheidende Rolle.** Umso wichtiger ist es, dass die Website nicht den Austausch der vorhandenen Ausstattung durch neue Geräte fördert, sondern dessen Lebensdauer.


              So funktioniert die Website ohne Verlangsamung und mit geringer Bandbreite ebenso gut auf einem alten Gerät wie auf einem neuen.


              <!--EndFragment-->
          - title: Besser werden
            text: >-
              <!--StartFragment-->


              Wir haben unsere Anforderungen heruntergeschraubt, um eine zugängliche, leistungsfähige und sichere Website zu schaffen. Im Vergleich zu unserer alten Website haben wir die Durchschnittsgröße einer Seite durch 23 dividiert (durchschnittlich 159 Ko), die Zahl der Anfragen durch 4 (durchschnittlich 16), die Ladezeit durch 5 (1,09 bei der ersten) und nun laden die Seiten der Website in 1,66 Sekunden bei einer niedrigen Bandbreite von 3G (780 Kbps).


              <!--EndFragment-->
---
