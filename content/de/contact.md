---
hCaptchaKey: 003087c8-4f6b-44fe-a1b7-640d55a62409
text: >-
  Wenn Sie Fragen zu unseren Miet- und Serviceangeboten haben, uns Ideen und
  Vorschläge für Partnerschaften mitteilen oder einfach nur (per Newsletter)
  über unsere Aktivitäten auf dem Laufenden gehalten werden möchten, schreiben
  Sie uns gerne über dieses Formular. \

  \

  **Übrigens:** Jeder neue Abonnent unseres Newsletters erhält von uns einen Gutschein für einen Gratis-Monat! (ohne zeitliche Beschränkung und für all unsere Angebote einlösbar)
translationKey: contact
title: "Kontaktieren Sie uns "
buttonDisplay: "Formular anzeigen "
successText: |-
  **Ihre Nachricht wurde erfolgreich verschickt ✓** \
  Wir bemühen uns, Ihre Anfrage so schnell wie möglich zu bearbeiten!
errorText: |-
  **Ihre Nachricht konnte nicht verschickt werden ×** \
  Überprüfen Sie bitte, ob Sie alle erforderlichen Felder ausgefüllt haben.
fields:
  department:
    label: Grund für Ihre Anfrage
    hint: Veuillez sélectionner le motif de votre demande.
  name:
    label: Ihr Name
    hint: Veuillez remplir ce champ.
  email:
    label: Ihre E-Mail-Adresse
    hint: Veuillez saisir une adresse électronique valide.
  subject:
    label: Betreff ihrer Nachricht
    hint: Veuillez remplir ce champ.
  body:
    label: Ihre Nachricht
    hint: Veuillez remplir ce champ.
  captcha:
    label: Captcha
    hint: Veuillez cocher la case du captcha ci-dessus.
actionURL: https://shop.commown.coop/contact
headless: true
buttonSend: Absenden
options:
  - label: Fragen zu unseren Angeboten / Kostenvoranschlag
    value: commercial
  - label: Abonnieren des Newsletters von Commown
    value: newsletter
  - label: Kommunikation / Presse / Events
    value: media
  - label: Partnerschaft / Förderung / Sponsoring
    value: commercial
  - label: Probleme mit der Website / Übersetzung
    value: webmaster
  - label: Sonstiges
    value: other
---
