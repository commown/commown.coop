---
title: Commown für die berufliche Nutzung
description: "Lösungen für Unternehmen: ökologische Computer, Laptops, Headsets
  und Smartphones mieten"
layout: invest
date: 2021-03-30T08:25:53.200Z
translationKey: business
menu:
  main:
    name: Für Unternehmen
    weight: 2
  footer:
    name: Für Unternehmen
    weight: 7
header:
  title: Commown für die berufliche Nutzung
  subtitle: Unsere Produkte für ein langfristiges Engagement
  ctas: []
sections:
  - id: nos-offres
    color: text-bleu-fonce-100
    bg_color: bg-bleu-clair-75
    containers:
      - title: Unsere Angebote
        blocks:
          - title: Fairphone und Shiftphone
            icon: mobile
            ctas:
              - outline: false
                href: https://greener-it-pro.commown.coop/de_DE/shop/category/smartphones-18
                text: Zu den Fair- und Shiftphones
          - title: Modulare Headsets
            icon: audio
            ctas:
              - outline: false
                text: Zu den Headsets
                href: https://greener-it-pro.commown.coop/de_DE/shop/category/audio-12
          - title: Laptops und PC
            ctas:
              - outline: false
                text: Zu den Laptops und PC´s
                href: https://greener-it-pro.commown.coop/de_DE/shop/category/computer-in-kurze-verfugbar-2
            icon: desktop
          - title: Tablets
            icon: tablet
            ctas:
              - outline: false
                text: Zu den Tablets
                href: https://greener-it-pro.commown.coop/de_DE/shop/product/crosscall-tablet-smartphone-2-in-1-core-t4-pro-fur-unternehmen-228
  - id: avantages
    color: text-bleu-fonce-100
    containers:
      - blocks:
          - title: Premium-Service
            text: >-
              Unser Angebot für Unternehmen und Organisationen passt sich allen
              Bedürfnissen an: von der Schulung der Mitarbeiter bis zur
              Bereitstellung von Lösungen für Geschäftskontinuität.


              Mit Commown bedeuten Ausfälle, Schäden und selbst Diebstahl nicht mehr automatisch den Rückgang Ihrer Produktivität.
          - title: Wirtschaftlicher Vorteil
            text: "Kostenkontrolle und vorteilhaftere Bilanz: Die monatlichen Zahlungen, die
              unter die Betriebskosten fallen, vereinfachen nicht nur das
              Budgetmanagement und Ihre Haushaltsplanung (Transparenz der
              Verbindlichkeiten) und sind von der Vorsteuer abziehbar
              (Liquiditätsoptimierung), sondern tragen auch zum Erhalt Ihres
              Eigenkapitals bei! Denn beim Mieten von Geräten können Sie die
              monatlichen Zahlungen als Aufwendungen verbuchen und auf diese
              Weise die Verschuldungskapazität für andere, strategisch
              sinnvollere Investitionen bewahren."
          - text: Unsere Kooperative ist eine gemeinnützige Genossenschaft (société
              coopérative d'intérêt collectif, kurz SCIC) und Mitglied in den
              Vereinen [ZeroWaste e.V.](https://zerowasteverein.de/), [SEND
              e.V.](https://www.send-ev.de/) und [Circular Berlin
              e.V](https://circular.berlin/), um eine nachhaltigere und
              solidarische Entwicklungsstrategie zu unterstützen. Unser
              vorrangiges Ziel besteht darin, die negativen Auswirkungen der
              Elektronikbranche zu reduzieren. Wenn Sie uns Ihren IT-Gerätepark
              anvertrauen, können Sie sicher sein, dass wir alles dafür tun
              werden, die Geräte so lange wie möglich in Betrieb zu halten, sei
              es durch Weitervermietung oder Weiternutzung von Einzelteilen. Mit
              einem Abonnement bei Commown unterstützen Sie außerdem innovative
              aufstrebende Akteure aus dem Bereich der verantwortungsvollen
              Elektronik.
            title: Vorteil der gesellschaftlichen Unternehmensverantwortung (CSR)
  - containers:
      - title: Unternehmen, die uns vertrauen
        blocks:
          - text: >-
              ![](/media/spielberger_mühle_logo_2022-gimp.png)


              Verabreitet Rohstoffe aus der Demeter-Landwirtschaft (ältesten und strengste Form der Bio-Landwirtschaft) zu gesunden und biodynamischen Lebensmittel. Neben Qualität und Genuss steht die Spielberger Mühle für eine nachhaltige und naturverträgliche Wertschöpfung.
          - text: >-
              ![](/media/logo.png)


              Von Pharmaexperten geleitetes Unternehmen, das im Bereich des Vertriebs von Gesundheitsprodukten, Outsourcing-Dienstleistungen für die Pharmaindustrie und Business-Intelligence-Aufträgen tätig ist.
          - text: >-
              ![](/media/bildschirmfoto-von-2023-03-17-13-37-18.png)


              Internationale Beratungsagentur, die auf digitale Transformation spezialisiert ist und die Werte des Unternehmertum, Co-Learnings und der Co-Konstruktion lebt.
          - text: >-
              ![](/media/bib-sans-frontiers.png)


              Gemeinnützige Organisation, die sich in über 50 Ländern für den Zugang zu Bildung, Kultur und Informationen für schutzbedürftige Menschen einsetzt.
    id: customer-reference
  - id: faq
    color: text-bleu-fonce-100
    containers:
      - title: Häufige Fragen
        title_max_w: 14ch
        icon: chat-alt
        blocks:
          - title: Warum mieten statt kaufen?
            details: true
            text: >-
              1. Einfache Entscheidung und Umsetzung: 

                 Ein Dienst mit monatlichem Abonnement ist in Ihrem Unternehmen einfacher und schneller einzurichten als ein Kauf, der eine Investition erfordert (Entscheidungskreislauf, Liquidität, Verwaltungsaufwand). 

                 | **MIETE (Gebrauch)**                                                                                                       | **KAUF**                                                                                                                                    |
                 | -------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------- |
                 | Hilfe bei der Konfiguration und Unterstützung in der täglichen Nutzung                                                     | Nicht inklusive                                                                                                                             |
                 | Entscheidet sich das Unternehmen für eine Open-Source-Software: Schulung der Mitarbeiter ohne zusätzlich anfallende Kosten | Nicht inklusive                                                                                                                             |
                 | Defekte:  Fehlerbehebung, Reparatur und/oder Ersatz der Geräte während der gesamten Mietdauer                              | Defekte: zeitlich begrenzte Herstellergarantie (2 Jahre für das Fairphone)                                                                  |
                 | Diebstahl/Verlust: Ersatz ohne zusätzlich anfallende Kosten                                                                | Hohe Selbstbeteiligungen der Versicherungen + Berücksichtigung der Abnutzung + Marktwertbasis + Versandkosten + Prüf- und Bearbeitungsdauer |
                 | Schaden: Reparaturen ohne zusätzlich anfallende Kosten                                                                     | Hohe Selbstbeteiligungen der Versicherungen + Berücksichtigung der Abnutzung + Prüf- und Bearbeitungsdauer                                  |
                 | Servicekontinuität: 2 Niveaus (Ersatzgeräte für die Zwischenzeit  oder Ersatzteile/einsatzbereite Geräte die vor Ort sind) | Nicht inklusive                                                                                                                             |
                 | Entwicklung und/oder Austausch von Modulen                                                                                 | Nicht inklusive                                                                                                                             |
                 | Verwaltung der Geräte am Ende der Vertragslaufzeit ohne zusätzlich anfallende Kosten                                       | Nicht inklusive                                                                                                                             |
                 | Versandkosten und Logistik (Lieferung und Kundendienst)                                                                    | Nicht inklusive                                                                                                                             |

                 \
                 Außerdem bedeutet der Kauf, dass Sie alle Interaktionen im Zusammenhang mit den Geräten und den einzelnen Nutzern selbst organisieren müssen, während beim Mieten alle diese Interaktionen von Commown gesteuert werden, was zu erheblichen Einsparungen führt (Kosten für Ersatzteile, Verwaltungs-, Arbeitsaufwand oder die Organisation des Outsourcings).
              2. Kostenkontrolle und positive Bilanzdarstellung:\
                 \
                 Die monatlichen Mietraten, die den Nutzungskosten entsprechen, ermöglichen Ihnen nicht nur die Vereinfachung der Organisation und Vorhersage Ihres Jahresbudgets (Planbarkeit der Ausgaben), sowie die Periodisierung der Mehrwertsteuerströme (Optimierung des Cashflows), sondern auch die Erhaltung Ihres Eigenkapitals! Wenn Sie sich für eine Anmietung entscheiden, können Sie die monatlichen Raten als Aufwand verbuchen und so Ihre finanzielle Kapazität für andere, strategischere Investitionen bewahren. 
              3. Besonders für öffentliche Körperschaften: Richtlinien zur Reduzierung von Elektroabfällen\
                 \
                 Körperschaften stehen eine Vielzahl von rechtlichen Instrumenten (im Gegensatz zu den sosnt üblichen Verwaltungs-/Buchhaltungsvorschriften) zur Verfügung, mit denen sie selbst, oder über öffentliche Aufträge die Lösungen umsetzen können, die sie für ihr Gebiet im Hinblick auf eine nachhaltige Entwicklung fördern möchten. Dies gilt beispielsweise für die Richtlinie Elektroschrott zu reduzieren, welche durch die Vermietung von elektronischen Geräten (HaaS) zielgerichtet entsprochen werden kann. Die von Commown angebotene Lösung reduziert die Menge an Elektroschrott, indem Bauteile wiederverwendet werden.
          - title: Warum sind unsere Preise wettbewerbsfähig?
            details: true
            text: >-
              Es wäre viel zu einfach gedacht, die anfallenden Mietkosten nur
              mit dem Kaufpreis eines neuen Gerätes zu vergleichen. Tatsächlich
              sollten mehrere Faktoren berücksichtigt werden:


              1. Ein Langzeitmietvertrag beinhaltet zahlreiche DIENSTLEISTUNGEN, die die mittel- bis langfrisitgen Gesamtkosten nicht nur ausgleichen, sondern darüber hinaus einen echten Mehrwert schaffen. Die Vorteile eines Umstieges von einem Besitzmodell zu einem nachhaltigeren Nutzungsmodell liegen unter anderem darin, dass die Maximierung der Lebensdauer der elektronsichen Geräte dazu führt, dass die Gesamtkosten sowohl für die Wartung der Geräte, als auch für die Erneuerung des IT-Inventars deutlich reduziert werden. Bei einem Kauf sind diese teils hohen Kosten nicht direkt ersichtlich, und können so schnell bei der internen Kalkulation vergessen werden.\
                 Deshalb bietet Ihnen Commown ein optionales All-Inclusive-Angebot an. Dieses beinhaltet einen vollumfänglichen Kundenservice über den gesamten Vertragszeitraum, sodass gewährleistet ist, dass Sie Ihre Geräte stets optimal nutzen können.\
                 \
                 Dazu zählen unter anderem:

                 * Hilfe und Unterstützung bei allen technischen Fragen
                 * Servicekontinuität
                 * Verlängerung der Herstellergarantie für Ausfälle während der gesamten   \
                   Vertragslaufzeit
                 * Garantien für Bruch, Diebstahl oder Verlust
                 * sowie weitere optionale Dienstleistung, die sonst indirekte Kosten für Ihr \
                   Unternehmen bedeuten würden
              2. Ein Vergleich mit der Konkurrenz verdeutlicht den Preisvorteil, den das Modell von Commown bietet. So bietet beispielsweise Grover die Miete von Android-Smartphones an, die allerdings NICHT den ethischen und ökologischen Nutzen stiften, so wie wir ihn uns als Ziel gesetzt haben. Gleichzeitig sind die Preise höher als die von Commown, und das bei einer Vertragsdauer von bis zu 3 Jahren. Für iPhones steigen die Preise sogar noch weiter an (bis zu 65€/Monat inkl. MwSt. bei 12 Monaten Laufzeit).

              3. Schließlich garantiert die Wahl unserer Unternehmensführung in Form einer gemeinnutzigen Genossenschaft, dass wir die bestmöglichen Preise anbieten können. Falls Commown Überschüsse erwirtschaftet, können die Commowners über die Verwendung dieser Überschüsse abstimmen, auch um die Preise zu senken, wenn dies der Mehrheitswunsch ist. 

              4. Auch der Vergleich eines kombinierten Abonnements aus Handy und Tarif zeigt, dass Sie durch das Angebot von Commown Kosten einsparen können.\
                 Die von den Anbietern angebotenen Abonnements, die sowohl ein Gerät, als auch ein Mobilfunktarif enthalten liegen in der Regel zwischen 50 und 80 €/Monat. Hinzu kommen die Kosten für die Erneuerung des Smartphones alle zwei Jahre (auch wenn die Tarife vergünstigt sind, fallen dennoch zusätzliche Kosten an, die bezahlt werden müssen), ganz zu schweigen von allen zuvor erwähnten indirekten Kosten. 

                 Reine Mobilfunkverträge sind jedoch schon ab 15€/Monat erhältlich. Kombinieren Sie so ein Vertrag mit dem All-Inckusive-Angebot von Commown, zahlen Sie höchstens ca. 45€/Monat (bei Auswahl des teuersten Mobiltelefon). Zusätzlich sinkt die monatliche Belastung, je länger Sie das Mobiltelefon benutzen.
              5. Allein das Prinzip, Smartphones systematisch alle zwei Jahre zu ersetzen, ist ein Widerspruch in sich, wenn es um den Übergang zu einer nachhaltigeren, ethischeren und umweltfreundlicheren Elektronik geht. Dasselbe gilt für Verträge zur Erneuerung von Computern.
          - title: Warum ist es ethisch sinnvoll, bei Commown zu mieten?
            details: true
            text: >-
              1. Die Langzeitmiete ohne Kaufoption ist zeitlich begrenzt. Sie
              ermöglicht es den Herstellern, trotz eines sich verändernden
              Marktes sichtbar zu sein. Es ist das gleiche Prinzip wie bei der
              solidarischen Landwirtschaft: 

                 Sie helfen dem Produzenten, mittelfristig eine bessere Kontrolle über seine Liquidität zu haben, was ihm würdige Arbeitsbedingungen ermöglicht, um eine qualitativ hochwertige Produktion zu gewährleisten. Darüber hinaus verpflichten wir uns, über die Hersteller zu berichten, die sich gemeinsam mit uns für nachhaltige Elektronik engagieren. Die daraus resultierende schrittweise Erhöhung ihres Marktanteils wird ihre Tätigkeit entsprechend stützen. Das langfristige Ziel ist die finanzielle Unterstützung von Forschung und Entwicklung, um bestehende Geräte zu verbessern und/oder neue, noch umweltfreundlichere und ethisch vertretbarere Geräte zu entwickeln. 
              2. Wir versuchen, die Verpflichtung der Hersteller, die sozialen und ökologischen Auswirkungen von Elektronik zu verringern, zu verlängern. Wenn Sie bei Commown mieten, ist das eine Garantie dafür, dass die Geräte so lange wie möglich genutzt werden. Denn selbst wenn Sie die Geräte nicht mehr benötigen, werden wir sie an andere Nutzer*innen vermieten, nachdem wir sie in einen optimalen Betriebszustand gebracht haben. 

              3. Weil wir kein "klassisches" Unternehmen sind, sondern eine gemeinnützige Genossenschaft. Die Entscheidung, eine gemeinnützige Genossenschaft zu sein, bedeutet zu bekräftigen, dass unsere Motivation nicht der Profit, sondern das kollektive Interesse und eine demokratische Verwaltung ist. In einer französischen gemeinnützigen Genossenschaft ist die Gewinnverteilung nämlich besonders streng geregelt: Mindestens 57,5 % der eventuellen Betriebsüberschüsse müssen reinvestiert werden. Darüber hinaus schützen die Entscheidungs- und Abstimmungsmechanismen in der Generalversammlung die gemeinnützige Genossenschaft vor einer feindlichen Übernahme. Die Stimmrechte in einer Genossenschaft funktionieren nach dem Prinzip "ein Commowner = eine Stimme", so dass alle Commown-Mitglieder (Kunden, Produzenten, Medien, Projektträger, Freiwillige, finanzielle Unterstützer usw.) gemeinsam alle gemieteten Geräte besitzen und große Entscheidungen gemeinsam treffen.\
                 \
                 Bei anderen Anbietern von Mietgeräten wird die Marge der monatlichen Raten oft dazu verwendet, Aktionäre zu bezahlen. So wie Sie also mit einem Abonnement bei Greenpeace Energy sicher sein können, dass Sie die Energiewende finanzieren und nicht die Aktionäre von ENGIE, können Sie bei Commown sicher sein, dass Sie den Übergang zu einer verantwortungsvolleren Elektronik finanzieren und nicht die Aktionäre von Apple, Samsung & Co.
          - title: Wie kann die CSR-Strategie meines Unternehmens das Mieten
              verantwortungsvoller elektronischer Geräte finanzieren?
            details: true
            text: >-
              1. CSR-Engagement  vs. Buchhaltung\
                 \
                 Eine Organisation wird nicht mehr nur nach ihrer Fähigkeit bewertet, finanzielle Werte zu schaffen (oder im Falle von Kommunen die Verwaltung von öffentlichen Dienstleistungen zu optimieren), sondern auch nach ihrer Fähigkeit, das Naturkapital wiederherzustellen. Durch die Umweltbilanz legt eine Organisation öffentlich dar, ob sie unterm Strich natürliche Ressourcen regeneriert (positive Bilanz) oder verbraucht (negative Bilanz). Für Investoren (oder für Bürgern im Falle von Kommunen) hat sie heute eine sehr hohe Bedeutung und wird ebenso wie die Finanzbuchhaltung bei allen Investitionsentscheidungen berücksichtigt.
              2. CSR-Engagement vs. Preis:

                 Jede Organisation, die eine CSR-Strategie verfolgt, verpflichtet sich, ihre Vorhaben nach folgendem Modell zu finanzieren: 

              * Einen gerechtfertigten Preis je nach Rentabilitätsbeschränkungen.

              * PLUS ein eigenes Budget, das den CSR-Verpflichtungen gewidmet ist.

                \
                In unserem Fall werden aus diesem zweckgebundenen Budget finanziert: 
              * Einerseits die positiven gesellschaftlichen und ökologischen Auswirkungen, die mit dem Geschäftsmodell und der Qualität der vermieteten Geräte verbunden sind

              * Andererseits das von der Genossenschaft getragene kollektive Interesse, das darauf abzielt, langfristig die gesamte Elektronikbranche zu verbessern


              \

              Zu einer nachhaltigen CSR-Strategie gehört es auch einen vernünftigen Leistungsanspruch des Geräts zu definieren (z.B eines Fairphones im Vergleich zu einem iPhone), der zu einer sparsameren Nutzung der Geräte führt (die Nutzer sollten z. B. auf Geräte verzichten, die in der Lage sind, CPU- oder energieintensive Anwendungen wie die neuesten Spiele oder andere überflüssige Anwendungen für die berufliche Nutzung auszuführen). Der bewusste Einsatz dieser Konzepte der sparsamen Nutzung ist besonders in Bezug auf die Verkürzung der Lebensdauer (welche durch die geplante Obsoleszens verkürzt wird) und der damit einhergehenden Verbesserung des ökologischen Fußabdruckes sinnvoll.
          - title: Wie setzt Commown sein CSR-Engagement um?
            details: true
            text: >-
              Das Angebot von Commown ermöglicht ein hohes Maß an sozialer und
              ökologischer Verantwortung. Dazu gehören Produktsiegel wie das
              Fairphone (B-Corp), aber auch das Commown-Projekt an sich :


              * eine gemeinnützige Genossenschaft.

              * als Sozialunternehmen anerkannt.

              * mit dem Label Efficient Solution Solar Impulse ausgezeichnet.

              * von Experten der Kreislaufwirtschaft wie C2C NGO, Zero Waste e.V. oder Circular Berlin empfohlen.


              Schließlich sind die Kohärenz und das Engagement von Commown auf allen Ebenen erkennbar, von der grünen Energie für die Server bis hin zu kohlenstoffneutralen Lieferungen. Weitere Informationen finden Sie im Abschnitt "Kohärenz" unten auf der Seite "[Wer sind wir](/de/wer-sind-wir/)"
        ctas:
          - outline: false
            href: /de/faq/
            text: Lesen Sie unsere FAQs
---
