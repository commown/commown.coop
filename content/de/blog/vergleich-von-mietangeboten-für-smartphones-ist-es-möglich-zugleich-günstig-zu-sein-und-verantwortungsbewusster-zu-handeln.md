---
title: "Vergleich von Mietangeboten für Smartphones: Ist es möglich, zugleich
  günstig zu sein und verantwortungsbewusster zu handeln?"
date: 2022-05-30T09:54:42.848Z
translationKey: location durable
image: /media/preisvergleich-zwischen-verschiedenen-optionen-für-die-anmietung-gleichwertiger-smartphones-7.png
---
“Es gibt immer mehr Angebote, Smartphones über einen längeren Zeitraum zu mieten. Der hier vorgenommene Vergleich der bekanntesten Angebote zeigt, dass Commown der umweltfreundlichste, aber auch günstigste Anbieter ist.”

### 1/ EIN NACHHALTIGERES UND ETHISCH VERANTWORTUNGSBEWUSSTERES ANGEBOT ZUM MIETEN EINES SMARTPHONES

Commown ist eine sozial und ökologisch engagierte Kooperative, der es mithilfe des [Produkt-Service-Systems](https://commown.coop/de/blog/warum-entscheidet-man-sich-heutzutage-f%C3%BCr-haas-beruhend-auf-einer-genossenschaftlichen-basis/) gelungen ist, ein weitestgehend umweltfreundliches Angebot zu schaffen. Die Ressourcen unseres Planeten sind begrenzt und wir müssen sie schonen. Wir vermieten umweltfreundlichere Smartphones (und Computer, Kopfhörer, usw.), um den Wandel zu einer verantwortungsbewussten Elektronik zu unterstützen. Unser Mehrwert besteht in unserem kooperativen Modell (gemeinnützige Genossenschaft). Dieses Modell kommt dem engagierten Verbraucher zugute. Es sorgt dafür, dass die Geräte gemeinsam genutzt werden und garantiert dem Nutzer den Zugang zu Ersatzteilen auch lange nachdem der Hersteller deren Verkauf eingestellt hat. Solange der Hersteller Ersatzteile verkauft, kauft Commown sie bei ihm ein. Eines Tages werden diese Teile aber nicht mehr verfügbar sein. Dann kann die Kooperative die gemeinsamen Geräte, die nicht mehr reparierbar sind, gewissermaßen als Ersatzteillager nutzen, um die sich noch in Gebrauch befindenden Geräte zu reparieren. Fazit: Ihre Geräte halten länger, als wenn Sie sie allein nutzen.

### 2/ DAS KOOPERATIVE MODELL ALS GARANTIE FÜR EINEN FAIREN PREIS…

In einem gewinnorientierten Unternehmen, dessen Geschäftsmodell auf der bloßen Vermietung von Geräten ohne Kaufoption basiert, hat der Kunde nicht viel Handlungsspielraum. Er hat keinen Einfluss auf das Unternehmen, das die Dienstleistung erbringt. Er weiß nicht, ob die von ihm geleisteten monatlichen Zahlungen “fair” sind oder nur dazu dienen, die Dividenden der Aktionäre am Jahresende zu erhöhen.

Bei Commown kann der Kunde Genossenschafter sein und so direkt auf die Kooperative Einfluss nehmen. Er erhält dadurch Einsicht in die Finanzen der Genossenschaft. Er kann die Art und Weise, wie sich die Kosten der Angebote zusammensetzen, infrage stellen. Dies führt dazu, dass bereits bei der Erstellung der Angebote durch die Kooperative dem kollektiven Interesse Vorrang eingeräumt wird.

Hinzu kommt, dass die Geräteflotte ein Gemeingut ist, das sich im gemeinsamen Besitz aller Mitglieder der Kooperative befindet.

### 3/ EIN EINFACHES ANGEBOT

Das Prinzip unseres Angebots ist einfach. Wir vermieten Produkte, die wir sorgfältig nach mehreren Kriterien ausgewählt haben: Robustheit, Ökodesign, Langlebigkeit, ethische Verträglichkeit, Leistung usw. Die Vermietung des Geräts wird durch Serviceleistungen ergänzt:

* Akku-Tausch und Austausch des Displayschutzes bei Bedarf.
* Abwicklung von Reparaturen, Übernahme der Kosten bei Schäden und Ausfällen, Lieferung von Ersatzgeräten (bei Diebstahl…) usw.
* Nutzer-Support (von allgemeinen Tipps über Schulungen für die Verwendung von Linux und freier Software bis zu einem personalisierten Support).
* Verhandlungen für mehr Nachhaltigkeit gegenüber Herstellern und politischen Entscheidungsträgern

### 4/ VERGLEICH MIT ANDEREN ANGEBOTEN AUF DEM MARKT

Wie lässt sich unser Angebot konkret mit anderen Angeboten auf dem Markt vergleichen? Nehmen wir das Beispiel Fairphone. Wir werden unser Angebot mit dem Kauf eines [Fairphones](https://commown.coop/de/unsere-angebote/fairphone/) und dem zusätzlichen Abschluss einer Versicherung gegen Schäden und Diebstahl vergleichen. Eine Grafik ist hier aussagekräftiger als lange Erklärungen.

![test alt](/media/preisvergleich-zwischen-commown-und-kaufen-bei-telekom-handyversicherung-bei-chubb.jpg "Preisvergleich zwischen Commown und kaufen bei Telekom + Handyversicherung bei Chubb")

Hier sehen wir, dass unser Angebot – verglichen mit einem Produkt mit gleichwertigem Service – das günstigste auf dem Markt ist. Zudem setzt sich Commown auch am stärksten für Nachhaltigkeit ein. Nicht zuletzt enthalten die Angebote der Konkurrenten nicht die umfassenden Serviceleistungen, die wir anbieten (Akkuwechsel, Austausch des mitgelieferten Displayschutzes bei Schäden, gemeinsame Nutzung von Ersatzteilen usw.).

### 5/ VERGLEICH VON ANGEBOTEN ZUM MIETEN EINES SMARTPHONES OHNE KAUFOPTION (2022)

**Der Einfachheit halber haben wir eine Grafik mit verschiedenen Mietangeboten ohne Kaufoption erstellt. Bei einem Vergleich der Preise und der von den Konkurrenten erbrachten Serviceleistungen stellt sich heraus, dass wir der günstigste Anbieter auf dem Markt sind. Als Referenzwert dient das [Fairphone 4 256GB 8 GB RAM](https://shop.fairphone.com/de/fairphone-4) (Neupreis mit Displayschutz und Case: 719 €).**

![](/media/preisvergleich-zwischen-verschiedenen-optionen-für-die-anmietung-gleichwertiger-smartphones-7.png "Preisvergleich zwischen verschiedenen Optionen für die Anmietung gleichwertiger Smartphones")

In dieser Grafik werden drei gleichwertige Angebote verglichen.  Die Preise umfassen Versicherungen gegen Schäden und Diebstahl, den Austausch von Modulen und einen personalisierten Service (Kundensupport).

### 6/ DIE WENIGER SICHTBAREN ASPEKTE UNSERER TÄTIGKEIT

Mit einem Abonnement unseres Serviceangebots unterstützen Sie indirekt auch die Generierung von Fachwissen in den Bereichen nachhaltige Elektronik und freie Software. Sie unterstützen außerdem unsere Advocacy-Arbeit, da wir uns staatlichen Einrichtungen gegenüber regelmäßig für mehr Nachhaltigkeit im Elektroniksektor einsetzen.