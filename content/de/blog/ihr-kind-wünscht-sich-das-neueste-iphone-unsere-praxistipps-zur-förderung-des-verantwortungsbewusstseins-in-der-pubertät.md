---
title: Ihr Kind wünscht sich das neueste iPhone? Unsere Praxistipps zur
  Förderung des Verantwortungsbewusstseins in der Pubertät
description: nachhaltigkeit, verantwortungsbewusstsein, konsum,
  nachhaltigkeitsbewusstsein, erziehung, pubertät
date: 2023-03-24T14:39:25.347Z
translationKey: elternschaft
image: /media/ados-et-smartphone.jpg
---


> *Sie sind umweltbewusste Eltern, die sich einer grüneren und gerechteren Welt verschrieben haben, mit allem was dazu gehört. Sie unternehmen kleine und große Schritte, um die von Ihnen angestrebte Veränderung auch in Ihrem eigenen Mikrokosmos umgesetzt zu sehen. Ihre Kinder werden hingegen von Modetrends, dem sozialen Druck von Freunden und dem Marketing großer Marken wie Apple oder Microsoft beeinflusst. Wenn Sie sich als Eltern vor die Wahl gestellt sehen, ihre eigenen Werte zu vertreten und gleichzeitig Ihren Kindern eine Freude zu bereiten, dann ist dieser Artikel vielleicht genau das Richtige für Sie.* 

### 1/ Jetzt haben wir den Salat...

Es kommt der Tag, an dem Ihr Kind zum Teenager wird. Aus einem Schulranzen wird ein cooler Rucksack. Die Schule verlangt von jedem Kind einen Laptop oder Computer-Zugang und die ANTON-App ist heutzutage wohl jedem Elternteil bekannt. Teenies und Pre-Teenies wollen immer früher ein eigenes Handy, schließlich haben ja auch alle Mitschüler eines, alles im Namen der digitalen Autonomie. Der Schock eines jeden Elternteils – eine Kindheit voll Naturdokus, Gute-Nacht-Geschichten über den Klimawandel und TED-Talks über Kreislaufwirtschaft endet abrupt mit einem von Gruppenzwang gelenkten Wunsch nach dm neuesten Smartphone-Modell. Nun laufen Sie als Eltern Gefahr, die Kontrolle zu verlieren über (1) den Inhalt dessen, was sich Ihr Kind ansieht, (2) was es verbreiten kann (Fotos, Nachrichten,...), auf welchen Websites und mit welchen Gesprächspartnern, (3) wie viel Zeit es mit seinem Smartphone verbringt und so weiter. Und als ob das noch nicht genug wäre, haben alle anderen Kinder auch noch Smartphones großer Marken. Da wünscht sich Ihr Kind natürlich dasselbe, nicht zuletzt, um dazu zu gehören.

### 2/ Seien Sie sich im Klaren über Ihre eigenen Vorstellungen

Zunächst einmal, liebe Eltern, seien Sie sanft zu sich selbst (wir empfehlen hierzu beispielsweise die *[Podcastfolge](https://plus.rtl.de/podcast/family-feelings-mit-marie-nasemann-und-sebastian-tigges-ie29ol5fh7j19/perfektionismus-die-inneren-kritikerinnen-n7tt5fsqa28xc)* “Perfektionismis: Die inneren Kritiker:innen” des Podcast Family Feelings).

Denken Sie daran, dass Sie mit Ihrer vermeintlichen Sackgasse nicht allein sind. Fragen Sie sich, was genau Sie stört. Dieser Artikel soll Sie dazu einladen, die ökologische Dissonanz, die auf die Digitalisierung angewandt wird, zu erkennen und in Worte zu fassen.

All Ihre umweltbewusste Erziehung und Ihre Werte, die Sie mit Ihren Kindern geteilt haben, reichen nicht aus, um dem Verlangen entgegenzuwirken, das durch (1) soziale Konformität (“Ich möchte haben, was die anderen haben”) und (2) die vorherrschende Werbung (Modetrends) geweckt wird.

Sobald Sie genau für sich formuliert haben, wo genau für Sie der Schuh drückt, müssen Sie einen Raum finden, in dem Sie sich mit Ihrem Teenager verbinden können.

### 3/ Bemühen Sie sich um einen Dialog, in dem sich Ihr Kind ernst genommen fühlt

In der Pubertät sind Jugendliche oftmals nur schwer bis gar nicht bereit, Autorität anzunehmen. Wenn man als Eltern dogmatische Einstellung vermitteln oder sogar aufzwingen will, ohne sich darum zu kümmern, wie sie vom Gegenüber aufgenommen wird, ist Erfolg quasi ausgeschlossen.

Das Geheimnis der großen modernen Pädagogen ist die aktive Pädagogik. Die Botschaft hat umso mehr Gewicht, wenn das Kind sie selbst und in seinen eigenen Worten formulieren kann.

Wir raten daher, nicht zu verbieten, sondern einen Raum für Dialog zu schaffen, in dem das Kind seine eigenen Argumente formulieren kann, und zwar so nah wie möglich an dem, was es erlebt.

Wir schlagen hierfür ein Vorgehen in zwei Schritten vor.

#### Schritt 1: Versetzen Sie sich in seine Lage

Versuchen Sie, sich ernsthaft, die Situation Ihres Kindes zu verstehen und die Welt mit seinen Augen zu sehen.

Versetzen Sie sich in seine Lage. Erinnern Sie sich daran, wie wichtig es für Sie in diesem Alter war, in der Gruppe akzeptiert zu werden, Rücksichtnahme, Wertschätzung und Anerkennung als Teenager zu erfahren.

Artikulieren Sie dies klar vor Ihrem Teenager, mit Ihren eigenen Worten, und zeigen Sie ihm, dass Sie verstehen oder zu verstehen versuchen, wie wichtig es für ihn ist, ein Smartphone zu besitzen. 

#### Schritt 2: Laden Sie Ihr Kind ein, sich in Ihre Lage zu versetzen

Fühlt sich Ihr Kind einmal ernst genommen, fragen Sie es, ob es bereit ist, die Welt wiederum mit Ihren Augen zu sehen und den elterlichen Standpunkt zu betrachten. Sprechen Sie konkret über Ihre Beziehung zur ökologischen Bewegung, Ihre Motivation hinter Ihrem Engagement und die Sorgen, die Sie für Ihr Kind und seine Zukunft plagen. 

### 4/ Ein paar gute Argumente to go 

Sobald Sie eine Diskussionsgrundlage und eine Verbindung hergestellt und Ihrem Teenager auch zugehört wurde, können Sie konkret werden. Zeigen Sie zum Beispiel, wie real die Ausbeutung von Kindern in Minen in Teilen Afrikas ist (*[Video](https://www.youtube.com/watch?v=niRtR6NU-aM "https\://www.youtube.com/watch?v=niRtR6NU-aM")*). Zeigen Sie ihm die ökologischen Kosten eines Smartphones.

Entlarven und entzaubern Sie mit ihm die Funktionsweise und die manipulativen Verfahren der modernen Werbung (*[Video](https://www.youtube.com/watch?v=tC1Q4GN6UYU "https\://www.youtube.com/watch?v=tC1Q4GN6UYU")*).

Machen Sie ihm die Möglichkeiten der Sparsamkeit und des "Weniger ist mehr" schmackhaft, wie wohltuend diese Art des Lebens ökologisch, ethisch, menschlich und spirituell sein kann (Empfehlung: Dokumentarfilm Tomorrow – Die Welt ist voller Lösungen).

Und lassen Sie ihn letztlich aber seine eigene Wahl treffen, in voller Verantwortung (und abhängig von Ihrem Geldbeutel...). Wenn er ein Handy wählt, das Ihnen nicht gefällt, sagen Sie sich, dass Sie eventuell einen Samen in Ihrem Kind gesät haben, der noch seine Zeit braucht, zu gedeihen.

### 5/ Die Alternativen sind genauso cool!

Einen Beitrag zur Lösung eines globalen Problems zu leisten, in diesem Fall die katastrophalen Auswirkungen von Elektronik auf den Planeten, ist zwar anders cool, aber *genauso* cool! Wenn Sie es geschafft haben, die Bedeutung des Problems teilweise zu vermitteln, nehmen Sie sich die Zeit, um zu zeigen, dass es auch Lösungen gibt. Lösungen, die sogar noch stilvoller sein können als die Smartphone-Marken der Freunde Ihres Kindes.

Den extremsten Weg gehen Sie, wenn Sie beispielsweise den Worten des Mitschöpfers des Computerspiels *The Witcher*, welches Ihr Kind mit großer Wahrscheinlichkeit zumindest vom Namen kennt, folgen. Der Techie griff zum Mudita Pure, einem einfachen Telefon (kein Smartphone!), welches in Europa hergestellt wurde, auf Langlebigkeit ausgelegt ist und mit einem Open-Source-Betriebssystem ausgestattet ist.

Eine weitere Möglichkeit wäre das Fairphone. Beim Fairphone handelt es sich um ein Smartphone mit Fair-Trade-Metallen, das gegen Kinderarbeit kämpft und modular angelegt, also leicht zu reparieren ist.

### 6/ Fröhliches Abschalten gewährleisten

Peter Lustig hat es schon immer gesagt: “Ihr wisst schon Bescheid: Abschalten!”. Nun, für Eltern gilt schließlich nichts anderes. Ein Smartphone im Besitz des Kindes zu akzeptieren bedeutet nicht, dass man in die Marketingfallen tappen muss, die die Aufmerksamkeit des Smartphonenutzers auf sich ziehen wollen. Sie können gemeinsam mit Ihrem Kind Bildschirmzeiten festlegen, in denen das Smartphone erlaubt oder eben tabu ist, und zwar für alle, Erwachsene und Teenager zugleich! Zum Beispiel bei Mahlzeiten, gemeinsamen Gesellschaftsspielabenden, Waldspaziergängen oder anderen gemeinsamen Aktivitäten, die zu Ihre Familie eventuell gemeinsam unternimmt. Ihr Kind kann Ihnen so helfen, das Abschalten für Sie selbst ebenfalls aufrecht zu erhalten. Wenn es sich in dieser Frage als aktiv und gleichberechtigt wahrnimmt, wird ihm die Teilnahme an der *Digital Detox* umso leichter fallen.
