---
title: Sechs Strategien gegen die geplante Obsoleszenz und ihre Akteure
description: Sechs Strategien gegen die geplante Obsoleszenz und ihre Akteure -
  von der Genossenschaft Commown
date: 2021-05-24T22:24:34.563Z
translationKey: efc
image: /media/sechs-strategien-gegen-die-geplante-obsoleszenz-und-ihre-akteure-main-illu.jpg
---
Wie jedes Jahr endete 2021 mit der Abfolge Black Friday/Weihnachten – einer Zeit, die von übermäßigem Ressourcenverbrauch und einer viel zu hohen CO2-Bilanz geprägt ist. 2022 beginnt dann wieder mit Sonderangeboten, die uns dazu animieren sollen, uns (wenn nicht schon geschehen) grundlos ein neues Gerät anzuschaffen. 2021 führte Commown das [Fairphone 4 Angebot](https://commown.coop/de/unsere-angebote/fairphone/) ein. Für 2022 heißt es nun, Strategien vorzustellen, um gegen die Mechanismen und Akteure der geplanten Obsoleszenz vorzugehen!

Geplante Obsoleszenz! Dieses Thema wird sowohl in der Öffentlichkeit als auch auf politischer Ebene diskutiert. Auf Seiten der Verbraucher wächst der Ärger darüber, Geräte ersetzen zu müssen, weil sie nicht reparierbar sind oder nicht mehr aktualisiert werden. Auf Seiten der Politik widmet sich die Regierung diesem Thema zaghaft im Rahmen eines Gesetzes über die Kreislaufwirtschaft, insbesondere durch die Einführung eines Reparatur-Indexes. 

Auf Seiten der Industrie jedoch ändert sich trotz einiger Ankündigungen (wie Bemühungen beim letzten [surface pro X](https://de.ifixit.com/Teardown/Microsoft+Surface+Pro+X+Teardown/127703)) im Allgemeinen gar nichts! So deckte IFixit ([Reparierbarkeit 0/10](https://de.ifixit.com/Teardown/Teardown+der+AirPods+Pro/127551)) die Unreparierbarkeit der letzten Airpods-Pro oder die sehr schlechte Reparierbarkeit des Galaxy Fold, des berühmten „faltbaren“ Smartphones von [Samsung (2/10)](https://de.ifixit.com/Teardown/Samsung+Galaxy+Fold+Teardown/122600) auf. 

Betrachtet man die Arbeitsbedingungen in den Fabriken, in denen unsere Elektronikgeräte hergestellt werden, bleiben Missstände auch dort weiter bestehen. In diesem Sommer wurde aufgedeckt, dass [Jugendliche von 16 bis 18 Jahren über Monate bei Foxconn dazu gezwungen waren zu arbeiten](https://www.theguardian.com/global-development/2019/aug/08/schoolchildren-in-china-work-overnight-to-produce-amazon-alexa-devices), um Alexa, den persönlichen Assistenten von Amazon zu produzieren. 

„Was können wir tun, um diese Situation zu ändern? 

Commown versucht diese Frage seit 2018 zu beantworten. [Mit der Einführung des Fairphone 4](https://commown.coop/de/unsere-angebote/fairphone/) lohnt es sich, die Grundlagen zu wiederholen!“

## 1 - Marketing-Obsoleszenz bekämpfen!

Bei Commown gibt es das neue Fairphone! Schnell, schmeißen Sie ihr altes und verantwortungsloses Smartphone weg und stürzen Sie sich auf dieses ethische Mietangebot! Für 29,60 pro Monat erhalten Sie einen Premium-Service und ein Smartphone, das endlich mit Ihren Werten im Einklang ist. 

Das hier ist ein perfektes Beispiel für Marketing-Obsoleszenz! 

Es soll Kunden dazu bringen, sich unter dem Vorwand eines neuen, attraktiveren Angebots ein neues Gerät anzuschaffen. Das Verb „wegschmeißen“ verdeutlicht die enorme Verschwendung, die sich hinter jeder „Neuheit“ und Marketing-Kampagne verbirgt. 

Daher liegt es auf der Hand, dass die Einführung des Fairphone 4 bei Commown mit dieser Praxis nichts zu tun hat! Zur Erinnerung: Das verantwortungsvollste Smartphone ist jenes, das noch funktioniert und täglich benutzt wird! 

Aus diesem Grund bietet Commown einen [Gutschein](https://shop.commown.coop/shop/product/bon-de-consomaction-differee-186) an, mit dem man die Kooperative sofort unterstützen und für den Tag vorsorgen kann, an dem BEDARF bestehen wird. Dieser Gutschein kann auch ein immaterielles Geschenk für einen Menschen sein, der bald ein Elektronikgerät BRAUCHEN könnte.

## 2 - Eigene Geräte so lange wie möglich nutzen!

Um es noch einmal zu betonen: Das umweltfreundlichste Produkt ist jenes, das funktioniert und das man bereits besitzt! 

Dieser Kampf um die Verlängerung der Lebensdauer elektronischer Produkte wird bei Commown durch die Aufrechterhaltung der Serviceleistungen an der Flotte von Fairphones 2 und Fairphones 3, die aktuell im Umlauf sind, erreicht. Die Genossenschaft arbeitet an Software- und Hardware-Lösungen, um die Lebensdauer der Fairphones zu verlängern. 

## 3 - Immer mehr kohärentes Handeln verlangen. 

In Zeiten des Green Washing und des Social Washing ist es unabdingbar, dass Verbraucher selbst aktiv werden und mehr kohärentes Handeln verlangen. Bei Unternehmen kann diese Suche nach Kohärenz nur durch die Bemühung um Transparenz gelingen. Deshalb hat sich Commown dazu entschlossen, seine derzeitigen Inkohärenzen aufzuzeigen und als Möglichkeiten zur Verbesserung zu betrachten. Die vollständige Auflistung finden Sie [hier](https://commown.coop/de/wer-sind-wir/). 

Unser Ziel besteht darin, kohärent zu sein. Durch eine minimalistischere und somit energieeffizientere Gestaltung unserer Website commown.coop und die Ersetzung von Google Analytics durch Matomo konnten wir dieses Ziel in diesen Bereichen bereits erreichen!

Kohärenz: Garant für Vertrauen!

Inkohärenz: Garant für Transparenz!

## 4 - Akteure bevorzugen, die kollektives Interesse vor Profit stellen

Viele Unternehmen bieten „grüne“ Angebote an und nicht immer ist es einfach, den Durchblick zu behalten. Im Fall Stromerzeugung gibt es zum Beispiel auf der einen Seite Direct Energie (eine Filiale von Total), dessen Ziel es ist, dem französischen Ölkonzern ein grünes Image zu verpassen, dessen Aktionäre jedoch weiterhin unverschämt hohe Dividenden kassieren. Auf der anderen Seite gibt es Enercoop, eine gemeinnützige Genossenschaft (SCIC), die ausschließlich auf erneuerbare Energien zurückgreift und dessen Satzung den Vorrang des Gemeinwohls gewährleistet.

Im Bereich Elektronikgeräte gibt es mehrere solche Unternehmen, die auf eine Verlängerung der Nutzungsdauer von Geräten und soziale Wiedereingliederung setzen. So kann man beispielsweise der Genossenschaft Label Emmaüs (SCIC) vertrauen.

Zu diesem Zweck hat sich Commown von Beginn an für die Unternehmensform einer SCIC, einer gemeinnützigen Genossenschaft französischen Rechts, entschlossen. Nutzern der Dienstleistung wird durch die Satzung gewährleistet, dass die Genossenschaft immer versucht, das beste Angebot zu einem „fairen Preis“ zu machen. So hat Commown im Rahmen seines Fairphone 4 Angebotes seine Angebotspalette beispielsweise überarbeitet. Die Genossenschaft setzt alles daran, selbst im Angebotsaufbau das kollektive Interesse greifbar zu machen.

Ein Angebot über die verbindliche Nutzung eines Gerätes über 12 Monate profitiert vom Engagement und der Verantwortung der Nutzer und setzt den Anreiz, das Gemeingut – in diesem Fall die Fairphone-Flotte – so lange wie möglich zu erhalten. Die Herausforderungen sind vielfältig. Sie reichen von einer Verlängerung der Lebensdauer der Geräte zu einer breiten Bekanntmachung des Angebots. Genossenschaftlich gibt es viele Möglichkeiten, an diesem Erlebnis teilzuhaben. Werden die Herausforderungen von Commown erfolgreich bewältigt, wird mehr Gewinn erzielt, was sich schließlich in einer Senkung der Preise niederschlägt. Die Commowner, die am sorgfältigsten mit Ihrem Gerät umgehen, haben dabei am meisten zu gewinnen.

## 5 - Engagierte Vereine und Kollektive unterstützen

Es genügt nicht, Alternativen vorzuschlagen oder sich ihnen anzuschließen. Der Sektor für verantwortungsvolle Elektronikgeräte steckt noch in den Anfängen und es ist strategisch wichtig, Druck auf mehreren Ebenen auszuüben. Es ist von entscheidender Bedeutung, Vereine wie [Open Source Ecology](http://opensourceecology.org/) Germany e.V. oder Cradle to Cradle NGO zu unterstützen, die in diesem Bereich über eine große Expertise verfügen und Lobbyarbeit leisten.

Commown arbeitet seit über einem Jahr vor allem beim Thema Gesetz über die Kreislaufwirtschaft mit ihnen zusammen, um dieses Anliegen bei den öffentlichen Stellen und Behörden voranzubringen.

Neben einer Wende im Elektroniksektor im weitesten Sinne muss jede Gelegenheit ergriffen werden, als Bürger oder engagiertes Unternehmen Fürsprache zu halten. 

## 6 - Boykottieren, Widerstand leisten, ungehorsam sein!

Der Klimawandel wartet nicht und die im Bereich Elektronik in Gang gesetzten Veränderungen sind in vielen Bereichen nicht schnell genug, um den übermäßigen Ressourcenverbrauch zu stoppen. Jedes Jahr leben wir auf Umweltkredit, in Frankreich verbrauchen wir ab dem Monat Mai mehr Ressourcen, als die Erde uns geben kann. Die Elektronikindustrie macht einen großen Teil dieses übermäßigen Konsums aus. Zur Erinnerung: Man benötigt über 70 kg Rohstoffe, um ein einziges Smartphone herzustellen – und wir produzieren über eine Milliarde Smartphones pro Jahr!

Aus diesem Grund ist es wichtig, den Wandel voranzutreiben. Dafür bieten sich mehrere Optionen:

1. Kampagnen von Verbänden wie [Right to repair](https://repair.eu/de/) unterstützen.
2. An Aktionen zivilen Ungehorsams teilnehmen, um über die Medien Druck auf Politik und Unternehmen auszuüben. Die Mobilisierung gegen Amazon am Black Friday ist ein gutes Beispiel dafür.

Gleichzeitig ist es für Akteure aus der Sozial- und Solidarwirtschaft wie Commown wichtig, sich auf ihrer Ebene zu mobilisieren. Nur so können sie sich mit ihrer diametral entgegengesetzten Vision von anderen Akteuren abheben, die auf Verkaufsmaximierung spezialisiert sind und so die Bedeutung von Schlichtheit und Widerstandsfähigkeit außer Acht lassen.