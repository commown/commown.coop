---
title: "FairTEC: ein Schritt weiter auf dem Weg zum Paradigmenwechsel!"
date: 2022-01-08T19:44:36.931Z
translationKey: fairtec
image: /media/fairtec2-compress.jpg
---
*"Ohne eine grundlegende Veränderung wird die Digitaltechnik bald in eine existenzielle Krise stürzen wie jene, in der sich der Luftverkehr heute befindet. Das Einschlagen eines neuen Weges durch die Kooperation von Akteuren, die sich auf europäischer Ebene engagieren, ist dabei von entscheidender Bedeutung.” (Adrien Montagut, Mitbegründer von Commown). Dieser neue Weg wurde mit FairTEC nun eröffnet.*

**Was ist [FairTEC](https://fairtec.io/de/)?**

Kurz gesagt ist FairTEC ein von Commown vermietetes Fairphone mit dem Open-Source-Betriebssystem /e/OS und einem Mobilfunktarif von TeleCoop (in Frankreich), WEtell (in Deutschland) oder Your Coop (in Großbritannien). FairTEC ist das bislang nachhaltigste und fairste Angebot auf dem europäischen Markt.

**Warum FairTEC ?**

Hinter den Vorteilen der Technologien verbergen sich auch Schattenseiten.

Diese Schattenseiten bestehen aus der Überproduktion von Geräten, dem Abbau seltener Rohstoffe in armen Ländern durch Männer, Frauen und Kinder unter menschenunwürdigen Bedingungen, dem Verlust der Biodiversität, den Clickworkern, der Nomophobie (und ihren Folgen), dem Monopol der GAFAM-Konzerne und vielem mehr.

Angesichts solch düsterer Aussichten forderte ein Arzt den drastischen und sofortigen technologischen Entzug. Leider fällt es vielen Menschen heutzutage schwer, auf ihr Smartphone zu verzichten… Wie kann hier Abhilfe geschaffen werden?

→ Indem die Lebensdauer der Geräte maximal verlängert wird!

**Wer ist FairTEC?**

FairTEC ist eine Kooperation europäischer Unternehmen, die gemeinsam nachhaltigere und fairere Lösungen für Smartphones entwickeln und anbieten. Sie besteht aus sechs Akteuren, die sich für einen Wandel zur Nachhaltigkeit einsetzen.

Das[ Fairphone](https://www.fairphone.com/de/) ist das bislang umweltfreundlichste Smartphone. [TeleCoop](https://telecoop.fr/) ist der erste französische genossenschaftliche Mobilfunkanbieter, der sich für den ökologischen und solidarischen Wandel einsetzt und einen einfachen, jederzeit kündbaren Vertrag zu einem erschwinglichen Preis anbietet. Ebenso engagiert sind auch die europäischen Mobilfunkanbieter [WEtell](https://www.wetell.de/) und [Your Coop](https://broadband.yourcoop.coop/). [/e/OS](https://e.foundation/de/) ist ein auf Android basierendes Open-Source-Betriebssystem, aus dem sämtliche Spuren von Google entfernt wurden und das die Privatsphäre des Nutzers so effektiv schützt. Unsere Kooperative stellt das Fairphone mit Serviceleistungen zur Verfügung, um gegen geplante Obsoleszenz vorzugehen: Kostenübernahme bei Ausfällen, Austausch des Akkus wenn nötig, Schutz vor Bruchschäden, usw.

**Warum ist FairTEC für Commown so wichtig?**

Wir beteiligen uns aus zwei Gründen am Projekt [FairTEC](https://fairtec.io/de/). Zum einen ist es der erste Schritt zu einem alternativen europäischen Komplettangebot. Zum anderen trägt es zur Demokratisierung des “Fairphone as a Service”-Konzepts bei, dessen Ziel es ist, die Lebensdauer von Geräten durch die Lossagung vom Geschäftsmodell des Verkaufs zu verlängern. Bei Commown geht es uns in erster Linie um eine möglichst lange Lebensdauer der Geräte, wodurch ökologische und soziale Auswirkungen verringert werden. Mit dem Geschäftsmodell des Verkaufs ist dieses Ziel nicht zu erreichen. Um zu überleben, sind Unternehmen regelrecht gezwungen, immer mehr zu verkaufen und mit Marktgesetzen zu jonglieren, die auf der Obsoleszenz von Hardware, Software und Marketing basieren.

Daher glauben wir, dass die Kooperation mit den anderen Akteuren von FairTEC uns dabei hilft, das “Fairphone as a Service”-Modell breiter bekannt und zugänglich zu machen.