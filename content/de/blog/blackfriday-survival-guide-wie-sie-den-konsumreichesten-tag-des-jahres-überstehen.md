---
title: "#BlackFriday Survival Guide - wie Sie den konsumreichesten Tag des
  Jahres überstehen"
date: 2021-11-26T13:41:11.097Z
translationKey: kit de survie blackfriday
image: /media/kermit-g460a9abd3_640-2-.jpg
---
Ihnen fehlt das Grundwissen und jede Kaufentscheidung bereitet ihnen quasi “nachhaltige” Kopfschmerzen? Wir helfen Ihnen! Diese einfache Methode soll Ihnen helfen ein geübter Verbraucher zu werden und besteht zu dem ausschließlich aus 2 Schritten! Wir werden Ihnen die Methode anhand eines Fallbeispiels näher bringen, das mit nachhaltiger Elektronik zu tun hat: das Beispiel eines umweltfreundlichen Smartphones!Machen Sie sich bereit!

**Der erste Schritt: Das Hinterfragen**

Am Anfang stand… ähm die Frage?!\
Für uns ist es offensichtlich, dass Zurückhaltung immer die klügste Wahl ist, wenn es um nachhaltige Elektronik geht. Unser Slogan bei Commown lautet: “Das umweltfreundlichste Produkt ist das, das wir bereits besitzen”. Daher bitten wir alle Verbraucher*innen, den Kauf eines elektronischen Produktes zu hinterfragen. Wir halten es für zwingend notwendig, so ehrlich wie möglich nach den Gründen zu fragen, warum wir ein elektronisches Produkt nutzen wollen. Im Französischen gibt es die sogenannte BISOU-Methode (Bisous ist die französische Begrüßung - ein Bussi quasi.) Wir stellen Ihnen nun die grundlegenden Fragen der französischen Methode, angepasst an den deutschen BUSSI vor:

**B wie Bedarf** 

*Brauche ich das Produkt wirklich? BIn ich mir sicher, dass es der Ursprung meines Verlangens ist, oder habe ich Werbung gesehen, die dieses Verlangen künstlich erzeugt hat?*

**U wie Ursprung**\
*Woher stammt das Produkt? Von wem kaufe ich es?*

**S wie “so ähnlich”**\
*Habe ich nicht bereits ein gleichwertiges Produkt, das so ähnlich ist wie das Produkt, welches ich erwerben möchte und das diese Bedürfnisse erfüllen könnte?*\
\
**S wie sachdienlich**\
*Kann ich auf das Produkt verzichten? Wozu wird es wirklich gebraucht?*\
\
**I wie In dem Moment**\
*Benötige ich das Produkt genau in diesem Moment? Kann ich mir Zeit zum Nachdenken nehmen?*\
\
Wenn ich merke, dass ich das Produkt unbedingt brauche, z.B. einen Computer, um mich zu informieren und zu arbeiten, oder ein Smartphone, um mit anderen kommunizieren zu können, dann kann ich zum zweiten Schritt übergehen...

**Schritt 2 : Die Wahl des Produktes: das Beispiel eines Smartphones**

Welches Angebot ist möglichst das umweltfreundlichste? Wie trennt man die Spreu vom Weizen? Wie navigiert man durch das Meer von Angeboten, von denen eines grüner ist als das andere (zumindest in der Rhetorik)?

Denken Sie an diese beiden Wörter: *HINTERFRAGEN* und *AUSSUCHEN*.

**HINTERFRAGEN**: Wie ein Detektiv sollte man sich über Unternehmen informieren, um Marketingfallen wie Greenwashing zu entgehen. Wie geht man konkret vor, um Nachforschungen anzustellen?

* Geben Sie, um Ihre Daten zu schützen, "Name des Herstellers + Meinung" in eine umweltfreundliche Suchmaschine wie Lilo und Ecosia oder Duckduckgo und Qwant ein.
* Indem Sie das Internet durchforsten, um zu recherchieren, ob das Unternehmen "echt" ist (im Handelsregister eingetragen), ob es seriös ist, ob es in Deutschland ansässig ist (was nicht ausschließt, dass es eventuell Steuerhinterziehung betreibt), ob es Gütesiegel erhalten hat, ob es in ein Wirtschaftssystem eingebunden ist, ob die Produkte mit den Äußerungen übereinstimmen, ob die Projektträger tatsächlich existieren usw.
* Achten Sie darauf, welche Art von Rechtsform und Satzung gewählt wurde und ob diese öffentlich ist? Wenn ja, wie stark ist das Kapital und wie ist die Unternehmensführung unter den verschiedenen Anteilseignern aufgeteilt?
* Indem Sie die Kommentare auf verschiedenen Websites (die sogenannte Online-Reputation) unter die Lupe nehmen und versuchen, diese mit dem zu vergleichen, was die Organisation erzählt.

**AUSSUCHEN**: Nachdem Sie recherchiert haben, kommt der Moment, in dem Sie sich entscheiden müssen. Unserer Meinung nach ist es die richtige Strategie, für ein Produkt zu entscheiden, das mit (1) Ihrer Ethik und (2) Ihren finanziellen Mitteln vereinbar ist.

Wenn wir also das Beispiel des Smartphones aufgreifen: 

* Wenn ich mich auf eine möglichst umweltfreundliche Smartphone-Marke festlegen möchte, kann ich mich bei Fairphone umschauen.
* Wenn ich ein Angebot unterstützen möchte, das dem Konsumdenken ein Ende setzt und die Entstehung eines grundlegend kreislauforientierten Modells fördert, kann ich mich über das HaaS-Modell von Commown informieren. 

Sie entscheiden! Bleiben Sie kritisch und passen Sie auf sich auf! :)