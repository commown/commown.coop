---
title: "Verantwortungsvolle Elektronik vs. Verantwortung der Elektronik "
date: 2022-05-31T08:48:49.118Z
translationKey: électronique responsable
image: /media/artificial-intelligence-ge9c0b1a1a_1920-1-.jpg
---
Es entstehen immer mehr neue Alternativen, die es ermöglichen, die Auswirkungen unseres Lebensstils auf die Umwelt zu verringern. Commown ist eine davon, denn die Kooperation schlägt vor, die riesige Herausforderung der Elektronikbranche anzugehen.

Warum ist dieser Sektor heute noch nicht verantwortungsbewusst? In diesem Artikel wird es versucht, diese Frage zu beantworten.

**Ein paar allgemeine Zahlen**…

Im Rahmen der letzten Statistik aus dem Jahre 2019, zum Thema Umweltauswirkungen der Digitalisierung, hat das franzöische Amt für Statistiken, [Insee](https://de.wikipedia.org/wiki/Institut_national_de_la_statistique_et_des_%C3%A9tudes_%C3%A9conomiques) (Institut national de la statistique et des études économiques – dt.: Nationales Institut für Statistik und Wirtschaftsstudien), folgende Zahlen veröffentlicht: „Im Jahre 2011 besaßen 17 % der französischen Bevölkerung ein Smartphone, 2018 waren es bereits 75 %.“ Innerhalb von sieben Jahren ist der Teil der französischen Bevölkerung, der ein Smartphone besitzt, von weniger als ¼ auf ¾ gestiegen. Gleichzeitig wurden 2019 laut [IDC](https://www.idc.com/de/about-idc) weltweit rund 1,373 Milliarden Smartphones verkauft. Zu beachten ist jedoch, dass IDC bis 2024 einen Anstieg von 7% prognostiziert. Diese Zahlen berücksichtige jedoch nicht den angekündigten explosionsartigen Anstieg der Zahl der „vernetzten Geräte“, die dank 5G auf den Markt kommen werden.

**Warum sind diese Zahlen so besorgniserregend?** 

Laut einer Studie von GreenIT, einer französischen Gemeinschaft von Akteuren, die sich für einen verantwortungsbewussten Umgang mit Technologien einsetzen, sei die Digitalisierung für rund 4% der Treibhausgase verantwortlich.  In Frankreich haben sich die Treibhausgas-Emissionen in den letzten 10 Jahren verdreifacht. Die Digitalisierung hat also einen Einfluss auf die Umwelt, auch wenn dieser meist verborgen bleibt.. Es ist beispielsweise nicht ersichtlich, dass bei der Herstellung eines Smartphone, das am Ende wenige hundert Gramm wiegt, Materialien von bis zu 70 Kilogramm (⅔ Rohstoffe) benötigt werden.Außerdem wird für die Herstellung von Elektrogeräten viel Süßwasser benötigt. In Taiwan, dem führenden Land der Halbleiter-Herstellung, haben die Behörden, nach einem Jahr der Dürre, die Bewässerung von landwirtschaftlichen Anbauflächen verboten, um das Wasser für die Elektroindustrie aufzusparen.Manchmal muss man sich entscheiden der Herstellung von Essen und Prozessoren…

**Menschliche Auswirkungen im Zusammenhang mit der Herstellung von elektronischen Geräten**

Wir haben festgestellt, dass der Sektor der Elektronikindustrie eine große Gefahr für die Umwelt darstellt. Wenn man den Faden weiterspinnt, bedeutet eine Gefahr für die Umwelt, gleichzeitig eine Gefahr für die Artenvielfalt und schlussendlich auch eine Gefahr für die Menschheit. So ist beispielsweise bekannt, dass der Klimawandel ganze Regionen aus dem Gleichgewicht bringen und zur Vertreibung von Klimaflüchtlingen führen wird. Es wird jedoch oft vergessen, dass für die Herstellung von elektronischen Geräten - seit vielen Jahren - Erwachsene und Kinder in Afrika und Asien ausgebeutet werden. Gleichgültig, ob es darum geht, die Rohstoffe aus dem Boden zu holen oder die verschiedenen Teilstücke zusammenzusetzen - diese Arbeitskräfte werden ausgebeutet, unterbezahlt und misshandelt.Ihre miserablen Arbeitsbedingungen sind eine direkte Folge des übermäßigen Konsums und der reichen kapitalistischen Unternehmen, die sich nicht scheuen, ihre Kommunikation unter dem Vorwand des Social Washing zu tarnen (siehe  [hier](https://www.youtube.com/watch?v=U5Yb7REe2UM&t=9s) , [hier](https://www.amnesty.org/en/latest/campaigns/2016/06/drc-cobalt-child-labour/), und [hier](https://www.spiegel.de/wirtschaft/service/apple-samsung-und-co-kinderarbeit-in-kobaltminen-im-kongo-a-1072704.html), etc.).\
\
**Weitere externe Effekte der Digitalisierung**

Obwohl in der Produktionsphase eines Smartphones die stärksten sozialen und ökologischen Auswirkungen entstehen, sind auch weitere Auswirkungen auf den Menschen zu vermelden… denn in der westlichen Welt brachte die Digitalisierung nicht nur Vorteile mit in unsere Gesellschaft. Wir stellen fest, dass die Digitalisierung und unser Umgang mit Bildschirmen im Allgemeinen oftmals weit davon entfernt sind, uns aus unseren Auflagen zu befreien, auch wenn dies im Gegensatz zu dem steht, was man in den Medien oder bestimmten Kreisen über die emanzipatorischen Vorzüge der digitalen Revolution hören kann. Darauf werden wir allerdings in einem anderen Artikel ausführlicher eingehen. 

**Was kann man tun, um eine verantwortungsvollere Elektronik anzustreben?** 

Es gibt eine Vielzahl an politischen Maßnahmen "des gesunden Menschenverstands", die schnell umgesetzt werden sollten... zum Beispiel:\
(1) Verbot der Werbung für elektronische Produkte\
(2) Verpflichtung zur markenübergreifenden Standardisierung von Ersatzteilen\
(3) Begrenzung der Anzahl neuer Modelle, die von den Marken entwickelt werden, z. B. zunächst 2 Modelle alle 5 Jahre, danach 1 Modell alle 10 Jahre\
(4) Freiheit, auf jedem Gerät ein freies OS zu installieren\
(5) Erhöhung der gesetzlichen Garantiezeit und der Verfügbarkeit von Ersatzteilen auf (zunächst) 7 Jahre , etc. 

Commown setzt sich für all diese Maßnahmen ein, aber bislang sind sie in den Treffen der Lobbyisten, an denen wir teilnehmen, noch nicht allgemein anerkannt worden. Ebenfalls setzen wir uns für einen Konsumverzicht ein, um all die negativen externen Effekte der Digitalisierung zu reduzieren und unseren Planeten zu schützen. Bevor Sie bei uns ein Angebot abschließen, sollten Sie sich übrigens unbedingt fragen, was Sie wirklich brauchen: "Funktioniert mein Gerät noch?", "Kann ich es reparieren?", "Kann ich auf ein Smartphone verzichten?" usw. Letztendlich ist das verantwortungsvollste Gerät, das es gibt, das Gerät, das Sie bereits besitzen!