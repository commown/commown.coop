---
title: Das Shift6mq, vorgestellt von Commown, der Kooperative für nachhaltige
  Elektronik
description: Die Kooperative für nachhaltige Elektronik Commown bietet ab sofort
  das Shiftphone 6mq an
date: 2023-02-28T14:14:49.549Z
translationKey: SHIFTPHONE blog article
image: /media/das-shift6mq.png
---
**Kompakt zusammengefasst: Was kann das Shift6mq?**

*Argument 1: Exemplarisch für Ökodesign*

Das Shift6mq steht exemplarisch für Ökodesign. Die hohe Modularität durch verschiedene, leicht auseinandernehmbare Module macht das Gerät einzigartig. Neugierig geworden? Sollten Sie Bedarf an einem einzelnen Modul haben, schickt Ihnen unser Suport-Team ein Ersatzmodul, das Sie kinderleicht auswechseln können. Unser Ziel der geplanten Obsoleszenz den Kampf anzusagen, geht durch die Verlängerung der Lebensdauer der Geräte aufgrund des hohen Reparaturindexes auf und verringert ganz nebenbei unseren ökologischen Fußabdruck.

![Das Bild stellt die einzelnen Teile des SHIFTPHONES dar. Modularität und Nachhaltigkeit spiegeln sich in den individuellen Teilen des Gerätes wieder. Der Artikel stellt drei Argumente vor die dich von dem SHIFT6mq überzeugen. Das SHIFT6mq kann bei Commown zu einem degressiven monatlichen Preis mieten.](/media/shiftphone-6mq.png "Das SHIFT6mq von dem deutschen Unternehmen SHIFTPHONE ist nicht nur modular sondern auch nachhaltig. Angezeigt wird das neueste Modell von SHIFTPHONE welches man nun auch bei Commown mieten kann.")

*Argument 2: Recycling*

Die hohe Modularität ist nicht der einzige Punkt, der beim Shiftphone überzeugt, denn das Shift6mq ist elektroschottpositiv. Für jedes verkaufte Shiftphone 6mq wird ein altes defektes Gerät aus dem globalen Süden nach Europa gebracht, um fachgerecht recycelt zu werden. Ermöglicht wird das durch eine Kooperation mit *[Closing the Loop](https://www.closingtheloop.eu/ "https\://www.closingtheloop.eu/")*. Die aus dem Recyclingvorgang wiedergewonnenen Rohstoffe kommen unter anderem wieder in Shiftphones zum Einsatz.

*Argument 3: Weniger ist mehr*

Das Shift6mq folgt ganz unserer Devise der Genügsamkeit. Bei diesem Modell geht es nicht darum, sechs Onboard-Kameras, 5G oder ein „Borderless‟- Display zu haben. Das Shiftphones 6mq basiert auf bewährten Technologien, die die Erwartungen von Nutzenden, die ein verantwortungsvolles, leistungsfähiges und zuverlässiges Smartphone haben möchten, so gut wie möglich erfüllen.

Diese Design-Entscheidungen, die vielleicht unbedeutend erscheinen mögen, stellen das Engagement von Shiftphone unter Beweis. Auf einem Markt, auf dem das Motto „immer mehr“ gilt, ist die Entscheidung gegen diese Marketing-Sackgassen ein wirklich wichtiger Wandel.

Unter diesem *[Link](https://shop.commown.coop/de_DE/shop/product/shift-6mq-mit-serviceleistungen-essenziell-mit-zweifach-schutz-schutzhulle-und-displayschutz-397 "https\://shop.commown.coop/de_DE/shop/product/shift-6mq-mit-serviceleistungen-essenziell-mit-zweifach-schutz-schutzhulle-und-displayschutz-397")* werden die technischen Details des Shift6mq genauer vorgestellt. 

**Das Unternehmen hinter SHIFT kurz vorgestellt**

Beim Unternehmen SHIFT lohnt es sich hinter die Kulissen zu schauen. Im Jahr 2014 schlossen sich der Designer und Erfinder Carsten Waldeck mit seinem Bruder, dem Mediengestalter Samuel Waldeck  und dem gemeinsamen Vater, Rolf Waldeck, zusammen, um die SHIFT GmbH zu gründen. Diese ist aus verschiedenen Crowdfunding-Projekten heraus entstanden, was die Unabhängigkeit von Grossinvestoren stets gesichert hat. Die Firmeneigentümer setzen auf Expertise aus der eigenen Community. Im hessichen Falkenberg, das 30km südlich von Kassel liegt, wurde von Anfang an groß und transformatorisch gedacht: Bei der Produktion der elektronischen Geräte und dessen Zubehör sollte, anders als bei den Big Playern der Branche, in allerster Linie der Mensch und die Umwelt im Vordergrund stehen. Durch den modularen Aufbau der Shiftphones ist es für die Nutzenden leicht, die Geräte zu öffnen und diese dadurch einfach wie auch kostengünstig durch den Austausch einzelner Module zu reparieren. Das hehre Ziel, die Lebensdauer der Gerätschaften zu maximieren, haben Shift und Commown somit gemeinsam.

Shift identifiziert sich zu 100% als “Social Business”. Statt gewinnorientiertem Handeln, steht hier das Sinnstiftende im Vordergrund. Sowohl im Bereich der Rohstoffgewinnung, als auch in der Lieferkettentransparenz wie auch in arbeistrechtlicher Hisicht, Shift setzt auf Fairness und verändernde Maßnahmen.So wird in der SHIFT-eigenen Produktionsstätte in Handgzhou, China, den Angestellten ethisch vertretbare Arbeitsbedingungen – wie eine 40-Stunden-Woche – sowie eine faire Entlohnung garantiert.

Bezüglich des  Aspekts der Nachhaltigkeit und der ethischen Richtlinien der Smartphones kann das SHIFT ebenso punkten. Die Modelle werden mit nachhaltig beschafften und abgebauten Rohstoffen hergestellt und verfügen über eine transparente Lieferkette. Für die Herstellung der Hauptplatine verwendet SHIFT beispielsweise die umweltfreundliche und bleifreie „ECO Solder Paste“ des japanischen Herstellers Senju Metal. SHIFT hat zudem seine gesamte Lieferkette bis Tier 3 in seinem *[Wirkungsbericht ](https://www.shiftphones.com/downloads/SHIFT-wirkungsbericht-2019-05-10.pdf "https\://www.shiftphones.com/downloads/SHIFT-wirkungsbericht-2019-05-10.pdf")*veröffentlicht.

SHIFT ist seit April 2018 Mitglied der Gemeinwohlökonomie (GWÖ). Die GWÖ definiert die Erhöhung des Gemeinwohls als oberstes Unternehmensziel, noch vor finanziellen Gewinnen. Die verwendeten Materialien werden soweit möglich aus verantwortungsvollen Quellen bezogen.

**Unser Angebot**

Der SHIFT6mq der Marke Shift kann für Privatpersonen bei Commown ab 28,90€/Monat im ersten Jahr gemietet werden, danach sinkt der Mietpreis auf 11,56€/Monat im fünften Jahr. Diese gestaffelte Gebühr soll Sie dazu ermutigen, Ihre Mietgeräte so lange wie möglich zu behalten und sie bei Bedarf zu reparieren.

Wenn Sie bei Commown mieten, müssen Sie keine Reparaturen (außer Diebstahl, Bruch und Wasserschäden) selbst durchführen, da die Kosten dafür und die technische Durchführung vollständig in der monatlichen Gebühr enthalten sind.