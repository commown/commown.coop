---
title: "Commown: unsere Glaubenssätze"
date: 2022-03-31T12:37:33.687Z
translationKey: faith
image: /media/commown-les-10-commandements.jpg
---
Große Einflussnahme bedeutet ... große Verantwortung ... Die Schaffung einer rechtlichen Struktur verleiht eine gewisse Einflussmöglichkeit, daher ist es unsere Pflicht, ethische Regeln zu befolgen und diese zu teilen (und sei es nur, um mit gutem Beispiel voranzugehen!). Aus diesem Grund finden Sie hier die 10 Gebote unserer Genossenschaft.

**Die 10 Gebote von Commown**

* **Der Ökologie sollst Du dienen**

Die Ökologie ist Commowns Kompass, sie ist der Sinn des Unterfangens, das das Unternehmen verfolgt. Wenn es seine Auswirkungen auf die Umwelt reduzieren und ökologische Werte teilen kann, dann wird es das tun.

* **Gegen die Machthaber, die Ungerechtigkeit und die Umweltverschmutzung wirst du kämpfen**

Wir wissen, dass kleine Gesten eine gute Sache sind, aber (leider!) werden sie nicht ausreichen, um die sich anbahnende Umweltkrise zu lösen. So müssen wir uns - in unserem Maßstab und mit den uns zur Verfügung stehenden Möglichkeiten - am Kampf gegen die Macht des Marktes, des Geldes und der multinationalen Konzerne beteiligen, die (größtenteils) die Umwelt beeinträchtigen und für das Leid vieler Menschen auf der Welt verantwortlich sind.

* **Du sollst deine Neugierde nutzen**

Neugier ist der Wunsch, etwas zu erfahren und zu wissen. Ein französisches Sprichwort besagt, dass "Neugier ein hässlicher Fehler ist". Für uns ist sie vielmehr eine wesentliche Eigenschaft. Bei Commown wird sie auf vielfältige Weise kultiviert: durch die Entdeckung der Natur (z. B. ein Waldspaziergang um essbare Pflanzen zu entdecken), durch kulturelle Entdeckungen (es ist immer schön, einen guten Film zu sehen, einen Roman zu lesen oder eine neue Aktivität auszuprobieren) oder auch durch die Überprüfung von Informationen auf ihren Wahrheitsgehalt (in einer Welt des Greenwashing und der "Fake News" suchen wir gerne nach dem Kleingedruckten, um die Spreu vom Weizen zu trennen)

* **Vernunft und gesunden Menschenverstand sollst Du gebrauchen**

Die Vernunft ist eine Fähigkeit des Denkens, die es uns normalerweise ermöglicht, die Dinge richtig zu beurteilen. Die Erfahrung zeigt uns, dass die Vernunft (allein) nicht immer ausreicht, sondern dass sie durch eine Portion gesunden Menschenverstand ergänzt werden muss. Konkret geht es darum, vor dem "Kauf" nachzudenken, sich zu fragen, bevor man etwas kauft, nüchtern zu denken und zu versuchen, die Probleme an der Wurzel zu packen (natürlich reden wir hier nicht davon, Bäume zu fällen!).

* **Mit Gewaltlosigkeit sollst du dich bewaffnen**

Die Gewalt, von der wir sprechen, ist moralisch und richtet sich oft gegen sich selbst oder andere. Wir versuchen bei Commown - so gut wir können - eine gewaltfreie Haltung gegenüber uns selbst, aber auch gegenüber anderen einzunehmen. Wie der Psychiater Christophe André sagt: "Tu dir nicht weh ... Das Leben wird das für dich erledigen.

* **Die Ethik sollst du schätzen**

Was soll ich tun? Das ist die ethische Frage schlechthin. Der Versuch, über diese Frage nachzudenken, ist bereits ein ethischer Schritt. Commown fragt sich, ob sein Handeln richtig ist und wie sich unsere Kommunikation auf die Umwelt und die Gesellschaft auswirkt.

* **In Demut sollst du dich hüllen**

Im römischen Zeitalter wurden die Siege der Generäle mit einer großen Zeremonie gefeiert. So zog ein General auf seinem Wagen durch Rom, aber er war nicht allein, sondern ein Sklave stand neben ihm und sagte immer wieder: "Respice post te! Hominem te esse memento!", d. h. "Schau dich um und denk daran, dass du nur ein Mensch bist!".

* **Die Emanzipation sollst du anstreben**

"Wenn man das Licht in die Welt bringen will, dann nur, damit alle es sehen können", sagt König Artus in der Fernsehserie Kaamelott (von Alexandre Astier). Commown träumt von einer Welt, in der alle Menschen selbstbestimmter\* und glücklicher\** sein können. Daher versuchen wir, unseren Nutzern dabei zu helfen, sich von der Werbung und ihren Bildern zu befreien. In der Werbung wird die Idee des Glücks häufig auf den Kauf eines bestimmten Produkts reduziert.

* **Das Wohlwollen sollst du umarmen**

"Zunächst einmal nicht schaden" stammt aus der medizinischen Ethik und es schien uns wichtig, darüber zu sprechen. Es ist nicht einfach, allen Menschen Gutes zu wünschen, aber der Versuch, nicht zu schaden, kann bereits ein Schritt in Richtung des Weges sein, auf den wir uns zu begeben versuchen.

* **... Drück nur auf die Klinke und die Tür springt auf**

! wie der böse Wolf zum Rotkäppchen sagt. Habt viel Humor - denn Lachen ist gesund... 

\*Autonomie: in dem Sinne, dass unsere Mitwirkenden und Mitarbeiter Wege finden, die Kontrolle über das Geschäft mit der Digitalisierung zurückzugewinnen.

\*\*Glück: Wie Frédéric Lenoir schreibt, "ein Zustand des Bewusstseins, der umfassenden und dauerhaften Zufriedenheit in einem sinnvollen Leben, das auf Wahrhaftigkeit beruht".