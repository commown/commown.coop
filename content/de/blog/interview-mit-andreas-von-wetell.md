---
title: Interview mit Andreas von WEtell
date: 2022-07-29T11:47:13.807Z
translationKey: interview wetell
image: /media/teamfoto_podest_juli-2021-scaled.jpg
---
Hallo Andreas! Vielen Dank für deine Zeit.

**Du bist einer der Gründer von WEtell. Wie kam dir die Idee zu diesem Projekt? Was war deine Motivation? Was hast du vorher gemacht?**

Hi! Klar, kein Ding. Für so eine super Initiative wie Commown nehme ich mir sehr gerne Zeit! Ja genau, ich bin einer der Gründenden von WEtell. Insgesamt sind wir zu viert. Von Haus aus haben wir einen naturwissenschaftlichen Hintergrund und kommen eigentlich aus dem Forschungsbereich für erneuerbare Energien.

Uns bewegt das Thema wie kriegen wir endlich die Klimawende gebacken. Rein technologisch sind wir als Menschheit hier schon ultraweit. Das Problem ist, dass wir aus gesellschaftlichen, strukturellen und politischen Gründen diesen Wechsel nicht schaffen.

Mit WEtell, einem nachhaltigem Mobilfunkangebot, erreichen wir Menschen direkt. Wir bieten auch in dieser Branche eine längst überfällige nachhaltige Alternative und wir zeigen, dass moderne und digitale Dienstleistungen durchaus auch enkel*innentauglich umgesetzt werden können. Deswegen sind wir hier und es macht mega Bock zu sehen, was wir schon alles erreichen konnten.

**Was habt ihr mit WEtell schon erreicht und was ist langfristig euer Ziel? Wie setzt ihr eure Überzeugungen in eurem Unternehmen um?**

Ha, die Perfekte Frage zu meinem letzten Satz. Was haben wir erreicht: Wir sind der erste klimaneutrale Mobilfunkanbieter Deutschlands. Zum Vergleich: Die 3 Netzbetreiber wollen frühestens 2040 hier sein. Das ist natürlich viel zu spät, wenn wir an unserem Klima noch etwas retten wollen. Zusätzlich haben wir bereits über 500kW Solarenergie in Deutschland gebaut. Das ist fett, weil es den Ausbau der erneuerbaren voran bringt. Vor allem aber setzen wir neue Standards in der Branche. Anders als unser Wettbewerbsumfeld schreiben wir den Schutz und die Integrität unserer Nutzer\*innendaten ganz groß und speichern zum Beispiel ausschließlich auf DSGVO zertifizierten Servern in Deutschland. Weiterhin räumen wir mit den bad habits der Branche auf. Bei uns gibt es nur monatlich kündbare Tarife, keine versteckten Kosten und das schärfste: Wir geben Tarifverbesserungen an alle unsere Kund\*innen weiter. Also auch an die Bestandskund*innen. Das macht sonst niemand und es macht uns mächtig stolz, dass wir diese Dinge aus der Idee in die Wirklichkeit bringen konnten.

**WEtell ist Teil von FairTEC, wie Commown. Worin bestand für euch der Reiz und der Vorteil, sich dem FairTEC-Kollektiv anzuschließen?**

Ganz klar geht es hier darum Überzeugungen, Kräfte und Kompetenzen zu bündeln, um die gemeinsamen Ziele voranzubringen. Wir streben alle für eine enkelinnentauglich Zukunft. In unserem Fall ganz konkret im Bereich der mobilen Kommunikation. Es ist super fruchtbar und motivierend sich europaweit zu diesem Thema auszutauschen und zu organisieren. So lernen wir zB auch super viel über die Journeys der nachhaltigen Mobilfunkanbieter in Belgien, Frankreich oder UK.

**Warum war es dir außerdem wichtig, dich mit WEtell an der Gemeinwohlökonomie zu beteiligen?**

Weil es uns genau darum geht. Mit WEtell wollen wir ein Produkt und ein Unternehmen schaffen, welches zum Gemeinwohl beiträgt. Die Gemeinwohlökonomie (GWÖ) gibt uns dafür ein super Tool in die Hand. Während der Bilanzierung hatten wir die Möglichkeit einmal um WEtell drum rum zu laufen, mitten durchzutauschen und alle Unternehmensbereiche nach ihrem Gemeinwohl-Impact abzuklopfen. Jetzt wissen wir, wo wir stehen und wir wissen was wir alles noch verbessern können. Vor allem aber ist unsere GWÖ Bilanz ein super Dokument für unsere Kund\*innen. In Zeiten von inflationär genutztem Nachhaltigkeits-Marketing ist es für Konsumenti\*innen mega schwierig zu differenzieren, ob ein Angebot ernst zu nehmen ist, oder nicht. Bei uns kann man nun alle Hintergründe in unserer GWÖ Bilanz nach lesen. Das schafft Vertrauen.

**Ende Juni ist Wetell ins Verantwortungseigentum übergegangen. Warum seid ihr diesen Schritt gegangen? Und was haben eure Kund*innen davon?**

Ja genau. WEtell ist nun im Verantwortungseigentum. Das ist ein großartiger Schritt und knüpft genau da an, wo wir thematisch eben waren. Es geht um Vertrauen. Durch die Überführung ins Verantwortungseigentum ist WEtell für immer und unzertrennbar mit ihren Werten verbunden. WEtell kann niemals mehr verkauft werden, es können keine Gewinne entzogen werden und Richtungsentscheidungen können immer nur von Mitarbeiter\*innen von WEtell getroffen werden. Es kann sich also zum Beispiel niemand mehr persönlich an WEtell bereichern. Und das schafft krasses Vertrauen für unsere Kund\*innen.

**Der Markt für Mobilfunkanbieter ist ein Oligopol mit wenigen, aber mächtigen Akteuren. Es ist auch ein Markt mit hohen strukturellen Anfangskosten. Wie habt ihr euch diesen Herausforderungen angenommen?** 

Genau. Es gibt 3 große Netzbetreiber mit mega riesigen Eigenmarken. Daneben gibt es dann über 100 Sub-Anbieter, so wie wir. Man sagt, der Mobilfunkmarkt ist ein Verdrängungsmarkt, denn nahezu alle Menschen in Deutschland haben bereits einen Tarif. Will man als Anbieter wachsen, muss man sich die Kund*innen bei der Konkurrenz holen. Und ja klar – wir wollen hier verdrängen. Nämlich die konventionellen Angebote, durch nachhaltigere Angebote ersetzen.\
Für den Einstieg haben wir natürlich kein eigenes Netz aufgebaut. Wir nutzen das Netz der Vodafone. Und wir haben auch noch keine eigene Service Provider Zulassung. Auch das wäre zu groß gewesen für den Anfang. An dieser Stelle greifen wir auf die hervorragende Kooperation mit unserem Service Provider Tele2 zurück. Mit ihm sind wir in der Lage unser Angebot wirksam und effizient umzusetzen und können so ein superzuverlässiges Produkt mit mega viel Nachhaltigkeits-Fokus schaffen.

**Wie siehst du die Zukunft der nachhaltigen Elektronik?**

Im Hardware-Bereich? Schwierig. Ich glaube nicht wirklich daran, dass wir es schaffen Produkte wie Laptops, Tablets oder Smartphones im Schnitt wesentlich länger als 3-5 Jahre zu nutzen. Das ist Hightech, die wir ständig dabeihaben und die ständig mit unserem Alltag konfrontiert wird. Wenn ich da an mich selbst denke und mir überlege, auf wie vielen Spielplätzen mein Handy bereits versenkt wurde, bin ich ehrlich gesagt krass erstaunt, dass es überhaupt noch funktioniert. Ich glaube in Mietmodellen mit Full-Service Angeboten, wie dem euren steckt richtig viel Potenzial für eine effiziente Ressourcennutzung. Und das dann noch kombiniert mit nachhaltigen Angeboten wie Fairphone oder Shiftphone, so könnte ein Schuh draus werden.

**Überschreitest du manchmal das Limit deines Mobilfunktarifs?**

Sehr selten. Ich habe mir aber auch den Luxus gegönnt unseren größten Tarif, Megafon, zu nutzen. Der kommt mit 25GB und ist damit mehr als großzügig ausgestattet. Mir ist vollkommen klar, dass das übertrieben ist, und dass ich das nicht brauche. Es hat mich aber gefreut in dem Moment aus den vollen zu schöpfen und ich freue mich sehr, dass ich einfach nicht mehr darüber nachdenken muss, ob ich die Daten reichen, oder nicht.

**Nun mal im Ernst: Gibt es Studien, die versuchen, das optimale Volumen an Mobilfunkdaten zu ermitteln, das mit einem nachhaltigen Lebensstil vereinbar ist?**

Das ist mir nicht bekannt. Und ich glaube, dass sich das so auch nicht beantworten oder erfassen lässt, da die Anforderungen in unseren Leben mannigfaltig sind. Es gibt aber Studien, die zeigen, dass die Deutschen im Schnitt 3GB Daten verbrauchen. Man kann sich also bei Kauf seines nächsten Tarifes durchaus die Frage stellen, ob man wirklich so viele Daten braucht, oder ob es auch der nächstkleinere Tarif tut. So lässt sich dann auch direkt Geld sparen. Und ja genau. Die gleiche Geschichte könnte ich auch auf mich anwenden. Übrigens Geld sparen: 30% der deutschen hängen in alten überteuerten Tarifen rum. Wechselt doch alle mal. Am besten zu einer nachhaltigeren Alternative. Am Ende spart ihr vermutlich noch viel Geld dabei!

Und damit euch der Wechsel noch viel leichter fällt, bietet WEtell euch 25€ Startguthaben beim Abschluss eines neuen Vertrags. Gebt einfach den Code auf [dieser Seite](https://www.wetell.de/commown/?pk_cpn=ex-commown-pm) bei eurer Bestellung ein und telefoniert ab sofort mit 100% Klimaschutz, Datenschutz und Fairness und Transparenz.