---
title: Warum entscheidet man sich heutzutage für Hardware as a Service (HaaS)
  beruhend auf einer genossenschaftlichen Basis?
date: 2022-01-24T17:05:40.324Z
translationKey: efc
image: /media/commown.png
---
Lesedauer: 5 Min\
\
Das klassische Verkaufsmodell führt uns in eine Sackgasse! 

Üblicherweise wird ein Produkt hergestellt, dann vermarktet und anschließend verkauft, bis es dann nicht mehr funktioniert und der Verbraucher ein neues Gerät kauft. Wissen wir überhaupt, welche Menge an Computern in den letzten Jahren den Geist aufgegeben haben? Leider viel zu viele! Die Überreste der kaputten Geräte ist in [Video-Reportagen](https://www.youtube.com/watch?v=qqYDWbVg2yw) zu sehen. 

Genau hier lässt sich die Grenze des klassischen Verkaufsmodelles erahnen! Würden die Produkte nicht aufhören zu funktionieren, so hätten die Hersteller keine Käufer mehr und würden Pleite gehen… Dies ist völlig selbstverständlich, denn ist der Markt erst einmal von einem Produkt überflutet, so kann das Unternehmen keine zusätzlichen Produkte mehr verkaufen. Unserer Meinung nach liegt genau hier die Quelle aller Mechanismen der vorzeitigen Obsoleszenz, sei es im Marketing, in der Software oder in der Hardware. 

Was genau ist “Hardware as a Service”? 

Das Modell von „Hardware as a Service“ (HaaS) ermöglicht das Vorgehen gegen die geplante Obsoleszenz. Die Interessen des Verbrauchers und des Herstellers laufen endlich zusammen: das Produkt soll so lange wie möglich erhalten bleiben und funktionieren.

Um es kurz zu fassen, geht es bei HaaS darum, die Nutzung eines Produktes zu verkaufen und nicht das Produkt an sich. Diese Methode ist tatsächlich gar nicht so neu, obwohl dies gängiger weise angenommen wird.

So hat beispielsweise Martin Eden, der Held des gleichnamigen Romans von Jack London, eine Schreibmaschine gemietet, um diese zu verwenden und Schriftsteller zu werden. Der Roman wurde vor über 100 Jahren veröffentlicht und beinhaltete quasi schon die Quintessenz des HaaS-Modelles.

Das Prinzip der Langzeitmiete ohne Kaufoption kennen wir aus anderen, eher „immateriellen“ Branchen… So werden beispielsweise Dienstleistungen verkauft, wie bei Online-Streaming-Plattformen oder Internet-Abonnements. Schließt man einen Internetvertrag ab, so schickt der Anbieter einem ein Modem zu, das man zurückgeben muss, wenn der Vertrag ausläuft. Man mietet also die Nutzung des Gerätes, um auf eine Dienstleistung zugreifen zu können. 

G wie Genossenschaft?

Jedoch sind nicht alle Modelle des Mietens umweltfreundlich. Es muss in der Tat “ein geringerer Verbrauch natürlicher Ressourcen im Hinblick auf eine nachhaltige Entwicklung für Menschen, Unternehmen und Gebiete” ermöglicht werden. Mehr Informationen zum Thema findet ihr [hier](https://searchitchannel.techtarget.com/definition/Hardware-as-a-Service-in-managed-services):

Im Sinne der Nachhaltigkeit ist Commown Teil dieses Modelles und bietet die langfristige Miete ohne Kaufoption an. 

Einerseits wählt unsere Genossenschaft die elektronischen Geräte so aus, dass sie so ökologisch designed sind wie möglich - bspw. das Fairphone oder die WHY-Computer. Andererseits bilden alle gemieteten Geräte eine Einheit, ein wahres Allgemeingut der Genossenschaft, das von allen Mitgliedern (Kunden, Angestellten, Partnern) mit besessen wird. Es ist wichtig zu wissen, dass in klassischen Unternehmen die Mehrheitsaktionäre das Sagen haben (im Verhältnis zur investierten Summe gemessen). In einer Genossenschaft hingegen hat jedes Mitglied lediglich eine Stimme.  Schlussendlich bedeutet dies, dass Gesellschafter, die 10.000€ investiert haben und Gesellschafter die 100€ investiert haben bei einer Abstimmung im Rahmen der Hauptversammlung, die exakt gleiche Entscheidungsbefugnis haben.