---
toc: true
layout: faq
title: FAQ · Commown · Ethische und nachhaltige Elektronik
description: Hier finden Sie Antworten auf Ihre Fragen zu den Dienstleistungen
  von Commown, die Kooperative für nachhaltige Elektronik
date: 2021-03-19T12:25:36+01:00
translationKey: faq
menu:
  main:
    name: FAQ
    weight: 11
  footer:
    name: FAQ
    weight: 8
header:
  title: Commown FAQ
  subtitle: Häufige Fragen & Wiki
  ctas:
    - outline: false
      text: Startseite des Wikis
      href: https://wiki.commown.coop/Startseite
sections:
  - id: commown
    containers:
      - title: Commown
        blocks:
          - title: Was ist “Commown”?
            text: Commown ist ein gemeinnütziges Genossenschaftsprojekt (unter der
              Rechtsform einer französischen Société coopérative d’intérêt
              collectif, kurz
              [SCIC](https://wiki.commown.coop/Die-Genossenschaft-Commown)). Es
              wird von einem Team getragen, das daran glaubt, dass die moderne
              Gesellschaft schrittweise nachhaltiger und ethischer werden kann.
            ctas:
              - text: Mehr erfahren
                href: https://wiki.commown.coop/Commown-definieren
          - details: true
            title: Welche Vorteile hat das Genossenschaftsmodell für die Kunden?
            text: Commown ist eine gemeinnützige Genossenschaft. Wir schlagen den Commownern
              “im kollektiven Interesse” fünf Win-Win-Strategien in Form von
              Herausforderungen vor.
            ctas:
              - href: https://wiki.commown.coop/Die-Herausforderungen-f%C3%BCr-Commown-und-die-Commowner?highlight=Die+Herausforderungen+f%C3%BCr+Commown+und+die+Commowner
                text: Mehr erfahren
          - title: Warum lieber mieten als kaufen?
            text: >-
              Weil der Verkauf von Geräten die strukturelle Ursache der
              geplanten Obsoleszenz ist…


              Weil das Mieten Serviceleistungen umfasst, die Sie sonst nicht nutzen könnten und langfristig günstiger ist als vergleichbare Leistungen…


              Weil Sie von der Risikoteilung profitieren und der Service für die meisten Probleme, die Ihnen begegnen könnten, eine Antwort geben kann (Beratung, technische Unterstützung, Ausfälle, Bruchschäden, gegebenenfalls notwendiger Akkutausch, Diebstahl, verbundene Bearbeitungszeiten und Versandkosten, Servicekontinuität, usw.), sich aber auch für eine verantwortungsvolle Elektronikbranche einsetzt (Unterstützung alternativer Hersteller, gemeinsame Nutzung von Ersatzteilen, F&E, Interessenvertretung, usw.)
            ctas:
              - text: Mehr erfahren
                href: https://wiki.commown.coop/Lieber-mieten-als-kaufen
          - title: Wie ist Ihre Beziehung zu den Herstellern?
            text: Das Projekt ist so konzipiert, dass es die Tätigkeit der Hersteller so gut
              wie möglich unterstützt und sie an das
              Hardware-as-a-Service-Prinzip heranführt. Wir ermutigen sie dabei,
              Ihr Engagement in Zusammenhang mit der Reduzierung der sozialen
              und ökologischen Auswirkungen von elektronischen Geräten,
              fortzuführen.
            ctas:
              - text: Mehr erfahren
                href: https://wiki.commown.coop/Beziehungen-zu-den-Herstellern
          - title: Warum ist es ethisch, bei Commown zu mieten?
            text: Aus vielen Gründen, die mit dem Konzept des Projekts zusammenhängen!
            ctas:
              - text: Mehr erfahren
                href: https://wiki.commown.coop/Ethik
          - title: Wie kann ich Ihnen helfen?
            text: Unsere Challenge besteht darin, die Nachhaltigkeit der Kooperative und des
              Geschäftsmodells sicherzustellen. Dementsprechend brauchen wir
              mehr Sichtbarkeit und größere Mengen.
            ctas:
              - text: Mehr erfahren
                href: https://wiki.commown.coop/Um-uns-zu-helfen
          - title: Und wenn ich im Moment kein elektronisches Gerät benötige?
            text: >-
              Durch den Kauf eines Aktionsgutscheins **unterstützen Sie unsere
              Kooperative** schon heute und **sparen bis zu 40 €** auf Ihre
              Monatsraten, sobald Sie ein neues elektronisches Gerät wirklich
              BRAUCHEN. 


              Außerdem **können Sie diesen Gutschein verschenken**, um Ihre Werte zu teilen**. 🙂**
            ctas:
              - text: Mehr erfahren
                href: https://shop.commown.coop/de_DE/shop/product/aktionsgutschein-186
          - title: Bieten sie alternative Betriebssysteme an?
            text: >-
              Ja, denn wir sind von der Bedeutung freier Software im Kampf gegen
              geplante Obsoleszenz überzeugt. \

              \

              Für die  Fairphone 3, 3+ und 4  (sowie für das SHIFT6mq) bieten wir als alternatives Betriebssystem [/e/OS](https://shop.commown.coop/de_DE/shop/product/e-os-auf-dem-fairphone-3-3-oder-4-installieren-239) an.\

              \

              Für die Laptops und Computer bieten wir als alternatives Betriebssystem Linux an.
  - id: particuliers
    containers:
      - title: Meine Online-Bestellung (Privatpersonen)
        blocks:
          - title: Wann erfolgt die Lieferung, wenn ich jetzt bestelle?
            text: >-
              Die Lieferfristen sind abhängig von den Geräten, Ihrer
              Verfügbarkeit und gegebenenfalls ihren Montagefristen. Lieferungen
              können also mehr oder weniger schnell erfolgen. Einzelheiten zu
              den Fristen finden Sie auf jedem Produktdatenblatt im
              Online-Shop.\

              \

              Gerrard Street Headset: Wenn Sie heute vorbestellen, sollten wir in der Lage sein, innerhalb von 5 Werktagen zu liefern, sofern alle Formalitäten erledigt sind.\

              \

              Fairphone 3: Das Fairphone 3 ist aktuell nicht mehr vorrätig, weshalb die Lieferfristen mehrere Wochen betragen können.\

              \

              Fairphone 3+: Wenn Sie heute vorbestellen, sollten wir in der Lage sein, innerhalb von 5 Werktagen zu liefern, sofern alle Formalitäten erledigt sind.\

              \

              Fairphone 4: Wenn Sie heute vorbestellen, sollten wir in der Lage sein, innerhalb von 5 Werktagen zu liefern, sofern alle Formalitäten erledigt sind.
            ctas:
              - text: Mehr erfahren
                href: https://wiki.commown.coop/Wann-wird-meine-Bestellung-best%C3%A4tigt-und-das-Ger%C3%A4t-verschickt%3F
          - title: Bin ich als Kunde verpflichtet,  Genossenschafter von Commown zu werden?
            ctas:
              - text: Mehr erfahren
                href: https://wiki.commown.coop/Bin-ich-als-Kunde-verpflichtet%2C-Genossenschafter-von-Commown-zu-werden%3F
            text: Nein, natürlich nicht! Doch wir würden uns freuen, Sie zu unseren
              Genossenschaftern zählen zu dürfen ;)
          - text: Unsere Serviceleistungen sind aktuell für Kunden in Deutschland, in
              Österreich, in Kontinentalfrankreich (Rechnungsadresse UND
              Einzugsermächtigung) und französischsprachige und deutschsprachige
              Kunden in Belgien verfügbar (Anfragen von französischsprachigen
              Schweizer Kunden können im Einzelfall ebenfalls geprüft werden).
            title: Welchen geografischen Geltungsraum haben die Serviceleistungen von
              Commown?
            ctas:
              - text: Mehr erfahren
                href: https://wiki.commown.coop/Geografischer-Geltungsraum
          - title: Welchen Schutz umfassen Ihre Serviceleistungen?
            text: >-
              Der Schutz ist von der Art des gemieteten Produkts und im Fall des
              Fairphones vom gewählten Angebot abhängig. Sämtliche
              Serviceleistungen und Absicherungen werden in den Besonderen
              Nutzungsbedingungen jedes Angebots [sowie unseren
              AGBs](https://commown.coop/de/allgemeine_gesch%C3%A4ftsbedingungen/)
              beschrieben.


              Der Schutz gegen Bruchschäden, der nur in den Mietangeboten für das Fairphone enthalten ist, erstreckt sich bis auf einige wenige Ausnahmen (nukleare Schäden, intensive elektromagnetische Bestrahlung…) auf alle Arten von Ereignissen. Allerdings ist nur ein Schadensfall pro Jahr abgedeckt. Commown wird alles im Rahmen der Angebote mögliche unternehmen, um die Unterbrechung des Services gering zu halten. Im Fall des Premium-Angebotes wird Ihnen innerhalb von höchstens 48 Stunden nach der Schadensmeldung ein Fairphone zugeschickt. Durch den Schaden wird der Mietvertrag nicht unterbrochen und die Bankeinzüge werden trotz eines eventuellen Verzugs durch Reparatur oder Austausch des Gerätes fortgesetzt.
            ctas: []
          - title: Kann ich meinen Vertrag vor dem Ende des Verpflichtungszeitraums
              kündigen?
            text: Die Kooperative hat mehrere Kündigungsmechanismen geschaffen.
            ctas:
              - text: Mehr erfahren
                href: https://wiki.commown.coop/K%C3%BCndigung
          - title: Wie lange dauert der Versand der Ersatzteile?
            text: Der Versand der Ersatzteile ist von den Lagerbeständen von Commown und des
              Herstellers abhängig. Sollte die Lieferzeit länger als zwei Tage
              dauern, stellt Commown Ihnen dank der im Angebot enthaltenen
              Servicekontinuität in der Zwischenzeit ein Ersatzgerät zur
              Verfügung und hilft Ihnen, wenn nötig, bei der Übertragung Ihrer
              Daten :)
          - title: Ich habe bereits eine Versicherung. In welchen Fällen nehme ich sie in
              Anspruch?
            text: >-
              **Wenn unser Angebot den Schaden (allgemeiner Fall) abdeckt**,
              können Sie sich trotzdem an Ihre Versicherung wenden. Denn wenn
              Commown eine Schadensersatzzahlung erhält, erhalten Sie einen
              Rabatt auf Ihre monatlichen Zahlungen, zum Dank dafür, dass Sie
              die finanziellen Auswirkungen des Schadens für die Kooperative
              gering gehalten haben.


              **Wenn unser Angebot den Schaden nicht abdeckt (Sonderfall)** und Sie (über Ihre Hausrat- oder Kfz-Versicherung, Ihre Kreditkartenversicherung oder eine Spezialversicherung) versichert sind, ersetzt Ihre Versicherung den Schaden und kompensiert somit die vollständige Erstattung des Gerätes an Commown.
          - title: Warum soll ich nach Ende des Verpflichtungszeitraums weiter für den
              Service zahlen?
            text: Weil ein solcher Service nur durch eine Umlage der Kosten funktionieren
              kann. Wenn Ihr Produkt nach drei Jahren kaputt geht oder einfach
              ausfällt und nicht repariert werden kann, wird Commown alles im
              Rahmen der verschiedenen Abo-Arten mögliche unternehmen, um das
              Produkt zu reparieren oder ein Ersatzgerät zur Verfügung zu
              stellen. Diese schrittweise Erneuerung der Flotte verursacht
              Kosten, die auf alle Nutzer umverteilt und mit der Zeit geglättet
              werden. In den ersten zwei Jahren zahlen Sie nicht nur die
              Anschaffungskosten für den Erwerb des Ihnen zur Verfügung
              gestellten Gerätes, sondern auch die der anderen Nutzer, deren
              Gerät in den ersten Jahren ausgefallen ist, sowie eine Provision
              für den unvermeidbaren Moment, in dem Ihr Gerät repariert oder
              vollständig ausgetauscht werden muss. Eine Mietkooperative wie
              Commown ist mehr als ein bloßer Anbieter elektronischer Geräte.
              Sie umfasst auch ein Serviceangebot, das seinen Preis hat.
          - title: Wann wird meine Bestellung bestätigt und versandt?
            text: >-
              Die Lieferfristen sind von den Geräten und ihrer Verfügbarkeit
              abhängig. Die Lieferungen können also mehr oder weniger schnell
              erfolgen.


              Einzelheiten zu den Fristen finden Sie auf jedem Produktdatenblatt im Online-Shop.
            ctas:
              - text: Mehr erfahren
                href: https://wiki.commown.coop/Wann-wird-meine-Bestellung-best%C3%A4tigt-und-das-Ger%C3%A4t-verschickt%3F
          - title: Was passiert, wenn ich mehrere Produkte gleichzeitig bestelle?
            text: Die Anzahl der Geräte hat keinen Einfluss auf die Bestellung. Mit der Zeit
              werden Sie Ihre Geräte an Ihren Bedarf anpassen können.
            ctas:
              - text: Mehr erfahren
                href: https://wiki.commown.coop/Was-passiert%2C-wenn-ich-mehrere-Produkte-gleichzeitig-bestelle
---
