---
title: Footer
headless: true
translationKey: footer
address: "Commown: die Kooperative für nachhaltige Elektronik"
legal: "*Commown ist eine gemeinnützige Genossenschaft*"
social: "[Facebook](https://www.facebook.com/kooperativecommown), <a
  href='https://mastodon.social/@commownde' rel='me'>Mastodon</a>,
  [LinkedIn](https://www.linkedin.com/company/commown),
  [Youtube](https://www.youtube.com/channel/UCgYvAOHyAicxxdfr0umhZOA),
  [Instagram](https://www.instagram.com/kooperativecommown/)"
eco: Diese Internetseite ist eine Website mit geringem Verbrauch. Die Größe der
  Seiten ist 12 mal geringer als der Durchschnitt der Websites.
---
