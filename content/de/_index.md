---
title: Commown · Startseite
description: Für einen verantwortungsvolleren Umgang mit Elektronik
date: 2021-03-19T12:25:03+01:00
translationKey: home
menu:
  footer:
    name: Startseite
    weight: 1
header:
  title: Die Kooperative für nachhaltige Elektronik
  subtitle: Ökologische und modulare Smartphones, Kopfhörer, Laptops und Computer
  ctas:
    - outline: false
      href: https://commown.coop/de/wer-sind-wir/
      text: Mehr über die Kooperative
    - outline: true
      text: Unsere Angebote
      href: "#unsere-angebote"
sections:
  - color: text-bleu-fonce-100
    bg_color: bg-white
    containers:
      - figure:
          image: /media/2021_03_commown_vignet_youtube.jpg
          figcaption: ""
          href: https://www.youtube.com/watch?v=LLE-W3qBfbs
          iframe:
            code: <iframe
              src="https://www.youtube.com/embed/LLE-W3qBfbs?autoplay=1&color=white"
              title="YouTube video player" frameborder="0" allow="accelerometer;
              autoplay; clipboard-write; encrypted-media; gyroscope;
              picture-in-picture" allowfullscreen></iframe>
          alt: Lernen Sie Commown in diesem Video besser kennen
        title: Eine verantwortungsvolle Elektronik mit Commown
        icon: commown
      - title: ""
        icon: ""
        blocks:
          - title: Öko-Design
            text: >-
              Die einzige Art, die negativen Auswirkungen elektronischer
              Produkte zu vermindern besteht in der maximalen Verlängerung ihrer
              Lebensdauer. Aus diesem Grund wählen wir Geräte aus, die
              reparierbarer und langlebiger sind. Unser Modell basiert auf dem
              Mieten ohne Kaufoption und garantiert das Interesse unserer
              Genossenschaft, die Geräte so lange wie möglich nutzbar zu machen,
              wenn nötig über mehrere Mietzyklen. \

              \

              Im Gegensatz zum Kaufmodell, bei dem den Herstellern jedes Mittel recht ist, um zu einer Neuanschaffung zu drängen, begleitet Sie Commown während der gesamten Nutzung desselben Gerätes.
          - title: Transparenz & Radikalität
            text: >-
              Ein unkontrolliertes Mietmodell könnte für ein kapitalistisches
              Unternehmen die ideale Möglichkeit sein, seine Kunden auszunutzen.
              Aus diesem Grund haben wir uns für die Unternehmensform einer
              gemeinnützigen Genossenschaft entschieden, die die Macht des
              Kapitals und seine Rendite drastisch einschränkt. Dank dieses
              Status können all unsere Kunden an der Führung von Commown
              teilnehmen.


              Der Status ist nicht alles, doch er schafft einen Rahmen, der zu unseren Werten passt: Schlichtheit, Umweltschutz, Ethik und Transparenz.
        ctas:
          - text: Mehr über unser Modell
            href: https://commown.coop/de/wer-sind-wir/
            outline: false
    id: vers-un-numerique-responsable-avec-commown
  - id: unsere-angebote
    color: text-bleu-fonce-100
    bg_color: bg-bleu-clair-75
    containers:
      - title: Unsere Angebote
        icon: ""
        cta:
          outline: true
          text: Entdecken Sie alle unsere Produkte
          href: https://shop.commown.coop/de_DE/shop/category/alle-produkte-23
        blocks:
          - title: Fairphone 3/3+, Fairphone 4, Fairphone 5 und SHIFT6mq
            icon: mobile
            ctas:
              - outline: false
                text: Zum Fairphone-Angebot
                href: https://commown.coop/de/unsere-angebote/fairphone/
              - outline: false
                text: Zum Shiftphone-Angebot
                href: https://commown.coop/de/unsere-angebote/shiftphone/
          - title: Nachhaltige Computer und Laptops
            icon: desktop
            ctas:
              - outline: false
                text: Laptops und PC
                href: https://shop.commown.coop/de_DE/shop/category/computer-2
          - title: Modulare Kopfhörer Gerrard Street und Fairphone Fairbuds XL
            icon: audio
            ctas:
              - outline: false
                text: Mehr zum Gerrard Street-Headset
                href: https://commown.coop/de/unsere-angebote/gerrard-street-kopfh%C3%B6rer/
              - outline: false
                text: Mehr zu den Fairbuds XL
                href: https://shop.commown.coop/de_DE/shop/product/fairbuds-xl-kopfhorer-675
          - title: Aktionsgutschein
            icon: euro
            ctas:
              - outline: false
                text: Mehr erfahren
                href: https://shop.commown.coop/de_DE/shop/product/aktionsgutschein-186
      - title: ""
        icon: ""
        blocks:
          - title: Für Unternehmen
            text: Mit dem Angebot von Commown können Sie als Businesskunde ein hohes Maß an
              sozialer und ökologischer Verantwortung unter Beweis stellen.
            ctas: []
          - title: ""
            ctas:
              - outline: false
                text: Seite für Unternehmer
                href: https://commown.coop/de/für-unternehmen/
  - color: text-bleu-fonce-100
    bg_color: bg-white
    containers:
      - title: Verantwortungsvolle Elektronikgeräte?
        icon: recycle
        blocks:
          - figure:
              image: /media/schema-extraction-decharge-sans-texte.png
              alt: Schéma Extraction → Fabrication → Distribution et marketing → Usage →
                Décharge
          - title: Heutige Situation des Elektroniksektors
            text: Minen im Besitz von **Milizen, Ausbeutung von Kindern** in Fabriken,
              **beklagenswerte Arbeitsbedingungen**, **astronomisch große
              CO2-Fußabdrücke**, **illegaler Handel mit Elektronikschrott**, in
              Geräten verbaute Metalle, deren Recycling **nahezu unmöglich
              ist**.... Diese sicher unvollständige Aufzählung veranschaulicht
              die vielen negativen Auswirkungen, die den Lebenszyklus heutiger
              elektronischer Produkte prägen.
    id: electronique-responsable
  - id: testimonials
    color: text-violet-100
    bg_color: bg-beige-25
    containers:
      - blocks:
          - title: Hilfsbereitschaft
            text: >-
              > Ich miete bei commown, weil ich die kooperative Ethik teile.\

              > Aus diesem Grund spreche ich mit meinem Umfeld darüber. Heute wurde ich beim Anschluss des gemieteten Computers an meinen alten Drucker begleitet und war begeistert von der Professionalität, Geduld, Hilfsbereitschaft und Freundlichkeit meines Gesprächspartners.\

              > Ich kann Commown wärmstens empfehlen!


              — Elisabeth
          - title: Professionell
            text: >-
              Sehr professioneller, extrem hilfsbereiter und aufmerksamer
              Service, der sicherstellt, dass das Problem vollkommen gelöst
              wurde und alle Fragen oder Bedenken berücksichtigt werden.

              Vielen Dank für diese effiziente und schnelle Bearbeitung!!!


              — Caroline
          - title: Immer zufrieden!
            text: >-
              Die Zusendung des Teils ging sehr schnell und der Austausch war
              sehr einfach. Immer zufrieden mit dem Service von Commown!


              — Sophie
          - title: Schnelle Antwortzeit
            text: >-
              Die Antwortzeit ist schnell und die Anweisungen, die zur
              Fehlerfindung an dem Gerät gegeben werden, sind klar und einfach
              umzusetzen. Die Kommunikation ist herzlich und einfach.


              — Julia
        font_size: smaller
        title: Was unsere Kunden über uns denken
        icon: chat
      - blocks:
          - title: Geräte im Betrieb
            text: ""
            data: +**6250**
          - title: Note des Kundenservice von Commown
            text: Durchschnittliche Bewertung des Kundenservice bei über 1000 Bewertungen.
            data: "**9,5**/10"
        color: text-bleu-fonce-100
        font_size: smaller
  - id: ""
    color: text-bleu-fonce-100
    bg_color: bg-bleu-clair-75
    containers:
      - title: Das Ökosystem Commown
        icon: network
        blocks:
          - title: Teil der Kooperative werden!
            text: "Als gemeinnützige Genossenschaft ist es unser Ziel, all diejenigen
              zusammenzubringen, die sich für mehr Fairness und Nachhaltigkeit
              im Elektroniksektor einsetzen möchten: Kund\\*innen, Mitglieder,
              Ehrenamtliche, IT-Hersteller\\*innen,
              Kommunikationspartner\\*innen und Investor\\*innen. Wie bereits
              Raiffeisen sagte: Was einer allein nicht schafft, das vermögen
              viele!"
            data: ""
          - title: Forschung & Entwicklung
            text: Commown wurde als [eine der 1000 “clean and profitable
              solutions”](https://solarimpulse.com/solutions-explorer/commown)
              von der [Solar Impulse Foundation](https://solarimpulse.com/)
              ausgewählt und durch
              [C-Voucher](https://c-voucher.fundingbox.com/) ausgezeichnet, der
              ersten paneuropäischen Initiative, die das Konzept der
              Kreislaufwirtschaft zur Neugestaltung der Wertschöpfungsketten
              aufgreift (EU-Programm für Forschung und Innovation Horizon 2020).
            icon: null
      - blocks:
          - title: Kollektiv FairTEC
            text: Commown ist Teil von [FairTEC](https://fairtec.io/de/), einer Gruppe von
              Akteur*innen, die sich der verantwortungsbewussten
              Telekommunikation verschrieben hat. Zu den anderen Mitgliedern
              zählen u. a. der nachhaltige Smartphone-Hersteller
              [Fairphone](https://www.fairphone.com/de/), das
              datenschutzfreundliche Betriebssystem [/e/
              OS](https://e.foundation/), das den Nutzern ihre Privatsphäre
              zurückgibt und der nachhaltige Mobilfunkanbieter
              [WEtell](https://www.wetell.de/).
          - title: Vereine & Open-Source
            text: Commown führt 5% seines durch freie Software erwirtschafteten Umsatzes an
              die Entwickler von „[Fdroid](https://f-droid.org/de/)“ ab und ist
              Mitglied in den Vereinen [Zero Waste Austria
              e.V.](https://www.zerowasteaustria.at/), [Runder Tisch
              Reparatur](https://runder-tisch-reparatur.de/), [SEND
              e.V.](https://www.send-ev.de/) und [Circular Berlin
              e.V](https://circular.berlin/), um eine nachhaltigere und
              solidarische Entwicklungsstrategie zu unterstützen. Seit Anfang
              2024 sind wir Product Partner der [Cradle to Cradle
              NGO](https://c2c.ngo/) und Teil der
              [C2C-Wanderausstellung](https://c2c.ngo/c2c-ausstellung/). Wir
              arbeiten auch regelmäßig mit [Topio
              e.V.](https://www.topio.info/), [Solderpunks
              e.V.](https://www.instagram.com/solder.punks/) und [Open Source
              Ecology Germany e.V](https://blog.opensourceecology.de/en/)
              zusammen.
            ctas: []
        title: ""
---
