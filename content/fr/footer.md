---
title: Pied de page
headless: true
translationKey: footer
address: |-
  8 A rue Schertz
  Parc Phoenix - Bât. B2
  67100 Strasbourg
legal: >-
  Commown est Société Coopérative d’Intérêt Collectif basée sur une Société
  Anonyme. \

  SIREN 828 811 489
social: "[Facebook](https://www.facebook.com/commownfr),
  [Twitter](https://twitter.com/CommownFR), <a
  href='https://mamot.fr/@commownfr' rel='me'>Mastodon</a>,
  [LinkedIn](https://www.linkedin.com/company/commown),
  [Instagram](https://www.instagram.com/commown/)"
eco: >-
  Ce site internet est un site basse consommation. \

  Le poids des pages est 12 fois moins important que sur la moyenne des sites web. [En savoir plus](/qui-sommes-nous#ecoconception).
---
