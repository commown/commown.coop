---
toc: true
layout: invest
title: Épargne Sociale & Solidaire · Devenir Sociétaire · Commown
description: "Reprenez le contrôle de votre épargne et investissez dans vos
  valeurs: Commown est la seule coopérative de location de l’électronique
  responsable en France."
date: 2025-02-06T10:48:51.987Z
translationKey: epargne
menu:
  main:
    name: Épargne solidaire
    weight: 3
  footer:
    name: Épargne solidaire
    weight: 7
header:
  title: Reprenez le contrôle de votre épargne
  subtitle: Contribuez au financement de Commown
  ctas:
    - outline: false
      text: Investissez dans vos valeurs
      href: https://shop.commown.coop/shop/product/investissement-en-parts-sociales-807
sections:
  - containers:
      - title: "Investir autrement : le crowdequity"
        title_max_w: 16ch
        icon: euro
        font_size: larger
        blocks:
          - text: Le saviez-vous ? La Caisse des Dépôts n’a pas de contrainte écologique sur
              l’utilisation de l’épargne de votre livret&nbsp;A. Au contraire,
              les **parts sociales Commown** contribuent **concrètement à la
              transition**.
      - blocks:
          - title: Avantages fiscaux
            title_max_w: 12ch
            icon: currency-euro
            text: Réduisez vos impôts ! Si vous êtes assujetti à l’impôt sur le revenu (IR),
              déduisez **18% de la valeur de votre investissement**.
          - title: Décidez de l’utilisation de votre épargne
            title_max_w: 20ch
            icon: chart-pie
            text: Vous savez exactement où va votre argent et comment il est utilisé.
              Contrairement au Livret de Développement Durable et Solidaire,
              vous êtes sûr de ce que vous financez. Alors choisissez un
              investissement qui porte vos valeurs.
      - blocks:
          - title: Économisez les frais des intermédiaires financiers
            icon: cash
            text: Vous n’êtes plus obligé de financer des fonds d’investissement, des
              gestionnaires de patrimoine, ou des banquiers privés pour investir
              votre argent.
          - title: Participez à la création d’emplois
            title_max_w: 20ch
            icon: briefcase
            text: L'investissement chez Commown consolide les 30 emplois créés par la
              coopérative.
    id: crowdequity
  - id: le-projet-commown
    color: text-bleu-clair-25
    bg_color: bg-bleu-fonce-100
    containers:
      - ctas:
          - outline: false
            text: Qui sommes-nous ?
            href: /qui-sommes-nous/
          - outline: true
            text: Découvrir l’équipe
            href: /qui-sommes-nous/#equipe
        title: Avant d’investir, découvrir le projet Commown
        title_max_w: 20ch
        icon: commown
      - blocks:
          - title: Points clés
            text: >-
              * Un coeur de métier solide : la location avec services
              d'appareils électroniques plus responsables.

              * Chiffre d'affaires en forte croissance, réparti à 50/50 entre les particuliers, et les entreprises.

              * Commown est devenue une référence française de l'électronique responsable, [recommandée par l'ADEME](https://agirpourlatransition.ademe.fr/particuliers/maison/numerique/achats-numeriques-bons-conseils). Le modèle économique de Commown est cohérent avec l'objectif écologique : la SCIC a un intérêt économique à faire durer les appareils.
          - title: Plaidoyer citoyen
            text: >-
              Commown utilise la reconnaissance de son expertise pour peser
              autant que possible dans les instances de décisions nationales sur
              les questions d'électronique. Par exemple la SCIC a contribué de
              façon significative aux négociations sur la mise en place de
              l'indice de réparabilité. 


              Investir dans Commown, c'est aussi soutenir une force de lobbying citoyen.
  - id: concretement
    containers:
      - title: Et pour vous concrètement ?
        title_max_w: 16ch
        blocks:
          - title: Vous êtes  assujetti·e à l’IR ?
            icon: currency-euro
            title_max_w: 16ch
            text: >-
              Vous avez le droit à une réduction d’impôts dite "IR-PME" de 18%.


              * Pour les contribuables célibataires, veufs ou divorcés, le montant est plafonné à 50.000€, dans la limite de **10 000€ de réduction d’impôts,** selon l'[article du 200-0 A](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000041467320/2019-12-30/) du Code des Impôts sur les niches fiscales.

              * Pour les contribuables mariés, PACSÉS ou vivant en concubinage, alors le montant est plafonné à théoriquement à 100.000€, mais toujours dans la même limite ci-dessus.
          - title: Rémunération avec les intérêts des parts sociales
            icon: chart-pie
            text: >-
              En plus des réductions d’impôt, et si Commown génère des
              excédents, des intérêts pourront être versés.


              Depuis la loi Sapin II de 2016, ces intérêts correspondent au plus à la moyenne, sur les trois années civiles précédant la date de l’assemblée générale, du taux moyen de rendement des obligations des sociétés privées (TMO), majorée de deux points, soit un rendement d’environ 3,6%.
      - blocks:
          - title: Type de financement
            title_max_w: 12ch
            icon: office-building
            text: C’est un investissement en parts sociales. L’investisseur devient donc
              **co-propriétaire de la coopérative**, et la coopérative augmente
              ses fonds propres.
          - title: Maximiser votre investissement
            icon: presentation-chart-bar
            text: Il est conseillé d’**investir sur 7 ans**, car cette durée conditionne
              légalement le droit aux baisses d’impôts. Cependant, vous pouvez
              demander le remboursement à tout moment. Si la SCIC est en bonne
              santé, il est effectué en général en moins d'un mois.
          - title: Accès à l’état de santé de Commown
            icon: document-search
            title_max_w: 16ch
            text: Un rapport est rédigé tous les deux mois pour les sociétaires, avec
              l’ensemble des indicateurs pertinents sur la santé de Commown et
              les grands rebondissements des derniers mois. Ce rapport est
              présenté lors du webinaire bimestriel dédié aux sociétaires.
        ctas:
          - outline: false
            text: Investir en parts sociales
            href: https://shop.commown.coop/shop/product/investissement-en-parts-sociales-807
  - containers:
      - title: L’écosystème Commown
        icon: network
        blocks:
          - title: Les sociétaires
            text: >-
              **Les coopératives :**

              [TeleCoop](https://telecoop.fr/), [Alternatives Économiques](https://www.alternatives-economiques.fr/), [Citiz](https://citiz.coop/), [Enercoop](https://www.enercoop.fr/), [Ateliers du Bocage](https://ateliers-du-bocage.fr/), [Windcoop](https://www.wind.coop/), etc.


              **Les producteurs :**

              [Fairphone](https://www.fairphone.com/fr), [why! Open Computing](https://whyopencomputing.ch/fr/), [PC Vert](https://pcvert.fr/), [Odiss](https://odiss.fr/).
          - text: |-
              **Les particuliers**
              Plus de 1250 Commowners soutiennent déjà la coopérative.
        ctas:
          - outline: false
            text: Devenir sociétaire
            href: https://shop.commown.coop/shop/product/investissement-en-parts-sociales-807
      - blocks:
          - title: Les partenaires
            text: "[La Nef](https://www.lanef.com/), [Crédit
              Coopératif](https://www.credit-cooperatif.coop/), [France
              Active](https://www.franceactive.org/),
              [HOP](https://www.halteobsolescence.org/),
              [Legicoop](https://legicoop.fr/), [Label
              Emmaüs](https://www.label-emmaus.co/fr/),
              [Finacoop](https://www.finacoop.fr/), [e
              Fondation](https://e.foundation/fr/)
              ([Murena](https://murena.com/fr/)),
              [Crosscall](https://www.crosscall.com/fr_FR/),
              [Shift](https://www.shift.eco/),
              [Positiv’R](https://positivr.fr/),
              [GreenIT.fr](https://www.greenit.fr/),
              [R-Cube](https://rcube.org/), [Coop des
              Communs](https://coopdescommuns.org/fr/association/),
              [Circul’R](https://www.circul-r.com/), [ESS
              France](https://www.ess-france.org/), [Les
              Scop](https://www.les-scop.coop/) (CG Scop), [Impact
              France](https://www.impactfrance.eco/),
              [INR](https://institutnr.org/), [Coop
              Circuits](https://coopcircuits.fr/),
              [Syllucid](https://syllucid.com/), [Cairn
              Devices](https://cairn-devices.eu/),
              [Modixia](https://www.modixia.fr/homepage), [Coop
              Medias](https://www.coop-medias.org/),
              [Kooglof](https://www.kooglof.fr/), etc."
          - title: Les Licoornes
            text: Commown est heureuse de faire partie du [collectif des
              Licoornes](https://www.licoornes.coop/), l'alliance des
              coopératives militantes .
            ctas: []
    id: ecosysteme-commown
    bg_color: bg-bleu-clair-75
    color: text-bleu-fonce-100
  - id: chiffres
    color: text-bleu-fonce-100
    bg_color: bg-white
    containers:
      - title: Quelques chiffres
        blocks:
          - text: Chiffres prévisionnels fin 2025
        font_size: larger
      - blocks:
          - title: Chiffre d’affaires (€)
            data: "**1,5M**"
          - title: Trésorerie (€)
            text: ""
            data: "**300k**"
          - title: Smartphones loués
            text: ""
            data: "**5500**"
          - title: PC loués
            text: ""
            data: "**2000**"
      - blocks:
          - text: Statut actuel
        font_size: larger
      - blocks:
          - title: Chiffre d’affaires du précédent exercice (€)
            data: "**1.36 M**"
          - title: Smartphones loués
            data: "**4100**"
          - title: PC loués
            data: "**1450**"
          - title: Matériels audio loués
            data: "**430**"
  - id: levier
    containers:
      - title: Votre épargne fait levier
        blocks:
          - data: "**523**k€"
            text: investis par plus de 1275 sociétaires
          - data: "**2**M€"
            text: levés en prêts bancaires (dep. 2017)
          - data: "**300**k€"
            text: Titres participatifs (France Active, Scopinvest)
            ctas: []
        ctas:
          - outline: true
            text: FAQ investissement
            href: "/faq/#investisseurs "
          - outline: false
            text: Investir en parts sociales
            href: https://shop.commown.coop/shop/product/investissement-en-parts-sociales-807
    bg_color: bg-bleu-clair-75
    color: text-bleu-fonce-100
  - id: atouts-difficultes
    color: text-bleu-fonce-100
    bg_color: bg-beige-25
    cols: true
    containers:
      - title: Atouts
        blocks:
          - text: >-
              * **Commown est la seule coopérative de location de l’électronique
              responsable en France**. Notre projet a pu attirer des partenaires
              de poids comme La Nef, PositivR.fr ou encore Alternatives
              Économiques …\
                Nous représentons plusieurs fabricant d'appareils plus durables, sociétaires de la coopérative : Fairphone, Whyopencomputing, PCvert.
              * Notre statut de SCIC nous **démarque de la concurrence** en apportant une cohérence tout en renforçant notre modèle éthique.

              * Commown possède un **portefeuille de contrats de long terme**, qui assurent un chiffre d'affaires futur de plus d'1M€
      - title: Difficultés
        blocks:
          - text: >-
              * **Trouver des financements** : Pour réussir ce pari, nous avons
              besoin  de votre aide en tant qu’investisseurs. Pour l’apport de
              fonds propres  bien sûr, mais surtout pour l’effet de levier
              important auprès des  organismes financiers qui prêteront d’autant
              plus facilement si nous  avons suffisamment de fonds propres.  

              * **Approvisionnement** : Il existe très peu de fournisseurs d'électronique plus responsable. Une rupture de stock chez ces fournisseurs peut générer un frein à la croissance de Commown. C'est arrivé plusieurs fois ces dernières années.
  - containers:
      - title: En cas de faillite
        blocks:
          - text: Nous attirons votre attention sur le fait que les investissements en parts
              sociales ne constituent pas un placement garanti. En cas de
              faillite de Commown, les actifs seront vendus pour rembourser les
              dettes de la société, et le reste remboursera les investisseurs.
              Dans le pire des cas, les pertes peuvent aller jusqu’à la totalité
              des montants investis.
          - text: Cependant, les financements servent à investir dans des appareils
              électroniques durables. Au dernier bilan, Commown avait plus de
              1,3 M€ d'actifs net composés de ces appareils, pour une dette
              restante de 1,1 M€
        color: null
    id: faillite
    bg_color: bg-bleu-fonce-100
    color: text-bleu-clair-25
  - id: scic
    bg_color: bg-bleu-clair-75
    color: text-bleu-fonce-100
    containers:
      - title: SCIC
        blocks:
          - title: Société
            text: >-
              C’est une société de personnes à forme commerciale, en
              l’occurrence une S.A.S.. Commown a commencé en association loi
              1901 qui a été transformée en SCIC en janvier 2018 en conservant
              la personne morale.


              Elle est inscrite au registre du Commerce et des Sociétés et est soumise à l’impôt sur les sociétés.
          - title: Coopérative
            text: >-
              Chaque personne membre de la SCIC a le droit de vote : **1
              personne = 1 vote.**


              **57.5% du bénéfice doit être ré-injecté dans le développement de la SCIC**, ce qui assure le bon développement de la SCIC au lieu de rémunérer des actionnaires qui posséderaient tous les pouvoirs.
          - title: D’intérêt collectif
            text: >-
              Elle permet de réunir les associés (salariés, clients membres,
              producteurs, investisseurs, partenaires de communication et
              bénévoles) autour d’un projet commun: avancer vers une
              électronique écologique et éthique !


              Les **milliers d'appareils** de la SCIC forment donc **un bien commun** d'un nouveau genre, dont l'équipe Commown ne fait que la gestion.
        ctas:
          - outline: true
            text: Investir en parts sociales
            href: https://shop.commown.coop/shop/product/investissement-en-parts-sociales-4
---
