---
toc: true
layout: faq
title: FAQ · Commown · Électronique éthique et responsable
description: Retrouvez ici questions les plus fréquentes sur les services de
  Commown dans notre FAQ.
date: 2021-03-19T12:25:36+01:00
translationKey: faq
menu:
  main:
    name: FAQ
    weight: 5
  footer:
    name: FAQ
    weight: 8
header:
  title: FAQ
  subtitle: Questions fréquentes & wiki
  ctas:
    - outline: false
      text: Consulter le wiki
      href: https://wiki.commown.coop/Accueil
sections:
  - id: commown
    containers:
      - title: Commown
        blocks:
          - title: “Commown”, c’est quoi ?
            text: Commown est un projet de société coopérative d’intérêt collectif
              ([SCIC](https://wiki.commown.coop/La%20SCIC%20Commown)) porté par
              une équipe **qui croit en la capacité de la société moderne à se
              transformer progressivement pour devenir durable et éthique**.
            ctas:
              - text: En savoir plus
                href: https://wiki.commown.coop/Pour-d%C3%A9finir-Commown
          - details: true
            title: Quels sont les avantages du modèle coopératif pour les clients ?
            text: Commown est une Société Coopérative d’Intérêt Collectif (SCIC), nous
              proposons donc 5 défis gagnants-gagnants aux commowners dans
              “l’intérêt collectif”.
            ctas:
              - href: https://wiki.commown.coop/Les-d%C3%A9fis-de-Commown-et-des-commowners
                text: En savoir plus
          - title: Pourquoi louer plutôt que d'acheter ?
            text: >-
              Parce que la vente d’appareils est la cause structurelle de
              l’obsolescence programmée…


              Parce que la location inclut des services que vous n’auriez pas autrement et reste moins chère à services équivalents sur la durée…


              Parce que vous profitez de la mutualisation des risques et les services permettent de répondre à la plupart des soucis que vous pouvez rencontrer (conseil, assistance, panne, casse, remplacement de la batterie le cas échéant, vol, temps de gestion et frais de ports associés, continuité de service, etc), mais également à soutenir la filière de l’électronique responsable (soutien aux producteurs alternatifs, mutualisation des pièces détachées, R&D, plaidoyer citoyen, etc.)
            ctas:
              - text: En savoir plus
                href: https://wiki.commown.coop/Louer-plut%C3%B4t-qu%27acheter
          - title: Quelle est votre relation avec les producteurs ?
            text: Le projet est conçu pour soutenir au mieux leur activité et les
              accompagner vers l’économie de la fonctionnalité. Nous cherchons
              notamment à prolonger leur engagement à diminuer les impacts
              sociaux et écologiques liés à l’électronique.
            ctas:
              - text: En savoir plus
                href: https://wiki.commown.coop/Relations-producteurs
          - title: Pourquoi est-ce éthique de louer chez Commown ?
            text: Pour plein de raisons liées à la cohérence du projet !
            ctas:
              - text: En savoir plus
                href: https://wiki.commown.coop/%C3%89thique
          - title: Comment puis-je vous aider ?
            text: Notre challenge c’est d’assurer la pérennité de la coopérative et du
              modèle économique, nous avons donc besoin de plus de visibilité et
              plus de volumes.
            ctas:
              - text: En savoir plus
                href: https://wiki.commown.coop/Pour-nous-aider
          - title: Et si je n'ai pas besoin d'un appareil électronique maintenant ?
            text: >-
              En prenant un Bon de Consom’Action Différée, **soutenez notre
              coopérative** dès aujourd’hui et **économisez jusqu’à 40 €** sur
              vos mensualités le jour où vous aurez réellement BESOIN d’un
              nouvel appareil électronique. 


              En plus, **vous pouvez offrir ce Bon** pour partager vos valeurs. 🙂
            ctas:
              - text: En savoir plus
                href: https://shop.commown.coop/shop/product/bon-de-consomaction-differee-186
  - id: particuliers
    containers:
      - title: Ma commande en ligne (particuliers)
        blocks:
          - title: Si je commande maintenant, quand serai-je livré ?
            text: Les délais de livraison dépendent des appareils, de leur disponibilité et
              des délais d'assemblage le cas échéant. Les livraisons peuvent
              donc être plus ou moins rapides, le détail sur les délais est
              disponible sur chaque fiche produit du magasin.
            ctas:
              - text: En savoir plus
                href: https://wiki.commown.coop/Quand-est-ce-que-ma-commande-sera-valid%C3%A9e-et-l%E2%80%99appareil-exp%C3%A9di%C3%A9-%3F
          - title: En tant que client, suis-je obligé d’être sociétaire de Commown ?
            ctas:
              - text: En savoir plus
                href: https://wiki.commown.coop/En-tant-que-client%2C-suis-je-oblig%C3%A9-d%E2%80%99%C3%AAtre-soci%C3%A9taire-de-Commown-%3F
            text: Non, bien sûr ! Mais nous serions ravis de vous comptez parmis les
              sociétaires ;)
          - text: "Nos services sont à ce jour
              disponibles pour les clients domiciliés (adresse de facturation ET
              domiciliation bancaire) en France Métropolitaine, en Allemagne et
              en Autriche, ainsi que pour les clients francophones et
              germanophones de Belgique (les demandes de clients francophones
              suisses peuvent également être étudiées au cas par cas)."
            title: Quelle est la zone géographique de validité des services Commown ?
            ctas:
              - text: En savoir plus
                href: https://wiki.commown.coop/Zone-g%C3%A9ographique-de-validit%C3%A9
          - title: Quelles sont les protections comprises dans le cadre de votre prestation
              de services ?
            text: >-
              Celles-ci dépendent du type de produit loué, et dans le cas des
              Fairphone de la formule choisie. L’ensemble des services et
              protections sont détaillés dans les Conditions Particulières liées
              à chaque formule [ainsi que dans nos CGS](/mentions-legales/).


              Concernant la protection casse seulement incluse dans les formules de location de Fairphone, celle-ci s’applique à tout type d’événements à quelques exceptions près (dégâts commis par l’atome, exposition électromagnétique intense…). Toutefois, une seule casse sera couverte par an, Commown mettra alors en oeuvre tous les moyens compris dans le cadre des formules pour limiter la rupture de service. Dans le cas du forfait Prémium, un Fairphone sera envoyé au plus tard 48 h après la déclaration du sinistre. La casse n’arrête pas le contrat de location et les prélèvements bancaires continuent, ce malgré un éventuel délai de réparation ou de substitution de l’appareil.
            ctas: []
          - title: Est-ce que je peux résilier mon contrat avant la fin de la durée
              d'engagement ?
            text: Plusieurs mécanismes ont été mis en place par la coopérative.
            ctas:
              - text: En savoir plus
                href: https://wiki.commown.coop/Resiliation
          - title: Combien de temps mettez-vous pour envoyer les pièces de rechange ?
            text: L’envoi des pièces de rechange dépend du stock de Commown et de celui du
              producteur. Dans tous les cas, si le délai dépasse 2 jours, grâce
              à l’option de continuité de service incluse dans votre offre,
              Commown s’engage à vous fournir un appareil de remplacement dans
              l’intervalle et à vous accompagner si nécessaire pour le transfert
              des données :)
          - title: "Je suis déjà assuré, dans quel cas solliciter mon assurance ? "
            text: >-
              **Dans le cas où notre offre couvre le sinistre (cas général)**,
              vous pouvez néanmoins faire appel à votre assurance, car si
              Commown reçoit une indemnisation, vous bénéficierez d’une remise
              sur vos mensualités pour vous remercier de la réduction induite
              sur l’impact financier du sinistre pour la coopérative. 


              **Dans le cas où notre offre ne couvrirait pas un sinistre (cas particulier)**, si vous êtes assuré (par le biais de votre assurance habitation ou automobile, par l’assurance liée à vos moyens de paiement, ou par une assurance spécifique), c’est votre assurance qui prendra en charge le sinistre, vous permettant ainsi de compenser le remboursement intégral de l’appareil à Commown.
          - title: À la fin de la période d’engagement, pourquoi continuer à payer pour le
              service ?
            text: Parce qu’un tel service ne peut fonctionner que par une mutualisation des
              coûts. Si après trois ans votre produit est cassé, ou simplement
              qu’il tombe en panne et ne peut être réparé, Commown mettra tout
              en oeuvre dans le cadre des différents types de souscription pour
              vous réparer le produit ou fournir un produit de substitution. Ce
              renouvellement progressif de la flotte a un coût, qui est réparti
              sur l’ensemble des utilisateurs et lissé dans le temps. Les deux
              premières années, vous ne payez pas seulement le coût
              d’acquisition par notre coopérative de l’appareil mis à votre
              disposition, mais également ceux des autres utilisateurs ayant
              subi une panne les premières années, ainsi qu’une provision pour
              le moment inévitable où votre propre appareil devra être réparé,
              ou changé intégralement. Une coopérative de location comme
              Commown, bien plus qu’un fournisseur d’appareils électroniques,
              c’est aussi un ensemble de services, lesquels ont un coût.
          - title: Quand est ce que ma commande sera validée et expédiée ?
            text: >-
              Les délais de livraison dépendent des appareils et de leur
              disponibilité, les livraisons peuvent donc être plus ou moins
              rapides.


              Le détail sur les délais est disponible sur chaque fiche produit du magasin.
            ctas:
              - text: En savoir plus
                href: https://wiki.commown.coop/Quand-est-ce-que-ma-commande-sera-valid%C3%A9e-et-l%E2%80%99appareil-exp%C3%A9di%C3%A9-%3F
          - title: Que se passe-t-il si je commande plusieurs produits en même temps ?
            text: Le nombre d’appareils n’a aucune influence sur la commande, vous pouvez
              donc grouper et/ou séquencer vos besoins dans le temps.
            ctas:
              - text: En savoir plus
                href: https://wiki.commown.coop/Commander-plusieurs-produits
  - id: investisseurs
    containers:
      - title: Investisseurs
        blocks:
          - title: À quoi va servir mon investissement ?
            ctas:
              - text: En savoir plus
                href: https://wiki.commown.coop/%C3%80+quoi+va+servir+mon+investissement+%3F
          - title: Si j’investis, aurai-je des obligations et des responsabilités en tant
              que sociétaire ?
            text: ""
            ctas:
              - text: En savoir plus
                href: https://wiki.commown.coop/Si+j%E2%80%99investis%2C+aurai-je+des+obligations+et+responsabilit%C3%A9s+en+tant+que+soci%C3%A9taire+%3F
          - title: Quel retour sur investissement puis-je attendre ?
            text: >-
              **a. Une SCIC, mais une exception**

              Commown est une exception dans le monde des Sociétés Coopératives d’Intérêt Collectif (SCIC). En effet, les coopératives sont souvent à but non lucratif et même si la loi sur les SCIC de 2001 a permis d’ouvrir l’accès au capital à des acteurs externes, de nombreuses SCIC revendiquent encore d’être à but non lucratif. Au contraire, Commown met en avant plusieurs mécanismes de rémunération du capital décrit ci-dessous. Une nécessité puisque Commown devra en permanence financer les appareils électroniques responsables loués à ses clients.


              **b. Réduction de l’impôt sur le revenu**

              La première rémunération possible concerne les particuliers qui investissent en parts sociales de Commown pour 5 ans minimum. Ils peuvent déduire une partie des sommes investies de leur impôt sur le revenu, à hauteur de 25 % (taux exceptionnel valable entre le 09 mai et le 31 décembre 2021).  Cela correspond à une rémunération de **4,56** % sur 5 ans. Ceci est cadré par l’[article 199 Terdecies-0 A](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=45E0857C059C7F9C9030428539DD3852.tplgfr33s_2?idArticle=LEGIARTI000036429011&cidTexte=LEGITEXT000006069577&categorieLien=id&dateTexte=) du code des impôts.


              **c. Intérêts aux parts sociales**

              En cas de succès économique de la SCIC, des intérêts seront versés pour rémunérer les parts sociales. Depuis la loi Sapin II de 2016, ces intérêts correspondent au plus à la moyenne, sur les trois années civiles précédant la date de l’assemblée générale, du taux moyen de rendement des obligations des sociétés privées (TMO), majorée de deux points.majorée de deux points. En janvier 2021, cela permettrait donc pour exemple une rémunération à hauteur de 2.48 %.
          - title: Pourquoi me demande-t-on une pièce d’identité par mail après le premier
              paiement ?
            ctas:
              - text: En savoir plus
                href: https://wiki.commown.coop/J%E2%80%99ai-pay%C3%A9-un-premier-acompte-et-pourtant-je-n%27ai-rien-sign%C3%A9-%3F
          - title: Que se passe-t-il si je désire récupérer mon argent ?
            ctas:
              - text: En savoir plus
                href: https://wiki.commown.coop/Que+se+passe-t-il+si+je+d%C3%A9sire+r%C3%A9cup%C3%A9rer+mon+argent+%3F
          - title: J’ai payé un premier acompte et pourtant je n'ai rien signé ?
            ctas:
              - text: En savoir plus
                href: https://wiki.commown.coop/J%E2%80%99ai+pay%C3%A9+un+premier+acompte+et+pourtant+je+n%27ai+rien+sign%C3%A9+%3F
          - title: "Est-ce que le poids de mon vote en tant que sociétaire est influencé par
              ma contribution financière ? "
            ctas:
              - text: En savoir plus
                href: https://wiki.commown.coop/Est-ce-que-le-poids-de-mon-vote-est-influenc%C3%A9-par-ma-contribution-financi%C3%A8re-%3F
          - title: "Que se passe-t-il si j’investis et que Commown fait faillite ? "
            ctas:
              - text: En savoir plus
                href: https://wiki.commown.coop/Que+se+passe-t-il+si+j%E2%80%99investis+et+que+Commown+fait+faillite+%3F
---
