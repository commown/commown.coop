---
title: Commown · La SCIC de l’électronique responsable et durable
description: Des appareils plus éthiques et durables. La coopérative Commown
  propose de faire durer le matériel informatique grâce à la location longue
  durée.
date: 2025-02-06T10:48:08.222Z
translationKey: home
menu:
  footer:
    name: Accueil
    weight: 1
header:
  title: La coopérative de l'électronique sobre et engagée
  subtitle: Mutualiser pour faire durer !
  ctas:
    - text: Découvrez Commown
      href: /qui-sommes-nous
      outline: false
    - text: Nos offres
      href: "#nos-offres"
      outline: true
sections:
  - color: text-bleu-fonce-100
    bg_color: bg-white
    containers:
      - title: Électronique responsable ?
        icon: recycle
        blocks:
          - figure:
              image: /media/schema-extraction-decharge.png
              alt: Schéma Extraction → Fabrication → Distribution et marketing → Usage →
                Décharge
          - title: État des lieux de l’électronique aujourd’hui
            text: >-
              Mines détenues par des **milices**, **exploitation d’enfants**
              dans les unités d’assemblage, **conditions de travail
              déplorables**, **empreinte carbone astronomique**, **trafic
              illégal de déchets électroniques**, **recyclage quasi-impossible**
              de certains métaux constitutifs des appareils… 


              Cette liste incomplète illustre les “coûts cachés” qui jalonnent le cycle de vie d’un produit électronique aujourd’hui.
    id: electronique-responsable
  - color: text-noir-100
    bg_color: bg-beige-25
    containers:
      - title: Vers un numérique responsable avec Commown
        icon: commown
        blocks:
          - title: Éco-conception et location sans option d’achat
            text: La seule façon de limiter l’impact des produits électroniques est
              d’**augmenter au maximum leur durée de vie**. Pour cela nous
              sélectionnons des appareils plus **réparables** et plus
              **durables**. Notre modèle de **location sans option d’achat**
              garantit que notre coopérative aura tout intérêt à faire vivre ces
              appareils le plus longtemps possible, sur plusieurs cycles de
              location si besoin. À l’inverse du modèle de vente où tous les
              moyens sont bons pour pousser au renouvellement, Commown vous
              accompagne dans la durée sur le même appareil.
          - title: Transparence & radicalité
            text: >-
              Un modèle locatif sans contrôle pourrait être le meilleur moyen
              pour une entreprise capitaliste d’abuser de ses clients. C’est
              pourquoi nous avons choisi un statut de **Société Coopérative
              d’Intérêt Collectif (SCIC)**, qui limite drastiquement le pouvoir
              et la rémunération du capital. Avec ce statut, tous nos clients
              peuvent prendre part à la gouvernance de Commown. \

              « Statut n’est pas vertu » mais cela offre un cadre en phase avec nos valeurs de sobriété, d’écologie, d’éthique et de transparence
        ctas:
          - text: En savoir plus sur notre modèle
            href: /qui-sommes-nous
            outline: false
      - figure:
          image: ""
          figcaption: ""
    id: vers-un-numerique-responsable-avec-commown
  - id: nos-offres
    color: text-bleu-fonce-100
    bg_color: bg-bleu-clair-75
    containers:
      - title: Nos offres
        icon: ""
        cta:
          outline: true
          text: Découvrir tous nos produits
          href: https://shop.commown.coop/shop
        blocks:
          - title: Fairphone
            icon: mobile
            ctas:
              - outline: false
                text: En savoir plus
                href: /nos-offres/fairphone
          - title: Ordinateurs
            icon: desktop
            ctas:
              - outline: false
                text: En savoir plus
                href: /nos-offres/location-pc-durable/
          - title: Audio
            icon: audio
            ctas:
              - outline: false
                text: En savoir plus
                href: /nos-offres/casque-audio-responsable/
          - title: Consom’Action
            icon: euro
            ctas:
              - outline: false
                text: En savoir plus
                href: /nos-offres/consomaction
      - title: ""
        icon: ""
        blocks:
          - title: Pour les entreprises
            text: Commown propose pour les professionnels des formules de location
              garantissant un haut niveau de service et de Responsabilité
              Sociétale et Environnementale.
          - title: ""
            ctas:
              - outline: false
                text: Offres professionnels
                href: /pour-les-entreprises
  - id: testimonials
    color: text-violet-100
    bg_color: bg-white
    containers:
      - blocks:
          - title: Réactifs
            text: >-
              > Vous êtes **très réactifs** et **soucieux de la compréhension de
              vos interlocuteurs** (qd ceux-ci ne sont pas des geeks). Et vous
              suivez tant que vous n’avez pas de réponse. \

              C’est très appréciable, MERCI !


              — Anne
          - title: Écoute
            text: >-
              > Assurément, je suis non seulement satisfaite mais **très
              satisfaite**. Ignorante au départ des multiples utilisations et
              possibilités d’un “smartphone”, votre **disponibilité**, votre
              **patience** et votre **écoute** me permettent d’en découvrir les
              clefs d’accès. \

              Si c’est l’esprit du “bien commun” et l’éthique de Commown qui m’ont fait vous rejoindre, c’est aussi eux qui me réjouissent toujours.


              — Mariam
          - title: Aux petits soins
            text: >-
              > Service **aux petits soins**. **Réactif**, **à l’écoute** et
              **sans complication**. On n’est pas habitué à ce genre de service
              après vente. \

              Ça fait plaisir.


              — Thibault
          - title: Sans faute
            text: |-
              1. Réponses par email - diagnostic très rapide
              2. Envoi du paquet en suivant, propre et sans chichi 
              3. Procédure de remplacement hyper bien documentée

              C’est **un sans faute** sur cette opération. Merci !

              — Antoine
        font_size: smaller
        title: Ce que nos clients pensent de nous
        icon: chat
      - ctas: []
        color: text-violet-100
      - blocks:
          - title: Appareils en service
            text: Une grande majorité de Fairphone, suivi par les ordinateurs
            data: +**6250**
          - title: Note du service Commown
            text: |-
              Moyenne des évaluations du service client,
              sur plus de 1000 évaluations.
            data: "**9,5**/10"
        color: text-bleu-fonce-100
        font_size: smaller
  - id: ecosysteme-commown
    color: text-bleu-fonce-100
    bg_color: bg-bleu-clair-75
    containers:
      - title: L’écosystème Commown
        icon: network
        blocks:
          - title: Les sociétaires
            text: >-
              **Les coopératives :**

              [TeleCoop](https://telecoop.fr/), [Alternatives Économiques](https://www.alternatives-economiques.fr/), [Citiz](https://citiz.coop/), [Enercoop](https://www.enercoop.fr/), [Ateliers du Bocage](https://ateliers-du-bocage.fr/), [Windcoop](https://www.wind.coop/), etc.


              **Les producteurs :**

              [Fairphone](https://www.fairphone.com/fr), [why! Open Computing](https://whyopencomputing.ch/fr/), [PC Vert](https://pcvert.fr/), [Odiss](https://odiss.fr/).
          - title: ""
            text: |-
              **Les particuliers**
              Plus de 1250 commowners soutiennent déjà la coopérative.
      - blocks:
          - title: Les partenaires
            text: "[Mobicoop](https://mobicoop.fr/), [Legicoop](https://legicoop.fr/),
              [Label Emmaüs](https://www.label-emmaus.co/fr/),
              [Éthi'Kdo](https://www.ethikdo.co/),
              [Finacoop](https://www.finacoop.fr/), [La
              Nef](https://www.lanef.com/), [Crédit
              Coopératif](https://www.credit-cooperatif.coop/Institutionnel),
              [Alsace Active](https://www.franceactive-grandest.org/),
              [HOP](https://www.halteobsolescence.org/),
              [Positiv’R](https://positivr.fr/),
              [GreenIT.fr](https://www.greenit.fr/), [Idée
              Alsace](http://www.ideealsace.com/), [RCube](https://rcube.org/),
              [Coop des Communs](https://coopdescommuns.org/fr/association/),
              [Circul’R](https://fr.circul-r.com/), [ESS
              France](https://www.ess-france.org/), [Les
              Scop](https://www.les-scop.coop/), [Impact
              France](https://impactfrance.eco/), [Coop
              Circuits](https://www.coopcircuits.fr/), [Fondation
              /e/](https://e.foundation/fr/),
              [Fairphone](https://www.fairphone.com/fr/),
              [Crosscall](https://www.crosscall.com/), [Repeat Audio (ex.
              Gerrard street)](https://repeat.audio), [Cairn
              Device](https://www.cairn-devices.eu/),
              [Wetell](https://www.wetell.de/),"
          - title: Les Licoornes
            text: "Commown est heureuse de faire partie des Licoornes : vers un changement
              radical de l'économie par l'intercoopération ! Pour en savoir plus
              n'hésitez pas à visiter [le site web des
              Licoornes](https://www.licoornes.coop/)."
---
