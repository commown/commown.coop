---
title: L’offre Mine Urbaine
description: "Commown rachète votre Fairphone 3 ou Fairphone 3+, jusqu'à 120 € !"
date: 2021-03-31T17:40:26.400Z
translationKey: mineurbaine
layout: invest
menu:
  main:
    name: Mine Urbaine
    parent: Particuliers
    weight: 7
header:
  title: L’offre Mine Urbaine
  subtitle: Donnez une nouvelle vie à votre FP3 et gagnez jusqu'à 120 € ! 
sections:
  - bg_color: bg-beige-25
    containers:
      - figure:
          image: ""
          alt: ""
        blocks:
          - title: ""
            text: >-
              Les mines de métaux deviennent de moins en moins productives car les gisements sont de plus en plus pauvres en métal. Par exemple, la plupart des gisements de cuivre ont une teneur en cuivre de moins de 1% ([Source 1](https://www.isige.minesparis.psl.eu/wp-content/uploads/Cuivre-Vf.pdf), [Source 2](https://www.sciencedirect.com/science/article/pii/S0921800922003962)). Inversement, en moyenne les smartphones usagés contiennent autour de 3% de cuivre ! Utiliser cette ressource, c'est ce que l'on nomme **la mine urbaine**.
              

              Cependant, le recyclage est très inefficace : broyer les appareils, essayer de séparer les métaux, pour ensuite les fondre, les raffiner, et les réutiliser pour des composants. Dans le cas de certains métaux, utilisés en très faibles quantités, c'est même impossible avec la technologie actuelle.


              Le plus efficace est donc de réutiliser des composants, voire des appareils entiers. C'est le sens de cette offre : récupérer le plus de Fairphone 3 et Fairphone 3+ possible. Soit pour les remettre en service, soit pour avoir une source de pièces détachées et faire durer plus longtemps ceux qui restent en service.


               
            
          - title: ""
            figure:
              image: /media/illu_fp3_parts.png
        title: Faire durer, c'est réutiliser !
  - bg_color: bg-bleu-clair-75
    containers:
      - title: "L'offre Mine Urbaine en détail"
        blocks:
          - text: >-
              Les conditions pour que Commown accepte votre Fairpĥone 3 ou Fairphone 3+ :
 
              * il ne doit pas avoir été exposé à l'eau ou à l'humidité.


              * Il doit être entier, avec tous les modules.
              

              Combien gagnerez vous ?

              * **60 €** , si vous choisissez un **virement**.


              * **120 €** , si vous choisissez un avoir sur une prochaine **nouvelle location Commown**. Soit par exemple 4 mois de Fairphone 4 Essentiel.
            title: Les conditions
          - text: >-
              C'est très simple !

              * Vous remplissez le formulaire que vous trouvez en cliquant sur le bouton ci-dessous.

              * Vous recevez une étiquette prépayée pour le retour de votre Fairphone 3 ou 3+.

              * Vous expédiez l'appareil.

              * L'équipe vérifie qu'il est entier et qu'il n'a pas été en contact avec l'eau ou l'humidité. Si l'appareil n'est pas éligible, si vous le souhaitez, l'équipe vous le renvoie.

              * L'équipe vous contacte par mail et vous choisissez le mode de réglement, en fournissant votre RIB si besoin, et vous recevez soit le code soit le virement selon votre choix.


            title: Et concrètement ?
            ctas:
              - outline: false
                text: Je vends mon FP3/3+ !
                href: https://shop.commown.coop/mine-urbaine
---
