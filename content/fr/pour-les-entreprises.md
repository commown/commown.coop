---
title: Location PC et smartphones responsables pour les entreprises
description: Commown est engagé à fournir aux entreprises des moyens de réduire
  la consommation et les coûts de leur parc informatique. Notre offre est idéale
  à la mise en place de politique RSE.
layout: invest
date: 2021-03-30T08:25:53.200Z
translationKey: business
menu:
  main:
    name: Professionnels
    weight: 2
  footer:
    name: Offres pro
    weight: 6
header:
  title: Commown au service des professionnels
  subtitle: Nos produits engagement longue durée
  ctas: []
sections:
  - id: nos-offres
    color: text-bleu-fonce-100
    bg_color: bg-bleu-clair-75
    containers:
      - title: Nos offres
        blocks:
          - title: Fairphone
            icon: mobile
            ctas:
              - outline: false
                href: https://greener-it-pro.commown.coop/shop
                text: En savoir plus
          - title: Crosscall
            icon: mobile
            ctas:
              - outline: false
                text: En savoir plus
                href: https://greener-it-pro.commown.coop/shop
          - title: Ordinateurs
            ctas:
              - outline: false
                text: En savoir plus
                href: https://greener-it-pro.commown.coop/shop
            icon: desktop
          - title: Tablettes
            icon: tablet
            ctas:
              - outline: false
                text: En savoir plus
                href: https://greener-it-pro.commown.coop/shop
  - id: avantages
    color: text-bleu-fonce-100
    containers:
      - blocks:
          - title: Service Premium
            text: >-
              L’offre Commown pour les professionnels, s’adapte à tous les
              besoins : de la sensibilisation des collaborateurs à leur
              formation, en passant par la mise en place de solutions de
              continuité de service.


              Avec l’offre Commown les pannes, casses et même les vols ne seront plus synonymes de ralentissement de votre productivité.
          - title: Gain Economique
            text: "Maîtrise des charges et présentation avantageuse du bilan : les
              mensualités de location, qui correspondent au coût d’utilisation,
              vous permettent non seulement de simplifier la gestion/prévision
              de vos budgets annuels (visibilité sur les engagements), de lisser
              les flux de TVA (optimisation de la trésorerie), mais aussi de
              préserver vos capitaux propres ! En effet, opter pour la location
              lorsque c’est possible permet de passer les mensualités en charges
              et donc de préserver la capacité d’endettement pour d’autres
              investissements plus stratégiques."
          - text: Notre coopérative (SCIC), structure de l’ESS a pour premier objectif la
              diminution des externalités négatives de l’électronique. Nous
              confier la gestion de votre parc IT c’est vous assurer que nous
              ferons tout notre possible pour exploiter celui-ci le plus
              lontemps possible que ce soit par la relocation ou la
              réutilisation des pièces détachées. De plus souscrire à l’offre
              Commown est un moyen de soutenir les acteurs émergents de
              l’électronique responsable.
            title: Gain RSE
  - id: faq
    color: text-bleu-fonce-100
    containers:
      - title: Questions fréquentes
        title_max_w: 14ch
        icon: chat-alt
        blocks:
          - title: "Pourquoi louer plutôt qu’acheter ? "
            details: true
            text: >-
              **1. Parce que la location inclut de nombreux services dont vous
              ne pourrez pas bénéficier en passant par un achat de matériel :**


              Tableau comparatif des services Location vs Achat :


              | **LOCATION (usage)**                                                                                                        | **ACHAT**                                                                                                 |

              | --------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------- |

              | Aide au paramétrage, assistance à l’usage au quotidien                                                                      | Non inclus                                                                                                |

              | Si l’entreprise choisit les logiciels libres, formation des collaborateurs sans surcoût.                                    | Non inclus                                                                                                |

              | Pannes : Dépannage, réparation et/ou remplacement du matériel pendant toute la durée de location                            | Pannes : Garantie constructeur limitée dans le temps (exemple : 2 ans pour le Fairphone)                  |

              | Vol/perte : remplacement sans coût inclus                                                                                   | Franchises importantes des assurances + application de la vétusté + base en valeur vénale + frais de port |

              | Casse : réparations sans coût incluses                                                                                      | Franchises importantes des assurances + application de la vétusté                                         |

              | Continuité de service : 2 niveaux possibles (appareil de courtoise, ou pièces détachées/appareils prêts à l’usage sur site) | Non inclus                                                                                                |

              | Evolution et/ou remplacement des modules                                                                                    | Non inclus                                                                                                |

              | Gestion des appareils en fin de cycle sans surcoût                                                                          | Non inclus                                                                                                |

              | Frais de port et logistique (livraison et SAV)                                                                              | Non inclus                                                                                                |


              De plus l’achat implique que vous gériez l’ensemble des interactions liées aux appareils et à chaque utilisateur, alors qu’en optant pour la location toutes ces interactions sont gérées par Commown, ce qui se traduit par une économie importante (coût des pièces détachées, temps de gestion, de main d’oeuvre, voire de sous-traitance).


              **2. Facilité de décision et de mise en oeuvre :**


              Un service par abonnement mensuel est plus simple et rapide à mettre en place dans votre organisation qu’un achat nécessitant une immobilisation (circuit de décision, trésorerie, gestion administrative).


              **3. Maîtrise des charges et présentation avantageuse du bilan :**


              Les mensualités de location, qui correspondent au coût d’utilisation, vous permettent non seulement de simplifier la gestion/prévision de vos budgets annuels (visibilité sur les engagements), de lisser les flux de TVA (optimisation de la trésorerie), mais aussi de préserver vos capitaux propres ! En effet, opter pour la location lorsque c’est possible permet de passer les mensualités en charges et donc de préserver la capacité d’endettement pour d’autres investissements plus stratégiques.


              *Et pour les collectivités publiques en particulier :*


              **4. Politiques de réduction des déchets électroniques :**


              Les collectivités locales ont à leur disposition une multiplicité d’outils juridiques (par opposition aux règles administratives/comptables) permettant de mettre en oeuvre elles-mêmes ou à travers la commande publique, les solutions qu’elles souhaitent promouvoir pour leur territoire\

              dans une perspective de développement durable. Il en est ainsi des politiques de réduction des déchets électroniques qui, par le biais de la location (économie de la fonctionnalité), pourraient répondre efficacement à ces enjeux. En effet, la solution de Commown optimise le réemploi des composants et des appareils et diminue d’autant la quantité de déchets électroniques.
          - title: Pourquoi nos prix sont-ils compétitifs ?
            details: true
            text: >-
              Il serait réducteur et incorrect de comparer le coût de la
              location rapportée au seul prix d’achat d’un appareil neuf. En
              effet :


              1. Un contrat de location longue durée inclut de nombreux SERVICES qui rentabilisent largement la charge globale induite à moyen/long-terme : **Il faut garder à l’esprit que pour votre organisation l’intérêt de la location c’est l’opportunité de pouvoir passer d’un modèle économique de la possession à un modèle économique de l’usage , ayant pour effet la maximisation de la durée de vie des appareils électroniques et le transfert complet des lourdes charges de maintenance et de renouvellement de la flotte que l’on oublie souvent dans les comparatifs. Commown propose donc une** offre tout compris sur la durée\*\* , au travers d’un service client performant pour assurer un usage optimal au quotidien pour vos utilisateurs.

                 En particulier : Aide et assistance, continuité de services, extension de la garantie constructeur pour les pannes pendant toute la durée du contrat, garanties casse, vol, perte, et autres services optionnels, **lesquels ont un coût** , en général porté par votre organisation de manière indirecte.

                 *NB : Les smartphones et ordinateurs proposés par Commown se prêtent bien à cette transition de modèle économique, puisqu’ils sont conçus pour durer longtemps et être facilement réparables.*
              2. Si vous comparez les coûts de location avec la concurrence : Lokéo propose le même type d’offre avec, pour exemple, des smartphones haut de gamme sous Android mais ceux-ci ne sont ni éthiques ni éco-responsables, et **les prix sont plus élevés que ceux proposés par Commown** , et avec une durée d’engagement jusqu’à 3 ans. Les prix montent encore plus pour des iPhones (jusqu’à 65€/mois TTC sur 2 ans).

              3. Enfin, le choix de notre gouvernance sous forme de Société Coopérative d’Intérêt Collectif (SCIC) garantit que nous offrirons les meilleurs prix possibles. Dans le cas où Commown génèrerait des excédents, les Commowners pourront voter sur l’utilisation de ces excédents, **y compris pour diminuer les prix si c’est le souhait majoritaire**.

                 Pour les smartphones, il serait tout aussi incorrect de comparer le coût de la location chez Commown avec les abonnements “appareil+forfait” proposés par les opérateurs. En effet :
              4. Les abonnements “appareil + forfait” proposés par les opérateurs sont en général compris entre 50 et 80€/mois, auxquels s’ajoutent les frais de renouvellement des smartphones tous les 2 ans (même si les tarifs sont préférentiels, il n’en reste pas moins des frais supplémentaires à payer), sans oublier tous les coûts indirects précédemment mentionnés.

                 Or il est courant de trouver des abonnements téléphoniques seuls à moins de 15€/mois, **ce qui laisse une marge conséquente pour une offre tout compris** comme celle proposée par Commown, et permet même de réduire la charge globale de l’usage de la téléphonie.
              5. Le principe même du remplacement systématique des smartphones tous les 2 ans est un contresens pour une transition vers **une électronique plus durable, éthique et écologique.** Il en est de même pour les contrats de renouvellement des ordinateurs.
          - title: Pourquoi est-ce plus éthique de louer chez commown ?
            details: true
            text: >-
              1. La location longue durée sans option d’achat s’inscrit dans le
              temps. Elle permet de donner une visibilité aux producteurs malgré
              un marché mouvant. C’est le même\

              principe que les AMAP en agriculture : vous aidez le producteur à avoir un meilleur contrôle de sa trésorerie à moyen terme, ce qui lui permet **des conditions de travail dignes pour assurer une production de qualité**. Par ailleurs, nous nous engageons à communiquer sur les producteurs qui s’engagent à nos côtés vers l’électronique durable. L’augmentation progressive de leur part de marché qui en résultera permettra de stabiliser d’autant leur activité. L’objectif à terme est de soutenir financièrement de la R&D pour améliorer les appareils existants et/ou en concevoir de nouveaux, encore plus écologiques et éthiques.


              2. Parce que nous cherchons à prolonger l’engagement des constructeurs de **diminuer les impacts sociaux et écologiques liés à l’électronique**. En louant avec Commown, c’est une garantie que les appareils vont servir le plus longtemps possible. En effet, même quand vous n’en aurez plus besoin, nous les louerons à d’autres utilisateurs après les avoir remis en état de fonctionnement optimum.


              3. Parce que nous ne sommes pas une entreprise “classique” mais une Société Coopérative d’Intérêt Collectif (SCIC). Choisir d’être une SCIC, c’est affirmer que notre motivation n’est pas le profit mais l’intérêt collectif et la gestion démocratique. En effet, dans une SCIC, la redistribution des bénéfices est particulièrement encadrée : 57,5 % au minimum des excédents d’exploitation éventuels sont obligatoirement réinvestis. De plus, les mécanismes de décision et de vote en assemblée générale protègent la SCIC d’un rachat inamical. En effet, les droits de vote dans la SCIC fonctionnent sur le principe de “un Commowner = une voix”, ainsi tous les Commowners sociétaires (clients, producteurs, médias, porteurs de projet, bénévoles, soutiens financiers, etc.) possèdent ensemble la flotte d’appareils loués et prennent les grandes décisions ensemble.


              4. Pour les autres enseignes de location, la marge sur les mensualités sert souvent à rémunérer des actionnaires. Donc au même titre qu’en prenant un abonnement chez Enercoop vous pouvez être certain de financer la transition énergétique et non les actionnaires d’ENGIE ou EDF, chez Commown vous serez certain de **financer la transition vers une électronique** **plus responsable** et non les actionnaires d’Apple, Samsung & cie.
          - title: " Comment la stratégie rse de mon organisation me permet de financer
              cette location d’appareils électroniques responsables ?"
            details: true
            text: >-
              **1\. Enjeux RSE vs Comptabilité :**


              Une organisation n’est plus seulement évaluée sur sa capacité à créer de la valeur financière (ou à optimiser la gestion du service public dans le cas des collectivités), mais également à restaurer le capital naturel. La comptabilité environnementale permet ainsi de savoir et faire savoir si l’organisation est globalement régénératrice (bilan positif) ou consommatrice (bilan négatif) de ressources naturelles. Elle est désormais regardée au même titre que la comptabilité financière, notamment par les investisseurs (ou par les citoyens dans la cas des collectivités).


              **2\. Enjeux RSE vs Prix :**


              Toute organisation qui affiche une politique RSE s’engage à financer ses opérations sur le modèle suivant :


              - Prix admissible selon les contraintes de rentabilité  

              - **PLUS un delta dédié aux engagements RSE**, dotés d’un budget propre


              Dans le cas présent, ce budget dédié permet de financer :


              - d’une part des **impacts positifs sociétaux et environnementaux** liés au modèle économique et aux qualités des appareils loués ;  

              - d’autre part l’intérêt collectif porté par la SCIC Commown visant à améliorer sur le long terme la filière de l’électronique dans son ensemble


              _NB : Une politique RSE s’appuie aussi sur le choix **d’une exigence raisonnable des performances attendues** (exemple : entre celles d’un iPhone et d’un Fairphone), conduisant **à une plus grande sobriété d’usage des appareils** (les utilisateurs devraient par exemple se passer d’appareils capables de faire fonctionner des applis gourmandes en CPU ou en énergie comme les derniers jeux ou autres applications superflues pour les usages professionnels). La prise de conscience de ces notions de sobriété d’usage prend tout son sens en terme d’impacts sur la durée de vie des appareils (réduite par l’obsolescence logicielle) et donc sur l’empreinte écologique de l’électronique._
          - title: Comment se formalise l’engagement rse pour commown ?
            details: true
            text: >-
              L’offre Commown permet de faire valoir un haut niveau de
              Responsabilité Sociétale et Environnementale. Cela inclut les
              labels de produits, comme le Fairphone (B-Corp), mais également le
              projet Commown en lui-même :


              - une société coopérative d’intérêt collectif (SCIC)  

              - reconnue entreprise de l’ESS (mention au Kbis)  

              - labellisé Efficient Solution Solar Impulse 

              - recommandée par des experts de l’économie circulaire comme Circul’R et La Famille Zéro Déchet


              Enfin, la cohérence et l’engagement de Commown se constatent à tous les niveaux, depuis l’énergie verte pour les serveurs, jusqu’aux livraisons neutres en carbone. Pour en savoir plus consultez la section cohérence en bas de la page [“qui sommes nous”](https://commown.coop/qui-sommes-nous/).
        ctas:
          - outline: false
            href: /faq
            text: Consultez notre FAQ
---
