---
title: "FairTEC, une marche de plus vers le changement de paradigme ! "
description: FairTEC est la nouvelle technologie propre au Fairphone, la plus
  durable et éthique du marché européen. Focus sur cette nouvelle technologie...
date: 2021-09-24T13:28:43.523Z
translationKey: fairtec
image: /media/fairtec2-compress.jpg
---
*"Sans changement radical, le numérique sera rapidement confronté à une crise existentielle comme le secteur aérien l’est aujourd’hui. Ouvrir une autre voie par la coopération entre acteurs engagés au niveau Européen est essentiel !” (Adrien Montagut, Co-fondateur de Commown), cette nouvelle voie vient de voir le jour avec FairTEC.*

**[FairTEC](https://fairtec.io/fr/) en deux mots ?**

Tout simplement, FairTEC c’est un Fairphone, muni du système d’exploitation /e/OS (open source), loué par Commown et avec un forfait TeleCoop (en France), WEtell (en Allemagne), ou Your Coop (au Royaume-Uni). À ce jour, FairTEC est l’offre mobile existante qui est la plus durable et éthique du marché européen.

**Pourquoi FairTEC ?**

Derrière les avantages des technologies se cachent des plaies.
Ces plaies s’appellent la surproduction d’appareils, l’extraction des matières premières rares réalisées dans des conditions déplorables par des hommes, des femmes et des enfants dans les pays pauvres, l'extinction de la biodiversité, les travailleurs du clic, la nomophobie (et ses conséquences), le monopole des GAFAM, etc. 
Face à ce sombre tableau, un médecin préconisait un sevrage technologique drastique et immédiat ! Malheureusement, pour beaucoup, il est difficile aujourd’hui de se passer de smartphone... Quelle solution palliative existe-t-il dans ce cas ?
 → Faire durer les appareils au maximum ! 

**Qu’est-ce que FairTEC ?** 

FairTEC est un collectif d’entreprises européennes qui travaillent ensemble pour créer et offrir des solutions intégrées plus durables et plus éthiques pour les smartphones. Elle réunit six acteurs engagés dans le changement pour la durabilité.

Le [Fairphone](https://www.fairphone.com/fr/) est le smartphone le plus éco-conçu à l’heure actuelle. [TeleCoop ](https://telecoop.fr/)est le premier opérateur coopératif français de télécoms engagé dans la transition écologique et solidaire, qui propose notamment un forfait sans engagement et au tarif accessible, et qui promeut la sobriété. [WEtell ](https://www.wetell.de/)et [Your Coop](https://broadband.yourcoop.coop/) sont également des opérateurs de télécoms européens engagés. [/e/OS](https://e.foundation/fr/) est un système d’exploitation open source basé sur Android dans lequel toutes les traces de Google ont été effacées et qui protège donc efficacement la vie privée de son utilisateur. Notre coopérative fournit enfin le Fairphone avec des services pour lutter contre l’obsolescence programmée : prise en charge des pannes, changement de batterie si nécessaire, protection contre la casse, etc.

**Pourquoi FairTEC est essentiel selon Commown ?** 

Deux raisons nous poussent à participer à l’aventure FairTEC. La première est qu’elle constitue une première pierre vers une offre européenne alternative et complète. La seconde est qu’elle démocratise le concept de “Fairphone as a Service” dont l’objectif est de faire durer les appareils en s'affranchissant du modèle de la vente. Le cœur de notre métier, ce qui nous anime chez Commown, c’est de faire durer les appareils dans le temps pour limiter les impacts écologiques et sociaux. Or, le modèle économique de la vente est insuffisant pour y parvenir. En effet, les entreprises sont contraintes pour survivre de continuer à vendre sans cesse et donc de jongler avec les lois du marché qui reposent aujourd'hui sur l’obsolescence matérielle, logicielle ou marketing. 

Ainsi nous pensons qu’un rapprochement avec les autres acteurs du collectif FairTEC permettra de diffuser le modèle du Fairphone as a Service au plus grand nombre.