---
title: "La 5G : technologie obsolète !"
description: La 5G est-elle une réellement technologie efficace, voire
  nécessaire ? Commown se penche sur ce cas controvresé.
date: 2021-10-08T15:48:50.391Z
translationKey: 5G
image: /media/56gcomp.jpg
---
*«J'entends beaucoup de voix qui s'élèvent pour nous expliquer qu'il faudrait relever la complexité des problèmes contemporains en revenant à la lampe à huile ! Je ne crois pas que le modèle Amish permette de régler les défis de l'écologie contemporaine»* - Emmanuel Macron septembre 2020

#### **5G avec ou “sans filtre” ?**

Difficile d’être passé à côté de ce débat en septembre 2020. Alors que l’une des mesures de la convention citoyenne pour le climat proposait un moratoire sur la 5G, notre président sortait un nouveau "joker" de sa manche pour écarter cette proposition de la loi climat. 

Cette loi qui se voulait sans filtre, arrivait tout juste à l’assemblée au moment même où [l'ARCEP](https://fr.wikipedia.org/wiki/Autorit%C3%A9_de_r%C3%A9gulation_des_communications_%C3%A9lectroniques,_des_postes_et_de_la_distribution_de_la_presse) s'apprêtait à vendre les bandes fréquences 3,4 ‑ 3,8 GHz aux opérateurs sans aucune remise en question. Ainsi, plutôt que d’écouter le bon sens des 150 citoyens, il était bien plus simple de les taxer d’Amish et de présenter la 5G comme un outil pour “relever la complexité des problèmes contemporains”. 

Un an plus tard, alors que le dernier Fairphone se rend compatible avec cette technologie, il nous semblait important de revenir sur ce débat.

#### **Nier les effets rebond... A ne pas reproduire chez vous  !**

En premier lieu, il est tout de même important de mettre en lumière cette notion d’effet rebond qui a fait l’objet d’un déni sans pareil lors de ce débat public.  

L’effet rebond qualifie les effets négatifs non anticipés d’une technologie présentée comme étant “efficace”. Cédric O, le secrétaire d'État chargé du Numérique mettait en avant de mirifiques gains énergétiques. Il affirmait que la 5G « pour la même utilisation consomme dix à vingt fois moins que la 4G » (en se référant à la présumé sobriété unitaire d’une antenne 5G vs une 4G). Deux bémols contredisent cette affirmation : \
\
(1) pour être rentable, le déploiement de cette technologie nécessite inévitablement une augmentation de la consommation de bande passante du réseau (les promoteurs de la 5G contredisent donc par construction cette idée de “même utilisation”). \
\
(2) Pour couvrir la même superficie il faut beaucoup plus d'antennes 5G que 4G du fait de leur faible portée. \
\
In fine, le bilan comptable énergétique sera forcément négatif comme l’a démontré le Shift Project dans [son rapport sur le sujet](https://theshiftproject.org/article/impact-environnemental-du-numerique-5g-nouvelle-etude-du-shift/). 

#### **La 5G nouvelle égérie de notre société de surproduction.**

Le débat sur la consommation énergétique - qui a principalement focalisé l’attention médiatique - a masqué la problématique essentielle de l’empreinte carbone et de la surconsommation de ressources. L'avènement de cette nouvelle technologie nous promet de multiples nouveaux usages : agriculture connectée, voitures autonomes, réalité augmentée, vidéo surveillance intelligente… 

Autrement dit, la 5G induit la mise en service de millions de nouveaux produits connectés tous plus consommateurs de ressources les uns que les autres et à la facture carbone astronomique. 

#### **Que faire face à cette fuite en avant effrénée ?**

En tant qu'acteur économique, dès que nous avons l'opportunité de pousser un plaidoyer radical sur la question, nous la saisissons. Nous participons à différentes “concertations” notamment, celles menées par l’ARCEP “pour un numérique soutenable” (vous pouvez retrouver notre contribution à la [page 50 de ce rapport](https://www.arcep.fr/la-regulation/grands-dossiers-thematiques-transverses/lempreinte-environnementale-du-numerique/plateforme-de-travail-pour-un-numerique-soutenable.html)). Toutefois, force est de constater que la bataille du déploiement des bandes fréquences 3,4 ‑ 3,8 GHz est perdue. La prochaine étape du déploiement de la 5G devrait être celui de la bande fréquence 26 GHz associée au “small cells” ces petites antennes qui devraient envahir l’espace urbain par milliers pour bien mailler le territoire. Des partenariats avec JCDECAUX sont déjà amorcés avec les opérateurs pour pouvoir les installer [sur les abribus](https://www.jcdecaux.com/fr/communiques-de-presse/jcdecaux-accelere-le-deploiement-des-petites-antennes-small-cells-sur-ses) ou sur les panneaux publicitaires lumineux (le combo ultime). Donc autant dire que nous continuerons nos actions de plaidoyer pour s’opposer au déploiement sans limitation de cette technologie. 

Mais, déjà se profile la 6G que l’on nous présente déjà comme une nouvelle prouesse technologique… [Notre vision du progrès](https://commown.coop/blog/progr%C3%A8s-vous-avez-dit-progr%C3%A8s/) est encore loin d’être la norme.

#### **Et Fairphone dans tout ça ?**

Fairphone ne pouvait malheureusement pas faire l’impasse sur cette technologie pour rester compétitif vis-à-vis des autres smartphones du marché, surtout pour un modèle qui a vocation à rester sur le marché plus longtemps que ses concurrents. Selon nous, ils subissent l’hérésie du marché dans lequel ils se trouvent. Mais, au regard des améliorations environnementales et éthiques du Fairphone 4, nous pensons qu’il est important de continuer à soutenir cet acteur emblématique de l’électronique responsable. 

#### **En tant que consom’acteur en opposition avec la 5G,  plusieurs options s’offrent à vous :**

**1** Boycotter autant que possible tous les produits connectés 5G inutiles qui vont émerger à l’avenir, de l’aspirateur à la poignée de porte en passant par les couettes connectées, si si ça existe déjà ;-) 

**2** Si votre smartphone actuel fonctionne, conservez-le le plus longtemps possible avant de changer d’appareil.

**3** Si la 5G vous irrite au plus haut point et que vous devez changer d'appareil, préférez plutôt notre [offre Fairphone 3](https://shop.commown.coop/shop/category/smartphones-fairphone-3) ou [Crosscall X4](https://shop.commown.coop/shop/product/crosscall-core-x4-234?category=17), ou mieux considérez l’option [de ne plus avoir de smartphone](https://lejournalminimal.fr/sans-telephone-portable/).

**4** Pensez à changer d’opérateur pour aller chez [TeleCoop](https://telecoop.fr/) qui propose des abonnements (sans 5G) et vous accompagne vers toujours plus de sobriété !



\----- \
\
Pour aller plus loin sur le sujet : \
Rapport "[Maîtriser l’impact carbone de la 5G](https://www.hautconseilclimat.fr/publications/maitriser-limpact-carbone-de-la-5g/)" du Haut Conseil pour le Climat\
Rapport "[La controverse de la 5G](https://gauthierroussilhe.com/ressources/la-controverse-de-la-5g)" de Gauthier Roussilhe
