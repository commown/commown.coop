---
title: Une brève Histoire du déni écologique
description: Don't look up a-t-il visé juste sur notre rapport avec l'écologie ?
  Commown apporte sa vision sur l'inaction face au réchauffement climatique
date: 2022-02-23T09:09:28.230Z
translationKey: dont look up
image: /media/dont-look-up-ecologie-commown.jpg
---
> De “*Notre maison brûle*” à “*make our planet great again*”, Commown dresse le récit de l'échec politique pour répondre aux enjeux écologiques. À noter, l’article ne se veut pas exhaustif, il s’agit d’un résumé, d’une vulgarisation très synthétique et militante.

Avant d'évoquer notre brève Histoire du déni écologique qui démarre pendant la deuxième moitié du XXème siècle, on pourrait parler de la pré-histoire de l’écologie. En effet, déjà chez les Grecs Anciens et plus particulièrement chez les Stoïciens, on retrouve une dimension  naturaliste, c’est-à-dire l’idée que les hommes doivent vivre en harmonie avec la nature. La révolution industrielle a peu à peu transformé le rapport que l’humanité entretient avec la nature.

C’est au cours de la fin des années soixante que l’écologie politique émerge en Europe et commence à critiquer le productivisme et ses conséquences funestes.

## Naissance de l’écologie politique

On peut dire que tout commence avec le Club de Rome (1968). Il s’agit d’un groupe d'experts qui a rédigé le premier rapport devenu célèbre connu sous le nom de “Rapport Meadows”. En français, il a été titré “Halte à la croissance” (1972) et, pour la version suivante “Sortir de l’ère du gaspillage” (1974). D’autres rapports ont été rédigés par la suite. Ces rapports montrent qu’il est impossible d’envisager une civilisation pérenne avec le modèle économique actuel, basé sur l’extraction de ressources et la croissance. C’est en 1972 à Stockholm qu’est organisée la Conférence des Nations-Unis sur l'environnement. Il en émerge la notion “d’éco-développement”. Elle sera complétée en 1978 par la WWF et s'appellera: ”éco-développement durable”.

## L’émergence du développement durable

C’est en 1987, dans le rapport Brundtland (lui-même résultant de la commission Bruntland de 1983), qu'apparaît le concept de développement durable ou soutenable. “*Le développement durable est un développement qui répond aux besoins du présent sans compromettre la capacité des générations futures de répondre aux leurs*”(cf. [wiki](<https://fr.wikipedia.org/wiki/D%C3%A9veloppement_durable >)). Cette quête du découplage de la croissance du PIB et de la consommation de ressources nous mène dans une impasse puisque les ressources sont limitées.\
En parallèle, en 1985, 22 pays signent la [convention de Vienne](https://fr.wikipedia.org/wiki/Convention_de_Vienne_sur_la_protection_de_la_couche_d%27ozone) en faveur de la protection de la couche d’ozone, historiquement, ce sera sans doute le seul moment où ce type d’accord aura eu un effet sur notre environnement.

## Inflation de sommets climatiques à l’international

C’est en 1988 que le GIEC ([Groupe d’experts Intergouvernemental sur l’Évolution du Climat](https://fr.wikipedia.org/wiki/Groupe_d'experts_intergouvernemental_sur_l'%C3%A9volution_du_climat)) est créé pour évaluer les informations scientifiques, techniques, socio-économiques sur l’évolution du climat. En 1992 a lieu le premier “Sommet de la terre” à Rio, à ce moment 172 chefs d'États signent “[l’agenda 21](https://fr.wikipedia.org/wiki/Agenda_21)” : celui-ci dresse les objectifs de développement durable avec plus de 2500 recommandations sur la santé, la pollution de l’air, la gestion des mers, forêts, montagnes, le logement, l’agriculture, les déchets, l’eau, la pauvreté… Le [Sommet de RIO ](https://fr.wikipedia.org/wiki/Conf%C3%A9rences_des_Nations_unies_sur_les_changements_climatiques)précède le lancement des COP dont la première se tiendra à Berlin en 1995. En 1997 a lieu la “Conférence sur l’effet de serre” de Kyoto (la COP3), celle-ci a abouti à la création du célèbre [protocole de Kyoto](https://fr.wikipedia.org/wiki/Protocole_de_Kyoto).

En 2002, c’est au tour du “Sommet mondial du développement durable” de faire son apparition à Johannesburg. C’est à ce Sommet que Jacques Chirac a prononcé l'illustre discours “*Notre maison brûle et nous regardons ailleurs…*” (cf. [extrait du discours de Chirac](https://www.youtube.com/watch?v=D6tRRetM4Mw)). Slogan qui est depuis entré au panthéon des déclarations écologiques (vides d’actions). En 2007, s’ouvre le “[Grenelle de l’environnement”](https://fr.wikipedia.org/wiki/Grenelle_de_l%27environnement) organisé par la France et visant comme les autres Sommets à poser un cadre en faveur de décisions communes et internationales sur le développement durable.

## La farandole des COP en musique : et 1, et 2, et 3°C…

![Infographie qui définit les COP (sigle pour "Conférence des Parties") comme des moments politiques lors desquels les États se réunissent pour définir les objectifs internationaux relatifs aux problématiques environnementales. ](/media/cop-cop-cop.jpg)

En 2012 a eu lieu une nouvelle conférence à Rio (20 ans après celle de 1992) intitulée “L’avenir que nous voulons” : ce fût un échec. Par la suite, plusieurs conférences ont eu lieu les fameuses COP* (voir [liste](https://fr.wikipedia.org/wiki/Conf%C3%A9rences_des_Nations_unies_sur_les_changements_climatiques)). L’une des plus célèbres est la COP21 (dites aussi Conférence de Paris) de 2015. Elle a débouché sur un accord adopté par 195 pays avec plusieurs critères à respecter pour limiter le réchauffement climatique à +2°C. Cependant, l’accord ne permet pas d’infliger de sanction aux pays ne jouant pas le jeu... En 2019 la COP25 a été un fiasco (voir l’article de [France Inter](https://www.franceinter.fr/environnement/desastreux-angoissant-du-jamais-vu-l-echec-de-la-cop25-apres-deux-semaines-de-vaines-negociations)) : “aucun des grands pays émetteurs de CO2 n'a pris d'engagement concret”. Enfin, l’année dernière, la COP26 n’a pas donné de meilleurs résultats (cf. l[e bilan qu’en font les Amis de la Terre](https://www.amisdelaterre.org/cop26-on-fait-le-point/)).

![Graphique qui montre que les sommets internationaux sur le climat sont sans effet sur l'augmentation des températures et de la concentration en CO2 dans l'atmosphère](/media/ecologie.jpg)

## Face à l’inaction des politiques, quels sont nos leviers ?

Au niveau international comme le montre la figure ci-dessus, les COP n’ont pas infléchi la trajectoire d’augmentation de la concentration en CO2. En France, l’État a été condamné pour préjudice écologique au titre de son inaction (voir [ici](https://www.vie-publique.fr/en-bref/282012-changement-climatique-la-france-condamnee-pour-prejudice-ecologique)). Malgré ce contexte alarmant, l’écologie est presque absente des débats de la campagne présidentielle. Certains politiques affirment que la France ne représente qu’1% de la production mondiale de CO2 et en déduisent qu’elle n’est pas concernée. Or, ce chiffre est à débunker car il ne tient pas compte des [consommations importées](https://www.francetvinfo.fr/meteo/climat/vrai-ou-fake-la-france-represente-1-des-emissions-de-co2-on-vous-explique-les-limites-de-l-argument-d-eric-zemmour-sur-le-dereglement-climatique_4952091.html). Une fois cette donnée intégrée, il apparaît que la France pollue davantage que la Chine par habitant (au regard de la consommation des ménages). Alors que nos politiques se fendent de belles phrases comme "*la maison brûle*”, “*make our planet great again*”, ils n'agissent pas. Que nous reste-t-il comme levier d'action à titre individuel ?

* Travailler - quand c'est possible - dans une entreprise qui a du sens et qui porte un projet écologique, social ou culturel ;
* Sensibiliser son entourage (ne pas avoir peur de confronter ses idées sur le sujet #RepasDeNoël) ;
* Rentrer en dé-consommation (consommer moins) ;
* Adopter une alter-consommation (consommer mieux) ;
* Boycotter (ex avec [I-buycott](https://i-buycott.org/)) ;
* Manifester pour dénoncer l’inaction (ex : rendez-vous à la [marche Lookup du 12 mars](https://reseauactionclimat.org/look-up-marche-pour-le-climat-rendez-vous-le-12-mars/) !) ;
* Faire pression sur vos élus locaux par du plaidoyer citoyen ;
* S'investir auprès d’une association locale (agir à son échelle en faisant une activité qui fait sens) ;
* Et éventuellement, quand il le faut, désobéir (ex : [ici contre Amazon](https://www.amisdelaterre.org/communique-presse/amazon-des-habitants-bloquent-les-travaux-de-2-entrepots-en-bretagne-et-en-normandie/) ou [ici contre l’industrie du charbon](https://www.youtube.com/watch?v=GYVXTz5kcMA)) ;

Nous conclurons cet article par la phrase d’une contributrice de Commown : “*Ceux qui luttent ne gagneront peut-être pas, mais ceux qui ne luttent pas ont déjà perdu*”

\**Attention, il ne faut pas confondre COP (“Conférences des parties”) avec COOP (les coopératives trop cools ;) ) : si le volet coop avec 2 O vous intéresse allez donc voir du côté des [Licoornes](https://www.licoornes.coop/))*
