---
title: Ce qu’on ne vous dira jamais à propos du smartphone !
description: Addictif, indispensable, le smartphone a bien des revers sur la
  société que vous n'entendrez jamais dans la communication des marques. Commown
  vous en résume les détails.
date: 2022-07-20T12:04:33.923Z
translationKey: smartphone
image: /media/smartphone.jpg
---
> Je suis addict à la cochonnerie du portable, vous imaginez qu’on regarde le portable entre 300 et 400 fois par jour. Personne ne mesure la catastrophe que ça représente. Personne ne sait que c’est la guerre contre la courtoisie, la délicatesse, le détour, la conversation. Et je suis addict ! Je ne suis pas là pour dire que je juge ! J’en suis esclave comme du crack !” [Fabrice Luchini, le 02/02/2022 sur France Inter](https://www.franceinter.fr/emissions/l-invite-de-7h50/l-invite-de-7h50-du-mercredi-02-fevrier-2022)

### C’est quoi un smartphone aujourd’hui ?

Un changement anthropologique inédit, un marqueur de la modernité... En 20 ans, la société a radicalement changé avec le numérique. Nos modes de vies, nos usages, nos pratiques sont impactés. Par exemple, on sait que les ⅔ des Français consultent leur smartphone dans l’heure qui suit leur réveil. Le smartphone est devenu le compagnon le plus fidèle de l’homme moderne. Il nous accompagne partout et s’incruste dans les moments les plus intimes de nos vies. Cet article n’a qu’une seule prétention, celle de présenter le smartphone et ses usages sous un angle humoristique.

### Un objet (devenu) indispensable

Il est d’abord un outil de communication (appels, sms, réseaux sociaux) : véritable lien de connexion au monde. Mais désormais, il dépasse cette simple fonctionnalité. Multifonctions, les nombreuses applications ouvrent un champ des possibles inouï. On peut commander à manger, trouver l’adresse d’un nouveau coiffeur, scanner un produit au supermarché pour voir sa composition, mesurer le nombre de calories dépensées en marchant, “matcher” un.e meuf/keum dans la rue, pister les déplacements de ses enfants, et plus classiquement lire, écouter de la musique, regarder une vidéo, dessiner, jouer à “candy crush”, filmer sa progéniture jouant au football, faire un selfi avec ses potes, etc. Bref, c’est un véritable couteau-suisse au cœur de nombreuses activités humaines. On se demande comment on pouvait vivre avant ça...? 

### Un objet exclusif (et jaloux)

Imaginez que vous soyez autour d’un repas dans un resto avec des amis. Vous êtes tous là, heureux de vous retrouver. Vous discutez avec votre voisin de table, assis à côté de vous. Soudain, il reçoit un message. Une notification apparaît sur l’écran du téléphone posé sur la table. Vous êtes en train de parler, mais la tentation est trop forte. Votre ami.e saisit son téléphone pour lire. Puis, il commence à pianoter une réponse en ignorant complètement votre présence et l’attention polie que vous lui portiez en discutant. Vous vous retrouvez seul, mais avec quelqu’un, étrange n‘est-ce pas ? Ce moment est caractéristique, votre voisin est marabouté, envoûté par l’objet. Le smartphone vous l'a dérobé, c'est fini !

### Un objet symbolique

Qui ne connaît pas cette personne attendrissante, fière d'exhiber - non sans pudeur - cet objet performant, dur et électroniquement puissant, que l’on nomme “smartphone” ? On connaît tous ce consommateur des temps modernes pour lequel il est important de posséder le dernier smartphone à la mode afin d'être “aimé”, “accepté”, “reconnu” et “respecté”  par les autres.  Celui-ci pense qu’avoir c’est être mais il ne sait pas qu’il s’est fait avoir (par les discours publicitaires). Les publicitaires se sont servis de cette faiblesse de la nature humaine pour vendre leur camelote électronique devenue le prolongement des égos en quête de reconnaissance. Rassurons les consommateurs dociles qui se sont égarés sur le chemin de la “tech”, même sans le dernier smartphone à la mode, on les aime quand même !