---
title: Pourquoi faire le choix de l’économie de la fonctionnalité et de la
  coopération (EFC) aujourd’hui ?
description: L'économie de la fonctionnalité est un moyen bien supérieur à la
  vente en terme de durabilité, de qualité et d'éthique. Commown vous explique
  pourquoi.
date: 2021-05-10T11:25:03.000Z
translationKey: efc
image: /media/efc.png
---
## Le modèle classique (la vente, quoi !) nous conduit dans une impasse !

Classiquement, on fabrique un produit, celui-ci est commercialisé puis vendu jusqu’à ce qu’il rende l’âme et que le consommateur en achète un autre. Sait-on seulement, ô combien d’ordi/nosaures se sont éteints depuis ces dernières décennies ? Trop, hélas ! On peut voir les vestiges de ces espèces disparues dans [des reportages vidéos](https://www.youtube.com/watch?v=mIlNGjKJK-M).

C’est là que nous pouvons entrevoir la limite du modèle classique ! Si les produits ne cessaient pas de fonctionner, les producteurs n’auraient plus d’acheteurs et feraient faillite… Cette locution est une évidence car une fois le marché inondé d’un produit l’entreprise ne peut plus vendre. C’est selon nous ici que réside la source de tous les mécanismes d'obsolescence prématurée, qu’ils soient d’ordre marketing, logiciel ou matériel. 

## Quésaco l'économie de la fonctionnalité ?

Le modèle de l’économie de la fonctionnalité permet de lutter contre l’obsolescence programmée car l’intérêt du consommateur et du fournisseur convergent (enfin !) vers un intérêt commun : que le produit dure le plus longtemps possible dans le temps… 

Pour faire simple l’économie de la fonctionnalité c’est vendre l’usage d’un produit au lieu de vendre le produit. Contrairement à ce que l’on peut croire, c’est une pratique qui n'est pas si récente que cela. Prenez par exemple le roman “Martin Eden” de Jack London dans lequel le héros éponyme loue la machine à écrire qui lui permettra de devenir écrivain. Ce roman a été publié il y a un plus d’un siècle et on l’on retrouve dans cet exemple l’essence même de l’économie de la fonctionnalité. 
Ce principe de location longue durée sans option d’achat est souvent admis pour un service “immatériel” comme par exemple, un abonnement à une plateforme de streaming en ligne ou un abonnement internet. Quand on s’abonne à internet, le Fournisseur d’Accès Internet vous envoie un modem qu’il faut restituer quand on arrête l’abonnement. On loue donc l’usage d’un bien pour accéder à un service. 

## C comme Coopération ?

Toutefois, tous les modèles “locatifs” ne sont pas forcément vertueux. L’économie de la fonctionnalité et de la coopération (EFC) doit véritablement permettre « une moindre consommation des ressources naturelles dans une perspective de développement durable pour les personnes, les entreprises et les territoires. » [voir le site de l’IEFC](https://www.ieefc.eu/).


Commown s’inscrit dans ce modèle nous proposons des offres de location longue durée avec services et sans option d’achat dans un objectif de durabilité. D’une part notre coopérative (SCIC*) sélectionne du matériel électronique (le plus éco-conçu possible !) comme le Fairphone ou les ordinateurs Why. D’autre part, l’ensemble des appareils loués constitue une flotte, véritable bien commun de la coopérative co-possédé par l’ensemble des sociétaires (clients, salariés, partenaires…). Il est important de savoir que dans une entreprise classique, ce sont les actionnaires majoritaires qui ont le pouvoir (en proportion de la somme investie). Dans une coopérative, chaque sociétaire compte pour une seule voix. Cela veut dire qu’un sociétaire qui investit 10 000 € et un sociétaire qui investit 100 € auront le même pouvoir décisionnel au moment d’un vote lors de l’Assemblée Générale.

*\* Nous reviendrons dans un prochain article de blog sur notre ADN coopératif et le statut de SCIC : Société Coopérative d’Intérêt Collectif.*