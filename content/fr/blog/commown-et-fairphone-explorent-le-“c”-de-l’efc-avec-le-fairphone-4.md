---
title: Commown et Fairphone explorent le “C” de l’EFC avec le Fairphone 4 !
description: Commown et Fairphone renforcent leur partenariat pour faire avancer
  l’entreprise hollandaise vers l’économie de la fonctionnalité et de la
  coopération (EFC) en France.
date: 2022-09-01T09:09:00.151Z
translationKey: fairphone-commown-faas-partnership
image: /media/puzzle-fairphone-efc-commown.jpeg
---
*Commown et Fairphone renforcent leur partenariat pour faire avancer l’entreprise hollandaise vers l’économie de la fonctionnalité et de la coopération (EFC) en France. Récit de cette aventure…*\
\
**L’origine**

En 2017, Commown s’est créée avec l’idée que le modèle économique dominant dans l’électronique n’était pas viable. En effet, ce modèle se résume à produire et vendre. Or, sur un marché saturé, pour vendre des appareils neufs, il faut pousser au renouvellement rapide des appareils.

Au contraire, dans le modèle de l’économie de la fonctionnalité et de la coopération (EFC), l’appareil est fourni dans le cadre d’un service : le client paye mensuellement pour l’usage de l’appareil et les services pour le faire durer. L’entreprise reste propriétaire de l’appareil et a donc un intérêt économique à le faire durer (voir [notre article de blog sur le sujet](/blog/economie-de-la-fonctionnalite/)).

Les fondateurs de Fairphone avaient très tôt compris les enjeux de ce qu’ils nommaient le Fairphone as a Service (FaaS), voir notamment l’article publié en 2018 [sur leur blog](https://www.fairphone.com/en/2018/01/08/from-ownership-to-service-new-fairphone-pilot-for-companies/). Cependant, leur priorité était de se consolider, gagner leur place auprès du grand public.

Par sa modularité, le choix du Fairphone s'est imposé à Commown pour expérimenter ce modèle économique circulaire. Il est devenu le produit pilote de notre coopérative.

![Photo d'un Fairphone 4 démonté](/media/fp4_exploded_green.jpeg "Le Fairphone 4, un smartphone modulaire")

Par ailleurs, dès sa création, notre coopérative avait inscrit dans le préambule de ses statuts l’accompagnement des producteurs vers l’EFC, en objectif de long terme.

\
**Le chemin : inciter économiquement à la durabilité**

Cet objectif d'accompagnement s’est réalisé beaucoup plus tôt que prévu avec l’arrivée des offres de casques Gerrard Street, en 2020. Le contrat avec ce producteur était le premier du genre : un achat à prix plus faible en échange d’une commission sur la location. Cela permet au producteur de garder un intérêt économique à la durabilité sans avoir à gérer la complexité d’une offre d’EFC.

Un an plus tard, nous avons mis en place un partenariat similaire avec notre fournisseur d’ordinateurs portables : why open computing.

Toujours en 2021, la coopérative a participé à la fondation [du collectif FairTEC](https://fairtec.io/fr/) avec notamment Fairphone, Telecoop, et la /e/ Foundation. Ce collectif a permis de resserrer les liens entre Commown et Fairphone.

Fairphone continuait à réfléchir à son modèle économique, ce qui a finalement mené à la création de l’offre [Fairphone Easy](https://www.fairphone.com/fr/2022/06/15/fairphone-easy-a-smartphone-subscription-for-a-fairer-future/) aux Pays-Bas : une offre de Fairphone as a Service (FaaS) proche de celle de Commown.

\
**L'aboutissement**

Fort des expériences avec Gerrard Street et why open computing, Commown a pu co-construire avec Fairphone un partenariat équivalent.

Ainsi, à partir du 1er septembre 2022, les commandes de Fairphone 4 passées par des particuliers sur notre boutique en ligne se feront dans le cadre de cette expérimentation commune.

Du côté de Fairphone, l’offre Commown est proposée comme alternative à l’achat sur la page francophone.

![screenshot du site de Fairphone montrant l'encart Commown](/media/illu-encart-fp4-site-fairphone-2022.jpeg)

Tout cela ne change rien pour les clients, [c’est la même offre au même tarif](https://shop.commown.coop/shop/product/fairphone-4-256-8go-avec-services-essentiel-et-duo-protecteur-357).\
\
Par contre, cela permet à Commown d’atteindre plus vite le seuil de rentabilité par appareil puisque les appareils sont achetés moins chers, et cela permet à Fairphone de toucher une commission pour chaque appareil en service. Un pas de plus vers un modèle économique durable pour Fairphone !\
\
“*Nous sommes très heureux d’intensifier notre partenariat avec Commown, car nous partageons cet objectif commun de changer la façon dont les smartphones sont utilisés et produits.*" 

Ronald van Harten, Directeur Marketing de Fairphone

**La coopération, nerf de la guerre dans un monde fini**

Dans un monde où les ressources s’amenuisent et les inégalités explosent, la coopération et le juste partage des richesses s’imposent ! C’est ce qui nous a poussé à choisir le statut de Société Coopérative d’Intérêt Collectif qui permet de rendre concrète cette volonté de coopération.

 \
Ainsi, ce projet permet d’intégrer au sein de la coopérative les intérêts économiques de Fairphone et de clients sociétaires. Ces derniers ont un droit de regard direct sur les offres tarifaires, ce qui nous oblige dès leur conception à tendre vers le prix le plus juste possible ! D’ailleurs, Commown s’engage à rembourser la différence si vous trouvez moins cher sur 5 ans, à service équivalent !