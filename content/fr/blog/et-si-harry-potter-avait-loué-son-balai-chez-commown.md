---
title: Et si Harry Potter avait loué son balai chez Commown ?
description: "Un Nimbus 2000 remplacé puis réparé en quelques jours, moins de
  déforestation à cause de l’industrie du balai volant, découvrez comment Harry
  Potter aurait pu sauver le monde grâce au modèle de Commown/l’économie de
  l’EFC "
date: 2023-01-11T13:27:15.220Z
translationKey: harrypotter
image: /media/bannière-harry-potter-official.jpg
---
> ### Cher·e·s moldu·e·s, soyez les bienvenu·e·s dans l'univers de Poudlard !

### 1/ Minerva McGonagall une marraine bien récompensée : 1 mois d'usage de balai gratuit

Le film nous suggère que la professeure McGonagall serait la bienfaitrice qui aurait offert anonymement son premier balai, le *Nimbus2000* à Harry Potter*. Si elle avait loué le balai chez Commown, elle aurait bénéficié d’un mois gratuit pour la location de son propre balai. L’on sait que la professeure apprécie les compétitions de Quiditch et soutient inconditionnellement l’équipe de Griffondor. Ce qu’on ignore généralement c’est qu’elle prend plaisir les soirs de pleines lunes à enfourcher son balai pour survoler le lac noir à toute vitesse (mais que ça reste entre nous !).

### 2/ Le balai d’Harry aurait été réparé après sa destruction par le Saule Cogneur

Dans Harry Potter et le prisonnier d’Azkaban, le balai d’Harry se retrouve malencontreusement victime de la violence du Saule Cogneur. Après sa mise en pièces, Harry se retrouve donc dans l’impossibilité de voler, jusqu’à ce que Sirius Black lui offre un *Éclair de feu* (le balai le plus cher, le plus puissant et le plus performant du monde des sorciers)… Si Harry avait loué son balai chez Commown, il en aurait reçu un autre de courtoisie en attendant que le sien soit réparé. Et s’il n’avait pas été réparable, Harry aurait finalement conservé le balai de courtoisie pour remplacer l’ancien. Les morceaux du *Nimbus2000* détruit auraient servi à réparer d’autres balais loués à de jeunes sorcières et sorciers.

### 3/ Harry aurait limité son impact sur la déforestation

Vous, les moldu·e·s, vous ignorez sûrement que la construction des balais nécessite de couper de nombreux arbres. Comme le Quidditch est devenu le sport le plus populaire du monde des sorciers, de plus en plus d’arbres magiques sont coupés pour les concevoir… Or, ces arbres ne sont pas une ressource illimitée (quelles que soient les allégations de croissance perpétuelle de votre modèle économique capitaliste). La location d'un balai chez Commown permet de limiter la coupe des arbres puisque les balais sont conçus pour durer et être réparés tant que c'est possible.

### 4/ Éclair de feu : un balai controversé chez les sorciers connaisseurs

La fabrication de l’Éclair de feu nécessite une surconsommation d’arbres magiques. Un Éclair de feu équivaut à une dizaine de *Nimbus2000* en termes de ressources. Ce balai fait donc polémique chez les sorciers. Ceux qui aiment la vitesse l'adorent, et ceux qui veulent préserver les ressources magiques pour les futures générations de sorcières et de sorciers, le haïssent. Commown ne louera jamais d’Éclair de feu, chez nous la préservation des arbres du monde magique est indispensable, s’il n’y a plus d’arbres, plus de Quidditch, et sans Quidditch le subtil équilibre du monde des sorcières et sorciers est en péril.

\*Pour les lecteurs et les lectrices, vous savez comme nous que l’histoire du film diffère un peu du roman ;)
