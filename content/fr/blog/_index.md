---
title: Blog
description: Nos derniers articles
date: 2021-05-07T11:25:03.000Z
translationKey: blog
menu:
  main:
    name: Blog
    weight: 6
  footer:
    name: Blog
    weight: 9
header:
  title: Blog
  subtitle: Nos derniers articles
  ctas:
    - outline: false
      text: Flux RSS
      icon: rss
      href: blog/index.xml
---