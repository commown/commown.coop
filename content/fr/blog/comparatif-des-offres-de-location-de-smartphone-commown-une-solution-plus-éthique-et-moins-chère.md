---
title: "Comparatif des offres de location de smartphone : peut-on avoir à la
  fois moins cher et plus éthique ?"
description: Envie de souscrire à un service de location de smartphone
  économique et responsable ? Commown compare son offre dans le détail avec
  celle de Boulanger, Samsung, Uz'it
date: 2022-04-06T07:17:18.138Z
translationKey: location durable
image: /media/illu_tableau_comparatif_assurance_smartphone_3.jpg
---
> Les offres de location longue durée de smartphone sont de plus en plus nombreuses. Voici une comparaison des offres les plus connues qui montre que celle de Commown est la plus écologique, mais aussi la moins chère !

### 1/ Une offre de location de smartphones plus durable et plus éthique

Commown est une coopérative militante qui a conçu l’offre électronique la plus écologique possible grâce au modèle de [l’économie de la fonctionnalité](/blog/economie-de-la-fonctionnalite/). Les [ressources de notre planète sont limitées](/blog/les-licoornes-une-solution-pour-lutter-contre-le-capitalisme/) et il nous faut en prendre soin. Nous louons des smartphones plus éco-conçus (et des ordinateurs, des casques, etc.) pour contribuer au changement vers [une électronique responsable](/blog/electronique-responsable-vs-responsabilité-de-lélectronique/). Notre plus value est notre modèle coopératif (SCIC).  Ce modèle protège le consom’acteur. Il permet de mutualiser les appareils et garantir ainsi que les utilisateurs auront accès à des pièces détachées longtemps après que le producteur ait cessé d’en vendre. En effet, tant que le producteur vend les pièces, Commown les achète. Mais il arrive un jour où ces pièces sont indisponibles. Alors, la SCIC utilise les appareils mutualisés qui sont irréparables comme un réservoir de pièces supplémentaires pour réparer les appareils qui sont encore en service. In fine, vos appareils dureront plus longtemps que si vous étiez seul. 

### 2/ Le modèle coopératif pour une garantie du prix juste...

Dans une entreprise capitaliste dont le modèle repose sur des locations sans option d’achat, le client est captif. Il n’a aucune emprise sur l’entreprise qui lui fournit le service. Aucun moyen pour lui de savoir si les mensualités qu’il paye sont “justes” ou bien si elles sont conçues pour gonfler les dividendes des actionnaires à la fin de l’année.

Chez Commown, le client peut être sociétaire et avoir un pouvoir sur la coopérative. Ceci lui garantit un droit de regard sur les finances de la coopérative. Il peut questionner, remettre en cause la façon dont sont constituées les offres. De fait, cela pousse à adopter une posture d’intérêt collectif dès la conception des offres par la coopérative. 

Enfin, la flotte constitue un bien commun co-possédé par l’ensemble des sociétaires. 

### 3/ Une offre simple

Notre offre est simple : nous louons des produits soigneusement sélectionnés selon plusieurs critères : robustesse, éco-conception, durabilité, éthique, performance, etc. Cette location est accompagnée de services :

* Le remplacement de la batterie et du protège écran si nécessaire.
* La prise en charge des réparations, des pannes et des casses, et le remplacement (en cas de  vol…), etc. 
* Des aides pour l’utilisation (des conseils généralistes à l'accompagnement personnalisé, en passant par la formation à Linux ou au logiciel libre).
* Des négociations pour la durabilité auprès des producteurs et des politiques

### 4/ Comparez notre offre sur le marché ?

Concrètement, comment comparer notre offre sur le marché ? Nous allons prendre l’exemple du Fairphone. Il suffit de comparer notre offre à l’achat d’un [Fairphone ](https://shop.commown.coop/shop/product/fairphone-3-avec-services-essentiel-et-duo-protecteur-333)couplée à la souscription d’une assurance (casses, vols, pannes). Une bonne image vaut mieux qu’un long discours. 

![évolution du coût cumulé au cours du temps entre l’offre Commown et l’achat du fairphone 3 associé à la souscription de diverses assurances.](/media/graphique-offre-commown.png "Evolution du coût cumulé au cours du temps entre l’offre Commown et l’achat du fairphone 3 associé à la souscription de diverses assurances.")

Nous observons que notre offre - si elle est comparée à service équivalent - est la moins chère du marché. Commown est également la plus engagée sur la durabilité. Enfin, les offres concurrentes ne comprennent pas l’ensemble des services que nous proposons (changement de batterie, changement du protège écran fourni lorsqu’il casse, la mutualisation des pièces détachées, etc.). 

### 5/ Comparaison de location d’un smartphone sans option d’achat en 2022

Pour faire simple, nous avons conçu un tableau qui présente différentes offres de location sans option d’achat. Si nous comparons les prix et les services engagés par les différents concurrents, nous pouvons affirmer que nous sommes les moins chers du marché. Nous prenons comme valeur de référence le [Fairphone 4 256GB 8 GO RAM](https://shop.fairphone.com/fr/fairphone-4) (qui coûte 719€ à l'achat neuf **avec le protège écran et la coque**).\
Ce tableau a été fabriqué pour des offres équivalentes, les prix comprennent les assurances casse, vol, panne, changement de pièces et assistance personnalisée (service client). 

| Entreprise  | Smartphone et prix d'achat                                              | Prix location 12mois\*\*                             | Mensualité  après 4 ans |
| ----------- | ----------------------------------------------------------------------- | ---------------------------------------------------- | ----------------------- |
| Boulanger   | Smartphone Samsung Galaxy S21 FE Gris 256 Go 8GO RAMG ( 759€ prix neuf) | 122,5 d’apport + 44,99€/mois                         | 34,99€                  |
| Samsung     | Smartphone Samsung Galaxy S21 FE Gris 256 Go 8GO RAMG ( 759€ prix neuf) | 79€ d’apport + 33,60€/Mois                           | 33,60€                  |
| Mobile club | Iphone 12 Pro 256 GO 6GO RAM (820 € prix reconditionné)                 | 209€ + 39,90€/mois                                   | 34,90€                  |
| UZ'it       | APPLE iPhone 12 Pro Max 256GO 6GO RAM ( 820 € prix reconditionné)       | 121,04€ + 75,99/mois                                 | 61,99€                  |
| Commown     | Fairphone 4 256GB 8 GO RAM (qui est à 719€/neuf)                        | 85 € de dépôt de garantie remboursable + 29,60€/mois | 11,85€                  |

\* tableau réalisé et valide en date du 06/04/2022
  Il est à noter que ces concurrents ne sont pas engagés comme nous dans la durabilité des appareils. Pour preuve l’offre de Samsung (voir[ faq](https://www.samsung.com/fr/up2you/faq/)) pousse au renouvellement puisque les clients sont invités à changer de mobile dès le 4ème mois !

\*\* protection casse et vol incluse.

### 6/ Les parties moins visibles de notre activité

Au-delà de la souscription à notre offre de services, vous soutenez indirectement la production de connaissances spécialisées sur les thèmes de l'électronique durable, du logiciel libre, etc. Vous soutenez également nos actions de plaidoyer, puisque nous intervenons souvent auprès des pouvoirs publics pour faire valoir un point de vue très engagé dans le domaine de l’électronique.