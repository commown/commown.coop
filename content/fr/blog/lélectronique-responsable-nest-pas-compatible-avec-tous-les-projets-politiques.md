---
title: L'électronique responsable n'est pas compatible avec tous les projets
  politiques
date: 2024-06-21T21:09:21.674Z
translationKey: commown-positionnement-politique
image: /media/blog-legislatives-2.png
---
À la veille de ces élections législatives, notre coopérative ne pouvait pas rester silencieuse.

Premièrement, nous tenons à rappeler que la lutte contre toutes les formes de discrimination est inscrite dans le préambule de nos statuts. À ce titre, nous ne pouvons qu’adhérer au [communiqué d’ESS France](https://www.ess-france.org/resolution-face-a-l-extreme-droite-les-acteurs-de-l-ess-appellent-a-l-engagement) qui appelle à l’engagement face à l’extrême droite.

Justement, notre coopérative depuis sa création est engagée dans des combats qui relèvent du champ politique. Nous n’avons pas pour prétention de juger les motivations qui poussent les personnes à voter pour tel ou tel parti. Toutefois, il nous semble important de témoigner de façon sourcée sur le travail que nous avons mené depuis 2018. Ce texte n’a pas été écrit pour être un article d’opinion, nous y relatons des faits. Peut être qu’il permettra de nourrir les discussions avec vos proches.

Pour rester le plus **factuel et objectif** possible, nous allons dans cet article :

* **Témoigner de notre travail de plaidoyer** citoyen ainsi qu’une approche sectorielle des évolutions législatives depuis 2018, pour **illustrer les positions du parti présidentiel** sur les sujets qui nous préoccupent.
* Lister les **votes du groupe RN à l’Assemblée** sur les **lois importantes pour la vision de Commown**.

## Le parti présidentiel – des ambitions qui deviennent des mesurettes ou des renoncements

*Témoignage de 5 ans de plaidoyer citoyen*

Depuis 2018, nous avons essayé avec nos humbles moyens de contribuer à des évolutions législatives pour une électronique plus responsable.

Nous avons suivi de près différents projets de loi, comme celui sur l’[Économie Circulaire](https://www.assemblee-nationale.fr/dyn/15/dossiers/lutte_gaspillage_economie_circulaire?etape=15-AN1), [la loi Climat et Résilience](https://www.assemblee-nationale.fr/dyn/15/dossiers/lutte_contre_le_dereglement_climatique) ou encore [la loi visant à réduire l'empreinte environnementale](https://www.assemblee-nationale.fr/dyn/15/dossiers/DLR5L15N40696). Nous avons parfois réussi à faire déposer des amendements que nous avions nous-mêmes rédigés ! Par exemple, [celui-ci](https://www.assemblee-nationale.fr/dyn/15/amendements/2454/AN/1552) sur l’**économie de la fonctionnalité**, déposé à l’époque par une députée de la majorité mais retiré après l’avis défavorable de Brune Poirson, alors secrétaire d’état dans le gouvernement d’Édouard Philippe.

Nous sommes allés jusqu’à consacrer des centaines d’heures de travail pour la conception des grilles d’évaluation des **indices de réparabilité** et durabilité*, en concertation avec nombreux acteurs industriels ou de la société civile. Travail qui permit ensuite au législateur de [rédiger les décrets d’application de la loi pour ces indices](https://www.ecologie.gouv.fr/indice-reparabilite). Dans ces groupes de travail, un représentant du gouvernement arbitrait les débats, sans jamais être en faveur d'une réglementation ambitieuse sur la réparabilité.

Par ailleurs, nombreuses étaient les concertations auxquelles nous avons participé. Par exemple celle de l'ARCEP sur **la 5G**, qui s’est déroulée après que le président soit revenu sur sa promesse d’accepter les propositions des 150 citoyennes et citoyens de la Convention Citoyenne pour le Climat, dont un moratoire sur la 5G. Malgré tout, nous y avons pris part et vous pouvez lire notre contribution à celle-ci en [page 50 de ce document](https://www.arcep.fr/uploads/tx_gspublication/rapport-pour-un-numerique-soutenable_dec2020.pdf).*

Nous avons également consacré de nombreuses heures de travail pour **soutenir le plaidoyer d’associations partenaires**. Ce fut le cas, et de manière assez intense, pour les Amis de la Terre à l’occasion de la [campagne surproduction](https://www.amisdelaterre.org/nos-thematiques/surproduction/) entre 2019 et 2022. Celle-ci avait pour objectif de faire passer un moratoire sur les entrepôts d’Amazon. **Amazon** est l’anti-thèse du modèle de notre coopérative. Destruction des commerces de proximités, objectif monopolistique, surproductions d’appareils électroniques, fraude fiscale, obsolescence marketing poussés à l’extrême... Commown incarne une alternative, mais continuer à la développer sans essayer de s’opposer à l’implantation durable de cette entreprise en France n’avait pas de sens.

Mais là encore, nous nous sommes heurtés à un mur, dressé [par Emmanuel Macron lui-même](https://www.challenges.fr/entreprise/les-dessous-de-l-incroyable-soutien-du-gouvernement-a-amazon_769927). Les militants partout en France avait pourtant réussi à convaincre près de 200 députés, tous bords politiques confondus, dont 40 députés de la majorité... Mais malgré cela, la consigne donnée aux députés de la majorité fut claire et les différentes propositions de moratoires portées par les Amis de la Terre que notre coopérative soutenait ont toutes été rejetées en bloc.

\* *Il nous semblait important d’y participer bien que cet indice soit une nouvelle fois une façon de reporter la responsabilité sur le consommateur au lieu de légiférer pour forcer les entreprises à allonger la durée de vie des produits et assumer les externalités négatives de leurs produits.*

## Le RN - la démocratie et l'écologie

Un autre combat qu’il nous a semblé important de mener, fut celui contre « l’expérimentation » de la **vidéosurveillance algorithmique** durant les jeux olympiques. Campagne menée cette fois-ci par La Quadrature du Net qui constitue l’un des derniers remparts de nos libertés individuelles face aux lois déployées [par la majorité depuis 2017](https://www.laquadrature.net/2022/02/03/emmanuel-macron-cinq-annees-de-surveillance-et-de-censure/). La vidéo surveillance algorithmique (VSA) était l’un des premiers prétextes économiques donnés pour déployer la 5G. Sur le plan écologique, c’est un non-sens, et sur le plan de nos données personnelles, la VSA nous fait sombrer directement dans une dystopie. La Chine illustre bien [le danger de ce genre de contrôle](https://information.tv5monde.com/international/surveillance-numerique-les-algorithmes-de-repression-du-gouvernement-chinois-devoiles) pour la démocratie et la liberté individuelle.

Ainsi, nous avons corédigés un amendement [de suppression et un autre de « repli »](https://nuage.commown.coop/s/SMqTPaBXKKcjRoS) qui proposait d’ouvrir le code des algorithmes. Ce fut une nouvelle défaite pour l’intérêt général et les libertés individuelles. Cette fois-ci, nous avons assisté à une [co-construction Renaissance/RN](https://www.liberation.fr/politique/la-majorite-adopte-un-amendement-cosigne-par-le-rn-la-nupes-sinsurge-20230323_E25JIOAOAZHLZLU4XB4X7EATFY/?redirected=1&redirected=1) du texte de loi dont l’article 7 sera ratifié largement avec l’aide du RN [et voté avec l’aide du RN](https://datan.fr/votes/legislature-16/vote_1274). À notre connaissance, sur ce sujet seul le Front Populaire propose de [revenir sur la vidéosurveillance](https://www.bfmtv.com/tech/vie-numerique/legislatives-le-nouveau-front-populaire-veut-interdire-la-reconnaissance-faciale_AV-202406140462.html) et interdire la reconnaissance faciale qui est [déjà utilisée en France](https://disclose.ngo/fr/article/la-police-nationale-utilise-illegalement-un-logiciel-israelien-de-reconnaissance-faciale/) hors de tout encadrement réglementaire.

Ce serait trop long de vous retracer ici l’intégralité des votes et des amendements sur notre secteur (sur la seule loi REEN il y en a plus de 300 à l’Assemblée). Aussi, il est important de noter que la plupart des textes relatifs à l’électronique sont passés à l’Assemblée entre 2017 et 2022 et il y avait encore très peu de députés RN à l’époque. Ainsi notre « sélection » peut paraître biaisée. Une autre sélection pourrait presque défendre que le RN se préoccupe des libertés individuelles au vu de [cet amendement sur la VSA](https://www.assemblee-nationale.fr/dyn/16/amendements/0809/CION_LOIS/CL326), ou bien qu’il est sensible aux questions environnementales au regard [de celui-ci sur l’obligation d’achat](https://www.assemblee-nationale.fr/dyn/15/amendements/4196/AN/276) de terminaux reconditionnés. Mais l’amendement en question n’a pas été soutenu vu que le député ne s’est pas donné la peine de venir en séance. \
 \
 Toutefois, le constat de toutes les ONG qui ont observé leur travail à l’Assemblée, ou décortiqué leur programme est sans appel : ce programme est climaticide. Voir cet article [du média Vert](https://vert.eco/articles/greenpeace-les-amis-de-la-terre-notre-affaire-a-tous-les-associations-ecologistes-appellent-a-se-mobiliser-contre-lextreme-droite-avant-les-legislatives), cet article [de blog de Greenpeace](https://www.greenpeace.fr/espace-presse/dissolution-de-lassemblee-nationale-greenpeace-france-appelle-a-lunion-pour-la-justice-sociale-et-environnementale/), ou encore [le bilan du travail des députés RN](https://vert.eco/articles/quand-le-rn-fait-la-loi-quel-bilan-apres-deux-ans-a-lassemblee-nationale) à l’assemblée toujours par le média Vert. 

Il y a aussi cette étude détaillée du RAC sur la législature 2022-2024 et les [votes des principaux blocs politiques sur les mesures liées à l'écologie](https://reseauactionclimat.org/analyse-de-la-legislature-2022-2024-les-positions-des-principaux-blocs-politiques-sur-10-mesures-concretes-sur-lecologie-et-la-justice-sociale/) et la justice sociale.

Vous pouvez aussi simplement **vous faire un avis** en consultant **les votes des différents blocs politiques** sur les grandes lois liées à l'environnement, et les autres domaines qui vous intéressent sur [Datan.fr](https://datan.fr/votes/decryptes/environnement).

## Résumé

**Commown** fait du **plaidoyer** notamment sur des questions **environnementales**, dans le **domaine de l’électronique**. Nous nous sommes battu au côté d’associations sur de nombreux sujets en opposition au parti présidentiel. Par ailleurs, le RN a montré par ses votes à l’Assemblée qu'il est la plupart du temps frontalement opposé aux mesures environnementales ou a minima qu'il n'en a rien à faire (abstention).

Pour toutes ces raisons **notre coopérative appelle à un réveil citoyen d’ampleur** et espère voir un taux de participation historique aux votes du 30 juin et 7 juillet. Dans ce cadre, nous avons facilité la pose de congés et d’ajustement des emplois du temps pour nos salariés qui souhaiteraient s’engager d’une façon ou d’une autre.  

- - -

### Annexes

 Ressources complémentaires :

* Votes du [RN au niveau de l’UE](https://www.touteleurope.eu/institutions/de-2014-a-2019-comment-le-rassemblement-national-a-t-il-vote-au-parlement-europeen/).
* Tableau comparatif réalisé par Bloom qui [liste des votes de différents partis](https://bloomassociation.org/wp-content/uploads/2024/06/V12-LE-FRONT-POPULAIRE-NOTRE-SEULE-VOIE-POUR-LOCEAN-ET-LE-CLIMAT.pdf) au parlement européen et à l'Assemblée Nationale.