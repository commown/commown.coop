---
title: Un livre d'histoire écrit en 2064
date: 2024-04-11T12:13:00.781Z
translationKey: Histoire 2064
image: /media/illu-blog-1_compressed.png
---
> *La bataille des imaginaires fait rage et à l’heure des inintelligences artificielles génératives qui incarnent la vision du toujours plus vite (dans le mur) il est important de proposer des récits alternatifs. C’est ce que nous proposons avec la BD des [aventures de Domi](https://mamot.fr/tags/DomiBD), dans cet article Marta et Dominique se plongent dans un livre d’histoire écrit en 2064...*

#### L'émergence des premières cabines téléphoniques

Inspirées par un collectif Grenoblois, deux coopératives autour de 2025 se sont associées pour réinstaller deux « cabines téléphoniques » grand format dans l’espace public. Pour certaines personnes, c'était là une pure opération de street marketing primaire, pour d’autres un happening artistique. Même si les deux coops ont reconnu plus tard qu’inévitablement les cabines avaient pour objectif d’augmenter leur visibilité, elles étaient avant tout pour elles des outils de bataille culturelle. Comme un manifeste de rue qui disait : *Souvenez-vous : à une époque, nous vivions très bien sans smartphone.*
\
Les deux coopératives ayant publié les plans de leur cabine en Creative Commons, petit-à-petit des communes et différents collectifs en installèrent aux quatre coins du pays. Car au-delà de ce qu’elles pouvaient représenter, elles trouvèrent différentes utilités :

* Certaines étaient dotées d’une tablette et représentaient un moyen de lutte contre l’exclusion numérique des personnes en grande précarité vivant dans la rue.
* Certaines permettant seulement de téléphoner et envoyer des SMS gratuitement étaient installées par des collectifs de parents d’élèves devant des écoles et collèges. Les parents les utilisaient pour repousser le moment d’équiper leur enfant d’un « smartphone », qui à l’époque semblait encore inévitable.
* D’autres encore étaient utilisées comme outil de médiation culturelle pour sensibiliser aux enjeux environnementaux et sociaux du numérique.

En 2025, ces cabines pouvaient paraître totalement anachroniques alors que la bulle spéculative des IA n’avait pas encore éclatée et qu’une vaste majorité des citoyens possédait un smartphone. D’ailleurs la fin des assistants personnels (descendants du smartphone) qui paraissait pourtant inéluctable après les crises d’approvisionnement massives de 2032 aura dû attendre 2046. En effet, le maintien d’une forte addiction de la population à ces bijoux technologiques permit aux entreprises capitalistes de pérenniser leur modèle productiviste basé sur la captation de données personnelles et le marketing ciblé. C’est à la folie de Musk (mort seul sur Mars) et de ses mines sur la Lune que l’on doit ce délai d’asservissement généralisé supplémentaire.

#### Printemps de l'Attention : le temps de la révolte

En parallèle, l’essaimage de ces cabines se poursuivit pendant une petite dizaine d’années, certaines des *Émancipatrices* datent de cette époque. Mais c’est au Printemps de l’Attention, en 2045, que la Cabine devint un symbole de résistance et de ralliement. À cette époque, l’environnement urbain était saturé de capteurs d’attention destinés à perpétuer la fuite effrénée de la surproduction. Le niveau d’addiction aux applications numériques avait atteint un degré pathologique, à tel point que la simple vue d’un écran lumineux pouvait plonger la personne dans un état second. Il était de fait très simple de jouer avec cette addiction pour pousser à consommer toujours plus. Entre 2045 et 2046, partout dans le monde, tous les outils d’asservissement furent mis hors service. Le symbole des cabines était tagué sur les écrans publicitaires, sur les murs holographiques ou sur les pavés des rues immersives. Dépassés par ce mouvement et les boycotts qui s'ensuivirent, la caste des Derniers Capitalistes s’est réfugiée dans des cités ultra-connectées, impénétrables. Leur règne est aujourd’hui en déclin, mais il existe encore quelques Cités Pommées aujourd’hui.

#### L'avènement d'une société sobre et libératrice

Progressivement la technologie fut remise à sa juste place, les cabines devenues un symbole de libération furent renommées *Émancipatrices* et installées sur toutes les places des villages. Certaines personnes parlent de cette période comme un grand ménage de printemps technologique. \
\
Aujourd’hui, les *Émancipatrices* permettent de converser entre communautés éloignées partout dans le pays. Pour éviter l’effet de sidération, l’usage d’écran à encre électronique en noir et blanc est devenu la norme pour les quelques ordinateurs fixes présents dans les communautés.\
\
Enfin, avec la fin de l’entretien des infrastructures du « Web » le développement de réseaux locaux décentralisés de partage de connaissance s’est massifié. Le savoir universel est stocké localement, mais pour autant, chaque communauté continue à contribuer collectivement à celui-ci.

Quand une contribution majeure est validée en conseil des communs numériques, un colporteur de données est appelé dans le village avec sa caravane serveur solaire. Cette personne apporte les dernières mises-à-jour des autres communautés et repart avec les nouvelles contributions.\
\
--- \
\
*Cet article s'inscrit dans une campagne de communication qui développe un contre-récit, pour prendre connaissance de notre réflexion sur les notions de storytelling [lire cet article. ](https://commown.coop/blog/produits-%C3%A9thiques-et-%C3%A9cologiques-linfluence-du-storytelling/)*\
*⁣*\
*Enfin, cette campagne vise à appeler aux soutiens de nos coopératives. Votre soutien nous permettra de continuer à nous développer, proposer des récits alternatifs, porter un plaidoyer radical. >* https://telecoop.commown.coop/