---
title: "#Black Friday - Kit de survie du consom'acteur en milieu hostile "
description: Comment garder le contrôle de soi lors du Black Friday ? Commown
  vous livre la réflexion anti-surconsommation à adopter à tout prix !
date: 2021-11-10T10:01:58.404Z
translationKey: kit de survie blackfriday
image: /media/kermit-g460a9abd3_640-2-.jpg
---
 
Vous n’avez pas les bases ? Chaque prise de décision est un casse-tête écologique pour vous ? Nous vous proposons une méthode (clé en main !) en seulement deux étapes pour devenir un consom'acteur aguerri.  Nous allons vous accompagner sur un “cas d’espèce” lié à l'électronique durable : l’exemple du smartphone écolo. Vous êtes prêt ?

**Étape 1 : Le questionnement !**   

Au commencement était… heu le questionnement ?! Il nous semble évident que la sobriété demeure le choix le plus sage en matière d’électronique durable. Notre slogan chez Commown est le suivant : “le produit le plus écologique est celui que nous avons déjà”. Ainsi, nous invitons tous les consom'acteurs à questionner l’utilité de l’achat d’un nouveau produit électronique. Il nous semble impératif d'interroger le plus honnêtement possible sur les raisons qui nous poussent à vouloir utiliser un produit électronique. Questions de base avec la [Méthode BISOU](https://lesecolohumanistes.fr/la-methode-bisou/). 

**B** comme **Besoin**

*Ai-je vraiment besoin de ce produit ? Est-ce que je suis sûr d’être à l’origine de mon désir ou est-ce que c’est la publicité qui a créé chez moi un désir artificiel ?*

**I** comme **Immédiat**

*En ai-je besoin immédiatement ? Est-ce que je peux prendre le temps de réfléchir ?*

**S** comme **Semblable**

*N’ai-je pas déjà un produit équivalent qui pourrait remplir ce besoin ?*

**O** comme **Origine**

*Quelle est l’origine du produit ? A qui j’achète ?* 

**U** comme **Utile** 

*Puis-je me passer du produit ? A quoi va-t-il vraiment servir ?* 

Si je m’aperçois que le produit est indispensable, par exemple un ordinateur pour m’informer et travailler ou un smartphone pour pouvoir communiquer avec les autres alors je peux passer à la seconde étape…

**Étape 2 : Le choix du produit : l’exemple du smartphone**

Quelle offre sera la plus vertueuse possible ? Comment séparer le bon grain de l'ivraie ? Comment louvoyer dans cet océan d’offres plus vertes les unes que les autres (du moins dans les discours) ?

Ici nous vous proposons deux maîtres mots : ENQUÊTER et CHOISIR.\
\
**ENQUÊTER :** Comme un détective le ferait, il faut se renseigner sur les entreprises afin d’échapper aux pièges marketing de type greenwashing. Concrètement, comment s’y prendre pour mener l’enquête ? 

* En tapant “le nom de la boite + avis” sur un moteur de recherche écolo comme Lilo et Ecosia ou Duckduckgo et Qwant pour la protection de vos données. 
* En fouillant sur le web pour rechercher si l’entreprise est bien “réelle” (référencée sur “societe.com”), si elle est sérieuse, si elle est domiciliée en France (ce qui n'exclut pas qu’elle puisse pratiquer l’évasion fiscale), si elle a obtenu des labels, si elle est insérée dans un écosystème, si son discours est en accord avec les produits, si les produits sont conformes aux discours, si les porteurs de projets existent réellement, etc.
* En regardant quel type de statut elle a choisi et si ceux-ci sont publics ? Le cas échéant, quel est le pouvoir du capital et comment est répartie la gouvernance entre les différents actionnaires ? 
* En décortiquant les commentaires sur plusieurs sites (la e-réputation) et en essayant de les comparer avec ce que raconte l’organisation.

**CHOISIR :** Après avoir enquêté, vient le moment où il faut choisir. Selon nous, la bonne stratégie c’est de se diriger vers une offre ou un produit qui soit compatible avec (1) votre éthique et (2) vos moyens financiers. 
Ainsi, en reprenant l’exemple du smartphone : 

* Si je souhaite  limiter mon impact écologique tout en favorisant le retour à l'emploi d’un public précaire je peux acheter un smartphone reconditionné sur le site du [Label Emmaüs](https://www.label-emmaus.co/fr/).
* Si je souhaite m’engager vers une marque de smartphone qui est la plus vertueuse possible, je peux me tourner vers le Fairphone. 
* Si je souhaite soutenir [une offre de rupture](https://commown.coop/nos-offres/fairphone/) avec le consumérisme, et favoriser l'émergence d’un modèle radicalement circulaire, je peux me tourner vers le modèle de l’économie de la fonctionnalité de Commown. 

À vous de juger ! Restez critiques et prenez soin de vous ! :)