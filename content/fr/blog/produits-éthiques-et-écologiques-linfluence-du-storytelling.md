---
title: Comprendre l’influence du storytelling pour les produits éthiques et
  écologiques
description: Découvrez comment le storytelling des sociétés dites
  responsable  influence le public cible. Commown vous dévoile tout en toute
  transparence.
date: 2024-03-04T10:36:26.012Z
translationKey: storytelling
image: /media/illu-storytelling.jpg
---
> Le constat porté par Commown sur le secteur électronique est sans appel :  La surproduction de terminaux (et la surconsommation induite)  génère une profusion d’externalités négatives, sociales et environnementales. La situation n’est absolument pas soutenable et une sobriété (choisie ou subie) finira par advenir.

Dans ce contexte, des produits et offres revendiqués plus écoresponsables émergent. Tous les grands constructeurs d’appareils essayent de se draper de vert, pendant que le secteur du reconditionné est en plein essor. Bien que ce dernier puisse représenter une solution provisoire,[ *il ne remet pas en question fondamentalement la racine du problème*](https://positivr.fr/appareils-reconditionnes-bonne-solution-contre-externalites-electronique/) : le rythme frénétique de mise sur le marché de nouveaux produits. Pour sortir de cette spirale productiviste, chez Commown, nous avons choisi l’économie de la fonctionnalité et de la coopération (location sans option d’achat avec services). Cependant, la démocratisation de ce modèle est loin d’être acquise. De nombreux freins y sont associés comme celui de se « détacher de la possession » et limitent l’adhésion à notre solution.

La thèse CIFRE, financée par Commown, visait à comprendre l’influence sur le public cible du storytelling des organisations qui vendent ou louent des produits électroniques présentés comme plus durables. En résumé, **comment mieux communiquer pour se différencier du greenwashing ambiant et augmenter l’adhésion à notre service ?**

### Résumé des travaux menés

Sur le plan théorique, la thèse aura permis de conceptualiser la notion de storytelling et d’explorer les théories et modèles expliquant le fonctionnement du phénomène d’influence. Sur le plan méthodologique, elle a comparé le storytelling de cinq organisations différentes (dont Commown) et analysé comment un public ne connaissant pas encore notre coopérative pouvait y réagir. En parallèle, des entretiens ont été menés avec des clients de Commown pour mieux comprendre ce qui les avait poussés à souscrire une offre d’abonnement.

Ce travail de recherche nous a conforté dans nos intuitions initiales sur les freins à la souscription de notre offre. Il semble par ailleurs que le storytelling de notre coopérative ne permette pas de lever ces freins auprès d’un public qui ne serait pas suffisamment sensible à nos valeurs. Ce qui est satisfaisant dans le sens où nous ne cherchons pas forcément à convaincre des personnes pour qui notre offre ne serait pas adaptée.

Dès les premiers échanges en 2019 autour de ce projet de thèse, une question éthique s’est posée :  ***le storytelling étant une technique communicationnelle qui peut être qualifié de manipulatoire, son usage est-il en cohérence avec les valeurs que porte notre coopérative ?***

### Prise de recul sur le développement de notre storytelling

Jusqu’ici, nous n’avions pas forcément conscience de développer un storytelling et surtout de ce que cela pouvait impliquer. Mais inévitablement, toute organisation développe un récit : date de création de la coopérative, vision de l’avenir, valeurs portées, offres proposées. Tout ceci constitue en soi un storytelling qui influence l’audience.

Toutefois, employer ces techniques communicationnelles manipulatoires de façon « consciente » semble être contraire aux valeurs que porte notre coopérative. Même si nos intentions sont louables comment continuer à communiquer tout en respectant le libre arbitre des personnes exposées à notre communication ?

Pour répondre à ce dilemme, nous avons choisi :

* De continuer à questionner systématiquement le BESOIN de souscrire chez nous. Car si notre storytelling venait à augmenter notre portée, il est crucial que celui-ci ne génère pas un renouvellement prématuré d’appareil. 
* De rédiger cet article de blog pour exposer nos interrogations sur le sujet en toute transparence. 
* D’introduire notre BD* (outils de communication préconisé par la thèse) par un avertissement sur le sujet. L’idée : permettre à notre audience de choisir de s’exposer de manière libre et consciente à notre storytelling en sachant que celui-ci a pour but d’inciter à l’engagement.

Vous pouvez retrouver l’intégralité du manuscrit de la thèse *en [libre accès ici !](https://nuage.commown.coop/s/sAGL6Zbt2cNNHRR)*                                                            Si vous voulez réagir à cet article de blog sur le bien-fondé de l’usage du storytelling ou autre, nous avons ouvert [un article sur notre forum](https://forum.commown.coop/t/debat-sur-linfluence-des-storytelling/2956?u=ikram).

\* Nous allons lancer une mini BD qui sera publiée sur nos réseaux sociaux jusqu’au mois de juin, ci-dessous, voici la première planche (la suite à venir sur nos différents réseaux).

![Première stripe BD Commown](/media/stripe-1.png)