---
title: Votre enfant veut le dernier iPhone ? Nos idées pratiques pour
  l’électronique responsable à l’adolescence
description: éducation, écologie, iphone, parentalité, ados, adolescent
date: 2023-02-22T08:16:48.956Z
translationKey: parentalité
image: /media/ados-et-smartphone.jpg
---
> Vous êtes un écolo engagé bec et ongle dans la lutte pour un monde plus vert et plus juste. Vous mettez en place dans votre vie tous les petits écogestes pour incarner le changement que vous prônez. Seulement, vous avez des enfants... Vos enfants sont influencés par les modes, la pression sociale des ami.e.s. et le marketing des grandes marques comme Apple ou Microsoft. Bref, si vous êtes pris en tenaille entre vos valeurs écologiques et votre envie de leur faire plaisir, alors cet article est fait pour vous. 

### 1/ Patatras...

Arrive le jour où votre enfant rentre dans l’adolescence... Il remplace son cartable par un sac à dos. Son collège le force à  travailler sur des ordinateurs Windows et l’ENT arrive dans sa vie d’ado (ce qui commence à l’angoisser parce qu’il scrute en permanence l’arrivée des notes et des nouveaux devoirs). 
Il veut obtenir son propre téléphone comme ses camarades, il réclame son droit à l’autonomie numérique. Vous êtes en état de choc. \
Votre progéniture - que vous avez biberonnée avec des documentaires sur la nature, des discours pour sauver la planète, etc. – est maraboutée par un désir social plus fort que vos valeurs écologistes. 
Désormais, vous risquez de perdre le contrôle sur (1) le contenu de ce qu’il visionne, (2) ce qu’il pourra diffuser (photos, messages...), sur quels sites, avec quels interlocuteurs, (3) le temps qu’il passe sur son smartphone... 
Et le comble, comme si ça ne suffisait pas, tous ses camarades ont des smartphones de grandes marques, il veut – bien évidemment – la même chose, alors que faire ?

### 2/ Être au clair avec vous-même

D’abord, chers parents, soyez doux avec vous-mêmes (nous vous recommandons les podcasts “ma vie de parent” sur France inter). \
Rappelez-vous que vous n’êtes pas seul.e.s. Questionnez-vous sur ce qui vous “gratouille” précisément. \
Cet article vous invite à nommer, à mettre en mots cette dissonance écologique appliquée au numérique. \
Toute l’éducation environnementale que vous avez prodiguée, et vos valeurs que vous avez partagées avec vos enfants, ne suffisent pas à contrer le désir suscité par (1) le conformisme social (“je veux avoir comme les autres”) et (2) la publicité ambiante(les mouvements de mode). \
Une fois que vous aurez formulé précisément ce qui dissone pour vous, il faut trouver un espace de connexion avec votre ado.

### 3/ Instaurez un dialogue dans lequel l’ado se sentira entendu

A l’adolescence, la verticalité éducative ne fonctionne pas, l’ado est bien souvent rétif à l’autorité. Lorsqu’on veut faire passer (ou imposer) un message péremptoire sans se préoccuper de comment il est reçu, il y a peu de chances que ça fonctionne. \
Le secret des grands pédagogues modernes c’est la pédagogie active. Le message aura d’autant plus de poids si l’enfant le formule lui-même, avec ses propres mots. \
Nous conseillons donc, non pas d’interdire, mais de créer un espace de dialogue dans lequel l’enfant pourra formuler ses arguments, au plus près de ce qu’il vit. 
Nous vous proposons une méthode en deux temps.

#### Temps 1 Entrez dans son monde

Essayez de rentrer concrètement dans son monde, de le comprendre, de voir les choses comme il les voit, d’aller avec lui sur son côté de la colline. \
Mettez-vous à sa place, souvenez-vous combien à son âge la problématique de l’acceptation au sein du groupe était importante, combien le besoin de considération, de valorisation, d’estime et de reconnaissance de soi sont importants pour les ados. \
Verbalisez-le devant votre ado, avec vos mots, montrez lui que vous comprenez l'importance que cela a pour lui d'avoir un smartphone. 

#### Temps 2 Invitez-le à visiter le votre

Une fois qu’il se sentira entendu, demandez-lui s’il est d’accord de vous accompagner sur votre côté de la colline. Là, parlez-lui à partir de votre cœur, dites en quoi votre combat écologique fait sens pour vous. \
Parlez concrètement de votre rapport à l’écologie, des difficultés que ça provoque chez vous (combien ça n’est pas facile), de la préoccupation pour lui (son avenir). 

### 4/ Quelques arguments pour la route 

Une fois la connexion relationnelle établie et l’écoute de votre ado en place. Soyez concrets. Montrez combien l’exploitation des enfants en Afrique est réelle ([vidéo](https://www.youtube.com/watch?v=WBNGHTyO5k8)). Montrez le coût écologique d’un smartphone. \
Démystifiez le fonctionnement, les procédés manipulatoires de la publicité avec lui ([vidéo](https://www.youtube.com/watch?v=4kUqZqIeWZ4)). \
Montez-lui les sirènes de la sobriété, ô combien elle est salutaire écologiquement, éthiquement, humainement et spirituellement (voir documentaire demain, lire “un monde sans fin”). \
Et, en définitive, laissez-lui faire son propre choix, en toute responsabilité (et fonction de votre bourse). S’il choisit un téléphone qui ne vous convient pas, dites-vous que vous avez semé une graine, viendra un temps où elle poussera.

### 5/ Les alternatives ont aussi la classe !

Contribuer à résoudre un problème planétaire, en l'occurrence l'impact désastreux de l'électronique sur la planète, c'est aussi super stylé ! Une fois que vous avez réussi à transmettre en partie l'importance du problème, prenez le temps de montrer qu'il existe aussi des solutions. Des solutions qui peuvent avoir encore plus la classe que les marques de smartphones de ses ami.e.s. 

\- le plus extrême, c'est de suivre la voie du co-créateur du jeu vidéo The Witcher : aller vers le Mudita Pure, un simple téléphone, pas un smartphone, mais produit en Europe, conçu pour durer, avec un système d'exploitation open source etc.

\- ensuite, simplement le Fairphone : un smartphone avec des métaux commerce équitable, qui lutte contre le travail des enfants, et facile à réparer, etc. Essayez de transmettre votre enthousiasme : c'est ultra stylé de sortir un Fairphone 4 de sa poche !

\- enfin, la marque Crosscall, marque française, dont les appareils robustes vont mieux survivre aux potentiels mauvais traitements. Et surtout Crosscall est le seul fabricant de smartphones à avoir annoncé une relocalisation de la production en France, pour 2025 ! Cela va éviter les conditions de travail terribles des usines en Chine.

### 6/ Préserver une déconnexion joyeuse

Accepter le smartphone, cela n'induit pas de sombrer dans les méandres des pièges marketing qui cherchent à capter l'attention. Vous pouvez décider ensemble de nombreux temps où le smartphone est interdit, pour tous, adultes et ados. Par exemple, les repas, jeux de société ensemble, balades en forêt, ou autres temps de partage qui correspondent à votre famille. Votre enfant pourra ainsi vous aider à maintenir cette déconnexion pour vous même, si vous en avez besoin. En se voyant actif et sur un pied d'égalité sur cette question, sa participation sera d'autant plus facile.
