---
title: "Les Licoornes : une solution pour lutter contre le capitalisme"
description: Les licoornes c'est avant tout un mouvement engagé envers la
  proposition d'une consommation alternative à celle du capitalisme
date: 2021-12-21T08:22:09.939Z
translationKey: Capitalisme, licoornes, écologie, développement durable,
  numérique responsable, coopérative
image: /media/225070-675x450-environmental-pollution-concept-1-.jpg
---
> *Quand le dernier arbre aura été abattu, quand la dernière rivière aura été empoisonnée, quand le dernier poisson aura été pêché, alors on saura que l'argent ne se mange pas.* 

*Geronimo, chef Apache Chiricahua*

## C’est quoi le capitalisme ?

[Wikipédia](https://fr.wikipedia.org/wiki/Capitalisme) nous dit “*Le capitalisme désigne un système économique et par extension l'organisation sociale induite par ce système. Il peut être défini par deux caractéristiques principales : la propriété privée des moyens de production ; une dynamique fondée sur l'accumulation du capital productif guidée par la recherche du profit*”.
Pour compléter cette définition, nous allons reprendre la comparaison d’Orwell (très bien racontée dans une [conférence de Jean-Claude Michéa)](https://www.youtube.com/watch?v=ZcA1Xj1oNWw) : le capitalisme c’est comme une partie de monopoly où sur le papier tout le monde a une chance de gagner. Néanmoins, en pratique, il y a des petites nuances : la partie a commencé il y a plusieurs siècles, des monopoles se sont constitués, l’héritage existe (et rompt d'emblée l’égalité des chances) et la rue de la paix a déjà été achetée... Nous vous recommandons - au passage - l’excellente vidéo intitulée “[Jeu de Société”](https://www.youtube.com/watch?v=EwK9glIxIoo) des vidéastes engagés “[Les Parasites”](https://www.youtube.com/c/LesParasitesFilms).

## Capitalocène : un anthropocène dû au capitalisme

La belle idée du capitalisme vient des premiers libéraux qui pensaient que la poursuite des intérêts individuels allait concourir au bien collectif (c’est le principe de la [main invisible](https://fr.wikipedia.org/wiki/Main_invisible) du marché de Smith). C’est parce que le boulanger veut gagner sa vie qu’il se lève pour faire du pain et donc l'égoïsme des uns fait le bonheur de tous, car il contribue à l’enrichissement de tous. Mais deux siècles plus tard cette "belle idée" libérale nous conduit dans une impasse ! La corrélation entre explosion des inégalités, destruction de la biodiversité, mise en danger de la survie de l’humanité avec notre modèle économique dominant n’est plus à démontrer (voir : [ici](https://www.franceculture.fr/emissions/le-pourquoi-du-comment-economie-social/anthropocene-ou-capitalocene), [ici](https://www.temoignages.re/developpement/changement-climatique/changement-climatique-le-rapport-du-giec-confirme-que-le-capitalisme-menace-la-survie-de-l-humanite,101943), et [ici](https://www.lemonde.fr/blog/huet/2021/08/09/le-rapport-du-giec-en-18-graphiques/)). Il est donc temps de changer. Alors… 

## Capitalisme vert : le business du développement durable

Les futés (ou cyniques) diront qu’il faut et qu’il suffit de prendre le meilleur du capitalisme et de l'idéologie écologiste, puis de mélanger énergiquement, et hop, on obtient le [capitalisme vert](https://mrmondialisation.org/capitalisme-vert-ils-detruisent-le-monde-en-pretendant-le-sauver-dossier/). Cela sonne mal, vous ne trouvez pas ? Les communicants ont donc réalisé un petit tour de magie sémantique, et abracadabra, est apparu le terme de “développement durable” ! En théorie, c’est “*un développement qui répond aux besoins du présent sans compromettre la capacité des générations futures de répondre aux leurs* (cf. [wiki](<https://fr.wikipedia.org/wiki/D%C3%A9veloppement_durable >))”. Sauf qu’en pratique, les besoins du présent sont insatiables - la publicité se charge de les stimuler pour nourrir la “machine à vendre” - et donc le développement ne sera jamais durable, puisqu’il est pris dans une recherche une croissance infinie du PIB, or nous vivons dans un monde fini (qui a des ressources naturelles limitées). 

## Les Licoornes : une alternative au capitalisme ?

Les [Licoornes](https://www.licoornes.coop/) *sont un mouvement qui rassemble plusieurs coopératives engagées : Mobicoop, Enercoop, Telecoop, CoopCircuits, Railcoop, Citiz, Label Emmaüs, Commown et La Nef. Comme le rappelle ce célèbre proverbe africain : “*seul on va plus vite, ensemble, on va plus loin*”. Ces coopératives partagent un idéal écologique, démocratique et solidaire. Elles co-construisent aujourd’hui un nouveau modèle économique radicalement différent de celui du capitalisme. Ce modèle s’accompagne de ruptures idéologiques avec le consumérisme, l’individualisme, la compétition, la recherche du profit, etc. Les activités des coopératives ré-investissent le vivant, les relations humaines, l’environnement, la solidarité, les territoires, etc. Il s’agit d’une réponse collective, pragmatique et anticapitaliste, qui permet aux consomm’acteurs de retrouver du pouvoir. Le pouvoir de s'engager, se mobiliser et participer à un projet collectif différent en devenant sociétaires. Pouvoir d’initier des changements structurels en soutenant des offres plus vertueuses. 

*\*Les licornes avec un seul "o" sont des start-up valorisées à plus d'1 milliard de dollars. Égérie du modèle capitaliste on y trouve Uber, SpaceX ou BlaBlaCar...*