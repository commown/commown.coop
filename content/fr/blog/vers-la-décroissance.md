---
title: Vers la décroissance
description: degrowth, décroissance, capitalisme, écologie, életronique durable
date: 2023-06-07T07:55:59.931Z
translationKey: décroissance
image: /media/decroissance.jpg
---
> Pour l’économiste Timothée Parrique, la décroissance se définit comme une réduction de la production et de la consommation pour alléger l'empreinte écologique, planifiée démocratiquement dans un esprit de justice sociale et dans le souci du bien-être. Cette notion s’oppose au modèle capitaliste qui promeut la croissance qui se définit comme étant l'augmentation de la production de biens et de services d'un ensemble économique sur une période donnée. 

### Le PIB boussole qui nous mène droit dans le mur !

Couramment, on pense que la croissance du PIB est corrélée avec l’augmentation du bien être de la population. Or, bien qu’il mesure la puissance industrielle d’un pays, il reste néanmoins peu éloquent en ce qui concerne le bien-être de la population. Il ne dit rien sur les circonstances qui produisent ladite richesse. En effet, destructions de terre agricole pour la construction d'une autoroute (augmentation du béton coulé et des profits du cimentier), une épidémie nécessitant des vaccins (augmentation de la production de médicaments), le sentiment d’insécurité (vente d’alarmes voire d’armes aux Etats-Unis), bref, autant de choses négatives favorisent la croissance du PIB.

Ainsi, passée la satisfaction des besoins d’une population (manger, boire, dormir, se loger...), le PIB ne dit plus grand-chose sur la santé physique et psychologique des gens, il ne précise pas le niveau de bien-être global des citoyens d’un pays. Pire, la croissance économique est aujourd’hui le coeur du problème écologique pour trois raisons : (1) la finitude du monde, (2) le réchauffement climatique, (3) la destruction du vivant.

### Les limites planétaires sont déjà franchies

Lors de l’Université d’été du Medef (en 2022), l’astrophysicien Aurélien Barrau a défini la croissance comme étant la « destruction des espaces gorgés de vie » pour les transformer en marchandises. Kenneth E. Boulding rappelle une évidence quand il dit que « celui qui croit qu'une croissance exponentielle peut continuer indéfiniment dans un monde fini est soit un fou, soit un économiste. ».

La terre n’est pas un ballon que l’on peut gonfler en fonction de nos désirs, elle est finie, ses ressources sont limitées, si on les épuise, il ne restera rien pour les générations futures. De plus, l’activité productive cause la catastrophe climatique (dont nous commençons à entrevoir les effets : sécheresse autour de la méditerranée cet été). Selon [le 6e rapport du GIEC](https://www.ecologie.gouv.fr/publication-du-6e-rapport-synthese-du-giec), les politiques actuelles conduiraient à un réchauffement global de +2,4 °C  à +3,5°C (avec une médiane +3,2 °C) d’ici la fin du siècle par rapport à l’ère pré-industrielle.

### Le modèle de Commown peut-il être qualifié de post-croissance ?

Notre coopérative est souvent mise en avant que ce soit comme exemple de modèle économique de la fonctionnalité ou que ce soit avec [le collectif Licoornes](https://www.licoornes.coop/) dans lequel nous défendons un récit de post-croissance et post-capitalisme. Mais factuellement Commown est aujourd’hui en croissance. Toutefois la croissance de notre coopérative n’est pas une finalité et n’a pas pour objectif la distribution de dividendes, ni la rémunération du capital. Ainsi, en théorie nous devrions pouvoir, à tout moment, viser un palier et non plus une croissance.

D’un autre côté, le modèle économique de Commown décorrèle en partie la croissance du chiffre d’affaires de la consommation des ressources. En effet, pour un appareil donné, plus celui-ci dure, c’est-à-dire plus nous repoussons la date de remplacement intégral, et plus la coopérative augmente son revenu. Cependant, actuellement la SCIC continue à mettre des appareils neufs sur le marché, dans le but de prendre autant que possible des parts de marchés aux entreprises ayant des modèles économiques délétères.

Quoi qu’il en soit, même un scénario utopique où le modèle de Commown deviendrait majoritaire sur le marché ne serait pas un ralentissement suffisant en termes de consommation de ressources. Un vrai scénario de décroissance électronique demande un changement profond de notre rapport au numérique, interrogeant la pertinence de ses usages et privilégiant la sobriété des fonctionnalités au profit d'un renforcement du tissu social à toutes les échelles : dans les services publics (numérisés jusqu'à l'exclusion aujourd'hui), jusqu'à l'habitat collectif où on pourrait privilégier les ordinateurs fixes mutualisés, conçus pour être réparés et munis de logiciels aux fonctionnalités soigneusement sélectionnées, dans un objectif de faire durer le matériel 20 ans voire plus !

### Vers un changement de société...

Il incombe au champ politique de mettre en place des mesures structurantes pour ralentir rapidement le rythme mortifère de notre société productiviste. Or, pour l’instant, “le changement c’est doucement” comme le chante le groupe des “Goguettes en trio mais à quatre”.

En attendant, il nous faut propager ces idées, et incarner cette nouvelle société que nous désirons voir émerger rapidement. Une société sans technologie omniprésente, sans travailleurs pauvres, sans surproduction et le matraquage publicitaire associé. Et enfin, une société où la coopération, la solidarité, la préservation des Communs et la restauration des écosystèmes détrôneraient le PIB.

Références : 
Allègre, G. (2015). Pourquoi les économistes sont-ils en désaccord ? Faits, valeurs et paradigmes. Revue de la littérature et exemple de la fiscalité. Revue de l’OFCE, 139(3), 197‑224. Cairn.info. https://doi.org/10.3917/reof.139.0197
Parrique, T. (2022). Ralentir ou périr : L’économie de la décroissance. Éditions du Seuil.
https://www.ecologie.gouv.fr/publication-du-6e-rapport-synthese-du-giec 
https://theshiftproject.org/article/climat-synthese-vulgarisee-6eme-rapport-giec/
