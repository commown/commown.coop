---
title: Gaming plus responsable ? le jeu vidéo indépendant sous Linux
description: Jouer de manière plus écologique et sécurisée aux meilleurs jeux,
  c'est possible grâce à Linux ! Commown vous détaille tout dans cet article.
date: 2022-12-09T10:24:25.510Z
translationKey: gaming-linux
image: /media/illu_gaming_pop_os.jpg
---
Chez Commown, nous aimons faire un pas de côté pour prendre le temps de questionner nos usages. Dans le domaine du gaming, cette démarche nous a menés à deux conclusions : d’une part, envisager la sortie du modèle du triple-A au profit des productions indépendantes, d’autre part jouer sous Linux.

## Les studios indépendants

Les bénéfices des jeux « indés » sont multiples :

1. liberté artistique
2. diversité ludique
3. sobriété matérielle
4. responsabilité sociale

Pour donner une idée, voici une sélection de titres marquants de ces dernières années, ayant reçu un excellent accueil critique et communautaire tant pour leurs qualités artistiques que ludiques :

* Hollow Knight
* Disco Elysium
* Outer Wilds
* Dead Cells
* Monument Valley
* Hades
* Celeste
* Ruiner
* The Witness
* Journey
* Stardew Valley
* Blasphemous
* Hotline Miami
* A Short Hike

## Liberté artistique

Là où les studios triple-A auront tendance à pousser une esthétique photo-réaliste afin de produire des *trailers* sensationnels à la manière des *blockbusters* Hollywoodiens, les studios indépendants possèdent une plus grande liberté artistique. Plus facile en effet d’adopter une démarche novatrice lorsque l’on n’est pas possédé par une multinationale dirigée par des actionnaires uniquement préoccupés par le rapport risques-bénéfices.

Il suffit d’un coup d’œil aux visuels de *Celeste, Disco Elysium, Outer Wilds,* ou encore *Hollow Knight* pour se faire une idée de la richesse des approches possibles :

![](/media/indie_celeste2.jpg "Celeste, un platformer arcade exigeant et valorisant.")

![](/media/indie_disco2.jpg "Disco Elysium, un RPG d’enquête immersif.")

![](/media/indie_hollow1.jpg "Hollow Knight, un nouveau classique : metroïdvania exigeant dans un univers aussi magnifique que mélancolique.")

![](/media/indie_outer2.jpg "Outer Wilds, un jeu centré sur l’exploration et l’expérimentation.")

![](/media/indie_ori1.jpg "Ori and the blind forest, un chef-d'oeuvre graphique*.")

\* remarque : Ori a été créé par Moon Studios, mais publié par Microsoft. L'exemple est valide pour la diversité artistique, mais pas pour la compatibilité Linux.

## Diversité ludique

Contrairement au jeu triple-A favorisant une expérience lissée, « bien dans les clous » afin de pouvoir être vendue à un maximum de consommateurs, l’écosystème indépendant favorise la ***pluralité des propositions ludiques*** : des balades contemplatives aux défis rapides et exigeants, des RPG aux platformers arcade en passant par les jeux de gestion : les possibilités sont légions.

Autre avantage du côté indé : la ***juste mesure*** face à la course au contenu.

On connaît depuis plusieurs années la rengaine des grosses sorties issues de franchises à succès : graphismes au top mais proposition ludique se renouvelant parfois trop peu, et surtout : du *contenu* à ne plus savoir qu’en faire. Pour justifier l’achat à 60 euros chaque année d’une nouvelle variation d’un thème déjà connu, un argument marketing phare est la durée de vie : une poignée de boucles de jeux copiées-collées encore et encore pour remplir la centaine d’heures de jeu ciblée.

À contre-courant de ce modèle, les studios indé n’ayant ni les moyens ni la contrainte de produire une telle quantité de vide, se concentrent sur le fait d’apporter ***une expérience riche dans un cadre plus modeste***, pour proposer un ensemble souvent bien plus cohérent, unique et pertinent.

## Sobriété matérielle

Une valeur centrale chez Commown : maximiser les usages à partir de ressources limitées.

Nous pensons qu’il est absolument nécessaire de remettre en question nos pratiques de consommateurs, d’autant plus lorsque l’on touche au domaine du jeu vidéo. En effet, les pratiques commerciales entourant cette industrie en forte croissance poussent rapidement les machines à l’obsolescence, peut-être plus vite que dans tout autre domaine.

S’il est possible de jouer aux derniers titres en date, il est aussi nécessaire de ***raisonner nos besoins en puissance de calcul***, notamment en se retirant de la course au photo-réalisme si importante dans le marché triple-A.

Pour ***utiliser nos machines plus longtemps***, des technologies graphiques moins consommatrices en ressources sont à favoriser : 2D, voxel, cell shading, rendus « cartoon »... Moins coûteuses en production, ces technologies sont fréquemment utilisées dans les productions indépendantes, et permettent une expressivité artistique décuplée.

Enfin, il est à noter que les jeux indépendants sont souvent proposés nativement pour Linux ! Un bon point supplémentaire, permettant d’amoindrir notre dépendance aux grosses entreprises de la tech en sortant du choix imposé entre Microsoft, Sony et Nintendo.

Pour appuyer nos dires, nous avons testé quelques jeux indés sur une de nos anciennes machines : un portable dédié à la bureautique et donc sans carte graphique dédiée :

* Modèle : Clevo N131ZU (2018)
* OS : Pop !_OS 22.04
* CPU : Intel Core i7 8565U
* GPU : Intel UHD Graphics 620 (intégré au CPU)
* RAM : 8GB

Conclusions :

* De nombreux jeux 2D sont jouables dans de très bonnes conditions (1080p - 60fps) avec parfois quelques ajustements dans la qualité des graphismes : Hollow Knight, Celeste, Hotline Miami 2, Darkest Dungeon, Don’t Starve Together
* Certains autres refusent malheureusement de se lancer, comme Ori and the Blind Forest : probablement un problème de compatibilité entre pilotes et matériel...
* Sur du vieux matériel, les jeux 3D relativement récents seront plutôt à éviter. En revanche certains classiques ont de bonnes chances de fonctionner (nous n’avons pas testé) : Bioshock, Portal, Counter Strike, Dota 2, League of Legends...

#### Responsabilité sociale

Un sujet assez dramatique ressortant souvent ces dernières années est la pratique du *[crunch](https://www.tryagame.fr/licenciements-harcelements-crunch-lactu-recente-des-employe-e-s-du-jeu-video/ "https\://www.tryagame.fr/licenciements-harcelements-crunch-lactu-recente-des-employe-e-s-du-jeu-video/")* : dans les grands studios de développement, plusieurs mois avant la date de sortie prévue, les employés se trouvent contraints à faire des semaines de plus de 80 heures afin de tenir les délais. Ce qui s’accompagne de pratiques managériales indécentes pour maintenir le rythme et étouffer les protestations.

Dans ce contexte, il nous apparaît évident de favoriser tant médiatiquement que financièrement les petites productions, initiatives motivées par la passion de bien faire plutôt que par la seule rentabilité financière.

Par ailleurs, les jeux indépendants vont plus avoir tendance à mettre en avant l’écologie que les grands studios plus consensuels. Par exemple, nous recommandons The Wandering Village, Terra Nils (demo disponible sur Steam, sortie prochaine) ou Eco (2018).

## Gaming Linux, un pas de plus

Faire durer un ordinateur ou un smartphone est beaucoup plus facile en faisant tourner l’appareil avec un système d’exploitation libre. En effet, cela permet de maintenir stable et sécurisé un appareil longtemps après la fin de la maintenance du producteur. Par ailleurs, ces systèmes sont plus respectueux de la vie privée, et consomment en général moins d’électricité à usage équivalent.

Ces dernières années, il est devenu possible de jouer sous Linux à de très nombreux jeux, y compris des grands studios, sans baisse de performance.