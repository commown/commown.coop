---
title: "L’impact écologique du numérique : des déchets pas si dématérialisés..."
description: Si le numérique est par définition non palpable, les déchets qu'il
  produit le sont indéniablement ! Commown vous révèle leur impact écologique.
date: 2022-11-30T08:20:48.521Z
translationKey: déchets numériques
image: /media/esclavage-moderne.jpg
---
> Nous devons coopérer et partager les ressources de la planète de façon équitable. Nous devons commencer à vivre dans les limites de ce que la  planète propose (...). Il faut que nous protégions la biosphère. L’air. Les forêts. La terre. (Greta Thunberg)                                                                                       

### La vie des déchets des équipements électriques et électroniques (DEEE)

Le volume des déchets des équipements électriques et électroniques (DEEE) ne cesse d’augmenter d’années en années au niveau mondial ! Selon Global e-waste monitor 2020, le rapport des Nations Unies, la quantité de déchets électroniques produit dans le monde en 2019 a atteint le chiffre record de 53,6 millions de tonnes. Soit l’équivalent de 38 millions de SUV ou 5300 Tours Eiffel jetées chaque année ! Les DEEE recouvrent un ensemble de “gros” produits : machines à laver, frigo, TV... Mais aussi des plus petits comme les smartphones. Concernant ces derniers un WEEE forum établissait qu’en 2022 près de 5,3 milliards d’entre eux deviendraient des déchets !

### Cachez ces déchets que je ne saurais voir !

A cela s’ajoute une traçabilité déplorable au niveau mondial. En effet, seulement 20% des DEEE sont correctement retraités (à peine plus en France : 44%). Les 80% restants sortent des filières agréées et finissent dans des décharges à ciel ouvert comme la tristement célèbre décharge d’Agbogbloshie au Ghana. Il est également regrettable que de nombreux ordinosaures décédés reste stockés dans nos greniers. Idem pour les antiquités smartphoniques au fond de nos tiroirs. Alors que ceux-ci pourraient être réparés, reconditionnés, ou éventuellement pour certaines pièces recyclées. En France l’ADEME estime qu’entre 50 et 100 millions de smartphones dormiraient dans nos tiroirs ! 

### Recyclage et reconditionnement, les réponses parfaites ?

Le recyclage et le reconditionnement sont souvent présentés comme le Salut des déchets électroniques. La principale limite est que ces derniers ne sont pas des modèles durables.

Le reconditionnement a actuellement du sens au regard des taux déments de renouvellement des produits électroniques. Toutefois, dans l’hypothèse où nous venions à disposer d’appareils capables d’être réparés infiniment ou presque, et où la pression publicitaire venait à cesser, alors peut-être que l’on aurait moins d’appareils à reconditionner. 
Concernant le recyclage, il n’est pas rare d’entendre des propos tels que “*continuons gaiement à surproduire des appareils à partir de matériaux recyclés et tout ira bien*”. 

Sauf que :

\- Les rendements de recyclage sont très variables selon les éléments chimiques (de 0% pour l’indium à 90% pour l’or par exemple). 

\- A chaque cycle de recyclage, on perd forcément en quantité et surtout en qualité des matériaux, ce qui fait que certains métaux ne peuvent pas être réutilisés pour les mêmes usages.  

\- Dans un marché en constante croissance et même si les rendements atteignaient 100%, le recyclage ne suffirait pas pour répondre à la demande. 

### Vers une remise en question de l'esprit du consommateur ?

Que faire face à ce tsunami de déchets ? En premier lieu, en tant que consom’acteurs, nous devons prendre conscience des enjeux écologiques et faire le choix de la sobriété (pour la préservation de la planète et le bien-être des futures générations). Nous devons apprendre à nous défendre face aux chants des sirènes de la publicité. Ne pas céder à la servitude volontaire de la consommation pavlovienne. S’émanciper des images identificatoires que l’on nous vend dans les pubs. En bref, s’affranchir du modèle où la consommation serait notre seul moyen de nous réaliser : ne plus céder au “*j’achète donc je suis*”. 

### Une révision nécessaire de l'activité des producteurs d’équipements électriques et électroniques 

Au regard de la raréfaction croissante des matériaux qui permettent de fabriquer les produits électroniques, les producteurs doivent réagir. Ils doivent revoir la conception des produits et se diriger vers la low-tech en fabriquant des produits moins puissants (donc moins énergivores), moins miniaturisés (donc plus facilement réparables et moins fragiles) et plus facilement réparables. Autre avantage de sortir de cette course à la miniaturisation : le recyclage en fin de vie devrait en être facilité. Ces questions de la sobriété et de l’éco-conception devraient être les points cardinaux de tous les départements de R&D. Or, la réalité est que l’on nous promet la 6G ou le metaverse... 

### Un manque de fermeté des législations actuelles

Il faut reconnaître que le législateur essaye de s’attaquer timidement à ces questions que ce soit au niveau européen comme au niveau français. Toutefois, il devient urgent de faire passer des mesures radicales en phase avec l’impératif de sobriété. Au niveau de l’Europe certaines propositions de réglementations semblent tendre dans cette direction. Par exemple, on parle d’une interdiction pure et simple de la vente d’écran 8K sur le marché UE. 

Au niveau Français, nos élus pourraient montrer la voie, notamment pour lutter contre l’un des facteurs clefs de la prolifération de déchets : l’obsolescence culturelle ! Une série de mesures pour s’atteler à ce problème est présentée dans le rapport de l’Institut Veblen*, comme l’interdiction pure et simple de la publicité pour certains produits ou secteurs (que nous pourrions appliquer à la vente de produits électroniques par exemple). 

Parallèlement il serait intéressant de mettre en place des quotas de ventes sur les produits électroniques neufs mis sur le marché chaque année pour imposer un rythme de production à la baisse. Cela pourrait aussi passer par un obligation légale à respecter un laps de temps (2-3 ans) entre la sortie de deux modèles différents dans une même gamme.

\*Dupré, M. (2022). La communication commerciale à l’ère de la sobriété. Taxer la publicité pour consommer autrement. https://www.veblen-institute.org/La-communication-commerciale-a-l-ere-de-la-sobriete.html
