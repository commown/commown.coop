---
title: 'Progrès, vous avez dit "progrès"? '
date: 2021-07-21T12:57:25.046Z
translationKey: progrès
image: /media/jeff-bezos-le-techno-solutionniste-sauveur-de-l-humanité.jpg
---
*Comment Commown se positionne-t-elle par rapport à la question du progrès ?*

**Quid du progrès ?** 

Le progrés vient du latin progressus qui signifie “l’action d’avancer”, le mot veut dire développement, mouvement dans une certaine direction. Le progrès peut être positif ou négatif, ainsi, on peut dire qu’un enfant progresse à l’école (il s’améliore) et que le dérèglement climatique progresse (le climat se détériore)… Souvent quand on parle du progrès, on parle du progrès des sociétés modernes, c'est-à-dire du progrès technique, technologique, scientifique, etc. Le philosophe Michel Serres exprime l’idée - dans son livre [Petite Poucette](https://www.placedeslibraires.fr/livre/9782501166829-petite-poucette-michel-serres/) - que l’humanité a connu un progrès extraordinaire en un siècle. Il affirme qu’un individu au début du XXe siècle avait un mode de vie plus proche de celui du moyen-âge que de notre monde actuel. De fait, on est passé de la charrue (inventée entre [le V et X après J.C](https://fr.wikipedia.org/wiki/Charrue)) au  tracteur qui est arrivé dans les années [1950 en France.](https://fr.wikipedia.org/wiki/Tracteur_agricole#:~:text=Le%20tracteur%20s%27est%20d%C3%A9velopp%C3%A9,essor%20dans%20les%20ann%C3%A9es%201950.) Les progrès techno-scientifiques sont très récents et ont bouleversé nos modes d'existence en quelques décennies. Il existe aujourd’hui des positions radicalement contraires face à ce changement, nous allons en évoquer deux (mais il en existe d’autres).

**Les pro-progrès ou technophiles : le modèle transhumaniste des GAFAM** 

L'une des postures en faveur du progrès techno-scientifique est incarnée par Elon Musk. Celui-ci fait l’éloge de la science, des nouvelles technologies et du transhumanisme. Il prône l’hybridation homme-machine et rêve d’un post-humain, qui serait un super-humain performant et résistant à tout, même à la mort. Il incarne la démesure, l’hybris. La catastrophe écologique ne l’inquiète guère. Souvent les technophiles adhèrent au crédo attribué à Obama :  “là où croît le péril, croît aussi ce qui sauve”. Ainsi, dans ce modèle, on pense que les start-up et les technologies sauveront l’humanité des catastrophes causées par ces mêmes technologies et la recherche de la croissance économique. Et puis, si on finit par épuiser toutes les ressources de la planète, il nous restera qu’à coloniser l’univers où simplement exploiter les ressources de la Lune.  

**Les technocritiques ou technosceptiques : le modèle prudent de Commown ?** 

Commown fait partie de ceux qui osent questionner et critiquer le dogmatisme technophile. Sur le plan purement pragmatique, il nous serait très difficile d’adopter une position radicalement technophobe et anti-progrès dans la mesure où notre activité relève de la location d'appareils électroniques. Toutefois, nous cheminons vers un idéal ! Nous trouvons que le “progrès” tel qu’il est envisagé par le discours actuel est trop techno-centré et occulte un aspect important : la nécessité de remettre le vivant au cœur de l’imaginaire progressiste.  

**Le progrès selon Commown** 

Le progrès doit être pensé autour des thématiques de l’égalité, de la connaissance immatérielle, de la spiritualité (c’est-à-dire l’art et la manière de “grandir en humanité” pour paraphraser [Frédéric Lenoir](https://www.fayard.fr/documents-temoignages/dun-monde-lautre-9782213716527)), de l’accession au bonheur, de l’entraide et la solidarité, du lien social, de la préservation de la biodiversité et du vivant, de l’émancipation aux injonctions consuméristes, de la culture et des arts, de la transition écologique, de la santé, de l’éducation, de la communication interindividuelle, de la démocratie, du rapport au temps. Bref, nous sommes loin de résumer le progrès à l’augmentation de niveau de technologie d’une société.

**Un long voyage qui ne se fait qu’un pas à la fois...**

Nous cheminons vers ce progrès. Comme vous vous en doutez c’est une démarche qui nous demande de réfléchir (beaucoup) et d’accepter que nous puissions avoir des contradictions pour les surmonter petit à petit.  Parler de progrès, c’est bien joli mais encore faut-il se demander “progrès au service de quoi ou de qui ?'’. Pour nous il faut qu’il soit au service du vivant et du plus grand nombre ! Or, aujourd’hui le progrès est principalement au service du capitalisme responsable de la destruction du vivant, de la surexploitation des ressources et des personnes. Bref, il ne va pas dans le sens d’une société humaniste et écologique qui tend à préserver le vivant.

C’est pour cette raison que nous avons choisi le statut de SCIC pour lancer notre coopérative et porter notre vision du progrès. Statut qui est selon nous la meilleure forme juridique pour mettre l'entreprise au service du bien commun (vision qui est à l’origine du rassemblement des [Licoornes](https://www.licoornes.coop/)).