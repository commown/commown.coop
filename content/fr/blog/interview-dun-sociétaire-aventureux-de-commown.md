---
title: Charles, activiste pour la justice sociale et environnementale
description: Quels engagements peux-t-on prendre face à l'urgence climatique ?
  Commown interview d'un activiste pour la justice sociale et environnementale.
date: 2022-06-21T12:17:20.810Z
translationKey: Licoornes
image: /media/charles-activiste-pour-le-climat.png
---
*Aujourd'hui nous vous proposons une interview d'un sociétaire Commown qui fait notre fierté. Charles est un citoyen engagé face à l'urgence environnementale et sociale. Nous espérons que son témoignage contribuera pour vous.* 

### Vouvoiement ou tutoiement ?

Tutoiement ! 

### Peux-tu te présenter en quelques mots ?

Je m’appelle Charles, j’ai 28 ans, je suis ingénieur de formation et surtout militant pour la justice sociale et environnementale.

### Vandana Shiva, Rob Hopkins, Brune Poirson ou Elon Musk ?

Elon Musk bien sûr, il me semble évident que la technologie va nous sauver ou, à défaut, nous envoyer sur Mars ! Plus sérieusement, je préfère ne pas avoir d’idoles, ça évite d’être déçu. Mais je serais plutôt du genre décroissant.

### Pourquoi avoir investi chez Commown ?

Quand j’ai commencé à travailler, en 2017, j’avais un salaire d’ingénieur – largement plus que ce dont j’avais besoin. J’ai commencé à chercher ce que mon argent pouvait faire d’utile, et quand j’ai découvert Commown (honnêtement je ne sais plus comment) je me suis dit que ça cochait pas mal de cases. Mon téléphone fonctionnait encore très bien, alors je suis devenu sociétaire sans être client !

### Que penses-tu de la raison d’être des Licoornes* : Donner à chacun·e le pouvoir de transformer radicalement l’économie pour la mettre au service de l’intérêt général ?

Ça ne suffira évidemment pas, mais il me semble que c’est un outil extrêmement pertinent. Difficile d’imaginer que les profondes transformations de la société qui sont nécessaires à notre survie puissent advenir de façon immédiate et instantanée ; des structures qui détournent le système pour bâtir maintenant l’avenir, je prends.

### Un autre moyen d’engagement que tu aimerais mettre en lumière ?

Évidemment la désobéissance civile. Face aux catastrophes environnementales, à l’urgence sociale absolue, impossible de rester de marbre, et je pense qu’il existe aujourd’hui plusieurs mouvements et organisations qui permettent à de nombreuses personnes de s’engager très concrètement. Mais de nombreuses autres formes d’actions collectives sont pertinentes, bien sûr : prendre part à des mobilisations de masse et contribuer à les organiser, s’investir dans un parti politique ou dans des associations de sensibilisation, de plaidoyer… L’essentiel pour moi est de ne pas rester statique et d’allier, comme le formulait Gramsci, au pessimisme de la raison l’optimisme de la volonté. Agir pour éviter le désespoir. C’est comme ça que pour ma part je me suis retrouvé au tribunal, pour avoir dénoncé le [sabotage climatique et social d’Emmanuel Macron](https://decrochons-macron.fr/le-vrai-bilan/) en décrochant (calmement) son portrait dans une mairie, avec le mouvement [Alternatiba/Action Non-Violente COP21](https://rhone.alternatiba.eu/). Je suis également actif au sein des [Amis de la Terre](https://www.amisdelaterre.org/) depuis maintenant plus de deux ans.

* Pour devenir sociétaire de Commown c’est [sur cette page de notre site !](/epargne-solidaire/)
* © Cyril Badet (photographie de Charles)
