---
title: La naissance d'un FrankenPhone
description: Un smartphone obsolète entièrement remis en service par le support
  Commown. Découverte de la création du Frankenphone de Commown.
date: 2022-10-25T13:39:28.033Z
translationKey: frankenphone
image: /media/frankenphone-visuel-principal-et-visuel-3.jpg
---
> #### **Enregistrement du journal de bord de Céline, ingénieure au service support chez Commown.**

*Jeudi 6 octobre 2022, 19 heures, 32 minutes :*\
Aujourd’hui, nous avons reçu un colis qui nous a été envoyé par un client, il était de taille moyenne, emballé dans son étui en cellulose biodégradable et il contenait la dépouille d’un Fairphone 2. \
Ce Fairphone 2 a vécu une vie heureuse possible dans un environnement urbain et réactif. Il avait déjà été renvoyé dans notre atelier pour des petits bobos communs à l’existence des smartphones : un écran cassé et un faux contact micro. \
Cependant, cette fois le diagnostic est sans appel. Après 5 ans d’existence, ce Fairphone 2 est mort. Il ne s’allume plus depuis déjà 4 jours. Nous procéderons à l’autopsie demain.

*Vendredi 7 octobre 2022, 9 heures, 16 minutes :*
L’autopsie s’est bien passée, nous sommes soulagés d’apprendre que ce Fairphone 2 s’est éteint paisiblement, le décès n’a pas été occasionné par noyade. \
Nous avons remarqué une connectique usée, une batterie épuisée au bord de la rupture, l’écran est cependant encore en bon état bien qu'il ne s'allume plus. \
Nous savons désormais que la mort a été causée par l’arrêt définitif de la carte-mère. Par analogie avec le corps humain, c’est comme si la mort était causée par l’arrêt brutal du cerveau. Nous rangeons sa dépouille dans la fosse commune des Fairphone Commown.

*Mardi 11 octobre 2022, 11 heures, 24 minutes :*
Logan (expert informatique chez Commown) expose une idée aventureuse. Et si nous essayions de ressusciter le Fairphone 2 fraîchement décédé de la semaine dernière. \
Il a parallèlement reçu la dépouille d’un autre Fairphone 2 qui contient une carte-mère encore fonctionnelle. Nous y réfléchissions, l’opération sera difficile et périlleuse mais comme les deux Fairphone sont déjà morts, nous décidons de tenter le tout pour le tout. \
Nous réunissons une équipe de choc composée de nous deux, ainsi que de nos experts maisons : Coline et Ayman. Nous nous donnons rendez-vous cet après-midi à 14 heures dans le labo spécial de Commown pour réaliser notre expérience. 

*Mardi 11 octobre 2022, 15 heures, 24 minutes :* 
Après une heure et demie d’une lourde opération sur notre défunt, son corps a l’air à nouveau sain. Grâce à la carte-mère apportée par Logan et à un module de charge prélevé sur un 3ème cadavre (un peu moins frais que les autres), ce n’est plus vraiment le même smartphone que nous découvrons. Adieu Fairphone, et bienvenue à toi FrankenPhone. 

*Vendredi 14 octobre 2022, 10 heures, 42 minutes :*
Après avoir effectué de nombreux tests matériels et logiciels, il reste à effacer le poids de son ancienne vie qui pèse encore sur le FrankenPhone. Son corps est comme neuf, mais son cerveau est toujours en mort cérébrale. Nous décidons, de lui insuffler une nouvelle énergie vitale en installant un nouvel OS libre (e/OS). \
Le courant électrique passe, l’écran s’allume à nouveau. Le FrankenPhone est vivant ! 

*Lundi 17 octobre 2022, 9 heures, 2 minutes :*
C’est l’heure des adieux, à peine de retour du royaume des morts (après quelques jours de rééducation), le FrankenPhone va repartir chez un nouveau propriétaire. \
Nous le remettons délicatement dans la boite en carton et papier déjà utilisée plusieurs fois depuis 2018 et nous attendons le passage du livreur à vélo de la coopérative Kooglof. C’est le début d’une nouvelle vie pour lui.