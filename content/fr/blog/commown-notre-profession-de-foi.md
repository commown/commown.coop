---
title: "Commown : notre profession de foi"
description: Commown présente ses 10 commandements, sa profession de foi. Elle
  expose son éthique et ses valeurs écologiques et humanistes.
date: 2022-01-18T09:16:01.031Z
translationKey: faith
image: /media/commown-les-10-commandements.jpg
---
> Un grand pouvoir implique… de grandes responsabilités… Créer une structure juridique confère un certain pouvoir, il est donc de notre devoir d’obéir à des règles éthiques et de les partager (ne serait-ce que pour montrer l’exemple !). De ce fait, voici les 10 commandements de notre coopérative.  

# Les 10 commandements de Commown

1. **L’écologie tu suivras**\
   L’écologie est la boussole de Commown, c’est le sens de la démarche qu’il poursuit. S’il peut réduire son impact environnemental et partager les valeurs écologiques, alors il le fera. 
2. **Contre les puissants, l’injustice, la pollution tu lutteras**
   L’on sait que les petits gestes sont une bonne chose mais (hélas !) ça ne suffira pas pour résoudre la crise écologique qui s’annonce. Ainsi, il nous faut participer - à notre échelle et avec les capacités qui sont les nôtres - à la lutte contre le pouvoir du marché, de l’argent, des multinationales qui impactent (très majoritairement) l’environnement et sont responsables de la souffrance de nombreuses personnes dans le monde.  
3. **De curiosité tu abuseras** 
   La curiosité, c'est le désir de connaître, de savoir. Un dicton français dit que “la curiosité est un vilain défaut”. Pour nous, il s’agit plutôt d’une qualité essentielle. Chez Commown, on la cultive de multiples façons : par la découverte de la nature (comme une balade en forêt sur les plantes comestibles), par l’exploration culturelle (il est toujours agréable de regarder un bon film, de lire un roman, ou d’essayer de pratiquer une nouvelle activité) ou encore, par la vérification de la véracité des informations (dans un monde de greenwashing et de “fake news”, nous aimons bien chercher la petite bête pour faire le tri). 
4. **De raison et de bon sens tu useras**
   La raison, c'est une faculté de la pensée qui permet habituellement de discerner avec justesse les choses. L’expérience, nous conduit à penser que la raison (seule) ne suffit pas toujours, il faut parfait la compléter utilement avec une dose de bon sens. Concrètement, il s'agira de pousser à [la réflexion avant “achat” de s’interroger avant d’acheter](https://commown.coop/blog/black-friday-kit-de-survie-du-consomacteur-en-milieu-hostile/), [de penser la sobriété,](https://commown.coop/blog/progr%C3%A8s-vous-avez-dit-progr%C3%A8s/) d’essayer de [s’attaquer aux racines des problèmes plutôt qu’aux branches](https://commown.coop/blog/les-licoornes-une-solution-pour-lutter-contre-le-capitalisme/) (bien sûr nous ne parlons pas d’abattre des arbres ici !). 
5. **De non-violence tu t’armeras**
   La violence dont nous parlons est morale et souvent dirigée contre soi ou les autres. Nous essayons chez Commown - comme nous le pouvons  - d’adopter une posture non-violente envers nous-même, mais également avec les autres. Comme le dit le psychiatre Christophe André : “Ne vous faites pas de mal… La vie s’en chargera pour vous.”  
6. **L’éthique tu chériras**
   Qu’est-ce que je dois faire ? Voilà la question éthique par excellence. Essayer de réfléchir à la question, c’est déjà une démarche éthique. Commown s’interroge sur la justesse de ses actions, et sur l’impact de notre communication sur l’environnement et la société.   
7. **D’humilité tu te draperas** 
   Dans l’antiquité romaine, on célébrait les victoires des généraux avec une grande cérémonie. Ainsi, un général paradait sur son char à Rome, mais il n’était pas seul, un esclave était à ses côtés et lui répétait sans discontinuer : « Respice post te! Hominem te esse memento! » c'est-à-dire « Regarde autour de toi, et souviens-toi que tu n'es qu'un homme ! ». 
8. **À l’émancipation tu aspireras** 
   “Si on souhaite apporter la lumière, c'est pour que tout le monde y voit”, dit peu ou prou Le Roi Arthur dans la série télé Kaamelott (d’Alexandre Astier) ! Commown rêve d’un monde où toutes personnes puissent être plus autonomes\* et heureuses\**. Par conséquent, nous essayons d’aider nos bénéficiaires à s’émanciper des discours et des images de la publicité. Discours qui réduit fréquemment l’idée du bonheur à acheter tel ou tel produit.  
9. **La bienveillance tu embrasseras**
   “D’abord, ne pas nuire” cela vient de l’éthique médicale et il nous semblait important d’en parler. Vouloir du bien à tout le monde n’est pas chose aisée, mais essayer de ne pas nuire peut déjà être un pas vers le chemin sur lequel nous essayons de nous engager.
10. **Tire la chevillette et la bobinette cherra**
    L’humour, parce que “rire est le propre de l’homme” comme dirait l’autre !

\*Autonomie : dans le sens où nos contributeurs et collaborateurs trouvent les moyens de reprendre le contrôle vis-à-vis du numérique. 
\**Bonheur : Comme l’écrit Frédéric Lenoir “*un état de conscience, de satisfaction globale et durable, dans une vie qui a du sens, et qui est fondé sur la vérité”*.