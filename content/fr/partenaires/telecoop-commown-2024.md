---
header2: true
layout: invest
title: "Financer l'électronique du futur : sobre et mutualisée"
description: TeleCoop et Commown ont besoin de votre soutien
date: 2024-02-27T08:58:04.616Z
translationKey: telecoop-commown-2024
image: /media/telecoop-commown-rs.png
menu:
  main:
    name: Le retour des cabines téléphoniques
    parent: Particuliers
    weight: 10
header:
  title: Le retour des cabines téléphoniques en France ?
  subtitle: TeleCoop et Commown ont besoin de votre soutien
  ctas: []
sections:
  - bg_color: bg-white
    containers:
      - blocks:
          - title: L’IA comme nouveau « relais de croissance » ?
            text: >-
              *La France doit investir 5 milliards d'euros par an sur cinq ans
              dans l'intelligence artificielle si elle veut faire jeu égal avec
              les États-Unis et la Chine, un "défi majeur", selon le rapport du
              comité IA remis le 13 mars au président Macron.*\

               \

              Personne, dans le gouvernement, ne semble se poser les questions suivantes :


              * Avons-nous réellement BESOIN de cette nouvelle technologie ? 

              * Pouvons-nous nous le permettre d’un point de vue environnemental ?
          - title: ""
            figure:
              image: /media/ia_compressed.png
        title: ""
    id: IA-nimp
  - id: questions-existentielles
    bg_color: bg-beige-25
    color: text-noir-100
    containers:
      - title: ""
        blocks:
          - figure:
              image: /media/l-avenir-du-numérique_compressed.png
          - text: >-
              * Devrons-nous choisir entre la production de panneaux solaires,
              d’instruments médicaux ou de casques de réalité virtuelle dès
              2033 ?

              * Aurons-nous [assez d’eau](https://mamot.fr/@commownfr/112172376363145638) pour nos cultures, alimenter la population, refroidir nos serveurs et produire nos semi-conducteurs en 2035 ?

              * Sera-t-il toujours possible de produire des smartphones en 2040 ?

              * Est-ce que les infrastructures de télécommunication pourront encore être maintenues en 2045 pour continuer à disposer du « web » ?
            title: L’avenir du numérique ?
  - id: aucune-certitude
    containers:
      - title: ""
        ctas: []
        blocks:
          - title: Chez Commown et TeleCoop nous n’avons aucune certitude...
            text: >-
              ... mais nous sommes persuadés que la voie de la sobriété et de la
              mutualisation est la seule possible. Nous devons remettre la
              technologie et le numérique à leur juste place.


              À notre échelle, nous nous sommes déjà engagés sur ces sentiers : des forfaits téléphoniques orientés vers la sobriété chez TeleCoop et des offres de services sur des appareils électroniques mutualisés chez Commown.
            data: ""
          - figure:
              image: /media/commown-telecoop_compressed.png
      - title: ""
        ctas: []
        blocks:
          - title: Qui est Commown ?
            details: true
            text: "**Commown** est un fournisseur militant d’appareils électroniques
              éco-conçus et de services pour lutter contre l’obsolescence
              prématurée. Le modèle économique dominant (production/vente),
              pousse à produire toujours plus. Cette surproduction engendre des
              dégâts écologiques et sociaux. Pour sortir de cette spirale,
              Commown propose des appareils éco-conçus tels que le Fairphone en
              location avec tous les services pour les faire durer."
            ctas:
              - outline: false
                text: Investir chez Commown
                href: "#contreparties"
          - title: Qui est TeleCoop ?
            details: true
            text: "**TeleCoop** est un opérateur d'intérêt général. Nous sommes le premier
              opérateur télécom coopératif engagé dans la transition écologique
              et sociale. Nous vous accompagnons vers la sobriété numérique et
              nous vous aidons à reprendre en main vos usages numériques."
            ctas:
              - outline: false
                text: S'abonner chez TeleCoop
                href: "#contreparties"
    color: text-bleu-fonce-100
  - id: le-retour-des-cabines-tel
    containers:
      - title: ""
        blocks:
          - text: >-
              C’est pourquoi TeleCoop et Commown s’associent* pour promouvoir le
              retour des cabines téléphoniques en France !


              Grâce à cette campagne, nous pourrons lancer une étude de faisabilité, qui inclura la création de prototypes installés à Strasbourg. Par cette étude, nous souhaitons requestionner la façon dont nous pourrions rendre le numérique et la communication accessibles dans l'espace public.


              Ainsi, nous cherchons à nous opposer au récit actuel de la surabondance, du toujours plus vite et du toujours plus connecté. Au contraire, nous cherchons à développer l'idée selon laquelle le numérique peut être un bien partagé et sobre. \

              \

              La cabine en est un symbole, le symbole d'un temps où notre attention n’était pas autant accaparée par les écrans de nos smartphones.


              \**l’idée nous vient d’un collectif de Grenoble :* *[l’OIRCT !](https://oirct.org/category/pourquoi/ "https\://oirct.org/category/pourquoi/")*
            ctas:
              - outline: false
                text: Soutenir le projet
                href: "#contreparties"
            title: Le retour des cabines téléphoniques en France ?
          - figure:
              image: /media/cabine-tel_compressed.png
        figure:
          image: ""
        ctas: []
    bg_color: bg-beige-25
    color: text-violet-100
  - id: pourquoi
    containers:
      - title: "À quoi pourraient servir ces cabines ? "
        blocks:
          - title: Lutte contre la précarité
            text: "*Dotées d’une tablette et d'une connexion internet, elles pourraient
              servir aux personnes en grande précarité pour accéder gratuitement
              à certains sites et communiquer avec leurs proches.*"
          - title: Cabine scolaire de déconnexion
            text: "*Pourvues d’un téléphone et de la capacité à envoyer des SMS
              gratuitement, elles pourraient être installées à la demande de
              collectifs de parents d’élèves devant des écoles et collèges.
              Ceci, afin de repousser le moment d’équiper leur enfant d’un
              « smartphone », ou simplement de pallier une batterie déchargée.*"
          - title: médiation culturelle
            text: "*Équipée d’un téléphone pour passer gratuitement des appels, la cabine
              serait surtout un outil de communication urbaine pour sensibiliser
              aux enjeux environnementaux et sociaux du numérique* *et orienter
              vers les acteurs locaux qui proposent des solutions.*"
  - id: concretement
    containers:
      - blocks:
          - text: >-
              Nous sommes actuellement à la phase d’étude du projet. Celui-ci a
              pour ambition de réutiliser les cartes-mères encore fonctionnelles
              de Fairphone 2 pour les transformer en « cabines téléphoniques ».
              Nous avons répondu à l’appel à projet ECONUM de l’ADEME et sommes
              dans l’attente de leur réponse pour le financement d’une première
              étude de faisabilité. En investissant chez Commown, vous augmentez
              notre capacité à obtenir des subventions de ce type par effet de
              levier.\

               \

              Bien entendu, si ce projet est une réussite, nous publierons tous les plans de conception en open-hardware, pour faciliter un déploiement par des collectifs locaux.


              ##### **Soutenir nos deux coopératives, c'est aussi soutenir ce projet !**
            title: Concrètement
          - figure:
              image: /media/concretement_compressed.png
        ctas:
          - outline: false
            text: Soutenir le projet
            href: "#contreparties"
    bg_color: bg-beige-25
    color: text-bleu-fonce-100
  - containers:
      - title: Objectifs de la campagne
        blocks:
          - text: >-
              Pour TeleCoop l’enjeu pour 2024 est d’atteindre **10 000 clients**
              afin de **stabiliser** leur modèle. 


              En souscrivant chez TeleCoop, vous affirmez résolument que la sobriété devrait être une priorité ! 


              Chaque ligne active chez TeleCoop est une façon de **renforcer notre plaidoyer commun**. Aussi, le développement de TeleCoop leur permettra d’assurer la mise à disposition des cartes SIM nécessaires aux communications gratuites dans les cabines.
            title: Pour TeleCoop
          - title: Pour Commown
            text: >-
              Pour Commown, l’objectif est de lever **200 000 €** en parts
              sociales. Cela consolidera nos capitaux propres pour **faire
              levier** sur des prêts bancaires et subventions, dont celle
              demandée (ECONUM) pour le **projet de cabine téléphonique**.


              En devenant sociétaire de notre coopérative, vous **donnez du sens à votre épargne** et vous contribuez à faire grandir le parc mutualisé d'appareils plus durables, de plus de 5000 appareils aujourd'hui. 


              La consolidation de notre coopérative ouvre la voie vers le déploiement et la maintenance des cabines téléphoniques sur le long terme.
            icon: null
        ctas: []
    id: objectifs
  - id: agir
    containers:
      - title: Comment agir ?
        blocks:
          - title: J'ai de l'épargne
            text: Je considère de placer un peu de mon épargne en parts sociales Commown.
              Placer 100€ permet de débloquer en moyenne 400 € de prêts
              bancaires pour financer des Fairphone et d'autres appareils
              responsables !
            ctas:
              - outline: false
                text: Par ici !
                href: "#contreparties"
          - title: "Je connais des gens "
            text: ... qui ont de l'épargne ! Je leur parle de Commown, je les invite à
              financer des Fairphone de la coopérative, au lieu des industries
              polluantes financées par le livret A.
            ctas:
              - outline: false
                text: Demander le kit de communication
                href: "#contact"
          - title: J'ai une carte SIM
            text: ... d'un géant de la téléphonie comme Orange, SFR ou Bouygues. Je
              considère passer chez TeleCoop. Selon les cas, cela peut vous
              revenir moins cher tout en soutenant l'électronique responsable !
            ctas:
              - outline: false
                text: Par là-bas
                href: "#contreparties"
        color: text-violet-100
    bg_color: bg-beige-25
  - id: compteurs
    containers:
      - blocks:
          - title: TeleCoop
            text: |-
              Vers le **premier palier : 9000 abonnés**.
              *mise à jour hebdomadaire
            data: "8600"
          - data: 22800 €
            text: |-
              Vers le **premier palier : 50 000 €**
              *mise à jour hebdomadaire
            title: Commown
            icon: null
        color: text-bleu-fonce-100
        title: Avancement
        icon: chart-pie
    bg_color: bg-bleu-clair-75
  - id: contreparties
    containers:
      - title: Contreparties
        blocks:
          - title: Un abonnement TeleCoop
            icon: device-mobile
            text: Pour une ouverture de ligne chez TeleCoop, vous recevez une **carte
              postale avec 4 cases de la BD** (indiquez le **code "Domi"** lors
              de votre commande).
          - title: 100  € de parts sociales Commown
            icon: currency-euro
            text: "*Pour 100  € de parts sociales* Commown, c'est-à-dire le minimum
              possible, vous recevez une **carte postale avec 4 cases de la
              BD**."
          - title: 500  € de parts sociales Commown
            icon: chart-pie
            text: "*À partir de 500 € de parts sociales* Commown, vous recevez une **carte
              postale avec 4 cases de la BD**, dédicacée par l'un des auteurs !"
          - title: 1000 € de parts sociales Commown
            icon: check-circle
            text: "*À partir de 1000 € de parts sociales* Commown, vous recevez un
              **exemplaire de la BD**, dédicacée par l'un des auteurs !"
        ctas:
          - outline: false
            text: Passer chez TeleCoop
            href: https://telecoop.fr/?mtm_campaign=commown&mtm_kwd=domi&mtm_source=site_web
          - outline: false
            text: Prendre des parts sociales Commown
            href: https://shop.commown.coop/shop/product/investissement-en-parts-sociales-4?aff_ref=30
    bg_color: bg-beige-25
    color: text-violet-100
  - id: BD
    containers:
      - title: ""
        blocks:
          - ctas: []
            text: >-
              Dans le cadre de cette campagne, Commown produit* une bande
              dessinée qui explore un imaginaire sobre et chaleureux. Alors que
              partout le culte du “toujours plus immédiat/immersif”, est imposé
              à grand coup de campagnes marketing, nous vous proposons avec
              cette BD un contre récit.


              Cette BD peut être considérée comme un outil d'influence communicationnelle. Cela pose des questions éthiques que nous présentons dans cet article de blog sur le storytelling, à [retrouver ici](https://commown.coop/blog/produits-%C3%A9thiques-et-%C3%A9cologiques-linfluence-du-storytelling/).


              Pour retrouver les épisodes de la BD, suivez Commown sur les réseaux sociaux, idéalement [sur Mastodon](https://mamot.fr/tags/DomiBD).\

              \

              *\*garantie sans IA bien sûr*
            title: Une BD pour rentrer dans la bataille des imaginaires
          - text: ""
            figure:
              image: /media/bd_compressed.png
---
