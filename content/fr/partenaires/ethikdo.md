---
title: Carte Éthi’Kdo · Cadeau éco-responsable · Commown
description: Vous pouvez convertir vos points éthi’Kdo en code cadeau Commown
  sur le site d’Ethi’Kdo. Ensuite vous pouvez utiliser ce code pour toutes nos
  offres ci-dessous
date: 2021-03-31T17:40:26.400Z
translationKey: ethikdo
layout: invest
header:
  title: Ethikdo
  subtitle: Dépenser ses points Ethi’Kdo chez Commown ? C'est possible !
sections:
  - bg_color: bg-beige-25
    containers:
      - figure:
          image: ""
          alt: logo ethikdo
        blocks:
          - title: éthi'Kdo chez Commown
            text: >-
              Notre coopérative est ravie de pouvoir accueillir les
              bénéficiaires de la carte éthi’Kdo !


              Pour en savoir plus sur ces cartes cadeau éco-responsable rendez-vous sur [le site de notre partenaire](https://www.ethikdo.co/).


              Vous pouvez convertir vos points éthi’Kdo en code cadeau Commown sur [cette page du site d’Ethi’Kdo](https://www.ethikdo.co/echoppe/commown/). Ensuite vous pouvez utiliser ce code pour toutes nos offres ci-dessous.


              Pour rappel, le code s’utilise dans le champ “COUPON” à droite au moment du récapitulatif du panier. Ce champ peut aussi se trouver en bas, selon la taille de votre écran.
          - title: Offrez vos valeurs !
            figure:
              image: /media/carte-ethikdo-compressée.jpg
  - id: nos-offres
    color: text-bleu-fonce-100
    bg_color: bg-bleu-clair-75
    containers:
      - title: Nos offres
        icon: ""
        cta:
          outline: true
          text: Découvrir tous nos produits
          href: https://shop.commown.coop/shop
        blocks:
          - title: Fairphone 3
            icon: mobile
            ctas:
              - outline: false
                text: En savoir plus
                href: /nos-offres/fairphone-3
          - title: Ordinateurs
            icon: desktop
            ctas:
              - outline: false
                text: En savoir plus
                href: /nos-offres/ordinateurs
          - title: Audio
            icon: audio
            ctas:
              - outline: false
                text: En savoir plus
                href: /nos-offres/audio
          - title: Consom’Action
            icon: euro
            ctas:
              - outline: false
                text: En savoir plus
                href: /nos-offres/consomaction
---
