---
layout: invest
title: L’offre TeleCommown
description: "Coopération Telecoop / Commown : avantages tarifaires en
  souscrivant aux deux offres"
date: 2023-12-27T08:58:04.616Z
translationKey: telecommown
image: /media/l-avenir-du-numérique_compressed.png
menu:
  main:
    name: Offre Telecommown
    parent: Particuliers
    weight: 5
header:
  title: L’offre TeleCommown
  subtitle: Premier pas vers une offre mobile 100% coopérative
  ctas:
    - outline: false
      href: https://commown.coop/partenaires/telecommown/#contact
      text: Formulaire de contact
sections:
  - id: campagne-cabines-tel
    containers:
      - title: Le retour des cabines téléphoniques en France ?
        blocks:
          - figure:
              image: /media/cabine-tel_compressed.png
          - title: Pour un autre récit technologique
            icon: null
            text: >-
              Mutualiser, upcycler, le plus lowtech possible.


              **TeleCoop** et **Commown** s’associent pour promouvoir le **retour des cabines téléphoniques** en France !


              De nouveaux usages peuvent être proposés, par exemple pour des cabines téléphoniques **devant les écoles primaires et collèges**. Pour permettre aux parents qui le souhaitent de repousser le moment du premier smartphone, ou simplement pallier une batterie déchargée.


              Nous avons **besoin de votre soutien !**
            ctas:
              - outline: false
                text: Découvrir la campagne
                href: https://telecoop.commown.coop/
    bg_color: bg-bleu-clair-75
  - bg_color: bg-beige-25
    containers:
      - figure:
          image: ""
          alt: ""
        blocks:
          - title: ""
            text: >-
              Depuis leurs origines, une très forte volonté de coopération entre
              nos deux coopératives est à l'œuvre. Regroupée au sein des
              [Licoornes](https://www.licoornes.coop/) et de
              [FairTEC](https://fairtec.io/fr/), nous passons nos semaines
              ensemble à phosphorer. Une offre combinée était une telle évidence
              que bon nombre de nos clients nous demandent de ses nouvelles
              régulièrement.


              Il ne restait donc plus qu’à la faire…


              Le but ultime serait de pouvoir un jour proposer un seul et même parcours client : une seule souscription pour obtenir à la fois le service de Commown et le forfait de TeleCoop.


              Seulement ceci représente des défis majeurs en terme de mutualisation de nos outils de facturation, de notre support client, partage de données personnelles #RGPD… Un sacré chantier qui même avec la meilleure volonté ne peut pas se faire en un jour.


              *C'est pourquoi, et sans attendre, nous avons lancé depuis fin 2021 une expérimentation à destination des clients particuliers de nos deux coopératives.*
          - title: ""
            figure:
              image: /media/v5_telecoopxcommown-compressé.jpg
        title: Vers la sobriété et au-delà !
  - bg_color: bg-bleu-clair-75
    containers:
      - title: "Premier jalon : réductions communes"
        blocks:
          - text: >-
              **L’objectif affiché :** 


              Que tous nos clients Commown passent chez TeleCoop, et qu’éventuellement, en fonction de leur besoin, les clients TeleCoop passent chez Commown. Bien évidemment, il est hors de question que, sous prétexte d’une “réduction”, vous vous sentiez incité.e à changer de smartphone trop tôt !


              *Attention cette offre n'est valable que pour les particuliers.*


              **Pourquoi cette opération :** 


              Parce que chacune de nos coopératives a besoin de grandir pour pérenniser et développer son activité et donc son impact. Grâce à cette opération conjointe, nous souhaitons mobiliser minimum 200 nouveaux abonnés par an. Atteindre cet objectif nous permettra de pérenniser cette proposition commune et de développer de nouveaux axes de collaboration entre nos coopératives et avec d’autres Licoornes.
            title: Pourquoi ?
          - text: >-
              **Parlons chiffres :** 


              Une personne cliente d’une coopérative, lorsqu’elle souscrira à la seconde, verra chaque mois ses **factures réduites de 1,5€ chez Commown et 1€ chez TeleCoop**. Dans le cadre de cette expérimentation et avant de pouvoir pérenniser cette opération, cette réduction sera **valable jusqu’au 31 décembre 2024**.


              **Comment faire pour en bénéficier :** 


              Rien de plus simple ! Au moment de votre souscription auprès de TeleCoop et/ou Commown, vous devez **entrer le code TeleCommown2021**. Si vous êtes effectivement client des 2 coopératives, la réduction s’appliquera sur votre prochain mois d’abonnement.


              ***Attention pour que nous puissions vérifier votre éligibilité il est important d'utiliser le même numéro de téléphone dans les deux parcours de souscription.***


              ***[Plus de détail sur notre FAQ dédiée. ](https://wiki.commown.coop/FAQ-offre-TeleCommown)***
            title: Comment ?
        ctas:
          - outline: false
            text: Souscrire chez Commown
            href: https://commown.coop/nos-offres/fairphone/
          - outline: false
            text: Souscrire chez TeleCoop
            href: https://telecoop.fr/
  - id: tuto
    bg_color: bg-beige-25
    containers:
      - blocks:
          - figure:
              image: /media/print-telecommown-parcours-commown-réduit.png
          - figure:
              image: /media/print-telecommown-parcours-telecoop-réduit.png
  - bg_color: bg-bleu-clair-75
    containers:
      - blocks:
          - title: Merci aux premiers soutiens
            text: C’est grâce à vous, qui avez déjà à la fois un abonnement TeleCoop et un
              abonnement Commown, que nous avons eu envie de lancer cette
              expérimentation. Avant toute chose, nous profitons donc de cette
              occasion pour vous remercier de la confiance que vous nous avez
              accordé depuis le début. **Vous êtes nos meilleurs ambassadeurs**.
          - title: Pour en profiter
            text: >-
              Jusqu’à la fin de l’année, l’objectif de cette expérimentation est
              bien de **trouver 200 nouveaux abonnés** qui pourraient bénéficier
              de notre offre commune.


              D’ici là nous avons besoin de votre ferveur. **Parlez-en autour de vous** pour nous aider à faire de cette expérimentation une franche réussite !


              N'oubliez pas que tout nouveau client qui se recommandera de votre part, en plus de pouvoir bénéficier des remises, **VOUS fera bénéficier de notre [offre de parrainage](https://forum.commown.coop/t/je-suis-deja-commowner-comment-faire-pour-parrainer-quelqu-un/2500)** ! :)
---
