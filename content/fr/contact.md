---
hCaptchaKey: 003087c8-4f6b-44fe-a1b7-640d55a62409
text: "**N﻿B : si vous souhaitez vous faire appeler n'oubliez pas de préciser
  votre n° de téléphone svp !**"
translationKey: contact
title: Contactez-nous
buttonDisplay: Afficher le formulaire
successText: >-
  **Votre message a bien été envoyé ✓**

  Nous faisons notre possible pour traiter votre demande aussi vite que nécessaire !
errorText: |-
  **Votre message n’a pas été envoyé ×**
  Veuillez vérifier que vous avez rempli les champs nécessaires.
fields:
  department:
    label: Motif de votre demande
    hint: Veuillez nous aider à aiguiller correctement votre demande.
  name:
    label: Votre nom
    hint: Aidez-nous à vous appeler par votre nom :-)
  email:
    label: Votre courriel
    hint: Veuillez saisir une adresse électronique valide.
  subject:
    label: Sujet de votre message
    hint: Merci de nous indiquer le sujet de votre message.
  body:
    label: Votre message
    hint: N'oubliez pas votre message !
  captcha:
    label: Captcha
    hint: Veuillez cocher la case du captcha ci-dessus.
actionURL: https://shop.commown.coop/contact
headless: true
buttonSend: Envoyer
options:
  - value: commercial
    label: Information sur la campagne TeleCoop-Commown
  - label: Questions sur nos offres / nos appareils
    value: commercial
  - label: Abonnement à la newsletter Commown
    value: newsletter
  - label: Postuler chez Commown
    value: recrutement
  - label: Communication / Presse / Événementiel
    value: media
  - label: Partenariat / Mécénat / Sponsoring
    value: partnership
  - label: Problème sur le site internet / Traduction
    value: webmaster
  - label: Autre
    value: other
---
