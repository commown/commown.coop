---
title: L’offre FrankenPhone
description: Smartphone monstrueusement durable en abonnement - Fairphone 2
  reconditionné muni de /e/OS, logiciel libre basé sur Android 11
date: 2021-03-31T17:40:26.400Z
translationKey: frankenphone
layout: invest
image: /media/FrankenPhone1.jpeg
header:
  title: L’offre FrankenPhone
  subtitle: Un smartphone monstrueusement durable à moins de 7€/mois
sections:
  - bg_color: bg-beige-25
    containers:
      - figure:
          image: ""
          alt: ""
        blocks:
          - title: ""
            text: >-
              **Ancienne offre 2022 :** Depuis des années, le mot sobriété
              apparaît sur toutes les lèvres, dans la bouche des ministres il
              sonne comme une contrainte conjoncturelle, alors qu’en fait la
              sobriété peut et doit être joyeuse et tendancielle !


              Concernant les smartphones, le seul levier de sobriété (en dehors de refuser d’en avoir un) reste de les faire durer le plus longtemps possible.


              Comme nous commencions à accumuler un certain nombre de Fairphone 2 au sein de notre coopérative : cassés pour les uns, renvoyés pour diagnostic pour les autres... Il était de notre devoir de leur donner une nouvelle vie !


              Pour ce faire, nous avons donc remis en état de fonctionnement des appareils que nous avons autrefois adaptés à la version Android 11 de la fondation /e/ OS.


              Nous avons ainsi pu, grâce à vos efforts collectifs, prolonger la durée de vie de nos FrankenPhone !


              **Il n'est aujourd'hui plus possible de commander un Frankenphone, mais un article de blog type narratif avait été rédigé sur le sujet.**
            ctas:
              - outline: false
                text: En savoir plus sur le FrankenPhone
                href: https://commown.coop/blog/la-naissance-dun-frankenphone/
          - title: ""
            figure:
              image: /media/FrankenPhone1.jpeg
        title: La sobriété passe par la durabilité !
  - bg_color: bg-bleu-clair-75
    containers:
      - blocks:
          - text: >-
              Pour 6,90 € TTC / mois, la coopérative vous proposait :


              * un FrankenPhone de compagnie. Comme tout appareil de Commown il est resté la co-propriété des sociétaires jusqu’à sa mort. Et jusque là il a fait de son mieux pour vous servir.

              * changement de la batterie si nécessaire,

              * changement du protège écran en cas de fissures,

              * changement de la coque en cas de casse, mais seulement si le niveau de casse ne permettait plus à la coque de maintenir la batterie. Les casses légères, notamment dans les coins, n'étaient pas prises en charge !

              * gestion des casses réparables sans surcoût,

              * gestion des pannes réparables sans surcoût,

              * assistance à l’usage de /e/OS,

              * protection contre le vol, avec franchise de 50 €.


              Les casses et pannes étaient considérées comme réparables si elles pouvaient être gérées par un changement de module.
            title: Les services inclus
          - title: Avant de se lancer ...
            text: >-
              Comme son nom l’indique, le FrankenPhone a déjà vécu, a pu être
              capricieux et perdre des bouts en chemin. Si la carte mère
              s’arrêtait, c’était la *fin de l’aventure*, dans ce cas pas de
              réparation possible.


              Un membre de l’équipe a vécu au quotidien pendant plusieurs mois avec un FrankenPhone pour tester. Le retour d’expérience et les conseils de prise en main se trouvent dans [cet article de notre wiki](https://wiki.commown.coop/Apprivoiser-le-FrankenPhone). On vous conseillait d’y jeter un oeil pour **vérifier que cette expérience vous convienne avant de commander**.\

              Aussi, si vous n’étiez pas habitués aux systèmes d’exploitation alternatifs, il était conseillé de prendre le temps de lire ***[notre page dédiée.](https://shop.commown.coop/shop/product/installer-un-environnement-sans-google-e-os-ou-revenir-sur-fairphone-os-239?page=3)*** Contrairement à nos autres offres, l’installation de /e/OS était obligatoire, il fallait donc commander l’article **“Frais de mise en service” à 36€ TTC en plus de votre commande de FrankenPhone**, sinon cette dernière n'était pas valide.


              **Cette offre datant de l'automne 2022, les commandes sont désormais fermées.**
        title: Le FrankenPhone à votre service
---
