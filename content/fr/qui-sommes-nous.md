---
layout: about
title: Qui sommes-nous ? · Commown l’électronique responsable
description: Commown est une coopérative engagée envers l'électronique
  responsable. Nous assurons que nos appareils tiennent le plus longtemps
  possible grâce à notre modèle économique.
date: 2021-03-30T14:27:17.743Z
translationKey: about
menu:
  main:
    name: À propos
    weight: 4
  footer:
    name: À propos
    weight: 6
header:
  title: Qui sommes-nous ?
  subtitle: Notre vision, nos valeurs, notre équipe
introduction:
  title: La vision Commown
  bg_color: bg-bleu-clair-75
  color: text-bleu-fonce-100
  text_left: >-
    Imaginez un avenir où nous ne serons plus captifs de nos objets
    électroniques. Logiciels et applications ne seront plus conçus pour piéger
    notre attention à des fins publicitaires. **Open source** et **libre**
    seront devenus la norme. 


    Nos **besoins** en matériel au même titre que nos besoins de mobilités seront **revus à la baisse** compte tenu du coût de l’énergie qui lui n’aura de cesse de croître. Ainsi fini les smartphones individualisés et multiples objets connectés en tout genre. 


    Le numérique sera revenu à sa juste place&#8239;: celle d’un outil permettant un **juste partage des ressources**, un accès horizontal à la connaissance et un fabuleux catalyseur de coopération.
  text_right: >-
    Des ordinateurs fixes **mutualisés** parfaitement **réparables** et
    **lowtech** auront “conquis le marché”. Cette flotte d’appareils constituant
    un **bien commun** géré par des **coopératives locales** regroupant
    citoyens, collectivités, associations et entreprises.


    Cette vision qui s’inscrit à l’extrême inverse de l’avenir que nous vendent actuellement les promoteurs de la 6G, pourrait être qualifiée d’idéaliste. Toutefois, elle constitue pour nous un cap en phase avec les enjeux écologiques et sociaux auxquels nous allons être confrontés.


    Voici les différents jalons qui participeront à l’émergence de cette vision&#8239;:
roadmap:
  title: Roadmap
  icon: roadmap
  events:
    - year: 01/2018
      title: Création de la coopérative
      text: Début de l’aventure Commown !
      icon: flag
    - year: 05/2018
      title: Actions de sensibilisation
      text: Début de notre travail de sensibilisation grand public
      icon: chat-alt-2
    - year: 09/2018
      title: Dé-GAFAM-isation
      text: Premier Fairphone dé-GAFAM-isé commercialisé
      icon: lock-open
    - year: 10/2018
      title: Plaidoyer d'intérêt collectif
      text: Premières actions  plaidoyer en soutient d'associations (HOP, Amis de la
        Terre)
      icon: speakerphone
    - year: 03/2019
      title: Recherche et développement
      text: Amorçage de notre travail de RetD
      icon: search
    - year: En cours
      title: Licoornes
      text: Rapprochement intercoopérative et mutualisation de service
      icon: puzzle
    - year: À venir
      title: Rassemblement
      text: "Accueil de nouveaux producteurs "
      icon: user-group
    - year: À venir
      title: EFC
      text: Démocratisation de l’économie de la fonctionnalité et de la coopération
      icon: thumb-up
    - year: À venir
      title: Lowtech
      text: Relocalisation et production lowtech
      icon: support
    - year: À venir
      title: BienS communS
      text: Diversification dans la préservation des communs
      icon: globe-alt
sections:
  - id: nos-valeurs
    color: text-bleu-fonce-100
    bg_color: bg-bleu-clair-75
    containers:
      - title: Nos valeurs
        color: text-bleu-fonce-100
        blocks:
          - title: Écologie
            text: "**Les enjeux écologiques** auxquels notre société est confrontée sont
              alarmants&#8239;: épuisement des ressources non renouvelables,
              effondrement de la biodiversité, dérèglement climatique. La
              surproduction de produits électroniques joue un rôle important
              dans ces bouleversements. Nous devons tout faire pour limiter au
              plus possible ces impacts négatifs."
          - title: Sobriété
            text: Les scientifiques sont unanimes, pour pouvoir respecter les limites
              planétaires nous devrons forcément **repenser notre monde sous le
              prisme de la sobriété**. Ainsi si vous n’avez pas de smartphone,
              ou que vous en éprouvez plus le besoin, l’idéal est de s’en passer
              &#8239;! Si votre appareil fonctionne encore, idem mieux vaut le
              pousser le plus loin possible avant éventuellement de souscrire à
              l’une de nos offres.
        ctas: []
      - blocks:
          - title: Éthique
            text: Nous sommes confrontés à l’explosion des inégalités sociales. Les
              conditions de travail pour la production d’électronique sont
              déplorables. Enfin les produits électroniques sont devenus les
              instruments d’une société de contrôle et de commercialisation des
              données personnelles. **Les logiciels libres** sont au cœur de ce
              combat, à la fois **pour protéger la vie privée** comme **pour
              faire durer les appareils**.
          - title: Coopération
            text: L’entraide et la coopération sont pour nous les seuls moyens d’envisager
              l’avenir avec confiance. C’est pourquoi nous avons choisi le
              **statut de Société Coopérative d’Intérêt Collectif (SCIC**). Ce
              statut permet aux différents acteurs (clients, producteurs,
              salariés) de devenir sociétaires. Ici **1 personne = 1 voix**, le
              pouvoir n’est donc pas lié au capital et la valorisation de
              celui-ci est très réduite. L’esprit coopératif encourage aussi le
              rapprochement avec d’autres SCIC.
          - text: Aujourd’hui le greenwashing est courant. Comment faire la différence ? En
              regardant les détails et en exigeant toujours plus de
              **transparence**. Dans une SCIC, l’accès aux assemblées générales
              ***[est très abordable](/epargne-solidaire/)***. Cela garantit un
              accès au plus grand nombre aux comptes-rendus détaillés de ses
              activités.
            title: Transparence
        ctas:
          - outline: true
            text: Nos statuts
            href: /media/commown-statuts-nov23.pdf
          - outline: false
            text: Devenir sociétaire
            href: https://commown.coop/epargne-solidaire/
        color: text-bleu-fonce-100
  - id: coherences-incoherences
    bg_color: bg-beige-25
    cols: true
    containers:
      - blocks:
          - title: Aujourd’hui, le greenwashing est courant
            text: >-
              Comment faire la différence&#8239;? En regardant les détails et en
              exigeant toujours plus de transparence. Commown c’est
              aujourd’hui&#8239;:


              * Un compte bancaire Crédit Coopératif

              * Un financement La Nef

              * Des serveurs chez iKoula

              * Des livraisons avec 100% compensation carbone

              * Un abonnement CITIZ pour les quelques déplacement en voiture

              * Une mise en avant des logiciels libres sur smartphone et PC

              * Optimisation d’un ERP libre pour l’économie de la fonctionnalité

              * Des contributions au code de certains projets libres

              * Un travail de plaidoyer auprès des institutions pour promouvoir l’économie d’usage et l’électronique responsable

              * Un travail de sensibilisation grand public pour démocratiser sur ces questions

              * Nous cherchons à prioriser le train devant l’avion

              * Nous utilisons [Matomo](https://fr.matomo.org/) au lieu de Google Analytics

              * Les livraisons sur Strasbourg sont faites à vélo avec [Kooglof](https://kooglof.coopcycle.org)

              * Notre site vitrine est éco-conçu

              * Nous disposons d'un Conseil d'Administration paritaire

              * L'utilisation du réseau social open source et décentralisé [Mastodon](https://mamot.fr/@commownfr) au lieu de Twitter

              * Et bien sûr notre statut de SCIC&#8239;!


              **… Gage de confiance&#8239;!**
        color: text-bleu-fonce-100
        title: Cohérences
      - title: Incohérences
        blocks:
          - title: Ci-dessous nos sources actuelles de dissonances
            text: >-
              En les inscrivant noir sur blanc, nous nous engageons à les
              réduire à la première occasion.


              * Nous avons eu un compte Amazon (utilisé seulement à deux reprises en deux ans et supprimé depuis)

              * Nous avons dû commander un lot de produits sur AliExpress

              * Nous avons un compte [Facebook](https://www.facebook.com/commownfr) (et nous n’utilisons pas encore assez notre compte [Communecter](https://www.communecter.org/#@commown))

              * Nous utilisons encore les outils de pub Facebook 

              * Nous avons réalisé une campagne de pub Google ADS

              * Nous avons réalisé plusieurs campagnes de Bing ADS

              * Nous avons un compte YouTube et pas encore de compte Peertube

              * Nous avons pris à ce jour 16 vols continentaux en 3 ans, c’est 16 vols de trop

              * Nos locaux sont hébergés dans une pépinière d'entreprises donc pour l'instant nous ne sommes pas en mesure de choisir notre fournisseur d'électricité 

              * Nous sommes pour l'instant dépendants de l'intermédiaire bancaire de paiement Slimpay, acteur peu éthique

              * La direction opérationnelle est entièrement masculine.


              **… Gage de transparence&#8239;!**
  - id: Photo
    containers:
      - figure:
          image: /media/dither_it_photo-retaillée-test-1.jpg
          figcaption: Team Commown 2020-2021
  - id: equipe
    color: text-bleu-fonce-100
    bg_color: bg-bleu-clair-75
    containers:
      - title: L’équipe
        font_size: smaller
        blocks:
          - title: Adrien
            text: Ancien chimiste, addict à l’huile d’olive, militant écologiste engagé à
              tendance activiste.
          - title: Élie
            text: Écologie dans les racines et physique théorique dans la tête, pour un
              optimisme zen à toute épreuve.
          - title: Florent
            text: Praticien de longue date du logiciel libre, ne conçoit le travail que pour
              construire des Communs en mode collaboratif. Converti à l’écologie
              quand il a compris que la planète était le Commun ultime 🙂
          - title: Fred
            text: Tombé dans la marmite entrepreneuriale depuis 15 ans, alsacien sans
              frontière, par définition rigo-extrémiste, mais qui se soigne en
              éco-thérapie de groupe.
          - title: Luc
            text: Fraîchement arrivé dans l’équipe IT de Commown pour utiliser à bon escient
              ma force de travail. Je vie pour les levers de soleils, la musique
              avec ou sans paroles et les espaces exempts de béton.
          - title: Léa
            text: Écologiste en herbe, ravie de travailler dans ce domaine et d’œuvrer pour
              le bien de notre planète. Aime par-dessus tout sillonner les
              routes de campagne en rollers, le casque sur les oreilles&#8239;!
          - title: Benoît
            text: Passionné de photographie dans la nature, de menuiserie, d'informatique et
              d'électronique. J’aime lire de la fiction, de la fantaisie, de
              l’Histoire, des BD et l'imagerie ancienne de la publicité. J'aime
              aussi randonner en vélo dans la nature.
          - text: Animatrice de la fresque du numérique, c’est tout naturellement que
              Commown m’a attiré avec son projet d’électronique durable et
              mutualisé. J’aime la technique, pourvu qu’elle serve le bien
              commun.
            title: Priscille
          - text: >-
              <!--StartFragment-->


              Amoureux d’outdoor et de cyclisme longue distance, je suis convaincu que l’avenir sera davantage low que high-tech. J’ai rejoint Commown pour le besoin de sobriété mis en avant par le modèle !


              <!--EndFragment-->
            title: Jérémy
          - text: Étudiant en Informatique à la CCI Campus, j’accompagne l’équipe pour mon
              apprentissage. J’ai pour passion les jeux-vidéos, les animés et
              les animaux. Je profite de l’ombre pour m’épanouir et apprendre.
            title: Erwan
          - text: Grand fan de la culture japonaise, sportif à mes heures perdues. Je suis
              très enthousiaste d’aider au développement de la coopérative et de
              contribuer à un monde plus responsable !
            title: Simon
          - title: Logan
            text: Jeune étudiant Strasbourgeois victime d’un rythme de vie dicté par la
              musique et les mangas. Ayant un intérêt particulier pour
              l’informatique et soucieux des enjeux environnementaux, le Colibri
              qui est en moi est fier de participer à l’aventure Commown.
          - title: Yaël
            text: Geek, batteur, rêveur... Je suis fier de pouvoir contribuer à ce projet
              qui correspond à mes idéaux !
          - text: >-
              <!--StartFragment-->


              Programmeuse et experte support en alternance passionnée par les séries, les jeux vidéo, l'illustration graphique et l’informatique c:


              <!--EndFragment-->
            title: Léna
          - text: >-
              <!--StartFragment-->


              Peut-on profiter des avantages du numérique sans (trop) subir ses contraintes ? Je suis venu chez Commown pour trouver des réponses à cette question. À part ça, vous me trouverez sûrement sur un vélo ou à la médiathèque.


              <!--EndFragment-->
            title: Lucien
          - text: Toujours à la recherche de nouvelles sensations et avide d’apprendre, en
              croisant le chemin de Commown, mon enthousiasme s’est emballé. Me
              consacrer à plus de justice devient un besoin vital qui est ici
              comblé.
            title: Delphine
          - title: Philippe
            text: Passionné de smartphones et d'ordinateurs, et après avoir travaillé chez
              Fairphone, je trouve en Commown une coopérative engagée et pleine
              de sens. Très curieux, je passe mon temps libre à bricoler et
              apprendre des langues étrangères.
          - text: Nouvellement installée à Strasbourg, je suis ravie d'intégrer l’équipe
              Commown et de pouvoir m’engager dans un projet environnemental,
              une cause qui me tient à cœur !
            title: Cléa
          - title: Himen
            text: >-
              <!--StartFragment-->


              En découvrant Commown, j’ai eu l’intuition que c’était LA bonne organisation à intégrer. Du concret, du sens et surtout de la cohérence ! Un mélange en accord avec moi-même, à ne pas rater. Me voilà donc au sein de l’équipe commerciale pour 1 an et qui sait, peut-être plus...


              <!--EndFragment-->
          - text: Communicante engagée un brin idéaliste, je suis ravie d’accompagner
              Commown pour contribuer à la sobriété numérique. Et, si vous me
              posez la question, je vous dirais que pour me détendre, je cuisine
              sur une musique de Queen ou d’Edith Piaf.
            title: Catherine
          - title: Ikram
            text: >-
              <!--StartFragment-->


              Jeune passionnée par le marketing digital, j’intègre Commown en tant qu’alternante au sein de l’équipe communication. Une aventure humaine qui s’annonce riche autant sur le plan professionnel que personnel ;)


              <!--EndFragment-->
          - text: >-
              <!--StartFragment-->


              Nerd artiste passionnée par le design graphique, les mangas et la musique. Je suis enthousiaste à l’idée de travailler en tant qu’alternante en direction artistique pour Commown, dont l’engagement et les valeurs me correspondent.


              <!--EndFragment-->
            title: Julia
          - title: Achot
            text: >-
              <!--StartFragment-->


              Passioné de boxe et du monde de la comptabilité, je serais dans l’équipe Commown pour 2 ans en alternance dans la team Gestion et Comptabilité. Jeune d’aujourd’hui apportant une aide pour l’environnement est un moyen pour les futurs jeunes de mieux se sentir.


              <!--EndFragment-->
          - text: >-
              <!--StartFragment-->


              Apprenti ingénieur génie électrique en dernière année, je suis passionné par l’électronique et volontaire pour orienter les innovations d’aujourd’hui vers un esprit d’éco-conception !


              <!--EndFragment-->
            title: Louis
          - text: >-
              <!--StartFragment-->


              Étudiant en Systèmes Numériques, passionné de sport, d’informatique et d’électronique, j’accompagne l’équipe Commown pour mon apprentissage et contribue avec envie et plaisir au projet environnemental de la coopérative


              <!--EndFragment-->
            title: Amine
          - text: >-
              <!--StartFragment-->


              Lycéen en seconde bac pro C.I.E.L en alternance, aime l’informatique, l’électronique, les jeux vidéo et les voitures.


              <!--EndFragment-->
            title: Jasmin
          - text: >-
              <!--StartFragment-->


              Étudiant en Mathématiques, passionné de musique, de jeux vidéos et d’escalade. J’intègre l’équipe support pour 1 an afin de continuer à résoudre des problèmes et chercher des solutions dans un environnement riche de belles personnes.


              <!--EndFragment-->
            title: Tom
  - id: merci
    color: text-bleu-clair-25
    bg_color: bg-bleu-fonce-100
    containers:
      - title: ""
        font_size: smaller
        blocks:
          - text: |-
              **Un grand merci \
              aux bénévoles et indépendants \
              qui ont rendu Commown possible&#8239;!**
          - text: >-
              Chloé, Ariane, Alexandre, Fabien, Sébastien, Faustine, Mickael,
              Caroline, Mathieu, Sylvain, Guillaume, Iris, Yohan, Clément,
              Jessica, Bernat, Tony, Louise, Jean Philippe, Gauthier, Timothée,
              Derek… \

              et un jour, vous peut-être&#8239;? &#128521;
  - id: ecoconception
    color: text-bleu-fonce-100
    bg_color: bg-white
    containers:
      - title: L’éco-conception numérique du site
        title_max_w: 22ch
        icon: mobile
        blocks:
          - title: Faire durer
            text: >-
              **La fabrication des équipements est le plus gros facteur d’impact
              environnemental d’un service numérique**, il est alors essentiel
              que le site ne favorise pas le renouvellement des équipements mais
              bien l’allongement de leur durée de vie. 

              Ainsi, le site fonctionne sans ralentissement aussi bien sur un appareil ancien que sur du neuf et avec une bande passante faible.
          - title: Faire mieux
            text: '**Nous avons remis à plat nos besoins et usages afin d’éco-concevoir
              notre nouveau site.** Cela nous a aussi permis de répondre aux
              objectifs d’accessibilité web, de performance et de sécurité que
              nous nous étions fixés. Par rapport à notre ancien site nous avons
              divisé le poids moyen d’une page par 23 (159&#8239;Ko en moyenne),
              le nombre de requêtes par 4 (16 en moyenne), les temps de
              chargement par 5 (1,09 secondes pour le <abbr title="First
              Contentful Paint" lang="en">FCP</abbr>) et aujourd’hui les pages
              du site se chargent en moyenne en 1,66 secondes via une connexion
              3G bas débit (780&#8239;Kbps).'
  - id: recompenses
    color: text-bleu-fonce-100
    bg_color: bg-bleu-clair-75
    containers:
      - title: Nos récompenses
      - blocks:
          - title: Prix Finansol 2019 Technologies et solidarités
        figure:
          iframe:
            code: <iframe
              src="https://www.youtube-nocookie.com/embed/UoDK2ZIZ9YM?autoplay=1&color=white"
              title="YouTube video player" frameborder="0" allow="accelerometer;
              autoplay; clipboard-write; encrypted-media; gyroscope;
              picture-in-picture" allowfullscreen></iframe>
          alt: Prix Finansol 2019 Technologies et solidarités
          image: /media/prix-finansol-2019.webp
      - title: ""
        blocks:
          - title: Gagnant du Programme Européen C-Voucher
          - title: Gagnant du prix Impact Startup Grand-Est
          - title: Lauréat national Prix Inspiration ESS 2019
          - title: Lauréat national Grand Prix de la Finance Solidaire 2019
        ctas:
          - outline: false
            text: Nos offres
            href: /#nos-offres
          - outline: false
            text: Offres professionnelles
            href: /pour-les-entreprises/
          - outline: false
            text: Devenir sociétaire
            href: /epargne-solidaire/
---
