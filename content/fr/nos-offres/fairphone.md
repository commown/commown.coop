---
title: "Fairphone · un smartphone responsable et réparable · Commown "
description: Commown propose en location avec service le smartphone le plus
  réparable et le plus éthique du marché.
date: 2021-03-19T12:25:36+01:00
translationKey: fairphone-3
type: product
menu:
  main:
    parent: Particuliers
    name: Fairphone
    weight: 1
  footer:
    name: Fairphone 3
    weight: 2
header:
  title: "Fairphone "
  subtitle: les smartphones écoconçus et éthiques
  ctas:
    - outline: false
      text: Louez un Fairphone
      href: https://shop.commown.coop/shop/category/smartphones-fairphone-3
    - outline: true
      text: En ai-je besoin ?
      href: "#consomaction"
    - outline: false
      text: Pour les pros
      href: /pour-les-entreprises/
  text: À partir de 19,90 € / mois
  figure:
    image: /media/fairphone-header-new.jpg
    alt: Fairphone 3
sections:
  - color: text-bleu-fonce-100
    containers:
      - blocks:
          - title: Matériaux recyclés
            text: "Fairphone augmente constamment l'utilisation de matériaux recyclés dans
              ses appareils : cuivre et plastique recyclés pour le Fairphone 3 ;
              plastique, indium, cuivre, aluminium pour le Fairphone 4 ; 70% de
              matériaux recyclés au total dans le Fairphone 5. Par ailleurs la
              modularité de leurs téléphones permet aussi d’améliorer leur
              recyclabilité en fin de vie puisque certains modules concentrent
              certains métaux, tels que l’or par exemple."
          - title: Éthique
            text: Fairphone est une entreprise sociale qui mise avant tout sur l’humain. Les
              employés des usines ont droit à plus de sécurité, un salaire juste
              et Fairphone réalise des audits régulièrement dans les usines. Par
              ailleurs Fairphone fait un travail exemplaire de traçabilité de
              ses matériaux, après l'or voici maintenant l'argent constitutif de
              leur appareil qui est labellisé FairTrade dans le Fairphone 4.
          - title: Modulaire
            text: L'extrême modularité des Fairphone est sans doute ce qui les rend si
              uniques. Composés de modules différents tous démontables très
              simplement les Fairphone 3, 4 et 5 illustrent parfaitement cette
              éco-conception. Avec notre assistance, nous vous enverrons en cas
              de besoin un module par la poste, pour que vous puissiez le
              changer vous-même. Cette modularité a pour effet de prolonger
              grandement la durée de vie des appareils de notre coopérative et
              ainsi de réduire notre empreinte carbone.
      - ctas:
          - outline: false
            text: Caractéristiques techniques Fairphone 4
            href: https://shop.commown.coop/shop/product/fairphone-4-avec-services-essentiel-et-duo-protecteur-357#carac-techniques
          - outline: false
            text: Caractéristiques techniques Fairphone 5
            href: https://shop.commown.coop/shop/product/fairphone-5-avec-services-essentiel-et-duo-protecteur-693#carac-techniques
          - outline: true
            text: Mon téléphone fonctionne toujours !
            href: "#consomaction"
      - figure:
          image: /media/visuelvideo.jpg
          alt: Fairphone 3 vidéo
          iframe:
            code: <iframe title="Vidéo de présentation de Commown"
              src="https://indymotion.fr/videos/embed/08278b6f-cdef-40d1-96a3-2d3e756c8548?autoplay=1"
              frameborder="0" allowfullscreen allow="accelerometer; autoplay;
              clipboard-write; encrypted-media; gyroscope;
              picture-in-picture"></iframe>
            lang: html
    id: features
  - id: service
    bg_color: bg-bleu-clair-75
    color: text-bleu-fonce-100
    containers:
      - title: Le service Commown c’est…
        title_max_w: 20ch
        icon: mobile
        blocks:
          - title: l’Offre Commown
            text: >-
              **À partir de 19,90€ / mois pendant 12 mois, vous bénéficiez de
              l’usage d’un Fairphone 4 et de multiples services associés, conçus
              pour faire durer l’appareil** (prise en charge des casses et
              pannes, assistance à l’usage…). 


              La vision de long terme de la coopérative permet de proposer **le meilleur tarif sur 5 ans**, à service équivalent. Si vous trouvez moins cher ailleurs, Commown s’aligne 10% en dessous !\

              \

              Et pour une **offre 100% coopérative**, découvrez également [notre partenariat avec TeleCoop](https://commown.coop/partenaires/telecommown/), premier opérateur coopératif engagé vers la sobriété.
          - figure:
              image: /media/fairphone-3-group-selfie.jpg
              alt: ""
      - blocks:
          - title: Remplacement rapide
            title_max_w: 10ch
            icon: refresh
            text: Ne restez plus de longues semaines sans smartphone. En cas de défaillance,
              Commown vous en envoie un autre sous 48h.
          - title: Réparations
            title_max_w: 10ch
            icon: support
            text: Votre smartphone est couvert pour toute la durée du contrat, même après la
              fin de la garantie constructeur.
          - title: Protection contre la casse
            title_max_w: 14ch
            icon: shield-check
            text: Vous avez cassé votre précieux Fairphone ? Vous êtes protégé ! Votre
              smartphone sera réparé ou remplacé.
          - title: Changement de batterie
            title_max_w: 10ch
            text: Nous avons conscience que la batterie est l’un des points faibles des
              smartphones en général. Une fois par an et si besoin, vous pouvez
              recevoir une batterie neuve.
            icon: battery-low
          - title: Le prix juste
            title_max_w: 10ch
            icon: scale
            text: Commown est une coopérative d’intérêt collectif ! Nous sommes les seuls à
              baisser fortement les prix dans la durée pour ce genre de service.
          - title: Service solide
            title_max_w: 10ch
            icon: thumb-up
            text: Notre service client s’ajoute à celui de Fairphone, ce qui permettra une
              plus grande disponibilité pour prendre soin de vous. Avec bien sûr
              une garantie d’un interlocuteur francophone.
        ctas:
          - outline: false
            text: Louer un Fairphone
            href: https://shop.commown.coop/shop/category/smartphones-fairphone-3
          - outline: true
            href: "#consomaction"
            text: Mon téléphone fonctionne toujours !
          - outline: false
            text: Pour les pros
            href: /pour-les-entreprises/
  - id: faq
    containers:
      - title: Questions fréquentes
        title_max_w: 14ch
        icon: chat-alt
        blocks:
          - title: Si je commande un Fairphone maintenant quand sera-t-il livré ?
            details: true
            text: >-
              Fairphone 4 et Fairphone 5 : Si vous commandez aujourd’hui, le
              Fairphone sera expédié sous 5 jours ouvrés (sous réserve que
              l’ensemble des formalités soient finalisées).\

              \

              Fairphone 3 et 3+ : Nous sommes en rupture de stock.
          - title: Que se passe-t-il après la période d’engagement ?
            details: true
            text: Le contrat continue après la période d’engagement avec une baisse de
              tarif, et vous pouvez arrêter quand bon vous semble. Cet avantage
              tarifaire pour les clients fidèles est décrit dans la fiche
              produit de l’offre. On vous précise que chez Commown, nos
              appareils sont loués exclusivement et aucun rachat ne sera
              possible ! Une fois votre abonnement achevé, votre appareil nous
              sera restitué pour pouvoir le louer à d'autres.
          - title: Proposez-vous des OS (systèmes d’exploitation) alternatifs ?
            details: true
            text: >-
              Oui, car nous sommes convaincus de l’importance du logiciel libre
              dans la lutte contre l’obsolescence programmée.

              Pour le Fairphone 2, nous avions proposé dès 2018 le système “Fairphone Open OS”, et nous proposons désormais /e/OS. Pour les Fairphone 4 et 5 **nous proposons /e/OS.**
            ctas:
              - outline: true
                text: En savoir plus
                href: https://wiki.commown.coop/Syst%C3%A8me-d%27exploitation-Android-ou-libre
        ctas:
          - outline: false
            text: Consultez notre FAQ
            href: /faq
    color: text-bleu-fonce-100
  - id: entreprise-collectivite-association
    bg_color: bg-bleu-clair-75
    color: text-bleu-fonce-100
    containers:
      - title: Entreprise, collectivité, association ?
        blocks:
          - title: Location avec service
            text: >-
              Commown propose pour les professionnels des formules **de location
              avec services de smartphones** (Fairphone, Crosscall), **PC fixes
              et portables, tablettes et casques audio pour le télétravail**.


              L’offre Commown pour les professionnels permet de faire valoir un **haut niveau de Responsabilité Sociétale et Environnementale**. Cela inclut les labels de produits, comme le Fairphone (B-Corp), une SCIC reconnue entreprise de l’ESS, labellisée Efficient Solution Solar Impulse, et recommandée par des experts de l’économie circulaire comme Circul-R, La Famille Zéro Déchet, etc.
          - figure:
              image: /media/sif_label_logo_institutional_2020_rvb.png
              alt: Label efficient solution solar impulse
              blend_mode: multiply
              grayscale: true
              initial: true
        ctas:
          - outline: false
            text: Nos offres pro
            href: /pour-les-entreprises/
          - outline: true
            text: Contactez-nous
            href: "#contact"
  - id: consomaction
    color: text-bleu-fonce-100
    containers:
      - title: Pas besoin d’électronique ? Soutenez la coop
        blocks:
          - text: >-
              Achetez un Bon Consom’Action Différée, **soutenez notre
              coopérative dès aujourd’hui** et économisez jusqu’à 40 € sur vos
              mensualités le jour où vous aurez BESOIN d’un nouvel appareil
              électronique. 


              En plus, **vous pouvez offrir ce Bon** pour partager vos valeurs.
          - figure:
              image: /media/bon-de-consomaction-non-modifiable-.svg
              alt: Découvrez le bon Consom’Action Différée !
              blend_mode: multiply
              initial: true
        ctas:
          - outline: false
            text: En savoir plus
            href: https://shop.commown.coop/shop/product/bon-de-consomaction-differee-186
---
