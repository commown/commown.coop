---
date: 2021-03-19T12:25:36+01:00
translationKey: consomaction
header:
  title: Votre appareil actuel fonctionne toujours ?
  subtitle: Ne souscrivez pas tout de suite à nos offres !
  ctas:
    - outline: false
      text: Bon de Consom'Action
      href: https://shop.commown.coop/shop/product/bon-de-consomaction-differee-186
  text: ""
  figure:
    image: /media/bon-de-consomaction-non-modifiable-.svg
    alt: Fairphone 3
sections:
  - color: text-bleu-fonce-100
    containers:
      - blocks:
          - title: Lutte contre l'obsolescence marketing
            text: "L'obsolescence programmée a de nombreux visages, elle se cache souvent
              derrière des campagnes marketing pour inciter aux renouvellements
              des appareils. Pas de ça chez Commown : nous vous invitons à
              conserver votre appareil actuel le plus longtemps possible. C’est
              pourquoi nous proposons ce bon “Différée” il sera activable
              uniquement dans 2 mois !"
          - title: Soutenez la coopérative
            text: Cette manifestation d’intérêt représente pour notre coopérative un soutien
              considérable qui nous donnera du poids face aux organismes
              financiers et nous permettra d’anticiper au mieux la demande. Dès
              10 € votre soutien compte !
          - title: Gain économique / idée cadeau
            text: Si dès aujourd’hui vous êtes certains de souscrire à notre offre le jour
              où votre appareil sera définitivement hors d’usage en achetant un
              bon vous pourrez économiser jusqu'à 40 € sur une commande future.
              Aussi ce bon n’a pas de durée de péremption et n’est pas nominatif
              donc vous pouvez l’offrir également.
      - ctas:
          - outline: true
            text: Plus d'information sur le bon
            href: https://shop.commown.coop/shop/product/bon-de-consomaction-differee-186
"layout:": single
title: Consom'Action
description: Avec la Consom'action je soutiens une coopérative engagée dans la
  promotion de l'électronique responsable et éthique, contre l'obsolescence
  programmée et anti GAFAM
type: product
menu:
  main:
    parent: Particuliers
    name: Consom’Action
    weight: 4
  footer:
    name: Consom’Action
    weight: 5
---
