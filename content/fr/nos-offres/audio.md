---
slug: casque-audio-responsable
title: Location casque audio responsable et réparable · Commown
description: Découvrez les casques audios éthiques et modulaires Gerrard Street,
  en location avec services de réparation au meilleur prix chez Commown !
date: 2021-03-19T12:25:36+01:00
translationKey: audio
type: product
menu:
  main:
    parent: Particuliers
    name: Audio responsable
    weight: 3
  footer:
    name: Audio responsable
    weight: 3
header:
  title: Casque audio réparable
  subtitle: Du son plus responsable c’est possible !
  ctas:
    - outline: false
      text: Commander un Casque Gerrard Street
      href: https://shop.commown.coop/shop/category/audio-12
    - outline: false
      text: Commander un casque Fairbuds XL
      href: https://shop.commown.coop/shop/product/casque-fairbuds-xl-675
  text: À partir de 7,50 € / mois, pour le casque filaire Gerrard Street
  figure:
    image: /media/gerrard-street-boss-top.jpeg
    alt: Casque Gerrard Street
sections:
  - color: text-bleu-fonce-100
    containers:
      - blocks:
          - title: Ultra réparable
            text: >-
              Les marques Gerrard Street et Fairphone ont conçu des casques
              **entièrement modulaires**, dont chaque pièce peut être démontée
              facilement ! Ils sont ainsi très faciles à réparer. Ne jetez plus
              votre casque lorsqu’il est cassé : remplacez le module défaillant
              par un neuf. 


              Dans le cas du Fairbuds XL, des **matériaux recyclés** sont utilisés : aluminium, étain, et plastique.
          - title: Un son de qualité
            text: Pour les passionnés de son et les amoureux de musique, profitez d’un son
              cristallin et d’une basse puissante. Design sonore exquis, parfait
              pour la musique acoustique et électronique. En bluetooth ou
              filaire, découvrez les premiers casques modulaires du marché. Les
              tests indépendants de l’équipe de 01net ont donné au casque
              Gerrard Street une note de 8/10 (n°933 p.61).
          - title: Parfait pour le télétravail
            text: Sélectionné par Commown, **pour le casque Gerrard Street** le **micro
              BoomPro de V-Moda** produit un son clair et agréable n’importe où
              grâce à la réduction du bruit ambiant et la clarification de la
              voix. Sa qualité en fait même un accessoire de gaming ! Sa perche
              flexible et son contrôleur clipable doté d’un bouton Mute et d’une
              molette de volume en fait un équipement idéal pour le télétravail.
      - ctas:
          - outline: false
            text: Caractéristiques techniques Casque BOSS
            href: https://shop.commown.coop/shop/product/boss-casque-gerrard-street-bluetooth-location-mensuelle-230#carac-techniques
          - outline: false
            text: Caractéristiques techniques casque DAY
            href: https://shop.commown.coop/shop/product/day-casque-gerrard-street-filaire-location-mensuelle-237#carac-techniques
          - outline: true
            text: Mon casque fonctionne encore !
            href: "#consomaction"
          - outline: false
            text: Caractéristiques techniques Casque Fairbuds XL
            href: https://shop.commown.coop/shop/product/casque-fairbuds-xl-675#carac-techniques
    id: features
  - id: service
    bg_color: bg-bleu-clair-75
    color: text-bleu-fonce-100
    containers:
      - title: Le service Commown c’est…
        title_max_w: 20ch
        icon: audio
        blocks:
          - title: l’Offre Commown
            text: >-
              **A partir de 7,5 € / mois**, vous bénéficiez de l’usage d’un
              casque Gerrard Street avec la protection contre tous les incidents
              qui pourraient survenir : **une panne ou une casse ? La pièce de
              rechange arrive rapidement et sans surcoût !** 


              Vous payez uniquement votre usage. Vous voulez résilier ? Aucun problème, vous êtes libre. **Commown garantit le reconditionnement et la remise en service du casque pour éviter les déchets électroniques**.
          - figure:
              image: /media/illu_gerrard_street_gs_parts.jpeg
              alt: ""
      - blocks:
          - title: Remplacement rapide
            title_max_w: 10ch
            icon: refresh
            text: Ne restez pas de longues journées sans pouvoir écouter votre musique. En
              cas de défaillance, Commown vous envoie la pièce défectueuse ou un
              autre casque sous 48h.
          - title: Réparations
            title_max_w: 10ch
            icon: support
            text: Votre casque est couvert pour toute la durée du contrat, même après la fin
              de la garantie constructeur.
          - title: Protection contre la casse
            title_max_w: 14ch
            icon: shield-check
            text: Le casque a subi un choc ou une autre détérioration&#8239;? Vous êtes
              protégé&#8239;! Il sera réparé ou remplacé sans surcoût.
          - title: Une vision de long terme
            title_max_w: 14ch
            text: Pour soutenir la vision de long terme du producteur, et l’ensemble des
              actions servant à faire durer les casques, Commown reverse une
              partie de la location à Gerrard Street.
            icon: chart-pie
          - title: Liberté
            title_max_w: 14ch
            icon: lock-open
            text: Vous êtes entièrement libre. À tout moment vous pouvez passer au modèle
              supérieur ou résilier votre abonnement. C’est simple et rapide,
              via votre espace client.
          - title: Fidélité récompensée
            title_max_w: 10ch
            icon: thumb-up
            text: Si vous vous engagez à nos côtés en prépayant un an d’abonnement, vous
              gagnez 2 mois gratuits !
        ctas:
          - outline: false
            text: Louer un casque modulaire
            href: https://shop.commown.coop/shop/category/audio-12
  - id: faq
    containers:
      - title: Questions fréquentes
        title_max_w: 14ch
        icon: chat-alt
        blocks:
          - title: Est-ce que mon abonnement est flexible ?
            details: true
            text: >-
              L’offre Gerrard Street de Commown est “flexible”. Un abonnement
              mensuel peut être annulé tous les mois&#8239;; un abonnement
              annuel peut être annulé tous les ans. C’est aussi simple que
              ça&#8239;!


              L'offre Fairbuds XL inclut un an d'engagement.
          - title: Le casque m’appartiendra-t-il un jour ?
            details: true
            text: Avec Commown, vous payez uniquement pour l’usage&#8239;; il est impossible
              d’acheter le casque. Et si c’était le cas, le coût de souscription
              aurait dû être bien plus élevé. Toute la beauté de notre concept
              réside là&#8239;! Vous pouvez comparer cela à Spotify, qui vous
              permet d’écouter à votre guise sans pour autant posséder la
              musique. Nous faisons cela pour garder la propriété de nos
              produits et rester ainsi responsable de leur réemploi et de leur
              recyclage.
          - title: Quand recevrai-je mon casque ?
            details: true
            text: Le délai moyen de livraison est de 3-4 jours ouvrés, sous réserve que vous
              ayez mis en ligne vos justificatifs d’identité.
            ctas:
              - outline: true
                text: En savoir plus
                href: https://forum.commown.coop/t/quand-est-ce-que-ma-commande-sera-validee-et-l-appareil-expedie/2505
          - title: Est-il possible de changer de modèle ?
            details: true
            text: Vous pouvez changer d’abonnement via votre espace client. Une fois le
              changement opéré, nous vous enverrons le nouveau modèle ainsi
              qu’un formulaire de retour pour votre ancien casque. Tout crédit
              lié à votre ancien contrat sera évidemment inclus.
          - title: Je ne trouve rien sur Gerrard Street sur le web ?
            text: La marque Gerrard Street a changé de nom et se nomme désormais
              Repeat.audio , la coopérative a encore des stocks des casques
              affichant la marque Gerrard Street, donc pour le moment nous
              continuons de mettre en avant cette marque.
        ctas:
          - outline: false
            text: Consultez notre FAQ
            href: /faq
    color: text-bleu-fonce-100
  - id: entreprise-collectivite-association
    bg_color: bg-bleu-clair-75
    color: text-bleu-fonce-100
    containers:
      - title: Entreprise, collectivité, association ?
        blocks:
          - title: Location avec service
            text: >-
              Commown propose pour les professionnels des formules de location
              avec services **de smartphones** (Fairphone, Crosscall), **PC
              fixes et portables, tablettes et casques audio pour le
              télétravail**.


              L’offre Commown pour les professionnels permet de faire valoir un **haut niveau de Responsabilité Sociétale et Environnementale**. Cela inclut les labels de produits, comme le Fairphone (B-Corp), une SCIC reconnue entreprise de l’ESS, labellisée Efficient Solution Solar Impulse, et recommandée par des experts de l’économie circulaire comme Circul-R, La Famille Zéro Déchet, etc.
          - figure:
              image: /media/sif_label_logo_institutional_2020_rvb.png
              alt: AFNOR Certification AFNOR e-Engagé RSE
              blend_mode: multiply
              grayscale: true
              initial: true
        ctas:
          - outline: false
            text: Nos offres pro
            href: /pour-les-entreprises/
          - outline: true
            text: Contactez-nous
            href: "#contact"
  - id: consomaction
    color: text-bleu-fonce-100
    containers:
      - title: Pas besoin d’électronique ? Soutenez la coop
        blocks:
          - text: >-
              Achetez un Bon Consom’Action Différée, **soutenez notre
              coopérative dès aujourd’hui** et économisez jusqu’à 40 € sur vos
              mensualités le jour où vous aurez BESOIN d’un nouvel appareil
              électronique. 


              En plus, **vous pouvez offrir ce Bon** pour partager vos valeurs.
          - figure:
              image: /media/bon-de-consomaction-non-modifiable-.svg
              alt: Découvrez le bon Consom’Action Différée !
              blend_mode: multiply
              initial: true
        ctas:
          - outline: false
            text: En savoir plus
            href: https://shop.commown.coop/shop/product/bon-de-consomaction-differee-186
---
