---
slug: location-pc-durable
title: Vers des PC durables, éco-responsables et réparables · Commown
description: Consommons autrement en choisissant un PC durable et
  éco-responsable. Découvrez la gamme d’ordinateurs en location avec service de
  réparation inclus proposée par Commown.
date: 2021-03-19T12:25:36+01:00
translationKey: computers
type: product
menu:
  main:
    parent: Particuliers
    name: PC durables
    weight: 2
  footer:
    name: PC durables
    weight: 3
header:
  title: PC durables
  subtitle: SOUTENEZ L'ÉMERGENCE D’ASSEMBLEURS PLUS RESPONSABLES
  ctas:
    - outline: false
      text: Louez un ordinateur responsable
      href: https://shop.commown.coop/shop/category/ordinateurs-2
  text: À partir de 25,30 € / mois
  figure:
    image: /media/why.png
    alt: PC durables
sections:
  - color: text-bleu-fonce-100
    containers:
      - blocks:
          - title: Basse consommation
            text: Les ordinateurs fixes de PCVert, assemblés à Toulouse, sont conçus et
              optimisés pour une très faible consommation électrique. Les
              ordinateurs portables sont réglés par défaut sur le mode “économie
              d’énergie”, lorsque c’est possible.
          - title: Conçu pour durer
            text: Ces ordinateurs (fixe ou portable) ont été conçus dans le but de durer le
              plus longtemps possible en améliorant la qualité des pièces et en
              permettant une réparation facile. De plus, la mutualisation des
              appareils permet de constituer un stock de pièces détachées bien
              au-delà de la disponibilité des pièces sur le marché.
          - title: Éthique
            text: En louant vos PC par la coopérative Commown vous soutenez directement le
              mouvement pour une électronique plus responsable. Nous avons aussi
              pour but de négocier, grâce aux volumes, avec les fournisseurs
              pour améliorer les pratiques sur toute la chaîne de valeur.
      - ctas:
          - outline: false
            text: Louer un ordinateur portable
            href: https://shop.commown.coop/shop/category/ordinateurs-portables-11
          - outline: false
            text: Louer un ordinateur fixe
            href: https://shop.commown.coop/shop/category/ordinateurs-fanless-4
          - outline: true
            text: Mon PC fonctionne toujours !
            href: "#consomaction"
    id: features
  - id: service
    bg_color: bg-bleu-clair-75
    color: text-bleu-fonce-100
    containers:
      - title: Le service Commown c’est…
        title_max_w: 20ch
        icon: desktop
        blocks:
          - title: l’Offre Commown
            text: "**À partir de 25,30 € / mois vous bénéficiez de l’usage d’un ordinateur
              puissant et de multiples services associés conçus pour faire durer
              l’appareil** (prise en charge des casses et pannes, assistance à
              l’usage…)."
          - figure:
              image: /media/dither_it_dsc01544.png
              alt: ""
      - blocks:
          - title: Garantie matérielle étendue
            title_max_w: 18ch
            icon: refresh
            text: Les pièces de votre PC sont garanties par Commown, même après la fin de la
              garantie constructeur.
          - title: Protection de vos données
            title_max_w: 14ch
            icon: lock-closed
            text: En proposant Linux par défaut et un accompagnement personnalisé, nous
              protégeons vos données.
          - title: PC Portable ou Fixe Sur Mesure
            title_max_w: 14ch
            icon: desktop-computer
            text: En fonction de votre utilisation, vous pourrez configurer votre PC sur
              mesure.
          - title: Assistance en ligne
            title_max_w: 10ch
            text: Vous avez une question concernant votre PC ou Linux ? Contactez-nous par
              mail ou via le chat de votre espace client. Réponse en quelques
              heures ouvrées.
            icon: chat-alt-2
          - title: Accompagnement personnalisé
            title_max_w: 12ch
            icon: terminal
            text: Commown vous accompagne via une formation sur-mesure à la prise en main de
              Linux.
          - title: Emplois locaux
            title_max_w: 12ch
            icon: location-marker
            text: Vous soutiendrez aussi la création d’emplois locaux pour l’assemblage et
              la maintenance des PC.
        ctas:
          - outline: false
            text: Louer un ordinateur
            href: https://shop.commown.coop/shop/category/ordinateurs-2
  - id: faq
    containers:
      - title: Questions fréquentes
        title_max_w: 14ch
        icon: chat-alt
        blocks:
          - title: Quand le produit sera livré ?
            details: true
            text: Si vous souscrivez une précommande aujourd’hui, l’assembleur devrait
              pouvoir vous expédier l’ordinateur en 5 jours ouvrés à condition
              que l’ensemble des formalités soient finalisées.
          - title: Que se passe-t-il après la période d’engagement ?
            details: true
            text: Le contrat continue après la période d’engagement avec une baisse de
              tarif, et vous pouvez arrêter quand bon vous semble. Cet avantage
              tarifaire pour les clients fidèles est décrit dans la fiche
              produit de l’offre. On vous précise que chez Commown, nos
              appareils sont loués exclusivement et aucun rachat ne sera
              possible ! Une fois votre abonnement achevé, votre appareil nous
              sera restitué pour pouvoir le louer à d’autres.
            icon: ""
          - title: Proposez-vous des OS (systèmes d’exploitation) alternatifs ?
            details: true
            text: Oui, car nous sommes convaincus de l’importance du logiciel libre dans la
              lutte contre l’obsolescence programmée. Pour les ordinateurs nous
              proposons depuis la création de la coopérative d’installer Linux
              (Ubuntu LTS), et de vous accompagner à sa prise en main. Mais vous
              pouvez également demander Windows si Linux n’est pas envisageable
              pour vous.
            ctas: []
        ctas:
          - outline: false
            text: Consultez notre FAQ
            href: /faq
    color: text-bleu-fonce-100
  - id: entreprise-collectivite-association
    bg_color: bg-bleu-clair-75
    color: text-bleu-fonce-100
    containers:
      - title: Entreprise, collectivité, association ?
        blocks:
          - title: Location avec service
            text: >-
              Commown propose pour les professionnels des formules **de location
              avec services de smartphones** (Fairphone, Crosscall), **PC fixes
              et portables, tablettes et casques audio pour le télétravail**.


              L’offre Commown pour les professionnels permet de faire valoir un **haut niveau de Responsabilité Sociétale et Environnementale**. Cela inclut les labels de produits, comme le Fairphone (B-Corp), une SCIC reconnue entreprise de l’ESS, labellisée Efficient Solution Solar Impulse, et recommandée par des experts de l’économie circulaire comme Circul-R, La Famille Zéro Déchet, etc.
          - figure:
              image: /media/sif_label_logo_institutional_2020_rvb.png
              alt: AFNOR Certification AFNOR e-Engagé RSE
              blend_mode: multiply
              grayscale: true
              initial: true
        ctas:
          - outline: false
            text: Nos offres pro
            href: /pour-les-entreprises/
          - outline: true
            text: Contactez-nous
            href: "#contact"
  - id: consomaction
    color: text-bleu-fonce-100
    containers:
      - title: Pas besoin d’électronique ? Soutenez la coop
        blocks:
          - text: >-
              Achetez un Bon Consom’Action Différée, **soutenez notre
              coopérative dès aujourd’hui** et économisez jusqu’à 40 € sur vos
              mensualités le jour où vous aurez BESOIN d’un nouvel appareil
              électronique. 


              En plus, **vous pouvez offrir ce Bon** pour partager vos valeurs.
          - figure:
              image: /media/bon-de-consomaction-non-modifiable-.svg
              alt: Découvrez le bon Consom’Action Différée !
              blend_mode: multiply
              initial: true
        ctas:
          - outline: false
            text: En savoir plus
            href: https://shop.commown.coop/shop/product/bon-de-consomaction-differee-186
---
