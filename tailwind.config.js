module.exports = {
  purge: {
  	content: ['layouts/**/*.html', 'content/**/*.md']
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    screens: {
      'xs': '30rem', // 480px
      'sm': '40rem', // 640px
      'md': '48rem', // 768px
      'lg': '64rem', // 1024px
      'xl': '80rem', // 1280px
      '2xl': '96rem', // 1536px
    },
    container: {
      center: true
    },
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      white: '#fff',
      noir: {
        '10':    'var(--color-noir-10)',
        '25':    'var(--color-noir-25)',
        '50':    'var(--color-noir-50)',
        '75':    'var(--color-noir-75)',
        '100':   'var(--color-noir-100)',
        DEFAULT: 'var(--color-noir-100)'
      },
      bleu: {
        'fonce-25':  'var(--color-bleu-fonce-25)',
        'fonce-50':  'var(--color-bleu-fonce-50)',
        'fonce-75':  'var(--color-bleu-fonce-75)',
        'fonce-100': 'var(--color-bleu-fonce-100)',
        'fonce':     'var(--color-bleu-fonce-100)',
        DEFAULT:     'var(--color-bleu-fonce-100)',
        'clair-25':  'var(--color-bleu-clair-25)',
        'clair-50':  'var(--color-bleu-clair-50)',
        'clair-75':  'var(--color-bleu-clair-75)',
        'clair-100': 'var(--color-bleu-clair-100)',
        'clair':     'var(--color-bleu-clair-75)',
      },
      beige: {
        DEFAULT: 'var(--color-beige-25)',
        '25':    'var(--color-beige-25)',
        '50':    'var(--color-beige-50)',
        '75':    'var(--color-beige-75)',
        '100':   'var(--color-beige-100)'
      },
      violet: {
        '10':    'var(--color-violet-10)',
        '25':    'var(--color-violet-25)',
        '50':    'var(--color-violet-50)',
        '75':    'var(--color-violet-75)',
        '100':   'var(--color-violet-100)',
        DEFAULT: 'var(--color-violet-100)'
      }
    },
    borderWidth: {
      DEFAULT: '2px',
    },
    extend: {
      typography: {
        DEFAULT: {
          css: {
            color: 'var(--color-noir-100)',
            a: {
              color: 'var(--color-bleu-fonce-100)',
              '&:hover': {
                textDecoration: 'none'
              },
            },
            strong: {
              color: 'var(--color-noir-100)',
            },
            h2: {
              color: 'var(--color-noir-100)',
              fontWeight: '400'
            },
            h3: {
              color: 'var(--color-noir-75)',
              fontWeight: '800',
              textTransform: 'uppercase'
            }
          },
        },
      },
      width: {
        'initial': 'initial'
      },
      height: {
        'initial': 'initial',
        '320': '20rem',
        '360': '22.5rem',
        '400': '25rem',
        '440': '27.5rem',
        '480': '30rem',
        '520': '32.5rem',
        '560': '35rem',
        '600': '37.5rem',
        '640': '40rem',
        '680': '42.5rem',
        '720': '45rem'
      },
      minHeight: {
        '14': '3.5rem',
        '320': '20rem',
        '480': '30rem',
        '640': '40rem'
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [
    require('@tailwindcss/typography')
  ],
}
