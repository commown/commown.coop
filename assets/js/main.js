// Mobile nav
var toggle = document.querySelector('#toggle'),
    menu = document.querySelector('.menu');

toggle.addEventListener('click', function(){
  if (menu.classList.contains('active')) {
    this.setAttribute('aria-expanded', 'false');
    this.setAttribute('title', this.getAttribute('data-open'));
    this.classList.add('btn-outline');
    document.body.classList.remove('darkner');
    menu.classList.remove('active');
  } else {
    menu.classList.add('active');
    this.classList.remove('btn-outline');
    document.body.classList.add('darkner');
    this.setAttribute('title', this.getAttribute('data-close'));
    this.setAttribute('aria-expanded', 'true');
  }
});

// Contact form
var contactForm = document.getElementById("contact-form");

document.addEventListener('DOMContentLoaded', (event) => {
  // Customize validity message
  // (see https://developer.mozilla.org/en-US/docs/Web/HTML/Element/Input)
  for (const field of contactForm.elements) {
    field.addEventListener('input', function() {
      field.setCustomValidity('');
      field.checkValidity();
    });
    field.addEventListener('invalid', function() {
      if (field.hasAttribute('aria-describedby')) {
        field.setCustomValidity(document.getElementById(
          field.getAttribute('aria-describedby')).textContent);
      }
    });
  }
  // Setup default required subject and body for newsletter
  document.getElementById('department').addEventListener('input', function(ev) {
    const subjectField = document.getElementById('subject');
    if (subjectField.value === "") {
      const lang = document.getElementById('lang').value;
      subjectField.value = "Newsletter (" + lang + ")";
    }
    const bodyField = document.getElementById('body');
    if (bodyField.value === "") {
      bodyField.value = ".";
    }
  });
});

function resetFormState() {
  contactForm.removeAttribute('disable');
  for (var field of contactForm.querySelectorAll(".field.error")) {
    field.classList.remove('error');
    field.setAttribute('aria-invalid', 'false')
  }
}

function contactFormSubmit(event) {
  event.preventDefault();
  event.target.setAttribute('disable', true);

  fetch(contactForm.action, {method: contactForm.method,
                             body: new FormData(contactForm)})
    .then(function(response) {
      resetFormState();
      if (response.status >= 200 && response.status <= 299) {
        return response.json();
      } else {
        throw Error(response.statusText);
      }
    })
    .then(function(data) {
      if (data.code === "ok") {
        contactForm.classList.add('success');
        contactForm.classList.remove('error');
      } else {
        contactForm.classList.add('error');
        contactForm.classList.remove('success');
        var field = document.getElementById(data.code);
        field.classList.add('error');
        field.setAttribute('aria-invalid', 'true');
        if (data.code === "captcha") {
          hcaptcha.reset()
        }
      }
    })
    .catch(function(error) {
      console.log(error);
      resetFormState();
      contactForm.classList.add('error');
    });
}

// Form explicit rendering
var displayBtn = document.querySelector('button#display-form');

displayBtn.addEventListener('click', function() {
  contactForm.classList.remove('hidden');
  this.classList.add('hidden');
  document.querySelector('#captcha').innerHTML = '';
  hcaptcha.render('captcha');
  contactForm.addEventListener('submit', contactFormSubmit);
}, {'once': true});


// Video
if (document.querySelectorAll(".video")) {
  var videos = document.querySelectorAll(".video-container");
  for (var video of videos) {
    var playBtn = video.querySelector("button.play");
    playBtn.addEventListener('click', function() {
      var iframe = video.querySelector("iframe");
      iframe.src = iframe.dataset.src;
      iframe.removeAttribute("data-src");
      playBtn.classList.add("hidden");
    })
  }
}
