# Commown website (commown.coop)

## Tools

- [Node.js](https://nodejs.org)
- [Hugo](https://gohugo.io)
- [Netlify CMS](https://www.netlifycms.org)
- [TailwindCSS](https://tailwindcss.com/)

## Assets

### Icons

The open-source icon pack used is [heroicons](https://heroicons.com/) (outline).
Each icon is an inline SVG stored as an HTML partial in [layouts/partials/svg/icons](https://gitlab.com/commown/commown.coop/-/tree/master/layouts/partials/svg) folder.

To add a missing icons:

1. Go to [heroicons.com](https://heroicons.com/)
2. Search for the outline icon you want
3. Click on Copy SVG (displayed on hover)
4. Create a new HTML file in [layouts/partials/svg/icons](https://gitlab.com/commown/commown.coop/-/tree/master/layouts/partials/svg) named with your heroicon’s name (eg: `arrow-circle-right.html`)
5. Paste SVG inside the file
6. Replace the first line `<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">` by `<svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 24 24" fill="none" stroke="currentColor" aria-hidden="true">`
7. Save file

### Images

To create dithered images with Commown custom palette:

1. Go to [ditherit.com](https://ditherit.com/)
2. Clicking on **Select images** button to upload your image
3. In Palette, click **Import** and enter the Commown custom palette `[{"hex":"#3c3c3b"},{"hex":"#cd412e"},{"hex":"#fbbe5e"},{"hex":"#9e9e9d"},{"hex":"#286484"},{"hex":"#c8d5dd"},{"hex":"#4b5084"},{"hex":"#fef6ee"}]`
4. Select the right **image size**:
  * `1280` for full column width (eg: homepage team image)
  * `720` for blog post cover image
  * `620` for 1/2 width (eg: Product page)
5. Click on **Dither** button
6. Click on **Save** button to download your image

## Development

### Setup

```sh
npm install
```

### Server

```sh
hugo server --baseURL http://localhost:1313 --port 1313
```

### Backend

1. Run `npx netlify-cms-proxy-server` from the root directory.
2. Start local development server `hugo server --baseURL http://localhost:1313 --port 1313`.
3. Open http://localhost:1313/admin to administer your content locally.

More info: https://www.netlifycms.org/docs/beta-features/#working-with-a-local-git-repository

### Build

Manual build from Hugo’s CLI:

1. Run `npm run prod-compile` from the root directory to create a `public` folder with all generated static files.
2. Deploy this folder to Commown's dedicated server: `npm run prod-deploy`

Warning: the `baseURL` param from `config.yaml` file is set to `https://commown.coop/`, so if you open the generated `public/index.html` in your browser, all the assets files (CSS, JS and media) will not load properly.

## Contributors (design and/or code, much more people for content)

- Florent Cayré <florent@commown.coop>
- Timothée Goguely <timothee@goguely.com>
- Derek S. <contact@pikselkraft.com>
